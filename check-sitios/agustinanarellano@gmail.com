<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE resultset[
<!ELEMENT resultset (summary,results)>
<!ELEMENT summary (status,sessionID,NumOfErrors,NumOfLikelyProblems,NumOfPotentialProblems,guidelines)>
<!ELEMENT status (#PCDATA)>
<!ELEMENT sessionID (#PCDATA)>
<!ELEMENT NumOfErrors (#PCDATA)>
<!ELEMENT NumOfLikelyProblems (#PCDATA)>
<!ELEMENT NumOfPotentialProblems (#PCDATA)>
<!ELEMENT guidelines (guideline)*>
<!ELEMENT guideline (#PCDATA)>
<!ELEMENT results (result)*>
<!ELEMENT result (resultType,lineNum,columnNum,errorMsg,errorSourceCode,repair*,sequenceID*,decisionPass*,decisionFail*,decisionMade*,decisionMadeDate*)>
<!ELEMENT resultType (#PCDATA)>
<!ELEMENT lineNum (#PCDATA)>
<!ELEMENT columnNum (#PCDATA)>
<!ELEMENT errorMsg (#PCDATA)>
<!ELEMENT errorSourceCode (#PCDATA)>
<!ELEMENT repair (#PCDATA)>
<!ELEMENT sequenceID (#PCDATA)>
<!ELEMENT decisionPass (#PCDATA)>
<!ELEMENT decisionFail (#PCDATA)>
<!ELEMENT decisionMade (#PCDATA)>
<!ELEMENT decisionMadeDate (#PCDATA)>
<!ENTITY lt "&#38;#60;">
<!ENTITY gt "&#62;">
<!ENTITY amp "&#38;#38;">
<!ENTITY apos "&#39;">
<!ENTITY quot "&#34;">
<!ENTITY ndash "&#8211;">
]>
<resultset>
  <summary>
    <status>FAIL</status>
    <sessionID>ffb490c89283fec6a2eacd3f7cb9908ff0d372ad</sessionID>
    <NumOfErrors>8</NumOfErrors>
    <NumOfLikelyProblems>8</NumOfLikelyProblems>
    <NumOfPotentialProblems>32</NumOfPotentialProblems>

    <guidelines>
      <guideline>Stanca Act</guideline>
      <guideline>WCAG 2.0 (Level AA)</guideline>

    </guidelines>
  </summary>

  <results>
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;requireLazy([&quot;InitialJSLoader&quot;], function(InitialJSLoader) {InitialJSLoader.loadOnDOMContent ...</errorSourceCode>
        
        <sequenceID>1_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;requireLazy([&quot;InitialJSLoader&quot;], function(InitialJSLoader) {InitialJSLoader.loadOnDOMContent ...</errorSourceCode>
        
        <sequenceID>1_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;requireLazy([&quot;InitialJSLoader&quot;], function(InitialJSLoader) {InitialJSLoader.loadOnDOMContent ...</errorSourceCode>
        
        <sequenceID>1_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;requireLazy([&quot;InitialJSLoader&quot;], function(InitialJSLoader) {InitialJSLoader.loadOnDOMContent ...</errorSourceCode>
        
        <sequenceID>1_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;requireLazy([&quot;InitialJSLoader&quot;], function(InitialJSLoader) {InitialJSLoader.loadOnDOMContent ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;requireLazy([&quot;InitialJSLoader&quot;], function(InitialJSLoader) {InitialJSLoader.loadOnDOMContent ...</errorSourceCode>
        
        <sequenceID>1_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;require(&quot;TimeSliceImpl&quot;).guard(function() {require(&quot;ServerJSDefine&quot;).handleDefines([[&quot;AsyncR ...</errorSourceCode>
        
        <sequenceID>2_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;require(&quot;TimeSliceImpl&quot;).guard(function() {require(&quot;ServerJSDefine&quot;).handleDefines([[&quot;AsyncR ...</errorSourceCode>
        
        <sequenceID>2_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;require(&quot;TimeSliceImpl&quot;).guard(function() {require(&quot;ServerJSDefine&quot;).handleDefines([[&quot;AsyncR ...</errorSourceCode>
        
        <sequenceID>2_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;require(&quot;TimeSliceImpl&quot;).guard(function() {require(&quot;ServerJSDefine&quot;).handleDefines([[&quot;AsyncR ...</errorSourceCode>
        
        <sequenceID>2_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;require(&quot;TimeSliceImpl&quot;).guard(function() {require(&quot;ServerJSDefine&quot;).handleDefines([[&quot;AsyncR ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;require(&quot;TimeSliceImpl&quot;).guard(function() {require(&quot;ServerJSDefine&quot;).handleDefines([[&quot;AsyncR ...</errorSourceCode>
        
        <sequenceID>2_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;var bigPipe = new (require(&quot;BigPipe&quot;))({&quot;forceFinish&quot;:true,&quot;config&quot;:{&quot;flush_pagelets_asap&quot;:t ...</errorSourceCode>
        
        <sequenceID>3_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;var bigPipe = new (require(&quot;BigPipe&quot;))({&quot;forceFinish&quot;:true,&quot;config&quot;:{&quot;flush_pagelets_asap&quot;:t ...</errorSourceCode>
        
        <sequenceID>3_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;var bigPipe = new (require(&quot;BigPipe&quot;))({&quot;forceFinish&quot;:true,&quot;config&quot;:{&quot;flush_pagelets_asap&quot;:t ...</errorSourceCode>
        
        <sequenceID>3_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>3</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;var bigPipe = new (require(&quot;BigPipe&quot;))({&quot;forceFinish&quot;:true,&quot;config&quot;:{&quot;flush_pagelets_asap&quot;:t ...</errorSourceCode>
        
        <sequenceID>3_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>3</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;var bigPipe = new (require(&quot;BigPipe&quot;))({&quot;forceFinish&quot;:true,&quot;config&quot;:{&quot;flush_pagelets_asap&quot;:t ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;var bigPipe = new (require(&quot;BigPipe&quot;))({&quot;forceFinish&quot;:true,&quot;config&quot;:{&quot;flush_pagelets_asap&quot;:t ...</errorSourceCode>
        
        <sequenceID>3_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>4</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;bigPipe.beforePageletArrive(&quot;first_response&quot;)&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>4_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>4</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;bigPipe.beforePageletArrive(&quot;first_response&quot;)&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>4_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>4</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;bigPipe.beforePageletArrive(&quot;first_response&quot;)&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>4_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>4</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;bigPipe.beforePageletArrive(&quot;first_response&quot;)&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>4_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>4</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;bigPipe.beforePageletArrive(&quot;first_response&quot;)&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>4</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;bigPipe.beforePageletArrive(&quot;first_response&quot;)&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>4_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>5</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;require(&quot;TimeSlice&quot;).guard((function(){bigPipe.onPageletArrive({allResources:[&quot;JIEfU&quot;,&quot;//7Kp ...</errorSourceCode>
        
        <sequenceID>5_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>5</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;require(&quot;TimeSlice&quot;).guard((function(){bigPipe.onPageletArrive({allResources:[&quot;JIEfU&quot;,&quot;//7Kp ...</errorSourceCode>
        
        <sequenceID>5_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>5</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;require(&quot;TimeSlice&quot;).guard((function(){bigPipe.onPageletArrive({allResources:[&quot;JIEfU&quot;,&quot;//7Kp ...</errorSourceCode>
        
        <sequenceID>5_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>5</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;require(&quot;TimeSlice&quot;).guard((function(){bigPipe.onPageletArrive({allResources:[&quot;JIEfU&quot;,&quot;//7Kp ...</errorSourceCode>
        
        <sequenceID>5_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>5</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;require(&quot;TimeSlice&quot;).guard((function(){bigPipe.onPageletArrive({allResources:[&quot;JIEfU&quot;,&quot;//7Kp ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>5</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;require(&quot;TimeSlice&quot;).guard((function(){bigPipe.onPageletArrive({allResources:[&quot;JIEfU&quot;,&quot;//7Kp ...</errorSourceCode>
        
        <sequenceID>5_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>6</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;bigPipe.setPageID(&quot;6754419553568089605-0&quot;);CavalryLogger.setPageID(&quot;6754419553568089605-0&quot;); ...</errorSourceCode>
        
        <sequenceID>6_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>6</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;bigPipe.setPageID(&quot;6754419553568089605-0&quot;);CavalryLogger.setPageID(&quot;6754419553568089605-0&quot;); ...</errorSourceCode>
        
        <sequenceID>6_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>6</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;bigPipe.setPageID(&quot;6754419553568089605-0&quot;);CavalryLogger.setPageID(&quot;6754419553568089605-0&quot;); ...</errorSourceCode>
        
        <sequenceID>6_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>6</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;bigPipe.setPageID(&quot;6754419553568089605-0&quot;);CavalryLogger.setPageID(&quot;6754419553568089605-0&quot;); ...</errorSourceCode>
        
        <sequenceID>6_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>6</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;bigPipe.setPageID(&quot;6754419553568089605-0&quot;);CavalryLogger.setPageID(&quot;6754419553568089605-0&quot;); ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>6</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;bigPipe.setPageID(&quot;6754419553568089605-0&quot;);CavalryLogger.setPageID(&quot;6754419553568089605-0&quot;); ...</errorSourceCode>
        
        <sequenceID>6_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>6</lineNum>
      <columnNum>32</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;bigPipe.beforePageletArrive(&quot;last_response&quot;)&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>6_32_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>6</lineNum>
      <columnNum>32</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;bigPipe.beforePageletArrive(&quot;last_response&quot;)&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>6_32_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>6</lineNum>
      <columnNum>32</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;bigPipe.beforePageletArrive(&quot;last_response&quot;)&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>6_32_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>6</lineNum>
      <columnNum>32</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;bigPipe.beforePageletArrive(&quot;last_response&quot;)&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>6_32_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>6</lineNum>
      <columnNum>32</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;bigPipe.beforePageletArrive(&quot;last_response&quot;)&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>6</lineNum>
      <columnNum>32</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;bigPipe.beforePageletArrive(&quot;last_response&quot;)&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>6_32_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>7</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;require(&quot;TimeSlice&quot;).guard((function(){bigPipe.onPageletArrive({resource_map:{FEt5G:{type:&quot;j ...</errorSourceCode>
        
        <sequenceID>7_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>7</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;require(&quot;TimeSlice&quot;).guard((function(){bigPipe.onPageletArrive({resource_map:{FEt5G:{type:&quot;j ...</errorSourceCode>
        
        <sequenceID>7_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>7</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;require(&quot;TimeSlice&quot;).guard((function(){bigPipe.onPageletArrive({resource_map:{FEt5G:{type:&quot;j ...</errorSourceCode>
        
        <sequenceID>7_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>7</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;require(&quot;TimeSlice&quot;).guard((function(){bigPipe.onPageletArrive({resource_map:{FEt5G:{type:&quot;j ...</errorSourceCode>
        
        <sequenceID>7_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>7</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;require(&quot;TimeSlice&quot;).guard((function(){bigPipe.onPageletArrive({resource_map:{FEt5G:{type:&quot;j ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>7</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;require(&quot;TimeSlice&quot;).guard((function(){bigPipe.onPageletArrive({resource_map:{FEt5G:{type:&quot;j ...</errorSourceCode>
        
        <sequenceID>7_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 

  </results>
</resultset>
