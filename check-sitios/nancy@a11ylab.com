<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE resultset[
<!ELEMENT resultset (summary,results)>
<!ELEMENT summary (status,sessionID,NumOfErrors,NumOfLikelyProblems,NumOfPotentialProblems,guidelines)>
<!ELEMENT status (#PCDATA)>
<!ELEMENT sessionID (#PCDATA)>
<!ELEMENT NumOfErrors (#PCDATA)>
<!ELEMENT NumOfLikelyProblems (#PCDATA)>
<!ELEMENT NumOfPotentialProblems (#PCDATA)>
<!ELEMENT guidelines (guideline)*>
<!ELEMENT guideline (#PCDATA)>
<!ELEMENT results (result)*>
<!ELEMENT result (resultType,lineNum,columnNum,errorMsg,errorSourceCode,repair*,sequenceID*,decisionPass*,decisionFail*,decisionMade*,decisionMadeDate*)>
<!ELEMENT resultType (#PCDATA)>
<!ELEMENT lineNum (#PCDATA)>
<!ELEMENT columnNum (#PCDATA)>
<!ELEMENT errorMsg (#PCDATA)>
<!ELEMENT errorSourceCode (#PCDATA)>
<!ELEMENT repair (#PCDATA)>
<!ELEMENT sequenceID (#PCDATA)>
<!ELEMENT decisionPass (#PCDATA)>
<!ELEMENT decisionFail (#PCDATA)>
<!ELEMENT decisionMade (#PCDATA)>
<!ELEMENT decisionMadeDate (#PCDATA)>
<!ENTITY lt "&#38;#60;">
<!ENTITY gt "&#62;">
<!ENTITY amp "&#38;#38;">
<!ENTITY apos "&#39;">
<!ENTITY quot "&#34;">
<!ENTITY ndash "&#8211;">
]>
<resultset>
  <summary>
    <status>FAIL</status>
    <sessionID>ec823c0de9dd5a4b42372296688fa60bbdf1fd2a</sessionID>
    <NumOfErrors>34</NumOfErrors>
    <NumOfLikelyProblems>9</NumOfLikelyProblems>
    <NumOfPotentialProblems>108</NumOfPotentialProblems>

    <guidelines>
      <guideline>Stanca Act</guideline>
      <guideline>WCAG 2.0 (Level AA)</guideline>

    </guidelines>
  </summary>

  <results>
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=270&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=270'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Unicode right-to-left marks or left-to-right marks may be required.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;path-frontpage has-glyphicons&quot;&gt; &lt;a href=&quot;#main-content&quot; class=&quot;visually-hidden focusabl ...</errorSourceCode>
        
        <sequenceID>37_37_270</sequenceID>
        <decisionPass>Unicode right-to-left marks or left-to-right marks are used whenever the HTML bidirectional algorithm produces undesirable results.</decisionPass>
        <decisionFail>Unicode right-to-left marks or left-to-right marks are not used whenever the HTML bidirectional algorithm produces undesirable results.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=250&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=250'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Text may refer to items by shape, size, or relative position alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;path-frontpage has-glyphicons&quot;&gt; &lt;a href=&quot;#main-content&quot; class=&quot;visually-hidden focusabl ...</errorSourceCode>
        
        <sequenceID>37_37_250</sequenceID>
        <decisionPass>There are no references to items in the document by shape, size, or relative position alone.</decisionPass>
        <decisionFail>There are references to items in the document by shape, size, or relative position alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=248&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=248'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Visual lists may not be properly marked.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;path-frontpage has-glyphicons&quot;&gt; &lt;a href=&quot;#main-content&quot; class=&quot;visually-hidden focusabl ...</errorSourceCode>
        
        <sequenceID>37_37_248</sequenceID>
        <decisionPass>All visual lists contain proper markup.</decisionPass>
        <decisionFail>Not all visual lists contain proper markup.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=262&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=262'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Groups of links with a related purpose are not marked.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;path-frontpage has-glyphicons&quot;&gt; &lt;a href=&quot;#main-content&quot; class=&quot;visually-hidden focusabl ...</errorSourceCode>
        
        <sequenceID>37_37_262</sequenceID>
        <decisionPass>All groups of links with a related purpose are marked.</decisionPass>
        <decisionFail>All groups of links with a related purpose are not marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=184&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=184'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Site missing site map.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;path-frontpage has-glyphicons&quot;&gt; &lt;a href=&quot;#main-content&quot; class=&quot;visually-hidden focusabl ...</errorSourceCode>
        
        <sequenceID>37_37_184</sequenceID>
        <decisionPass>Page is not part of a collection that requires a site map.</decisionPass>
        <decisionFail>Page is part of a collection that requires a site map.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=241&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=241'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Tabular information may be missing table markup.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;path-frontpage has-glyphicons&quot;&gt; &lt;a href=&quot;#main-content&quot; class=&quot;visually-hidden focusabl ...</errorSourceCode>
        
        <sequenceID>37_37_241</sequenceID>
        <decisionPass>Table markup is used for all tabular information.</decisionPass>
        <decisionFail>Table markup is not used for all tabular information.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=271&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=271'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;dir&lt;/code&gt; attribute may be required to identify changes in text direction.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;path-frontpage has-glyphicons&quot;&gt; &lt;a href=&quot;#main-content&quot; class=&quot;visually-hidden focusabl ...</errorSourceCode>
        
        <sequenceID>37_37_271</sequenceID>
        <decisionPass>All changes in text direction are marked using the &lt;code&gt;dir&lt;/code&gt; attribute.</decisionPass>
        <decisionFail>All changes in text direction are not marked using the &lt;code&gt;dir&lt;/code&gt; attribute.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=131&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=131'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Long quotations may not be marked using the &lt;code&gt;blockquote&lt;/code&gt; element.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;path-frontpage has-glyphicons&quot;&gt; &lt;a href=&quot;#main-content&quot; class=&quot;visually-hidden focusabl ...</errorSourceCode>
        
        <sequenceID>37_37_131</sequenceID>
        <decisionPass>All long quotations have been marked with the &lt;code&gt;blockquote&lt;/code&gt; element.</decisionPass>
        <decisionFail>All long quotations are not marked with the &lt;code&gt;blockquote&lt;/code&gt; element.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=276&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=276'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Repeated components may not appear in the same relative order each time they appear.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;path-frontpage has-glyphicons&quot;&gt; &lt;a href=&quot;#main-content&quot; class=&quot;visually-hidden focusabl ...</errorSourceCode>
        
        <sequenceID>37_37_276</sequenceID>
        <decisionPass>Repeated components appear in the same relative order on this page.</decisionPass>
        <decisionFail>Repeated components do not appear in the same relative order on this page.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>82</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#main-content&quot; class=&quot;visually-hidden focusable skip-link&quot;&gt; Pasar al contenido principal &lt;/ ...</errorSourceCode>
        
        <sequenceID>37_82_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>82</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#main-content&quot; class=&quot;visually-hidden focusable skip-link&quot;&gt; Pasar al contenido principal &lt;/ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>82</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#main-content&quot; class=&quot;visually-hidden focusable skip-link&quot;&gt; Pasar al contenido principal &lt;/ ...</errorSourceCode>
        
        <sequenceID>37_82_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>637</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;logo navbar-btn pull-left&quot; href=&quot;/&quot; &gt; &lt;img src=&quot;/themes/a11y/logo.png&quot; alt=&quot;P&aacute;gina inicia ...</errorSourceCode>
        
        <sequenceID>37_637_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>637</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;logo navbar-btn pull-left&quot; href=&quot;/&quot; &gt; &lt;img src=&quot;/themes/a11y/logo.png&quot; alt=&quot;P&aacute;gina inicia ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>637</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;logo navbar-btn pull-left&quot; href=&quot;/&quot; &gt; &lt;img src=&quot;/themes/a11y/logo.png&quot; alt=&quot;P&aacute;gina inicia ...</errorSourceCode>
        
        <sequenceID>37_637_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>685</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/themes/a11y/logo.png&quot; alt=&quot;P&aacute;gina inicial Accessiibility Lab&quot; /&gt;</errorSourceCode>
        
        <sequenceID>37_685_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>685</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/themes/a11y/logo.png&quot; alt=&quot;P&aacute;gina inicial Accessiibility Lab&quot; /&gt;</errorSourceCode>
        
        <sequenceID>37_685_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>685</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/themes/a11y/logo.png&quot; alt=&quot;P&aacute;gina inicial Accessiibility Lab&quot; /&gt;</errorSourceCode>
        
        <sequenceID>37_685_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>881</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/en&quot; aria-label=&quot;English version of the site&quot; &gt;English&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_881_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>881</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/en&quot; aria-label=&quot;English version of the site&quot; &gt;English&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>881</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/en&quot; aria-label=&quot;English version of the site&quot; &gt;English&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_881_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>1094</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/sobre-nosotros&quot; data-drupal-link-system-path=&quot;sobre-nosotros&quot;&gt;&iquest;Quienes Somos?&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_1094_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>1094</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/sobre-nosotros&quot; data-drupal-link-system-path=&quot;sobre-nosotros&quot;&gt;&iquest;Quienes Somos?&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>1094</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/sobre-nosotros&quot; data-drupal-link-system-path=&quot;sobre-nosotros&quot;&gt;&iquest;Quienes Somos?&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_1094_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>1196</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/servicios&quot; data-drupal-link-system-path=&quot;servicios&quot;&gt;Servicios&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_1196_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>1196</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/servicios&quot; data-drupal-link-system-path=&quot;servicios&quot;&gt;Servicios&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>1196</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/servicios&quot; data-drupal-link-system-path=&quot;servicios&quot;&gt;Servicios&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_1196_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>1281</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/legislacion&quot; data-drupal-link-system-path=&quot;legislacion&quot;&gt;Legislaci&oacute;n&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_1281_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>1281</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/legislacion&quot; data-drupal-link-system-path=&quot;legislacion&quot;&gt;Legislaci&oacute;n&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>1281</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/legislacion&quot; data-drupal-link-system-path=&quot;legislacion&quot;&gt;Legislaci&oacute;n&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_1281_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>1373</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/estadisticas&quot; data-drupal-link-system-path=&quot;estadisticas&quot;&gt;Estad&iacute;sticas&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_1373_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>1373</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/estadisticas&quot; data-drupal-link-system-path=&quot;estadisticas&quot;&gt;Estad&iacute;sticas&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>1373</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/estadisticas&quot; data-drupal-link-system-path=&quot;estadisticas&quot;&gt;Estad&iacute;sticas&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_1373_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>1468</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/blog&quot; data-drupal-link-system-path=&quot;blog&quot;&gt;Blog&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_1468_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>1468</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/blog&quot; data-drupal-link-system-path=&quot;blog&quot;&gt;Blog&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>1468</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/blog&quot; data-drupal-link-system-path=&quot;blog&quot;&gt;Blog&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_1468_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>1538</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/contact&quot; data-drupal-link-system-path=&quot;contact&quot;&gt;Contacto&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_1538_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>1538</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/contact&quot; data-drupal-link-system-path=&quot;contact&quot;&gt;Contacto&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>1538</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/contact&quot; data-drupal-link-system-path=&quot;contact&quot;&gt;Contacto&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_1538_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>1618</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://a11yrankings.com&quot;&gt;Rankings&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_1618_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>1618</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://a11yrankings.com&quot;&gt;Rankings&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>1618</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://a11yrankings.com&quot;&gt;Rankings&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_1618_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>1889</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=42&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=42'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h1&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h1&gt;Bienvenido a &quot;Accessibility Lab&quot;&lt;/h1&gt;</errorSourceCode>
        
        <sequenceID>37_1889_42</sequenceID>
        <decisionPass>This &lt;code&gt;h1&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h1&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>1982</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a id=&quot;main-content&quot;&gt;&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>2373</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=43&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=43'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h2&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h2 class=&quot;headline-brd&quot;&gt;Beneficios de la accesibilidad&lt;/h2&gt;</errorSourceCode>
        
        <sequenceID>37_2373_43</sequenceID>
        <decisionPass>This &lt;code&gt;h2&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h2&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>2705</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;a2a_button_facebook&quot; aria-label=&quot;compartir en Facebook&quot;&gt;&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>2775</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;a2a_button_twitter&quot; aria-label=&quot;compartir en twitter&quot;&gt;&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>2843</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;a2a_button_google_plus&quot; aria-label=&quot;compartir en google plus&quot;&gt;&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>2919</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;a2a_button_linkedin&quot; aria-label=&quot;compartir en linkedin&quot;&gt;&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>4425</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=43&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=43'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h2&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h2 class=&quot;headline-brd&quot;&gt;Diversidad y accesibilidad en el trabajo&lt;/h2&gt;</errorSourceCode>
        
        <sequenceID>37_4425_43</sequenceID>
        <decisionPass>This &lt;code&gt;h2&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h2&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>4787</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;a2a_button_facebook&quot; aria-label=&quot;compartir en Facebook&quot;&gt;&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>4857</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;a2a_button_twitter&quot; aria-label=&quot;compartir en twitter&quot;&gt;&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>4925</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;a2a_button_google_plus&quot; aria-label=&quot;compartir en google plus&quot;&gt;&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>5001</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;a2a_button_linkedin&quot; aria-label=&quot;compartir en linkedin&quot;&gt;&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>6627</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/&quot; &gt;&lt;img id=&quot;logo-footer&quot; class=&quot;footer-logo&quot; src=&quot;/themes/a11y/logo2-default.png&quot; alt=&quot;Reg ...</errorSourceCode>
        
        <sequenceID>37_6627_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>6627</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/&quot; &gt;&lt;img id=&quot;logo-footer&quot; class=&quot;footer-logo&quot; src=&quot;/themes/a11y/logo2-default.png&quot; alt=&quot;Reg ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>6627</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/&quot; &gt;&lt;img id=&quot;logo-footer&quot; class=&quot;footer-logo&quot; src=&quot;/themes/a11y/logo2-default.png&quot; alt=&quot;Reg ...</errorSourceCode>
        
        <sequenceID>37_6627_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>6640</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode></errorSourceCode>
        
        <sequenceID>37_6640_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>6640</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode></errorSourceCode>
        
        <sequenceID>37_6640_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>6640</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode></errorSourceCode>
        
        <sequenceID>37_6640_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>7160</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=43&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=43'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h2&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h2&gt;Cont&aacute;ctanos&lt;/h2&gt;</errorSourceCode>
        
        <sequenceID>37_7160_43</sequenceID>
        <decisionPass>This &lt;code&gt;h2&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h2&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>7249</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;tel://5549508224&quot; class=&quot;blanco&quot; aria-label=&quot;L&aacute;manos al tel&eacute;fono +52 1 55 4950 8224&quot;&gt;+52  ...</errorSourceCode>
        
        <sequenceID>37_7249_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>7249</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;tel://5549508224&quot; class=&quot;blanco&quot; aria-label=&quot;L&aacute;manos al tel&eacute;fono +52 1 55 4950 8224&quot;&gt;+52  ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>7249</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;tel://5549508224&quot; class=&quot;blanco&quot; aria-label=&quot;L&aacute;manos al tel&eacute;fono +52 1 55 4950 8224&quot;&gt;+52  ...</errorSourceCode>
        
        <sequenceID>37_7249_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>7373</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;mailto:nancy@a11ylab.com&quot; class=&quot;blanco&quot; aria-label=&quot;escr&iacute;benos al correo nancy@a11ylab.co ...</errorSourceCode>
        
        <sequenceID>37_7373_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>7373</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;mailto:nancy@a11ylab.com&quot; class=&quot;blanco&quot; aria-label=&quot;escr&iacute;benos al correo nancy@a11ylab.co ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>7373</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;mailto:nancy@a11ylab.com&quot; class=&quot;blanco&quot; aria-label=&quot;escr&iacute;benos al correo nancy@a11ylab.co ...</errorSourceCode>
        
        <sequenceID>37_7373_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>7633</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/terminos-y-condiciones&quot;&gt;T&eacute;rminos y condiciones &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_7633_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>7633</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/terminos-y-condiciones&quot;&gt;T&eacute;rminos y condiciones &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>7633</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/terminos-y-condiciones&quot;&gt;T&eacute;rminos y condiciones &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_7633_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>7697</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/aviso-de-privacidad&quot;&gt;Aviso de Privacidad &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_7697_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>7697</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/aviso-de-privacidad&quot;&gt;Aviso de Privacidad &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>7697</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/aviso-de-privacidad&quot;&gt;Aviso de Privacidad &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_7697_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>7755</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/sitemap&quot;&gt;Mapa del Sitio&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_7755_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>7755</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/sitemap&quot;&gt;Mapa del Sitio&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>7755</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/sitemap&quot;&gt;Mapa del Sitio&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>37_7755_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>7795</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://www.w3.org/WAI/WCAG2AA-Conformance&quot; title=&quot;Explicaci&oacute;n de la conformidad WCAG 2.0 N ...</errorSourceCode>
        
        <sequenceID>37_7795_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>7795</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://www.w3.org/WAI/WCAG2AA-Conformance&quot; title=&quot;Explicaci&oacute;n de la conformidad WCAG 2.0 N ...</errorSourceCode>
        
        <sequenceID>37_7795_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>7795</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://www.w3.org/WAI/WCAG2AA-Conformance&quot; title=&quot;Explicaci&oacute;n de la conformidad WCAG 2.0 N ...</errorSourceCode>
        
        <sequenceID>37_7795_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>7910</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img height=&quot;32&quot; width=&quot;88&quot; src=&quot;http://www.w3.org/WAI/wcag2AA-blue&quot; alt=&quot;Level Double-A conformance ...</errorSourceCode>
        
        <sequenceID>37_7910_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>7910</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img height=&quot;32&quot; width=&quot;88&quot; src=&quot;http://www.w3.org/WAI/wcag2AA-blue&quot; alt=&quot;Level Double-A conformance ...</errorSourceCode>
        
        <sequenceID>37_7910_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>7910</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img height=&quot;32&quot; width=&quot;88&quot; src=&quot;http://www.w3.org/WAI/wcag2AA-blue&quot; alt=&quot;Level Double-A conformance ...</errorSourceCode>
        
        <sequenceID>37_7910_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>8142</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.facebook.com/a11ylab/&quot; target=&quot;_blank&quot; aria-label=&quot;Facebook de Accessibility La ...</errorSourceCode>
        
        <sequenceID>37_8142_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>8142</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.facebook.com/a11ylab/&quot; target=&quot;_blank&quot; aria-label=&quot;Facebook de Accessibility La ...</errorSourceCode>
        
        <sequenceID>37_8142_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>8142</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.facebook.com/a11ylab/&quot; target=&quot;_blank&quot; aria-label=&quot;Facebook de Accessibility La ...</errorSourceCode>
        
        <sequenceID>37_8142_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>8420</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://plus.google.com/b/109544171554219182189/&quot; target=&quot;_blank&quot; aria-label=&quot;Google Plus d ...</errorSourceCode>
        
        <sequenceID>37_8420_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>8420</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://plus.google.com/b/109544171554219182189/&quot; target=&quot;_blank&quot; aria-label=&quot;Google Plus d ...</errorSourceCode>
        
        <sequenceID>37_8420_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>8420</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://plus.google.com/b/109544171554219182189/&quot; target=&quot;_blank&quot; aria-label=&quot;Google Plus d ...</errorSourceCode>
        
        <sequenceID>37_8420_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>8722</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.instagram.com/a11ylab/&quot; target=&quot;_blank&quot; aria-label=&quot;Instagram de Accessibility  ...</errorSourceCode>
        
        <sequenceID>37_8722_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>8722</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.instagram.com/a11ylab/&quot; target=&quot;_blank&quot; aria-label=&quot;Instagram de Accessibility  ...</errorSourceCode>
        
        <sequenceID>37_8722_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>8722</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.instagram.com/a11ylab/&quot; target=&quot;_blank&quot; aria-label=&quot;Instagram de Accessibility  ...</errorSourceCode>
        
        <sequenceID>37_8722_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9004</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://twitter.com/a11ylab&quot; class=&quot;tooltips&quot; target=&quot;_blank&quot; aria-label=&quot;Twitter de Access ...</errorSourceCode>
        
        <sequenceID>37_9004_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9004</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://twitter.com/a11ylab&quot; class=&quot;tooltips&quot; target=&quot;_blank&quot; aria-label=&quot;Twitter de Access ...</errorSourceCode>
        
        <sequenceID>37_9004_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9004</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://twitter.com/a11ylab&quot; class=&quot;tooltips&quot; target=&quot;_blank&quot; aria-label=&quot;Twitter de Access ...</errorSourceCode>
        
        <sequenceID>37_9004_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9273</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.youtube.com/channel/UC1D5Sad3wbTwJpTZVWZRvhg?view_as=subscriber&quot; target=&quot;_blank ...</errorSourceCode>
        
        <sequenceID>37_9273_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9273</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.youtube.com/channel/UC1D5Sad3wbTwJpTZVWZRvhg?view_as=subscriber&quot; target=&quot;_blank ...</errorSourceCode>
        
        <sequenceID>37_9273_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9273</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.youtube.com/channel/UC1D5Sad3wbTwJpTZVWZRvhg?view_as=subscriber&quot; target=&quot;_blank ...</errorSourceCode>
        
        <sequenceID>37_9273_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>9645</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;application/json&quot; data-drupal-selector=&quot;drupal-settings-json&quot;&gt;{&quot;path&quot;:{&quot;baseUrl&quot;:&quot;\/&quot;, ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9645</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;application/json&quot; data-drupal-selector=&quot;drupal-settings-json&quot;&gt;{&quot;path&quot;:{&quot;baseUrl&quot;:&quot;\/&quot;, ...</errorSourceCode>
        
        <sequenceID>37_9645_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9645</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;application/json&quot; data-drupal-selector=&quot;drupal-settings-json&quot;&gt;{&quot;path&quot;:{&quot;baseUrl&quot;:&quot;\/&quot;, ...</errorSourceCode>
        
        <sequenceID>37_9645_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9645</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;application/json&quot; data-drupal-selector=&quot;drupal-settings-json&quot;&gt;{&quot;path&quot;:{&quot;baseUrl&quot;:&quot;\/&quot;, ...</errorSourceCode>
        
        <sequenceID>37_9645_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9645</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;application/json&quot; data-drupal-selector=&quot;drupal-settings-json&quot;&gt;{&quot;path&quot;:{&quot;baseUrl&quot;:&quot;\/&quot;, ...</errorSourceCode>
        
        <sequenceID>37_9645_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9645</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;application/json&quot; data-drupal-selector=&quot;drupal-settings-json&quot;&gt;{&quot;path&quot;:{&quot;baseUrl&quot;:&quot;\/&quot;, ...</errorSourceCode>
        
        <sequenceID>37_9645_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>9744</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/sites/default/files/js/js_nNk4zljk50fv-1CJpVhRTZIZ2bvwRVRhe0n3QXkBCEk.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9744</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/sites/default/files/js/js_nNk4zljk50fv-1CJpVhRTZIZ2bvwRVRhe0n3QXkBCEk.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_9744_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9744</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/sites/default/files/js/js_nNk4zljk50fv-1CJpVhRTZIZ2bvwRVRhe0n3QXkBCEk.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_9744_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9744</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/sites/default/files/js/js_nNk4zljk50fv-1CJpVhRTZIZ2bvwRVRhe0n3QXkBCEk.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_9744_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9744</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/sites/default/files/js/js_nNk4zljk50fv-1CJpVhRTZIZ2bvwRVRhe0n3QXkBCEk.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_9744_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9744</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/sites/default/files/js/js_nNk4zljk50fv-1CJpVhRTZIZ2bvwRVRhe0n3QXkBCEk.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_9744_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>9855</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;//cdn.jsdelivr.net/bootstrap/3.3.7/js/bootstrap.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9855</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;//cdn.jsdelivr.net/bootstrap/3.3.7/js/bootstrap.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_9855_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9855</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;//cdn.jsdelivr.net/bootstrap/3.3.7/js/bootstrap.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_9855_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9855</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;//cdn.jsdelivr.net/bootstrap/3.3.7/js/bootstrap.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_9855_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9855</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;//cdn.jsdelivr.net/bootstrap/3.3.7/js/bootstrap.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_9855_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9855</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;//cdn.jsdelivr.net/bootstrap/3.3.7/js/bootstrap.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_9855_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>9947</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/sites/default/files/js/js_dk7fwgZkB15bQygcJE2ppnL_BcI12ZQI7lva3MV6wPU.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9947</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/sites/default/files/js/js_dk7fwgZkB15bQygcJE2ppnL_BcI12ZQI7lva3MV6wPU.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_9947_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9947</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/sites/default/files/js/js_dk7fwgZkB15bQygcJE2ppnL_BcI12ZQI7lva3MV6wPU.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_9947_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9947</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/sites/default/files/js/js_dk7fwgZkB15bQygcJE2ppnL_BcI12ZQI7lva3MV6wPU.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_9947_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9947</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/sites/default/files/js/js_dk7fwgZkB15bQygcJE2ppnL_BcI12ZQI7lva3MV6wPU.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_9947_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>9947</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/sites/default/files/js/js_dk7fwgZkB15bQygcJE2ppnL_BcI12ZQI7lva3MV6wPU.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_9947_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>10058</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;https://static.addtoany.com/menu/page.js&quot; async&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>10058</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;https://static.addtoany.com/menu/page.js&quot; async&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_10058_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>10058</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;https://static.addtoany.com/menu/page.js&quot; async&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_10058_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>10058</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;https://static.addtoany.com/menu/page.js&quot; async&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_10058_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>10058</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;https://static.addtoany.com/menu/page.js&quot; async&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_10058_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>10058</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;https://static.addtoany.com/menu/page.js&quot; async&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_10058_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>10142</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/sites/default/files/js/js_KhEMDsJO0ZcAWOpm1dsbY3UZyiBaMdUJe8zBb-Ud4Ss.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>10142</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/sites/default/files/js/js_KhEMDsJO0ZcAWOpm1dsbY3UZyiBaMdUJe8zBb-Ud4Ss.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_10142_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>10142</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/sites/default/files/js/js_KhEMDsJO0ZcAWOpm1dsbY3UZyiBaMdUJe8zBb-Ud4Ss.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_10142_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>10142</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/sites/default/files/js/js_KhEMDsJO0ZcAWOpm1dsbY3UZyiBaMdUJe8zBb-Ud4Ss.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_10142_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>10142</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/sites/default/files/js/js_KhEMDsJO0ZcAWOpm1dsbY3UZyiBaMdUJe8zBb-Ud4Ss.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_10142_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>10142</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/sites/default/files/js/js_KhEMDsJO0ZcAWOpm1dsbY3UZyiBaMdUJe8zBb-Ud4Ss.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>37_10142_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>37</lineNum>
      <columnNum>10253</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
jQuery(document).ready(function($){
$(&quot;#contact-message-formulario-de-contacto-form input&quot;) ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>10253</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
jQuery(document).ready(function($){
$(&quot;#contact-message-formulario-de-contacto-form input&quot;) ...</errorSourceCode>
        
        <sequenceID>37_10253_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>10253</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
jQuery(document).ready(function($){
$(&quot;#contact-message-formulario-de-contacto-form input&quot;) ...</errorSourceCode>
        
        <sequenceID>37_10253_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>10253</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
jQuery(document).ready(function($){
$(&quot;#contact-message-formulario-de-contacto-form input&quot;) ...</errorSourceCode>
        
        <sequenceID>37_10253_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>10253</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
jQuery(document).ready(function($){
$(&quot;#contact-message-formulario-de-contacto-form input&quot;) ...</errorSourceCode>
        
        <sequenceID>37_10253_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>37</lineNum>
      <columnNum>10253</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
jQuery(document).ready(function($){
$(&quot;#contact-message-formulario-de-contacto-form input&quot;) ...</errorSourceCode>
        
        <sequenceID>37_10253_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>96</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;&#039;undefined&#039;=== typeof _trfq || (window._trfq = []);&#039;undefined&#039;=== typeof _trfd &amp;&amp; (window._t ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>96</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;&#039;undefined&#039;=== typeof _trfq || (window._trfq = []);&#039;undefined&#039;=== typeof _trfd &amp;&amp; (window._t ...</errorSourceCode>
        
        <sequenceID>96_17_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>96</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;&#039;undefined&#039;=== typeof _trfq || (window._trfq = []);&#039;undefined&#039;=== typeof _trfd &amp;&amp; (window._t ...</errorSourceCode>
        
        <sequenceID>96_17_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>96</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;&#039;undefined&#039;=== typeof _trfq || (window._trfq = []);&#039;undefined&#039;=== typeof _trfd &amp;&amp; (window._t ...</errorSourceCode>
        
        <sequenceID>96_17_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>96</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;&#039;undefined&#039;=== typeof _trfq || (window._trfq = []);&#039;undefined&#039;=== typeof _trfd &amp;&amp; (window._t ...</errorSourceCode>
        
        <sequenceID>96_17_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>96</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;&#039;undefined&#039;=== typeof _trfq || (window._trfq = []);&#039;undefined&#039;=== typeof _trfd &amp;&amp; (window._t ...</errorSourceCode>
        
        <sequenceID>96_17_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>96</lineNum>
      <columnNum>48</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&#039;https://img1.wsimg.com/tcc/tcc_l.combined.1.0.6.min.js&#039;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>96</lineNum>
      <columnNum>48</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&#039;https://img1.wsimg.com/tcc/tcc_l.combined.1.0.6.min.js&#039;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>96_48_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>96</lineNum>
      <columnNum>48</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&#039;https://img1.wsimg.com/tcc/tcc_l.combined.1.0.6.min.js&#039;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>96_48_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>96</lineNum>
      <columnNum>48</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&#039;https://img1.wsimg.com/tcc/tcc_l.combined.1.0.6.min.js&#039;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>96_48_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>96</lineNum>
      <columnNum>48</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&#039;https://img1.wsimg.com/tcc/tcc_l.combined.1.0.6.min.js&#039;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>96_48_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>96</lineNum>
      <columnNum>48</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&#039;https://img1.wsimg.com/tcc/tcc_l.combined.1.0.6.min.js&#039;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>96_48_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 

  </results>
</resultset>
