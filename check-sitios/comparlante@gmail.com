<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE resultset[
<!ELEMENT resultset (summary,results)>
<!ELEMENT summary (status,sessionID,NumOfErrors,NumOfLikelyProblems,NumOfPotentialProblems,guidelines)>
<!ELEMENT status (#PCDATA)>
<!ELEMENT sessionID (#PCDATA)>
<!ELEMENT NumOfErrors (#PCDATA)>
<!ELEMENT NumOfLikelyProblems (#PCDATA)>
<!ELEMENT NumOfPotentialProblems (#PCDATA)>
<!ELEMENT guidelines (guideline)*>
<!ELEMENT guideline (#PCDATA)>
<!ELEMENT results (result)*>
<!ELEMENT result (resultType,lineNum,columnNum,errorMsg,errorSourceCode,repair*,sequenceID*,decisionPass*,decisionFail*,decisionMade*,decisionMadeDate*)>
<!ELEMENT resultType (#PCDATA)>
<!ELEMENT lineNum (#PCDATA)>
<!ELEMENT columnNum (#PCDATA)>
<!ELEMENT errorMsg (#PCDATA)>
<!ELEMENT errorSourceCode (#PCDATA)>
<!ELEMENT repair (#PCDATA)>
<!ELEMENT sequenceID (#PCDATA)>
<!ELEMENT decisionPass (#PCDATA)>
<!ELEMENT decisionFail (#PCDATA)>
<!ELEMENT decisionMade (#PCDATA)>
<!ELEMENT decisionMadeDate (#PCDATA)>
<!ENTITY lt "&#38;#60;">
<!ENTITY gt "&#62;">
<!ENTITY amp "&#38;#38;">
<!ENTITY apos "&#39;">
<!ENTITY quot "&#34;">
<!ENTITY ndash "&#8211;">
]>
<resultset>
  <summary>
    <status>FAIL</status>
    <sessionID>1bbfd10f26245d48117b8e4c3d367d20fbd9c898</sessionID>
    <NumOfErrors>207</NumOfErrors>
    <NumOfLikelyProblems>30</NumOfLikelyProblems>
    <NumOfPotentialProblems>501</NumOfPotentialProblems>

    <guidelines>
      <guideline>Stanca Act</guideline>
      <guideline>WCAG 2.0 (Level AA)</guideline>

    </guidelines>
  </summary>

  <results>
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>32</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=54&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=54'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;title&lt;/code&gt; might not describe the document.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;title&gt;Pide presupuesto o compra productos de ortopedia online - Discubre&lt;/title&gt;</errorSourceCode>
        
        <sequenceID>32_5_54</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the document.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the document.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1437</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery/2.1.1/jquery.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>1437_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1437</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery/2.1.1/jquery.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>1437_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1437</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery/2.1.1/jquery.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>1437_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1437</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery/2.1.1/jquery.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>1437_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1437</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery/2.1.1/jquery.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>1437_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1443</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=131&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=131'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Long quotations may not be marked using the &lt;code&gt;blockquote&lt;/code&gt; element.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home home  &quot;&gt;



    &lt;script&gt;
        window.isMobileOrTablet = function() {
           ...</errorSourceCode>
        
        <sequenceID>1443_1_131</sequenceID>
        <decisionPass>All long quotations have been marked with the &lt;code&gt;blockquote&lt;/code&gt; element.</decisionPass>
        <decisionFail>All long quotations are not marked with the &lt;code&gt;blockquote&lt;/code&gt; element.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1443</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=28&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=28'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Document may be missing a &quot;skip to content&quot; link.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home home  &quot;&gt;



    &lt;script&gt;
        window.isMobileOrTablet = function() {
           ...</errorSourceCode>
        
        <sequenceID>1443_1_28</sequenceID>
        <decisionPass>Document contians a &quot;skip to content&quot; link or does not require it.</decisionPass>
        <decisionFail>Document does not contain a &quot;skip to content&quot; link and requires it.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1443</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=271&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=271'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;dir&lt;/code&gt; attribute may be required to identify changes in text direction.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home home  &quot;&gt;



    &lt;script&gt;
        window.isMobileOrTablet = function() {
           ...</errorSourceCode>
        
        <sequenceID>1443_1_271</sequenceID>
        <decisionPass>All changes in text direction are marked using the &lt;code&gt;dir&lt;/code&gt; attribute.</decisionPass>
        <decisionFail>All changes in text direction are not marked using the &lt;code&gt;dir&lt;/code&gt; attribute.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1443</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=276&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=276'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Repeated components may not appear in the same relative order each time they appear.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home home  &quot;&gt;



    &lt;script&gt;
        window.isMobileOrTablet = function() {
           ...</errorSourceCode>
        
        <sequenceID>1443_1_276</sequenceID>
        <decisionPass>Repeated components appear in the same relative order on this page.</decisionPass>
        <decisionFail>Repeated components do not appear in the same relative order on this page.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1443</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=275&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=275'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Loading web page may cause new window to open.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home home  &quot;&gt;



    &lt;script&gt;
        window.isMobileOrTablet = function() {
           ...</errorSourceCode>
        
        <sequenceID>1443_1_275</sequenceID>
        <decisionPass>Loading the page does not cause a new window to open.</decisionPass>
        <decisionFail>Loading the page causes a new window to open.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1443</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=270&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=270'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Unicode right-to-left marks or left-to-right marks may be required.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home home  &quot;&gt;



    &lt;script&gt;
        window.isMobileOrTablet = function() {
           ...</errorSourceCode>
        
        <sequenceID>1443_1_270</sequenceID>
        <decisionPass>Unicode right-to-left marks or left-to-right marks are used whenever the HTML bidirectional algorithm produces undesirable results.</decisionPass>
        <decisionFail>Unicode right-to-left marks or left-to-right marks are not used whenever the HTML bidirectional algorithm produces undesirable results.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1443</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=250&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=250'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Text may refer to items by shape, size, or relative position alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home home  &quot;&gt;



    &lt;script&gt;
        window.isMobileOrTablet = function() {
           ...</errorSourceCode>
        
        <sequenceID>1443_1_250</sequenceID>
        <decisionPass>There are no references to items in the document by shape, size, or relative position alone.</decisionPass>
        <decisionFail>There are references to items in the document by shape, size, or relative position alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1443</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=184&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=184'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Site missing site map.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home home  &quot;&gt;



    &lt;script&gt;
        window.isMobileOrTablet = function() {
           ...</errorSourceCode>
        
        <sequenceID>1443_1_184</sequenceID>
        <decisionPass>Page is not part of a collection that requires a site map.</decisionPass>
        <decisionFail>Page is part of a collection that requires a site map.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1443</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=248&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=248'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Visual lists may not be properly marked.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home home  &quot;&gt;



    &lt;script&gt;
        window.isMobileOrTablet = function() {
           ...</errorSourceCode>
        
        <sequenceID>1443_1_248</sequenceID>
        <decisionPass>All visual lists contain proper markup.</decisionPass>
        <decisionFail>Not all visual lists contain proper markup.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1443</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=262&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=262'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Groups of links with a related purpose are not marked.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home home  &quot;&gt;



    &lt;script&gt;
        window.isMobileOrTablet = function() {
           ...</errorSourceCode>
        
        <sequenceID>1443_1_262</sequenceID>
        <decisionPass>All groups of links with a related purpose are marked.</decisionPass>
        <decisionFail>All groups of links with a related purpose are not marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1447</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        window.isMobileOrTablet = function() {
            var check = false;
            i ...</errorSourceCode>
        
        <sequenceID>1447_5_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1447</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        window.isMobileOrTablet = function() {
            var check = false;
            i ...</errorSourceCode>
        
        <sequenceID>1447_5_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1447</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        window.isMobileOrTablet = function() {
            var check = false;
            i ...</errorSourceCode>
        
        <sequenceID>1447_5_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1447</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        window.isMobileOrTablet = function() {
            var check = false;
            i ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1447</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        window.isMobileOrTablet = function() {
            var check = false;
            i ...</errorSourceCode>
        
        <sequenceID>1447_5_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1447</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        window.isMobileOrTablet = function() {
            var check = false;
            i ...</errorSourceCode>
        
        <sequenceID>1447_5_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1469</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=189&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=189'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;label&lt;/code&gt; may not describe its associated control.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;hidden&quot; id=&quot;dispatcher_path&quot; value=&quot;/es/accesibilidad/&quot;&gt;</errorSourceCode>
        
        <sequenceID>1469_5_189</sequenceID>
        <decisionPass>&lt;code&gt;label&lt;/code&gt; describes its associated control.</decisionPass>
        <decisionFail>&lt;code&gt;label&lt;/code&gt; does not describe its associated control.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1485</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;navbar-brand&quot; href=&quot;/es/&quot;&gt;
                &lt;img src=&quot;https://cs-static-pro.s3.amazonaws.co ...</errorSourceCode>
        
        <sequenceID>1485_13_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1485</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;navbar-brand&quot; href=&quot;/es/&quot;&gt;
                &lt;img src=&quot;https://cs-static-pro.s3.amazonaws.co ...</errorSourceCode>
        
        <sequenceID>1485_13_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1485</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;navbar-brand&quot; href=&quot;/es/&quot;&gt;
                &lt;img src=&quot;https://cs-static-pro.s3.amazonaws.co ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1486</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F93%2F43%2F9343d6 ...</errorSourceCode>
        
        <sequenceID>1486_17_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1486</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F93%2F43%2F9343d6 ...</errorSourceCode>
        
        <sequenceID>1486_17_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1486</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F93%2F43%2F9343d6 ...</errorSourceCode>
        
        <sequenceID>1486_17_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1486</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F93%2F43%2F9343d6 ...</errorSourceCode>
        
        <sequenceID>1486_17_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1486</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F93%2F43%2F9343d6 ...</errorSourceCode>
        
        <sequenceID>1486_17_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1486</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F93%2F43%2F9343d6 ...</errorSourceCode>
        
        <sequenceID>1486_17_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1492</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/contacto/&quot; class=&quot;phone&quot;&gt;&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com/cobi%2Fmedia%2Fd ...</errorSourceCode>
        
        <sequenceID>1492_21_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1492</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/contacto/&quot; class=&quot;phone&quot;&gt;&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com/cobi%2Fmedia%2Fd ...</errorSourceCode>
        
        <sequenceID>1492_21_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1492</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/contacto/&quot; class=&quot;phone&quot;&gt;&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com/cobi%2Fmedia%2Fd ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1492</lineNum>
      <columnNum>56</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F2018%2F6%2F ...</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1492</lineNum>
      <columnNum>56</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F2018%2F6%2F ...</errorSourceCode>
        
        <sequenceID>1492_56_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1492</lineNum>
      <columnNum>56</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F2018%2F6%2F ...</errorSourceCode>
        
        <sequenceID>1492_56_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1498</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/&quot;&gt;Productos&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>1498_21_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1498</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/&quot;&gt;Productos&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>1498_21_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1498</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/&quot;&gt;Productos&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1501</lineNum>
      <columnNum>47</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/access/&quot; class=&quot;btn&quot; &gt;Iniciar Sesi&oacute;n&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>1501_47_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1501</lineNum>
      <columnNum>47</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/access/&quot; class=&quot;btn&quot; &gt;Iniciar Sesi&oacute;n&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>1501_47_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1501</lineNum>
      <columnNum>47</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/access/&quot; class=&quot;btn&quot; &gt;Iniciar Sesi&oacute;n&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1504</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/pide-presupuesto/&quot; class=&quot;btn btn-warning&quot; &gt;Pide presupuesto&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>1504_21_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1504</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/pide-presupuesto/&quot; class=&quot;btn btn-warning&quot; &gt;Pide presupuesto&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>1504_21_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1504</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/pide-presupuesto/&quot; class=&quot;btn btn-warning&quot; &gt;Pide presupuesto&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1509</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-primary&quot; style=&quot;margin-left: 15px;&quot; href=&quot;/es/eres-un-profesional/&quot; onclick=&quot;show_ ...</errorSourceCode>
        
        <sequenceID>1509_25_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1509</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-primary&quot; style=&quot;margin-left: 15px;&quot; href=&quot;/es/eres-un-profesional/&quot; onclick=&quot;show_ ...</errorSourceCode>
        
        <sequenceID>1509_25_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1509</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-primary&quot; style=&quot;margin-left: 15px;&quot; href=&quot;/es/eres-un-profesional/&quot; onclick=&quot;show_ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1509</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-primary&quot; style=&quot;margin-left: 15px;&quot; href=&quot;/es/eres-un-profesional/&quot; onclick=&quot;show_ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1553</lineNum>
      <columnNum>57</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=42&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=42'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h1&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h1&gt;&lt;span style=&quot;color:#FFFFFF;&quot;&gt;ENCUENTRA SOLUCIONES&lt;br /&gt;
DE ORTOPEDIA Y&lt;br /&gt;
​ACCESIBILIDAD&lt; ...</errorSourceCode>
        
        <sequenceID>1553_57_42</sequenceID>
        <decisionPass>This &lt;code&gt;h1&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h1&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1553</lineNum>
      <columnNum>61</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=301&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=301'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;The contrast between the colour of text and its background for the element is not sufficient  to meet WCAG2.0 Level AA.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;span style=&quot;color:#FFFFFF;&quot;&gt;ENCUENTRA SOLUCIONES&lt;br /&gt;
DE ORTOPEDIA Y&lt;br /&gt;
​ACCESIBILIDAD&lt;/spa ...</errorSourceCode>
        <repair>Use a colour contrast evaluator to determine if text and background colours provide a contrast ratio of 4.5:1 for standard text, or 3:1 for larger text. Change colour codes to produce sufficient contrast.

http://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html#visual-audio-contrast-contrast-resources-head</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1561</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-warning btn-lg pide-presupuesto&quot; href=&quot;pide-presupuesto/&quot; style=&quot;font-size: 20px;  ...</errorSourceCode>
        
        <sequenceID>1561_4_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1561</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-warning btn-lg pide-presupuesto&quot; href=&quot;pide-presupuesto/&quot; style=&quot;font-size: 20px;  ...</errorSourceCode>
        
        <sequenceID>1561_4_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1561</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-warning btn-lg pide-presupuesto&quot; href=&quot;pide-presupuesto/&quot; style=&quot;font-size: 20px;  ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1587</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a
                                data-target=&quot;#carousel_18&quot;
                                data-s ...</errorSourceCode>
        
        <sequenceID>1587_25_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1587</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a
                                data-target=&quot;#carousel_18&quot;
                                data-s ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1636</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fel- ...</errorSourceCode>
        
        <sequenceID>1636_1_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1636</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=178&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=178'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text does not convey the same information as the image.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fel- ...</errorSourceCode>
        
        <sequenceID>1636_1_178</sequenceID>
        <decisionPass>Alt text conveys the same information as the image.</decisionPass>
        <decisionFail>Alt text does not convey the same information as the image.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1636</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fel- ...</errorSourceCode>
        
        <sequenceID>1636_1_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1636</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=239&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=239'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; has &lt;code&gt;title&lt;/code&gt; attribute and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fel- ...</errorSourceCode>
        
        <sequenceID>1636_1_239</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1636</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fel- ...</errorSourceCode>
        
        <sequenceID>1636_1_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1636</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fel- ...</errorSourceCode>
        
        <sequenceID>1636_1_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1636</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fel- ...</errorSourceCode>
        
        <sequenceID>1636_1_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1650</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Femp ...</errorSourceCode>
        
        <sequenceID>1650_1_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1650</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=178&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=178'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text does not convey the same information as the image.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Femp ...</errorSourceCode>
        
        <sequenceID>1650_1_178</sequenceID>
        <decisionPass>Alt text conveys the same information as the image.</decisionPass>
        <decisionFail>Alt text does not convey the same information as the image.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1650</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Femp ...</errorSourceCode>
        
        <sequenceID>1650_1_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1650</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=239&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=239'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; has &lt;code&gt;title&lt;/code&gt; attribute and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Femp ...</errorSourceCode>
        
        <sequenceID>1650_1_239</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1650</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Femp ...</errorSourceCode>
        
        <sequenceID>1650_1_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1650</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Femp ...</errorSourceCode>
        
        <sequenceID>1650_1_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1650</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Femp ...</errorSourceCode>
        
        <sequenceID>1650_1_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1664</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fobr ...</errorSourceCode>
        
        <sequenceID>1664_1_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1664</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=178&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=178'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text does not convey the same information as the image.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fobr ...</errorSourceCode>
        
        <sequenceID>1664_1_178</sequenceID>
        <decisionPass>Alt text conveys the same information as the image.</decisionPass>
        <decisionFail>Alt text does not convey the same information as the image.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1664</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fobr ...</errorSourceCode>
        
        <sequenceID>1664_1_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1664</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=239&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=239'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; has &lt;code&gt;title&lt;/code&gt; attribute and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fobr ...</errorSourceCode>
        
        <sequenceID>1664_1_239</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1664</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fobr ...</errorSourceCode>
        
        <sequenceID>1664_1_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1664</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fobr ...</errorSourceCode>
        
        <sequenceID>1664_1_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1664</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fobr ...</errorSourceCode>
        
        <sequenceID>1664_1_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1678</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fla- ...</errorSourceCode>
        
        <sequenceID>1678_1_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1678</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=178&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=178'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text does not convey the same information as the image.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fla- ...</errorSourceCode>
        
        <sequenceID>1678_1_178</sequenceID>
        <decisionPass>Alt text conveys the same information as the image.</decisionPass>
        <decisionFail>Alt text does not convey the same information as the image.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1678</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fla- ...</errorSourceCode>
        
        <sequenceID>1678_1_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1678</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=239&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=239'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; has &lt;code&gt;title&lt;/code&gt; attribute and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fla- ...</errorSourceCode>
        
        <sequenceID>1678_1_239</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1678</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fla- ...</errorSourceCode>
        
        <sequenceID>1678_1_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1678</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fla- ...</errorSourceCode>
        
        <sequenceID>1678_1_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1678</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcms_page_media%2F1%2Fla- ...</errorSourceCode>
        
        <sequenceID>1678_1_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1709</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=43&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=43'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h2&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h2 style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;color: rgb(0, 128, 128);&quot;&gt;&lt;span style=&quot;font-size: 26px; ...</errorSourceCode>
        
        <sequenceID>1709_1_43</sequenceID>
        <decisionPass>This &lt;code&gt;h2&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h2&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1738</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;catalogo de productos ortopedicos online&quot; src=&quot;https://cs-static-pro ...</errorSourceCode>
        
        <sequenceID>1738_13_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1738</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=178&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=178'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text does not convey the same information as the image.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;catalogo de productos ortopedicos online&quot; src=&quot;https://cs-static-pro ...</errorSourceCode>
        
        <sequenceID>1738_13_178</sequenceID>
        <decisionPass>Alt text conveys the same information as the image.</decisionPass>
        <decisionFail>Alt text does not convey the same information as the image.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1738</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;catalogo de productos ortopedicos online&quot; src=&quot;https://cs-static-pro ...</errorSourceCode>
        
        <sequenceID>1738_13_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1738</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;catalogo de productos ortopedicos online&quot; src=&quot;https://cs-static-pro ...</errorSourceCode>
        
        <sequenceID>1738_13_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1738</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;catalogo de productos ortopedicos online&quot; src=&quot;https://cs-static-pro ...</errorSourceCode>
        
        <sequenceID>1738_13_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1738</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;catalogo de productos ortopedicos online&quot; src=&quot;https://cs-static-pro ...</errorSourceCode>
        
        <sequenceID>1738_13_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1755</lineNum>
      <columnNum>34</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=177&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=177'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;font&lt;/code&gt; used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;font color=&quot;#25434b&quot;&gt;P&lt;/font&gt;</errorSourceCode>
        <repair>Remove the &lt;code&gt;font&lt;/code&gt; element from the document.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1759</lineNum>
      <columnNum>34</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-warning btn-outline-warning btn-lg&quot; href=&quot;pide-presupuesto/&quot;&gt;PEDIR PRESUPUESTO&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>1759_34_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1759</lineNum>
      <columnNum>34</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-warning btn-outline-warning btn-lg&quot; href=&quot;pide-presupuesto/&quot;&gt;PEDIR PRESUPUESTO&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>1759_34_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1759</lineNum>
      <columnNum>34</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-warning btn-outline-warning btn-lg&quot; href=&quot;pide-presupuesto/&quot;&gt;PEDIR PRESUPUESTO&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1779</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;cuidadores&quot; src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fm ...</errorSourceCode>
        
        <sequenceID>1779_13_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1779</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=178&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=178'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text does not convey the same information as the image.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;cuidadores&quot; src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fm ...</errorSourceCode>
        
        <sequenceID>1779_13_178</sequenceID>
        <decisionPass>Alt text conveys the same information as the image.</decisionPass>
        <decisionFail>Alt text does not convey the same information as the image.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1779</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;cuidadores&quot; src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fm ...</errorSourceCode>
        
        <sequenceID>1779_13_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1779</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;cuidadores&quot; src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fm ...</errorSourceCode>
        
        <sequenceID>1779_13_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1779</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;cuidadores&quot; src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fm ...</errorSourceCode>
        
        <sequenceID>1779_13_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1779</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;cuidadores&quot; src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fm ...</errorSourceCode>
        
        <sequenceID>1779_13_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1796</lineNum>
      <columnNum>34</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=177&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=177'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;font&lt;/code&gt; used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;font color=&quot;#25434b&quot;&gt;C&lt;/font&gt;</errorSourceCode>
        <repair>Remove the &lt;code&gt;font&lt;/code&gt; element from the document.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1800</lineNum>
      <columnNum>34</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-info btn-outline-warning btn-lg pide-presupuesto&quot; href=&quot;http://marketplace.discubr ...</errorSourceCode>
        
        <sequenceID>1800_34_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1800</lineNum>
      <columnNum>34</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-info btn-outline-warning btn-lg pide-presupuesto&quot; href=&quot;http://marketplace.discubr ...</errorSourceCode>
        
        <sequenceID>1800_34_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1800</lineNum>
      <columnNum>34</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-info btn-outline-warning btn-lg pide-presupuesto&quot; href=&quot;http://marketplace.discubr ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1820</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;accesibilidad en la vivienda&quot; src=&quot;https://cs-static-pro.s3.amazonaw ...</errorSourceCode>
        
        <sequenceID>1820_13_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1820</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=178&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=178'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text does not convey the same information as the image.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;accesibilidad en la vivienda&quot; src=&quot;https://cs-static-pro.s3.amazonaw ...</errorSourceCode>
        
        <sequenceID>1820_13_178</sequenceID>
        <decisionPass>Alt text conveys the same information as the image.</decisionPass>
        <decisionFail>Alt text does not convey the same information as the image.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1820</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;accesibilidad en la vivienda&quot; src=&quot;https://cs-static-pro.s3.amazonaw ...</errorSourceCode>
        
        <sequenceID>1820_13_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1820</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;accesibilidad en la vivienda&quot; src=&quot;https://cs-static-pro.s3.amazonaw ...</errorSourceCode>
        
        <sequenceID>1820_13_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1820</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;accesibilidad en la vivienda&quot; src=&quot;https://cs-static-pro.s3.amazonaw ...</errorSourceCode>
        
        <sequenceID>1820_13_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1820</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;accesibilidad en la vivienda&quot; src=&quot;https://cs-static-pro.s3.amazonaw ...</errorSourceCode>
        
        <sequenceID>1820_13_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1841</lineNum>
      <columnNum>34</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-success btn-outline-warning btn-lg pide-presupuesto&quot; href=&quot;http://discubre.es/es/e ...</errorSourceCode>
        
        <sequenceID>1841_34_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1841</lineNum>
      <columnNum>34</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-success btn-outline-warning btn-lg pide-presupuesto&quot; href=&quot;http://discubre.es/es/e ...</errorSourceCode>
        
        <sequenceID>1841_34_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1841</lineNum>
      <columnNum>34</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-success btn-outline-warning btn-lg pide-presupuesto&quot; href=&quot;http://discubre.es/es/e ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1882</lineNum>
      <columnNum>134</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=301&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=301'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;The contrast between the colour of text and its background for the element is not sufficient  to meet WCAG2.0 Level AA.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;span style=&quot;font-weight: 300;&quot;&gt;Creemos en las personas, familias y colectivos con&lt;/span&gt;</errorSourceCode>
        <repair>Use a colour contrast evaluator to determine if text and background colours provide a contrast ratio of 4.5:1 for standard text, or 3:1 for larger text. Change colour codes to produce sufficient contrast.

http://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html#visual-audio-contrast-contrast-resources-head</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1884</lineNum>
      <columnNum>134</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=301&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=301'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;The contrast between the colour of text and its background for the element is not sufficient  to meet WCAG2.0 Level AA.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;span style=&quot;font-weight: 300;&quot;&gt;discapacidad y/o diversidad funcional.&lt;/span&gt;</errorSourceCode>
        <repair>Use a colour contrast evaluator to determine if text and background colours provide a contrast ratio of 4.5:1 for standard text, or 3:1 for larger text. Change colour codes to produce sufficient contrast.

http://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html#visual-audio-contrast-contrast-resources-head</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1888</lineNum>
      <columnNum>153</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=301&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=301'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;The contrast between the colour of text and its background for the element is not sufficient  to meet WCAG2.0 Level AA.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;span style=&quot;font-weight: 300;&quot;&gt;&amp;nbsp;Nuestro objetivo es poder encontrar soluciones a sus necesidad ...</errorSourceCode>
        <repair>Use a colour contrast evaluator to determine if text and background colours provide a contrast ratio of 4.5:1 for standard text, or 3:1 for larger text. Change colour codes to produce sufficient contrast.

http://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html#visual-audio-contrast-contrast-resources-head</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1892</lineNum>
      <columnNum>85</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-warning btn-outline-warning btn-lg pide-presupuesto&quot; href=&quot;pide-presupuesto/&quot;&gt;PIDE ...</errorSourceCode>
        
        <sequenceID>1892_85_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1892</lineNum>
      <columnNum>85</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-warning btn-outline-warning btn-lg pide-presupuesto&quot; href=&quot;pide-presupuesto/&quot;&gt;PIDE ...</errorSourceCode>
        
        <sequenceID>1892_85_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1892</lineNum>
      <columnNum>85</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-warning btn-outline-warning btn-lg pide-presupuesto&quot; href=&quot;pide-presupuesto/&quot;&gt;PIDE ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1914</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a
                                data-target=&quot;#carousel_30&quot;
                                data-s ...</errorSourceCode>
        
        <sequenceID>1914_25_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1914</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a
                                data-target=&quot;#carousel_30&quot;
                                data-s ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1947</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=43&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=43'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h2&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h2 style=&quot;text-align: center;&quot;&gt;&nbsp;&lt;/h2&gt;</errorSourceCode>
        
        <sequenceID>1947_2_43</sequenceID>
        <decisionPass>This &lt;code&gt;h2&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h2&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1949</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=43&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=43'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h2&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h2 style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-size: 36px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 128, 128); ...</errorSourceCode>
        
        <sequenceID>1949_1_43</sequenceID>
        <decisionPass>This &lt;code&gt;h2&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h2&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1980</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/12/andador-iron/&quot;&gt;&lt;div class=&quot;velado&quot;&gt;&lt;/div&gt;
                        
     ...</errorSourceCode>
        
        <sequenceID>1980_21_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1980</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/12/andador-iron/&quot;&gt;&lt;div class=&quot;velado&quot;&gt;&lt;/div&gt;
                        
     ...</errorSourceCode>
        
        <sequenceID>1980_21_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1980</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/12/andador-iron/&quot;&gt;&lt;div class=&quot;velado&quot;&gt;&lt;/div&gt;
                        
     ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1982</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F3e%2Ffd%2F3efd70 ...</errorSourceCode>
        
        <sequenceID>1982_29_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1982</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F3e%2Ffd%2F3efd70 ...</errorSourceCode>
        
        <sequenceID>1982_29_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1982</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F3e%2Ffd%2F3efd70 ...</errorSourceCode>
        
        <sequenceID>1982_29_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1982</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F3e%2Ffd%2F3efd70 ...</errorSourceCode>
        
        <sequenceID>1982_29_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1982</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F3e%2Ffd%2F3efd70 ...</errorSourceCode>
        
        <sequenceID>1982_29_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1982</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F3e%2Ffd%2F3efd70 ...</errorSourceCode>
        
        <sequenceID>1982_29_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1986</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1986</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        
        <sequenceID>1986_25_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1986</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        
        <sequenceID>1986_25_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1986</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1987</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1987</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        
        <sequenceID>1987_25_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1987</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        
        <sequenceID>1987_25_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1987</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1991</lineNum>
      <columnNum>39</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/12/andador-iron/&quot;&gt;El andador Iron Rueda Walker Cart Red. Precio asequible  ...</errorSourceCode>
        
        <sequenceID>1991_39_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1991</lineNum>
      <columnNum>39</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/12/andador-iron/&quot;&gt;El andador Iron Rueda Walker Cart Red. Precio asequible  ...</errorSourceCode>
        
        <sequenceID>1991_39_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1991</lineNum>
      <columnNum>39</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/12/andador-iron/&quot;&gt;El andador Iron Rueda Walker Cart Red. Precio asequible  ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2000</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/12/andador-iron/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt; Comparte ...</errorSourceCode>
        
        <sequenceID>2000_29_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2000</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/12/andador-iron/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt; Comparte ...</errorSourceCode>
        
        <sequenceID>2000_29_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2000</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/12/andador-iron/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt; Comparte ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2000</lineNum>
      <columnNum>73</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt;</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2000</lineNum>
      <columnNum>73</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2000_73_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2000</lineNum>
      <columnNum>73</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2000_73_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2003</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/12/andador-iron/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt; Comenta&lt;/ ...</errorSourceCode>
        
        <sequenceID>2003_29_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2003</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/12/andador-iron/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt; Comenta&lt;/ ...</errorSourceCode>
        
        <sequenceID>2003_29_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2003</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/12/andador-iron/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt; Comenta&lt;/ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2003</lineNum>
      <columnNum>73</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt;</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2003</lineNum>
      <columnNum>73</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2003_73_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2003</lineNum>
      <columnNum>73</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2003_73_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2029</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/06/handiscover-es/&quot;&gt;&lt;div class=&quot;velado&quot;&gt;&lt;/div&gt;
                        
   ...</errorSourceCode>
        
        <sequenceID>2029_21_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2029</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/06/handiscover-es/&quot;&gt;&lt;div class=&quot;velado&quot;&gt;&lt;/div&gt;
                        
   ...</errorSourceCode>
        
        <sequenceID>2029_21_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2029</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/06/handiscover-es/&quot;&gt;&lt;div class=&quot;velado&quot;&gt;&lt;/div&gt;
                        
   ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2031</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F90%2Ffd%2F90fd02 ...</errorSourceCode>
        
        <sequenceID>2031_29_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2031</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F90%2Ffd%2F90fd02 ...</errorSourceCode>
        
        <sequenceID>2031_29_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2031</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F90%2Ffd%2F90fd02 ...</errorSourceCode>
        
        <sequenceID>2031_29_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2031</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F90%2Ffd%2F90fd02 ...</errorSourceCode>
        
        <sequenceID>2031_29_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2031</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F90%2Ffd%2F90fd02 ...</errorSourceCode>
        
        <sequenceID>2031_29_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2031</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F90%2Ffd%2F90fd02 ...</errorSourceCode>
        
        <sequenceID>2031_29_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2035</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2035</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        
        <sequenceID>2035_25_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2035</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        
        <sequenceID>2035_25_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2035</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2036</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2036</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        
        <sequenceID>2036_25_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2036</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        
        <sequenceID>2036_25_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2036</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2040</lineNum>
      <columnNum>39</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/06/handiscover-es/&quot;&gt;.&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2040_39_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2040</lineNum>
      <columnNum>39</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/06/handiscover-es/&quot;&gt;.&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2040_39_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2040</lineNum>
      <columnNum>39</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/06/handiscover-es/&quot;&gt;.&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2049</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/06/handiscover-es/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt; Compar ...</errorSourceCode>
        
        <sequenceID>2049_29_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2049</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/06/handiscover-es/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt; Compar ...</errorSourceCode>
        
        <sequenceID>2049_29_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2049</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/06/handiscover-es/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt; Compar ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2049</lineNum>
      <columnNum>75</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt;</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2049</lineNum>
      <columnNum>75</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2049_75_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2049</lineNum>
      <columnNum>75</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2049_75_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2052</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/06/handiscover-es/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt; Comenta ...</errorSourceCode>
        
        <sequenceID>2052_29_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2052</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/06/handiscover-es/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt; Comenta ...</errorSourceCode>
        
        <sequenceID>2052_29_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2052</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/06/handiscover-es/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt; Comenta ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2052</lineNum>
      <columnNum>75</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt;</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2052</lineNum>
      <columnNum>75</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2052_75_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2052</lineNum>
      <columnNum>75</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2052_75_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2078</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/04/air-wheel/&quot;&gt;&lt;div class=&quot;velado&quot;&gt;&lt;/div&gt;
                        
        ...</errorSourceCode>
        
        <sequenceID>2078_21_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2078</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/04/air-wheel/&quot;&gt;&lt;div class=&quot;velado&quot;&gt;&lt;/div&gt;
                        
        ...</errorSourceCode>
        
        <sequenceID>2078_21_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2078</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/04/air-wheel/&quot;&gt;&lt;div class=&quot;velado&quot;&gt;&lt;/div&gt;
                        
        ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2080</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F59%2F68%2F59681a ...</errorSourceCode>
        
        <sequenceID>2080_29_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2080</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F59%2F68%2F59681a ...</errorSourceCode>
        
        <sequenceID>2080_29_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2080</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F59%2F68%2F59681a ...</errorSourceCode>
        
        <sequenceID>2080_29_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2080</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F59%2F68%2F59681a ...</errorSourceCode>
        
        <sequenceID>2080_29_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2080</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F59%2F68%2F59681a ...</errorSourceCode>
        
        <sequenceID>2080_29_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2080</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F59%2F68%2F59681a ...</errorSourceCode>
        
        <sequenceID>2080_29_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2084</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2084</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        
        <sequenceID>2084_25_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2084</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        
        <sequenceID>2084_25_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2084</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2085</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2085</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        
        <sequenceID>2085_25_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2085</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        
        <sequenceID>2085_25_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2085</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2089</lineNum>
      <columnNum>39</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/04/air-wheel/&quot;&gt;SILLA DE RUEDAS AIRWHEEL&amp;nbsp;A6S&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2089_39_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2089</lineNum>
      <columnNum>39</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/04/air-wheel/&quot;&gt;SILLA DE RUEDAS AIRWHEEL&amp;nbsp;A6S&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2089_39_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2089</lineNum>
      <columnNum>39</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/04/air-wheel/&quot;&gt;SILLA DE RUEDAS AIRWHEEL&amp;nbsp;A6S&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2098</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/04/air-wheel/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt; Comparte&lt;/a ...</errorSourceCode>
        
        <sequenceID>2098_29_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2098</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/04/air-wheel/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt; Comparte&lt;/a ...</errorSourceCode>
        
        <sequenceID>2098_29_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2098</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/04/air-wheel/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt; Comparte&lt;/a ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2098</lineNum>
      <columnNum>70</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt;</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2098</lineNum>
      <columnNum>70</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2098_70_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2098</lineNum>
      <columnNum>70</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2098_70_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2101</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/04/air-wheel/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt; Comenta&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2101_29_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2101</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/04/air-wheel/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt; Comenta&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2101_29_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2101</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/06/04/air-wheel/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt; Comenta&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2101</lineNum>
      <columnNum>70</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt;</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2101</lineNum>
      <columnNum>70</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2101_70_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2101</lineNum>
      <columnNum>70</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2101_70_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2127</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/29/getup/&quot;&gt;&lt;div class=&quot;velado&quot;&gt;&lt;/div&gt;
                        
            ...</errorSourceCode>
        
        <sequenceID>2127_21_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2127</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/29/getup/&quot;&gt;&lt;div class=&quot;velado&quot;&gt;&lt;/div&gt;
                        
            ...</errorSourceCode>
        
        <sequenceID>2127_21_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2127</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/29/getup/&quot;&gt;&lt;div class=&quot;velado&quot;&gt;&lt;/div&gt;
                        
            ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2129</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F13%2F05%2F130597 ...</errorSourceCode>
        
        <sequenceID>2129_29_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2129</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F13%2F05%2F130597 ...</errorSourceCode>
        
        <sequenceID>2129_29_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2129</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F13%2F05%2F130597 ...</errorSourceCode>
        
        <sequenceID>2129_29_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2129</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F13%2F05%2F130597 ...</errorSourceCode>
        
        <sequenceID>2129_29_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2129</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F13%2F05%2F130597 ...</errorSourceCode>
        
        <sequenceID>2129_29_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2129</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F13%2F05%2F130597 ...</errorSourceCode>
        
        <sequenceID>2129_29_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2133</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2133</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        
        <sequenceID>2133_25_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2133</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        
        <sequenceID>2133_25_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2133</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2134</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2134</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        
        <sequenceID>2134_25_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2134</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        
        <sequenceID>2134_25_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2134</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2138</lineNum>
      <columnNum>39</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/29/getup/&quot;&gt;SILLA GET UP una soluci&oacute;n&amp;nbsp;vertical&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2138_39_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2138</lineNum>
      <columnNum>39</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/29/getup/&quot;&gt;SILLA GET UP una soluci&oacute;n&amp;nbsp;vertical&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2138_39_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2138</lineNum>
      <columnNum>39</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/29/getup/&quot;&gt;SILLA GET UP una soluci&oacute;n&amp;nbsp;vertical&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2147</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/29/getup/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt; Comparte&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2147_29_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2147</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/29/getup/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt; Comparte&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2147_29_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2147</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/29/getup/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt; Comparte&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2147</lineNum>
      <columnNum>66</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt;</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2147</lineNum>
      <columnNum>66</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2147_66_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2147</lineNum>
      <columnNum>66</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2147_66_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2150</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/29/getup/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt; Comenta&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2150_29_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2150</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/29/getup/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt; Comenta&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2150_29_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2150</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/29/getup/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt; Comenta&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2150</lineNum>
      <columnNum>66</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt;</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2150</lineNum>
      <columnNum>66</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2150_66_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2150</lineNum>
      <columnNum>66</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2150_66_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2176</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/23/handiscover-uk/&quot;&gt;&lt;div class=&quot;velado&quot;&gt;&lt;/div&gt;
                        
   ...</errorSourceCode>
        
        <sequenceID>2176_21_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2176</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/23/handiscover-uk/&quot;&gt;&lt;div class=&quot;velado&quot;&gt;&lt;/div&gt;
                        
   ...</errorSourceCode>
        
        <sequenceID>2176_21_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2176</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/23/handiscover-uk/&quot;&gt;&lt;div class=&quot;velado&quot;&gt;&lt;/div&gt;
                        
   ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2178</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F7e%2F93%2F7e930e ...</errorSourceCode>
        
        <sequenceID>2178_29_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2178</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F7e%2F93%2F7e930e ...</errorSourceCode>
        
        <sequenceID>2178_29_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2178</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F7e%2F93%2F7e930e ...</errorSourceCode>
        
        <sequenceID>2178_29_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2178</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F7e%2F93%2F7e930e ...</errorSourceCode>
        
        <sequenceID>2178_29_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2178</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F7e%2F93%2F7e930e ...</errorSourceCode>
        
        <sequenceID>2178_29_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2178</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2F7e%2F93%2F7e930e ...</errorSourceCode>
        
        <sequenceID>2178_29_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2182</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2182</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        
        <sequenceID>2182_25_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2182</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        
        <sequenceID>2182_25_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2182</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2183</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2183</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        
        <sequenceID>2183_25_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2183</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        
        <sequenceID>2183_25_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2183</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2187</lineNum>
      <columnNum>39</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/23/handiscover-uk/&quot;&gt;.&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2187_39_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2187</lineNum>
      <columnNum>39</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/23/handiscover-uk/&quot;&gt;.&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2187_39_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2187</lineNum>
      <columnNum>39</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/23/handiscover-uk/&quot;&gt;.&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2196</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/23/handiscover-uk/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt; Compar ...</errorSourceCode>
        
        <sequenceID>2196_29_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2196</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/23/handiscover-uk/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt; Compar ...</errorSourceCode>
        
        <sequenceID>2196_29_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2196</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/23/handiscover-uk/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt; Compar ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2196</lineNum>
      <columnNum>75</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt;</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2196</lineNum>
      <columnNum>75</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2196_75_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2196</lineNum>
      <columnNum>75</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2196_75_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2199</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/23/handiscover-uk/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt; Comenta ...</errorSourceCode>
        
        <sequenceID>2199_29_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2199</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/23/handiscover-uk/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt; Comenta ...</errorSourceCode>
        
        <sequenceID>2199_29_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2199</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/23/handiscover-uk/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt; Comenta ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2199</lineNum>
      <columnNum>75</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt;</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2199</lineNum>
      <columnNum>75</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2199_75_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2199</lineNum>
      <columnNum>75</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2199_75_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2225</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/22/transferencia/&quot;&gt;&lt;div class=&quot;velado&quot;&gt;&lt;/div&gt;
                        
    ...</errorSourceCode>
        
        <sequenceID>2225_21_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2225</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/22/transferencia/&quot;&gt;&lt;div class=&quot;velado&quot;&gt;&lt;/div&gt;
                        
    ...</errorSourceCode>
        
        <sequenceID>2225_21_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2225</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/22/transferencia/&quot;&gt;&lt;div class=&quot;velado&quot;&gt;&lt;/div&gt;
                        
    ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2227</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2Fc5%2F9f%2Fc59f7a ...</errorSourceCode>
        
        <sequenceID>2227_29_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2227</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2Fc5%2F9f%2Fc59f7a ...</errorSourceCode>
        
        <sequenceID>2227_29_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2227</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2Fc5%2F9f%2Fc59f7a ...</errorSourceCode>
        
        <sequenceID>2227_29_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2227</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2Fc5%2F9f%2Fc59f7a ...</errorSourceCode>
        
        <sequenceID>2227_29_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2227</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2Fc5%2F9f%2Fc59f7a ...</errorSourceCode>
        
        <sequenceID>2227_29_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2227</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc65%2Fcache%2Fc5%2F9f%2Fc59f7a ...</errorSourceCode>
        
        <sequenceID>2227_29_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2231</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2231</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        
        <sequenceID>2231_25_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2231</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        
        <sequenceID>2231_25_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2231</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img style=&quot;cursor: pointer;&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.like&#039;).show();$(this).hide();&quot;  ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2232</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2232</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        
        <sequenceID>2232_25_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2232</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        
        <sequenceID>2232_25_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2232</lineNum>
      <columnNum>25</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/like_2.svg&quot; onclick=&quot;$(this).closest(&#039;div&#039;).find(&#039;.nolike&#039;).show(); ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2236</lineNum>
      <columnNum>39</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/22/transferencia/&quot;&gt;&iquest;QU&Eacute; DEBO SABER PARA HACER TRANSFERENCIAS DE PERSONAS ...</errorSourceCode>
        
        <sequenceID>2236_39_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2236</lineNum>
      <columnNum>39</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/22/transferencia/&quot;&gt;&iquest;QU&Eacute; DEBO SABER PARA HACER TRANSFERENCIAS DE PERSONAS ...</errorSourceCode>
        
        <sequenceID>2236_39_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2236</lineNum>
      <columnNum>39</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/22/transferencia/&quot;&gt;&iquest;QU&Eacute; DEBO SABER PARA HACER TRANSFERENCIAS DE PERSONAS ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2245</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/22/transferencia/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt; Compart ...</errorSourceCode>
        
        <sequenceID>2245_29_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2245</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/22/transferencia/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt; Compart ...</errorSourceCode>
        
        <sequenceID>2245_29_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2245</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/22/transferencia/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt; Compart ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2245</lineNum>
      <columnNum>74</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt;</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2245</lineNum>
      <columnNum>74</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2245_74_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2245</lineNum>
      <columnNum>74</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comparte.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2245_74_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2248</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/22/transferencia/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt; Comenta&lt; ...</errorSourceCode>
        
        <sequenceID>2248_29_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2248</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/22/transferencia/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt; Comenta&lt; ...</errorSourceCode>
        
        <sequenceID>2248_29_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2248</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/blog/2018/05/22/transferencia/&quot;&gt;&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt; Comenta&lt; ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2248</lineNum>
      <columnNum>74</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=1&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=1'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element missing &lt;code&gt;alt&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt;</errorSourceCode>
        <repair>Add an &lt;code&gt;alt&lt;/code&gt; attribute to your &lt;code&gt;img&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2248</lineNum>
      <columnNum>74</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2248_74_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2248</lineNum>
      <columnNum>74</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;/static/cs17/img/icons/comenta.svg&quot;&gt;</errorSourceCode>
        
        <sequenceID>2248_74_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2276</lineNum>
      <columnNum>42</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a style=&quot;width: 220px;&quot; class=&quot;btn btn-warning btn-outline-warning btn-lg pide-presupuesto&quot; href=&quot;h ...</errorSourceCode>
        
        <sequenceID>2276_42_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2276</lineNum>
      <columnNum>42</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a style=&quot;width: 220px;&quot; class=&quot;btn btn-warning btn-outline-warning btn-lg pide-presupuesto&quot; href=&quot;h ...</errorSourceCode>
        
        <sequenceID>2276_42_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2276</lineNum>
      <columnNum>42</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a style=&quot;width: 220px;&quot; class=&quot;btn btn-warning btn-outline-warning btn-lg pide-presupuesto&quot; href=&quot;h ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2301</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/access/&quot; class=&quot;btn btn-primary log-in&quot; onclick=&quot;show_loading();&quot;&gt; Inicia ...</errorSourceCode>
        
        <sequenceID>2301_21_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2301</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/access/&quot; class=&quot;btn btn-primary log-in&quot; onclick=&quot;show_loading();&quot;&gt; Inicia ...</errorSourceCode>
        
        <sequenceID>2301_21_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2301</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/access/&quot; class=&quot;btn btn-primary log-in&quot; onclick=&quot;show_loading();&quot;&gt; Inicia ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2301</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/access/&quot; class=&quot;btn btn-primary log-in&quot; onclick=&quot;show_loading();&quot;&gt; Inicia ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2325</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/ortopedias/&quot;&gt;&lt;span style=&quot;color: #FFFFFF;&quot;&gt;ORTOPEDIAS ONLINE&lt;/span&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2325_35_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2325</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/ortopedias/&quot;&gt;&lt;span style=&quot;color: #FFFFFF;&quot;&gt;ORTOPEDIAS ONLINE&lt;/span&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2325_35_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2325</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/ortopedias/&quot;&gt;&lt;span style=&quot;color: #FFFFFF;&quot;&gt;ORTOPEDIAS ONLINE&lt;/span&gt;&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2325</lineNum>
      <columnNum>75</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=301&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=301'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;The contrast between the colour of text and its background for the element is not sufficient  to meet WCAG2.0 Level AA.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;span style=&quot;color: #FFFFFF;&quot;&gt;ORTOPEDIAS ONLINE&lt;/span&gt;</errorSourceCode>
        <repair>Use a colour contrast evaluator to determine if text and background colours provide a contrast ratio of 4.5:1 for standard text, or 3:1 for larger text. Change colour codes to produce sufficient contrast.

http://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html#visual-audio-contrast-contrast-resources-head</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2328</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/movilidad/&quot;   &gt;
        Directorio de Ortopedias online
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2328_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2328</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/movilidad/&quot;   &gt;
        Directorio de Ortopedias online
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2328_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2328</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/movilidad/&quot;   &gt;
        Directorio de Ortopedias online
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2334</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/fabricantes-e-importadores/&quot;   &gt;
        Directorio de Fabricantes
    &lt;/ ...</errorSourceCode>
        
        <sequenceID>2334_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2334</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/fabricantes-e-importadores/&quot;   &gt;
        Directorio de Fabricantes
    &lt;/ ...</errorSourceCode>
        
        <sequenceID>2334_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2334</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/fabricantes-e-importadores/&quot;   &gt;
        Directorio de Fabricantes
    &lt;/ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2340</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/movilidad/madrid/&quot;   &gt;
        Ortopedias en Madrid
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2340_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2340</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/movilidad/madrid/&quot;   &gt;
        Ortopedias en Madrid
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2340_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2340</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/movilidad/madrid/&quot;   &gt;
        Ortopedias en Madrid
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2346</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/movilidad/barcelona/&quot;   &gt;
        Ortopedias en Barcelona
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2346_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2346</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/movilidad/barcelona/&quot;   &gt;
        Ortopedias en Barcelona
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2346_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2346</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/movilidad/barcelona/&quot;   &gt;
        Ortopedias en Barcelona
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2352</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/movilidad/sevilla/&quot;   &gt;
        Ortopedias en Sevilla
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2352_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2352</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/movilidad/sevilla/&quot;   &gt;
        Ortopedias en Sevilla
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2352_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2352</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/movilidad/sevilla/&quot;   &gt;
        Ortopedias en Sevilla
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2358</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/movilidad/zaragoza/&quot;   &gt;
        Ortopedias en Zaragoza
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2358_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2358</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/movilidad/zaragoza/&quot;   &gt;
        Ortopedias en Zaragoza
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2358_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2358</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/movilidad/zaragoza/&quot;   &gt;
        Ortopedias en Zaragoza
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2364</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/rampa/&quot;   &gt;
        Rampa
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2364_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2364</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/rampa/&quot;   &gt;
        Rampa
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2364_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2364</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/rampa/&quot;   &gt;
        Rampa
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2370</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/rehabilitacion/&quot;   &gt;
        Rehabilitaci&oacute;n
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2370_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2370</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/rehabilitacion/&quot;   &gt;
        Rehabilitaci&oacute;n
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2370_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2370</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/rehabilitacion/&quot;   &gt;
        Rehabilitaci&oacute;n
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2376</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/salud/&quot;   &gt;
        Salud
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2376_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2376</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/salud/&quot;   &gt;
        Salud
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2376_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2376</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/salud/&quot;   &gt;
        Salud
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2382</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/juegos-memoria-adultos/&quot;   &gt;
        Juegos para discapacitados
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2382_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2382</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/juegos-memoria-adultos/&quot;   &gt;
        Juegos para discapacitados
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2382_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2382</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/juegos-memoria-adultos/&quot;   &gt;
        Juegos para discapacitados
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2388</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/accesibilidad/amigo-24-malaga-costa-del-sol-169&quot;   &gt;
        Alq ...</errorSourceCode>
        
        <sequenceID>2388_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2388</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/accesibilidad/amigo-24-malaga-costa-del-sol-169&quot;   &gt;
        Alq ...</errorSourceCode>
        
        <sequenceID>2388_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2388</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/accesibilidad/amigo-24-malaga-costa-del-sol-169&quot;   &gt;
        Alq ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2395</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/accesibilidad/vehiculos-adaptados/&quot;&gt;&lt;span style=&quot;color: #FFFFFF; ...</errorSourceCode>
        
        <sequenceID>2395_35_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2395</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/accesibilidad/vehiculos-adaptados/&quot;&gt;&lt;span style=&quot;color: #FFFFFF; ...</errorSourceCode>
        
        <sequenceID>2395_35_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2395</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/accesibilidad/vehiculos-adaptados/&quot;&gt;&lt;span style=&quot;color: #FFFFFF; ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2395</lineNum>
      <columnNum>107</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=301&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=301'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;The contrast between the colour of text and its background for the element is not sufficient  to meet WCAG2.0 Level AA.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;span style=&quot;color: #FFFFFF;&quot;&gt;COCHES ADAPTADOS&lt;/span&gt;</errorSourceCode>
        <repair>Use a colour contrast evaluator to determine if text and background colours provide a contrast ratio of 4.5:1 for standard text, or 3:1 for larger text. Change colour codes to produce sufficient contrast.

http://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html#visual-audio-contrast-contrast-resources-head</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2400</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/accesibilidad/vehiculos-adaptados/&quot;   &gt;
        Talleres de coch ...</errorSourceCode>
        
        <sequenceID>2400_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2400</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/accesibilidad/vehiculos-adaptados/&quot;   &gt;
        Talleres de coch ...</errorSourceCode>
        
        <sequenceID>2400_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2400</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/accesibilidad/vehiculos-adaptados/&quot;   &gt;
        Talleres de coch ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2406</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/accesibilidad/utilcastell-10230/&quot;   &gt;
        Alquiler de coches ...</errorSourceCode>
        
        <sequenceID>2406_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2406</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/accesibilidad/utilcastell-10230/&quot;   &gt;
        Alquiler de coches ...</errorSourceCode>
        
        <sequenceID>2406_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2406</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/accesibilidad/utilcastell-10230/&quot;   &gt;
        Alquiler de coches ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2413</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/encuentra-profesionales/&quot;&gt;&lt;span style=&quot;color: #FFFFFF;&quot;&gt;GU&Iacute;A DE FABRICANTES&lt;/span&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2413_35_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2413</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/encuentra-profesionales/&quot;&gt;&lt;span style=&quot;color: #FFFFFF;&quot;&gt;GU&Iacute;A DE FABRICANTES&lt;/span&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2413_35_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2413</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/encuentra-profesionales/&quot;&gt;&lt;span style=&quot;color: #FFFFFF;&quot;&gt;GU&Iacute;A DE FABRICANTES&lt;/span&gt;&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2413</lineNum>
      <columnNum>74</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=301&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=301'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;The contrast between the colour of text and its background for the element is not sufficient  to meet WCAG2.0 Level AA.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;span style=&quot;color: #FFFFFF;&quot;&gt;GU&Iacute;A DE FABRICANTES&lt;/span&gt;</errorSourceCode>
        <repair>Use a colour contrast evaluator to determine if text and background colours provide a contrast ratio of 4.5:1 for standard text, or 3:1 for larger text. Change colour codes to produce sufficient contrast.

http://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html#visual-audio-contrast-contrast-resources-head</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2422</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/atencion-personal/&quot;&gt;&lt;span style=&quot;color: #FFFFFF;&quot;&gt;ATENCI&Oacute;N PERSONAL&lt;/spa ...</errorSourceCode>
        
        <sequenceID>2422_35_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2422</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/atencion-personal/&quot;&gt;&lt;span style=&quot;color: #FFFFFF;&quot;&gt;ATENCI&Oacute;N PERSONAL&lt;/spa ...</errorSourceCode>
        
        <sequenceID>2422_35_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2422</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/atencion-personal/&quot;&gt;&lt;span style=&quot;color: #FFFFFF;&quot;&gt;ATENCI&Oacute;N PERSONAL&lt;/spa ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2422</lineNum>
      <columnNum>82</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=301&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=301'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;The contrast between the colour of text and its background for the element is not sufficient  to meet WCAG2.0 Level AA.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;span style=&quot;color: #FFFFFF;&quot;&gt;ATENCI&Oacute;N PERSONAL&lt;/span&gt;</errorSourceCode>
        <repair>Use a colour contrast evaluator to determine if text and background colours provide a contrast ratio of 4.5:1 for standard text, or 3:1 for larger text. Change colour codes to produce sufficient contrast.

http://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html#visual-audio-contrast-contrast-resources-head</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2425</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/cuidadores/&quot;   &gt;
        Cuidadores
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2425_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2425</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/cuidadores/&quot;   &gt;
        Cuidadores
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2425_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2425</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/cuidadores/&quot;   &gt;
        Cuidadores
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2431</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/terapeutas-ocupacionales/&quot;   &gt;
        Terapeutas ocupacionales
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2431_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2431</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/terapeutas-ocupacionales/&quot;   &gt;
        Terapeutas ocupacionales
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2431_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2431</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/terapeutas-ocupacionales/&quot;   &gt;
        Terapeutas ocupacionales
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2437</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/sexologos/&quot;   &gt;
        Sexologo
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2437_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2437</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/sexologos/&quot;   &gt;
        Sexologo
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2437_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2437</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/sexologos/&quot;   &gt;
        Sexologo
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2443</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/fisioterapia/&quot;   &gt;
        Fisioterapeutas
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2443_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2443</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/fisioterapia/&quot;   &gt;
        Fisioterapeutas
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2443_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2443</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/fisioterapia/&quot;   &gt;
        Fisioterapeutas
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2449</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/terapia-asistida-por-animales/&quot;   &gt;
        Terapia asistida con animales ...</errorSourceCode>
        
        <sequenceID>2449_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2449</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/terapia-asistida-por-animales/&quot;   &gt;
        Terapia asistida con animales ...</errorSourceCode>
        
        <sequenceID>2449_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2449</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/terapia-asistida-por-animales/&quot;   &gt;
        Terapia asistida con animales ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2455</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/psicologos/&quot;   &gt;
        Psicologos
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2455_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2455</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/psicologos/&quot;   &gt;
        Psicologos
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2455_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2455</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/psicologos/&quot;   &gt;
        Psicologos
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2461</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/asistencia-a-domicilio/&quot;   &gt;
        Ayuda a domicilio
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2461_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2461</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/asistencia-a-domicilio/&quot;   &gt;
        Ayuda a domicilio
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2461_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2461</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/asistencia-a-domicilio/&quot;   &gt;
        Ayuda a domicilio
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2467</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/insercion-laboral/&quot;   &gt;
        Inserci&oacute;n laboral
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2467_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2467</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/insercion-laboral/&quot;   &gt;
        Inserci&oacute;n laboral
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2467_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2467</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/insercion-laboral/&quot;   &gt;
        Inserci&oacute;n laboral
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2474</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/ortopedias/&quot;&gt;&lt;span style=&quot;color: #FFFFFF;&quot;&gt;PRODUCTOS EN LA TIENDA ONLINE&lt; ...</errorSourceCode>
        
        <sequenceID>2474_35_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2474</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/ortopedias/&quot;&gt;&lt;span style=&quot;color: #FFFFFF;&quot;&gt;PRODUCTOS EN LA TIENDA ONLINE&lt; ...</errorSourceCode>
        
        <sequenceID>2474_35_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2474</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/ortopedias/&quot;&gt;&lt;span style=&quot;color: #FFFFFF;&quot;&gt;PRODUCTOS EN LA TIENDA ONLINE&lt; ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2474</lineNum>
      <columnNum>75</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=301&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=301'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;The contrast between the colour of text and its background for the element is not sufficient  to meet WCAG2.0 Level AA.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;span style=&quot;color: #FFFFFF;&quot;&gt;PRODUCTOS EN LA TIENDA ONLINE&lt;/span&gt;</errorSourceCode>
        <repair>Use a colour contrast evaluator to determine if text and background colours provide a contrast ratio of 4.5:1 for standard text, or 3:1 for larger text. Change colour codes to produce sufficient contrast.

http://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html#visual-audio-contrast-contrast-resources-head</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2477</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/98-movilidad&quot;   &gt;
        Sillas de ruedas
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2477_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2477</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/98-movilidad&quot;   &gt;
        Sillas de ruedas
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2477_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2477</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/98-movilidad&quot;   &gt;
        Sillas de ruedas
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2483</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/98-movilidad&quot;   &gt;
        Scooter electrico
    &lt;/a ...</errorSourceCode>
        
        <sequenceID>2483_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2483</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/98-movilidad&quot;   &gt;
        Scooter electrico
    &lt;/a ...</errorSourceCode>
        
        <sequenceID>2483_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2483</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/98-movilidad&quot;   &gt;
        Scooter electrico
    &lt;/a ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2489</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/91-vida-diaria&quot;   &gt;
        Camas articuladas elect ...</errorSourceCode>
        
        <sequenceID>2489_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2489</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/91-vida-diaria&quot;   &gt;
        Camas articuladas elect ...</errorSourceCode>
        
        <sequenceID>2489_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2489</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/91-vida-diaria&quot;   &gt;
        Camas articuladas elect ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2495</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/98-movilidad&quot;   &gt;
        Andadores
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2495_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2495</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/98-movilidad&quot;   &gt;
        Andadores
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2495_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2495</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/98-movilidad&quot;   &gt;
        Andadores
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2501</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/90-rampas&quot;   &gt;
        Rampas
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2501_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2501</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/90-rampas&quot;   &gt;
        Rampas
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2501_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2501</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/90-rampas&quot;   &gt;
        Rampas
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2507</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/107-gruas&quot;   &gt;
        Gr&uacute;as
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2507_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2507</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/107-gruas&quot;   &gt;
        Gr&uacute;as
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2507_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2507</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/107-gruas&quot;   &gt;
        Gr&uacute;as
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2513</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/96-infantil&quot;   &gt;
        Productos de apoyo infanti ...</errorSourceCode>
        
        <sequenceID>2513_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2513</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/96-infantil&quot;   &gt;
        Productos de apoyo infanti ...</errorSourceCode>
        
        <sequenceID>2513_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2513</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://discubre.com/product/es/category/96-infantil&quot;   &gt;
        Productos de apoyo infanti ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2523</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/VIVIENDA/&quot;&gt;&lt;span style=&quot;color: #FFFFFF;&quot;&gt;ACCESIBILIDAD POR LA VIVIENDA&lt;/s ...</errorSourceCode>
        
        <sequenceID>2523_35_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2523</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/VIVIENDA/&quot;&gt;&lt;span style=&quot;color: #FFFFFF;&quot;&gt;ACCESIBILIDAD POR LA VIVIENDA&lt;/s ...</errorSourceCode>
        
        <sequenceID>2523_35_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2523</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/VIVIENDA/&quot;&gt;&lt;span style=&quot;color: #FFFFFF;&quot;&gt;ACCESIBILIDAD POR LA VIVIENDA&lt;/s ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2523</lineNum>
      <columnNum>73</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=301&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=301'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;The contrast between the colour of text and its background for the element is not sufficient  to meet WCAG2.0 Level AA.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;span style=&quot;color: #FFFFFF;&quot;&gt;ACCESIBILIDAD POR LA VIVIENDA&lt;/span&gt;</errorSourceCode>
        <repair>Use a colour contrast evaluator to determine if text and background colours provide a contrast ratio of 4.5:1 for standard text, or 3:1 for larger text. Change colour codes to produce sufficient contrast.

http://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html#visual-audio-contrast-contrast-resources-head</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2526</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/consultor-en-accesibilidad/&quot;   &gt;
        Asesor en accesibilidad
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2526_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2526</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/consultor-en-accesibilidad/&quot;   &gt;
        Asesor en accesibilidad
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2526_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2526</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/consultor-en-accesibilidad/&quot;   &gt;
        Asesor en accesibilidad
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2532</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/instaladores-de-salvaescaleras/&quot;   &gt;
        Instaladores de salvaescaler ...</errorSourceCode>
        
        <sequenceID>2532_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2532</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/instaladores-de-salvaescaleras/&quot;   &gt;
        Instaladores de salvaescaler ...</errorSourceCode>
        
        <sequenceID>2532_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2532</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/instaladores-de-salvaescaleras/&quot;   &gt;
        Instaladores de salvaescaler ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2538</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/instaladores-de-grua-de-techo/&quot;   &gt;
        Instaladores de gr&uacute;a de tech ...</errorSourceCode>
        
        <sequenceID>2538_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2538</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/instaladores-de-grua-de-techo/&quot;   &gt;
        Instaladores de gr&uacute;a de tech ...</errorSourceCode>
        
        <sequenceID>2538_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2538</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/instaladores-de-grua-de-techo/&quot;   &gt;
        Instaladores de gr&uacute;a de tech ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2544</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/instaladores-de-grua-piscina/&quot;   &gt;
        Instaladores de gr&uacute;a de pisci ...</errorSourceCode>
        
        <sequenceID>2544_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2544</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/instaladores-de-grua-piscina/&quot;   &gt;
        Instaladores de gr&uacute;a de pisci ...</errorSourceCode>
        
        <sequenceID>2544_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2544</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/accesibilidad/instaladores-de-grua-piscina/&quot;   &gt;
        Instaladores de gr&uacute;a de pisci ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2551</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=301&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=301'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;The contrast between the colour of text and its background for the element is not sufficient  to meet WCAG2.0 Level AA.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;span style=&quot;color: #FFFFFF;&quot;&gt;PRESUPUESTOS DE REFORMAS&lt;/span&gt;</errorSourceCode>
        <repair>Use a colour contrast evaluator to determine if text and background colours provide a contrast ratio of 4.5:1 for standard text, or 3:1 for larger text. Change colour codes to produce sufficient contrast.

http://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html#visual-audio-contrast-contrast-resources-head</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2554</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-salvaescaleras&quot;   &gt;
        presupuestos de salvaesc ...</errorSourceCode>
        
        <sequenceID>2554_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2554</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-salvaescaleras&quot;   &gt;
        presupuestos de salvaesc ...</errorSourceCode>
        
        <sequenceID>2554_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2554</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-salvaescaleras&quot;   &gt;
        presupuestos de salvaesc ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2560</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-gruas&quot;   &gt;
        presupuestos de gr&uacute;as
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2560_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2560</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-gruas&quot;   &gt;
        presupuestos de gr&uacute;as
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2560_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2560</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-gruas&quot;   &gt;
        presupuestos de gr&uacute;as
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2566</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-bipedestador&quot;   &gt;
        presupuestos de bipedestad ...</errorSourceCode>
        
        <sequenceID>2566_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2566</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-bipedestador&quot;   &gt;
        presupuestos de bipedestad ...</errorSourceCode>
        
        <sequenceID>2566_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2566</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-bipedestador&quot;   &gt;
        presupuestos de bipedestad ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2572</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-rampas&quot;   &gt;
        presupuestos de rampas 
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2572_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2572</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-rampas&quot;   &gt;
        presupuestos de rampas 
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2572_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2572</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-rampas&quot;   &gt;
        presupuestos de rampas 
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2578</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-camas&quot;   &gt;
        presupuestos de camas
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2578_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2578</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-camas&quot;   &gt;
        presupuestos de camas
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2578_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2578</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-camas&quot;   &gt;
        presupuestos de camas
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2584</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-plataforma&quot;   &gt;
        presupuestos de plataformas
 ...</errorSourceCode>
        
        <sequenceID>2584_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2584</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-plataforma&quot;   &gt;
        presupuestos de plataformas
 ...</errorSourceCode>
        
        <sequenceID>2584_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2584</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-plataforma&quot;   &gt;
        presupuestos de plataformas
 ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2591</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=301&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=301'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;The contrast between the colour of text and its background for the element is not sufficient  to meet WCAG2.0 Level AA.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;span style=&quot;color: #FFFFFF;&quot;&gt;PRESUPUESTOS DE ORTOPEDIA&lt;/span&gt;</errorSourceCode>
        <repair>Use a colour contrast evaluator to determine if text and background colours provide a contrast ratio of 4.5:1 for standard text, or 3:1 for larger text. Change colour codes to produce sufficient contrast.

http://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html#visual-audio-contrast-contrast-resources-head</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2594</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-silla-de-ruedas/&quot;   &gt;
        presupuestos sillas de ...</errorSourceCode>
        
        <sequenceID>2594_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2594</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-silla-de-ruedas/&quot;   &gt;
        presupuestos sillas de ...</errorSourceCode>
        
        <sequenceID>2594_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2594</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-silla-de-ruedas/&quot;   &gt;
        presupuestos sillas de ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2600</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-andador&quot;   &gt;
        presupuestos de andador
    &lt;/a ...</errorSourceCode>
        
        <sequenceID>2600_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2600</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-andador&quot;   &gt;
        presupuestos de andador
    &lt;/a ...</errorSourceCode>
        
        <sequenceID>2600_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2600</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-andador&quot;   &gt;
        presupuestos de andador
    &lt;/a ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2606</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-scooter&quot;   &gt;
        presupuestos scooter electrico
 ...</errorSourceCode>
        
        <sequenceID>2606_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2606</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-scooter&quot;   &gt;
        presupuestos scooter electrico
 ...</errorSourceCode>
        
        <sequenceID>2606_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2606</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.discubre.es/es/presupuesto-scooter&quot;   &gt;
        presupuestos scooter electrico
 ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2616</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=301&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=301'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;The contrast between the colour of text and its background for the element is not sufficient  to meet WCAG2.0 Level AA.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;span style=&quot;color: #FFFFFF;&quot;&gt;AYUDA EN DISCUBRE&lt;/span&gt;</errorSourceCode>
        <repair>Use a colour contrast evaluator to determine if text and background colours provide a contrast ratio of 4.5:1 for standard text, or 3:1 for larger text. Change colour codes to produce sufficient contrast.

http://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html#visual-audio-contrast-contrast-resources-head</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2619</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/que-es-discubre/&quot;   &gt;
        &iquest;Qu&eacute; es Discubre? 
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2619_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2619</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/que-es-discubre/&quot;   &gt;
        &iquest;Qu&eacute; es Discubre? 
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2619_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2619</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/que-es-discubre/&quot;   &gt;
        &iquest;Qu&eacute; es Discubre? 
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2625</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/ayuda/&quot;   &gt;
        Ayuda para elegir un profesional
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2625_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2625</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/ayuda/&quot;   &gt;
        Ayuda para elegir un profesional
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2625_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2625</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/ayuda/&quot;   &gt;
        Ayuda para elegir un profesional
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2631</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/ayuda-para-empresas/&quot;   &gt;
        Ayuda para empresas
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2631_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2631</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/ayuda-para-empresas/&quot;   &gt;
        Ayuda para empresas
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2631_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2631</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/ayuda-para-empresas/&quot;   &gt;
        Ayuda para empresas
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2637</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/preguntas-frecuentes-de-usuarios/&quot;   &gt;
        Preguntas frecuentes de usuarios
    &lt;/a ...</errorSourceCode>
        
        <sequenceID>2637_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2637</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/preguntas-frecuentes-de-usuarios/&quot;   &gt;
        Preguntas frecuentes de usuarios
    &lt;/a ...</errorSourceCode>
        
        <sequenceID>2637_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2637</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/preguntas-frecuentes-de-usuarios/&quot;   &gt;
        Preguntas frecuentes de usuarios
    &lt;/a ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2643</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/articulos/&quot;   &gt;
         Noticias de prensa 
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2643_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2643</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/articulos/&quot;   &gt;
         Noticias de prensa 
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2643_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2643</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/articulos/&quot;   &gt;
         Noticias de prensa 
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2649</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/partners/&quot;   &gt;
        Colaboradores
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2649_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2649</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/partners/&quot;   &gt;
        Colaboradores
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2649_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2649</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/partners/&quot;   &gt;
        Colaboradores
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2655</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/contacto/&quot;   &gt;
        Contacta con nosotros
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2655_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2655</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/contacto/&quot;   &gt;
        Contacta con nosotros
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2655_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2655</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/contacto/&quot;   &gt;
        Contacta con nosotros
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2661</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/cookies/&quot; rel=&quot;nofollow&quot;  &gt;
        Pol&iacute;tica de cookies 
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2661_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2661</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/cookies/&quot; rel=&quot;nofollow&quot;  &gt;
        Pol&iacute;tica de cookies 
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2661_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2661</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/cookies/&quot; rel=&quot;nofollow&quot;  &gt;
        Pol&iacute;tica de cookies 
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2667</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/politica-de-ley-privacidad/&quot; rel=&quot;nofollow&quot;  &gt;
        Pol&iacute;tica de privacidad 
    &lt;/a ...</errorSourceCode>
        
        <sequenceID>2667_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2667</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/politica-de-ley-privacidad/&quot; rel=&quot;nofollow&quot;  &gt;
        Pol&iacute;tica de privacidad 
    &lt;/a ...</errorSourceCode>
        
        <sequenceID>2667_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2667</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/politica-de-ley-privacidad/&quot; rel=&quot;nofollow&quot;  &gt;
        Pol&iacute;tica de privacidad 
    &lt;/a ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2673</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/legal-condiciones/&quot; rel=&quot;nofollow&quot;  &gt;
        Nota Legal y Condiciones
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2673_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2673</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/legal-condiciones/&quot; rel=&quot;nofollow&quot;  &gt;
        Nota Legal y Condiciones
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2673_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2673</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/legal-condiciones/&quot; rel=&quot;nofollow&quot;  &gt;
        Nota Legal y Condiciones
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2680</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a&gt;&lt;span style=&quot;color: #FFFFFF;&quot;&gt;PREGUNTAS Y RESPUESTAS &lt;/span&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2680_35_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2680</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a&gt;&lt;span style=&quot;color: #FFFFFF;&quot;&gt;PREGUNTAS Y RESPUESTAS &lt;/span&gt;&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2680</lineNum>
      <columnNum>38</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=301&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=301'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;The contrast between the colour of text and its background for the element is not sufficient  to meet WCAG2.0 Level AA.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;span style=&quot;color: #FFFFFF;&quot;&gt;PREGUNTAS Y RESPUESTAS &lt;/span&gt;</errorSourceCode>
        <repair>Use a colour contrast evaluator to determine if text and background colours provide a contrast ratio of 4.5:1 for standard text, or 3:1 for larger text. Change colour codes to produce sufficient contrast.

http://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html#visual-audio-contrast-contrast-resources-head</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2685</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/scooters-electricos&quot;   &gt;
        Scooters El&eacute;ctricos
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2685_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2685</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/scooters-electricos&quot;   &gt;
        Scooters El&eacute;ctricos
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2685_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2685</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/scooters-electricos&quot;   &gt;
        Scooters El&eacute;ctricos
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2691</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/camas-articuladas&quot;   &gt;
        Camas articuladas 
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2691_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2691</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/camas-articuladas&quot;   &gt;
        Camas articuladas 
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2691_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2691</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/camas-articuladas&quot;   &gt;
        Camas articuladas 
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2697</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/adaptacion-coches&quot;   &gt;
        Adaptaci&oacute;n de coches
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2697_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2697</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/adaptacion-coches&quot;   &gt;
        Adaptaci&oacute;n de coches
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2697_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2697</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/adaptacion-coches&quot;   &gt;
        Adaptaci&oacute;n de coches
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2703</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/salvaescaleras&quot;   &gt;
        Salvaescaleras
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2703_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2703</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/salvaescaleras&quot;   &gt;
        Salvaescaleras
    &lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2703_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2703</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;/es/salvaescaleras&quot;   &gt;
        Salvaescaleras
    &lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2710</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a style=&quot;font-size: 20px; text-align: center;&quot; class=&quot;btn btn-warning btn-lg pide-presupuesto&quot; href ...</errorSourceCode>
        
        <sequenceID>2710_35_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2710</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a style=&quot;font-size: 20px; text-align: center;&quot; class=&quot;btn btn-warning btn-lg pide-presupuesto&quot; href ...</errorSourceCode>
        
        <sequenceID>2710_35_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2710</lineNum>
      <columnNum>35</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a style=&quot;font-size: 20px; text-align: center;&quot; class=&quot;btn btn-warning btn-lg pide-presupuesto&quot; href ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2710</lineNum>
      <columnNum>186</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=301&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=301'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;The contrast between the colour of text and its background for the element is not sufficient  to meet WCAG2.0 Level AA.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;span style=&quot;text-align: center;&quot;&gt;P&Iacute;DENOS PRESUPUESTO&lt;/span&gt;</errorSourceCode>
        <repair>Use a colour contrast evaluator to determine if text and background colours provide a contrast ratio of 4.5:1 for standard text, or 3:1 for larger text. Change colour codes to produce sufficient contrast.

http://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html#visual-audio-contrast-contrast-resources-head</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2723</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;&quot; src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc ...</errorSourceCode>
        
        <sequenceID>2723_13_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2723</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=178&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=178'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text does not convey the same information as the image.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;&quot; src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc ...</errorSourceCode>
        
        <sequenceID>2723_13_178</sequenceID>
        <decisionPass>Alt text conveys the same information as the image.</decisionPass>
        <decisionFail>Alt text does not convey the same information as the image.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2723</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;&quot; src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc ...</errorSourceCode>
        
        <sequenceID>2723_13_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2723</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;&quot; src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc ...</errorSourceCode>
        
        <sequenceID>2723_13_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2723</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot;picturebanner&quot; alt=&quot;&quot; src=&quot;https://cs-static-pro.s3.amazonaws.com:443/cobi%2Fmedia%2Fdsc ...</errorSourceCode>
        
        <sequenceID>2723_13_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2771</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://www.facebook.com/discubre/?fref=ts&#039;); return ...</errorSourceCode>
        
        <sequenceID>2771_13_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2771</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://www.facebook.com/discubre/?fref=ts&#039;); return ...</errorSourceCode>
        
        <sequenceID>2771_13_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2771</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://www.facebook.com/discubre/?fref=ts&#039;); return ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2771</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://www.facebook.com/discubre/?fref=ts&#039;); return ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2773</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;fa fa-facebook&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2779</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://twitter.com/discubre&#039;); return false;&quot;  targ ...</errorSourceCode>
        
        <sequenceID>2779_13_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2779</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://twitter.com/discubre&#039;); return false;&quot;  targ ...</errorSourceCode>
        
        <sequenceID>2779_13_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2779</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://twitter.com/discubre&#039;); return false;&quot;  targ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2779</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://twitter.com/discubre&#039;); return false;&quot;  targ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2781</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;fa fa-twitter&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2787</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://plus.google.com/+DiscubreComparadordeproduct ...</errorSourceCode>
        
        <sequenceID>2787_13_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2787</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://plus.google.com/+DiscubreComparadordeproduct ...</errorSourceCode>
        
        <sequenceID>2787_13_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2787</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://plus.google.com/+DiscubreComparadordeproduct ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2787</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://plus.google.com/+DiscubreComparadordeproduct ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2789</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;fa fa-google-plus&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2795</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://www.linkedin.com/company/discubre&#039;); return  ...</errorSourceCode>
        
        <sequenceID>2795_13_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2795</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://www.linkedin.com/company/discubre&#039;); return  ...</errorSourceCode>
        
        <sequenceID>2795_13_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2795</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://www.linkedin.com/company/discubre&#039;); return  ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2795</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://www.linkedin.com/company/discubre&#039;); return  ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2797</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;fa fa-linkedin&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2803</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://www.instagram.com/actitudiscubre/&#039;); return  ...</errorSourceCode>
        
        <sequenceID>2803_13_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2803</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://www.instagram.com/actitudiscubre/&#039;); return  ...</errorSourceCode>
        
        <sequenceID>2803_13_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2803</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://www.instagram.com/actitudiscubre/&#039;); return  ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2803</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://www.instagram.com/actitudiscubre/&#039;); return  ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2805</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;fa fa-instagram&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2811</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://www.youtube.com/channel/UCP2meM1b7t8CsMi08YF ...</errorSourceCode>
        
        <sequenceID>2811_13_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2811</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://www.youtube.com/channel/UCP2meM1b7t8CsMi08YF ...</errorSourceCode>
        
        <sequenceID>2811_13_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2811</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://www.youtube.com/channel/UCP2meM1b7t8CsMi08YF ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2811</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; class=&quot;link&quot;  onclick=&quot;window.open(&#039;https://www.youtube.com/channel/UCP2meM1b7t8CsMi08YF ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2813</lineNum>
      <columnNum>21</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;fa fa-youtube&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2861</lineNum>
      <columnNum>31</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=301&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=301'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;The contrast between the colour of text and its background for the element is not sufficient  to meet WCAG2.0 Level AA.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;span style=&quot;font-family: &#039;Open Sans&#039;, sans-serif; font-size: 14px; line-height: 18px; background-co ...</errorSourceCode>
        <repair>Use a colour contrast evaluator to determine if text and background colours provide a contrast ratio of 4.5:1 for standard text, or 3:1 for larger text. Change colour codes to produce sufficient contrast.

http://www.w3.org/TR/UNDERSTANDING-WCAG20/visual-audio-contrast-contrast.html#visual-audio-contrast-contrast-resources-head</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2885</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery-ui/1.11.1/jquery-ui.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2885_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2885</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery-ui/1.11.1/jquery-ui.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2885_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2885</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery-ui/1.11.1/jquery-ui.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2885_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2885</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery-ui/1.11.1/jquery-ui.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2885</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery-ui/1.11.1/jquery-ui.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2885_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2885</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery-ui/1.11.1/jquery-ui.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2885_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2886</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery-validate/jquery.validate.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2886_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2886</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery-validate/jquery.validate.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2886_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2886</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery-validate/jquery.validate.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2886_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2886</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery-validate/jquery.validate.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2886</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery-validate/jquery.validate.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2886_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2886</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery-validate/jquery.validate.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2886_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2887</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery-validate/additional-methods.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2887_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2887</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery-validate/additional-methods.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2887_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2887</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery-validate/additional-methods.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2887_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2887</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery-validate/additional-methods.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2887</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery-validate/additional-methods.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2887_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2887</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/jquery-validate/additional-methods.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2887_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2888</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/fancybox/2.1.5/jquery.fancybox.pack.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2888_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2888</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/fancybox/2.1.5/jquery.fancybox.pack.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2888_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2888</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/fancybox/2.1.5/jquery.fancybox.pack.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2888_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2888</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/fancybox/2.1.5/jquery.fancybox.pack.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2888</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/fancybox/2.1.5/jquery.fancybox.pack.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2888_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2888</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/fancybox/2.1.5/jquery.fancybox.pack.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2888_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2889</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/bootstrap/3.1.1/js/bootstrap.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2889_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2889</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/bootstrap/3.1.1/js/bootstrap.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2889_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2889</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/bootstrap/3.1.1/js/bootstrap.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2889_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2889</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/bootstrap/3.1.1/js/bootstrap.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2889</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/bootstrap/3.1.1/js/bootstrap.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2889_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2889</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/bootstrap/3.1.1/js/bootstrap.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2889_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2891</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/js/jquery.cookie.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2891_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2891</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/js/jquery.cookie.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2891_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2891</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/js/jquery.cookie.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2891_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2891</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/js/jquery.cookie.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2891</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/js/jquery.cookie.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2891_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2891</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/js/jquery.cookie.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2891_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2892</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/js/jquery.tinysort.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2892_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2892</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/js/jquery.tinysort.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2892_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2892</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/js/jquery.tinysort.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2892_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2892</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/js/jquery.tinysort.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2892</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/js/jquery.tinysort.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2892_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2892</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/js/jquery.tinysort.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2892_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2893</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/tooltipster/3.3.0/jquery.tooltipster.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2893_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2893</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/tooltipster/3.3.0/jquery.tooltipster.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2893_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2893</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/tooltipster/3.3.0/jquery.tooltipster.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2893_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2893</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/tooltipster/3.3.0/jquery.tooltipster.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2893</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/tooltipster/3.3.0/jquery.tooltipster.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2893_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2893</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/tooltipster/3.3.0/jquery.tooltipster.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2893_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2896</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
            $(document).ready(function(){
                $(&#039;#carousel_18&#039;).carousel({
     ...</errorSourceCode>
        
        <sequenceID>2896_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2896</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
            $(document).ready(function(){
                $(&#039;#carousel_18&#039;).carousel({
     ...</errorSourceCode>
        
        <sequenceID>2896_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2896</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
            $(document).ready(function(){
                $(&#039;#carousel_18&#039;).carousel({
     ...</errorSourceCode>
        
        <sequenceID>2896_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2896</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
            $(document).ready(function(){
                $(&#039;#carousel_18&#039;).carousel({
     ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2896</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
            $(document).ready(function(){
                $(&#039;#carousel_18&#039;).carousel({
     ...</errorSourceCode>
        
        <sequenceID>2896_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2896</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
            $(document).ready(function(){
                $(&#039;#carousel_18&#039;).carousel({
     ...</errorSourceCode>
        
        <sequenceID>2896_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2905</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        $(document).ready(function(){
            $(window).on(&quot;resize&quot;, function () {
     ...</errorSourceCode>
        
        <sequenceID>2905_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2905</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        $(document).ready(function(){
            $(window).on(&quot;resize&quot;, function () {
     ...</errorSourceCode>
        
        <sequenceID>2905_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2905</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        $(document).ready(function(){
            $(window).on(&quot;resize&quot;, function () {
     ...</errorSourceCode>
        
        <sequenceID>2905_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2905</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        $(document).ready(function(){
            $(window).on(&quot;resize&quot;, function () {
     ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2905</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        $(document).ready(function(){
            $(window).on(&quot;resize&quot;, function () {
     ...</errorSourceCode>
        
        <sequenceID>2905_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2905</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        $(document).ready(function(){
            $(window).on(&quot;resize&quot;, function () {
     ...</errorSourceCode>
        
        <sequenceID>2905_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2923</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
            $(document).ready(function(){
                $(&#039;#carousel_30&#039;).carousel({
     ...</errorSourceCode>
        
        <sequenceID>2923_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2923</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
            $(document).ready(function(){
                $(&#039;#carousel_30&#039;).carousel({
     ...</errorSourceCode>
        
        <sequenceID>2923_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2923</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
            $(document).ready(function(){
                $(&#039;#carousel_30&#039;).carousel({
     ...</errorSourceCode>
        
        <sequenceID>2923_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2923</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
            $(document).ready(function(){
                $(&#039;#carousel_30&#039;).carousel({
     ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2923</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
            $(document).ready(function(){
                $(&#039;#carousel_30&#039;).carousel({
     ...</errorSourceCode>
        
        <sequenceID>2923_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2923</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
            $(document).ready(function(){
                $(&#039;#carousel_30&#039;).carousel({
     ...</errorSourceCode>
        
        <sequenceID>2923_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2932</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        $(document).ready(function(){
            $(window).on(&quot;resize&quot;, function () {
     ...</errorSourceCode>
        
        <sequenceID>2932_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2932</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        $(document).ready(function(){
            $(window).on(&quot;resize&quot;, function () {
     ...</errorSourceCode>
        
        <sequenceID>2932_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2932</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        $(document).ready(function(){
            $(window).on(&quot;resize&quot;, function () {
     ...</errorSourceCode>
        
        <sequenceID>2932_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2932</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        $(document).ready(function(){
            $(window).on(&quot;resize&quot;, function () {
     ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2932</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        $(document).ready(function(){
            $(window).on(&quot;resize&quot;, function () {
     ...</errorSourceCode>
        
        <sequenceID>2932_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2932</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        $(document).ready(function(){
            $(window).on(&quot;resize&quot;, function () {
     ...</errorSourceCode>
        
        <sequenceID>2932_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2950</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
            $(document).ready(function(){
                var parameter = getUrlVars()[&#039;ema ...</errorSourceCode>
        
        <sequenceID>2950_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2950</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
            $(document).ready(function(){
                var parameter = getUrlVars()[&#039;ema ...</errorSourceCode>
        
        <sequenceID>2950_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2950</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
            $(document).ready(function(){
                var parameter = getUrlVars()[&#039;ema ...</errorSourceCode>
        
        <sequenceID>2950_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2950</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
            $(document).ready(function(){
                var parameter = getUrlVars()[&#039;ema ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2950</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
            $(document).ready(function(){
                var parameter = getUrlVars()[&#039;ema ...</errorSourceCode>
        
        <sequenceID>2950_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2950</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
            $(document).ready(function(){
                var parameter = getUrlVars()[&#039;ema ...</errorSourceCode>
        
        <sequenceID>2950_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2960</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/cs16/js/ajaxForm.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2960_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2960</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/cs16/js/ajaxForm.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2960_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2960</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/cs16/js/ajaxForm.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2960_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2960</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/cs16/js/ajaxForm.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2960</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/cs16/js/ajaxForm.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2960_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2960</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/cs16/js/ajaxForm.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2960_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2961</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;https://maps.googleapis.com/maps/api/js?key=AIzaSyA7VYQFO3-dL2iYrkWsVjKq66yf9soqhxY&quot;&gt;&lt;/ ...</errorSourceCode>
        
        <sequenceID>2961_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2961</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;https://maps.googleapis.com/maps/api/js?key=AIzaSyA7VYQFO3-dL2iYrkWsVjKq66yf9soqhxY&quot;&gt;&lt;/ ...</errorSourceCode>
        
        <sequenceID>2961_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2961</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;https://maps.googleapis.com/maps/api/js?key=AIzaSyA7VYQFO3-dL2iYrkWsVjKq66yf9soqhxY&quot;&gt;&lt;/ ...</errorSourceCode>
        
        <sequenceID>2961_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2961</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;https://maps.googleapis.com/maps/api/js?key=AIzaSyA7VYQFO3-dL2iYrkWsVjKq66yf9soqhxY&quot;&gt;&lt;/ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2961</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;https://maps.googleapis.com/maps/api/js?key=AIzaSyA7VYQFO3-dL2iYrkWsVjKq66yf9soqhxY&quot;&gt;&lt;/ ...</errorSourceCode>
        
        <sequenceID>2961_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2961</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;https://maps.googleapis.com/maps/api/js?key=AIzaSyA7VYQFO3-dL2iYrkWsVjKq66yf9soqhxY&quot;&gt;&lt;/ ...</errorSourceCode>
        
        <sequenceID>2961_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2962</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/cs12/js/fileinput.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2962_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2962</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/cs12/js/fileinput.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2962_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2962</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/cs12/js/fileinput.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2962_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2962</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/cs12/js/fileinput.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2962</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/cs12/js/fileinput.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2962_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2962</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/cs12/js/fileinput.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2962_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2963</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&#039;https://www.google.com/recaptcha/api.js?v=1.1&#039;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2963_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2963</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&#039;https://www.google.com/recaptcha/api.js?v=1.1&#039;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2963_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2963</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&#039;https://www.google.com/recaptcha/api.js?v=1.1&#039;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2963_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2963</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&#039;https://www.google.com/recaptcha/api.js?v=1.1&#039;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2963</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&#039;https://www.google.com/recaptcha/api.js?v=1.1&#039;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2963_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2963</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&#039;https://www.google.com/recaptcha/api.js?v=1.1&#039;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2963_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2964</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/cs17/js/discubre.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2964_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2964</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/cs17/js/discubre.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2964_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2964</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/cs17/js/discubre.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2964_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2964</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/cs17/js/discubre.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2964</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/cs17/js/discubre.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2964_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2964</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;/static/cs17/js/discubre.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2964_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2967</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
            /* &lt;![CDATA[ */
            var google_conversion_id = 9 ...</errorSourceCode>
        
        <sequenceID>2967_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2967</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
            /* &lt;![CDATA[ */
            var google_conversion_id = 9 ...</errorSourceCode>
        
        <sequenceID>2967_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2967</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
            /* &lt;![CDATA[ */
            var google_conversion_id = 9 ...</errorSourceCode>
        
        <sequenceID>2967_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2967</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
            /* &lt;![CDATA[ */
            var google_conversion_id = 9 ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2967</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
            /* &lt;![CDATA[ */
            var google_conversion_id = 9 ...</errorSourceCode>
        
        <sequenceID>2967_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2967</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
            /* &lt;![CDATA[ */
            var google_conversion_id = 9 ...</errorSourceCode>
        
        <sequenceID>2967_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2974</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;//www.googleadservices.com/pagead/conversion.js&quot;&gt;
        
&lt;/scr ...</errorSourceCode>
        
        <sequenceID>2974_9_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2974</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;//www.googleadservices.com/pagead/conversion.js&quot;&gt;
        
&lt;/scr ...</errorSourceCode>
        
        <sequenceID>2974_9_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2974</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;//www.googleadservices.com/pagead/conversion.js&quot;&gt;
        
&lt;/scr ...</errorSourceCode>
        
        <sequenceID>2974_9_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2974</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;//www.googleadservices.com/pagead/conversion.js&quot;&gt;
        
&lt;/scr ...</errorSourceCode>
        
        <sequenceID>2974_9_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2974</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;//www.googleadservices.com/pagead/conversion.js&quot;&gt;
        
&lt;/scr ...</errorSourceCode>
        
        <sequenceID>2974_9_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2978</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img height=&quot;1&quot; width=&quot;1&quot; style=&quot;border-style:none;&quot; alt=&quot;&quot; src=&quot;//googleads.g.doubleclick.net/pagea ...</errorSourceCode>
        
        <sequenceID>2978_17_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2978</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=178&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=178'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text does not convey the same information as the image.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img height=&quot;1&quot; width=&quot;1&quot; style=&quot;border-style:none;&quot; alt=&quot;&quot; src=&quot;//googleads.g.doubleclick.net/pagea ...</errorSourceCode>
        
        <sequenceID>2978_17_178</sequenceID>
        <decisionPass>Alt text conveys the same information as the image.</decisionPass>
        <decisionFail>Alt text does not convey the same information as the image.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2978</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img height=&quot;1&quot; width=&quot;1&quot; style=&quot;border-style:none;&quot; alt=&quot;&quot; src=&quot;//googleads.g.doubleclick.net/pagea ...</errorSourceCode>
        
        <sequenceID>2978_17_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2978</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img height=&quot;1&quot; width=&quot;1&quot; style=&quot;border-style:none;&quot; alt=&quot;&quot; src=&quot;//googleads.g.doubleclick.net/pagea ...</errorSourceCode>
        
        <sequenceID>2978_17_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2978</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img height=&quot;1&quot; width=&quot;1&quot; style=&quot;border-style:none;&quot; alt=&quot;&quot; src=&quot;//googleads.g.doubleclick.net/pagea ...</errorSourceCode>
        
        <sequenceID>2978_17_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2988</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/cookielaw/js/cookielaw.js&quot; type=&quot;text/javascript&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2988_5_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2988</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/cookielaw/js/cookielaw.js&quot; type=&quot;text/javascript&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2988_5_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>2988</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/cookielaw/js/cookielaw.js&quot; type=&quot;text/javascript&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2988_5_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2988</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/cookielaw/js/cookielaw.js&quot; type=&quot;text/javascript&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2988</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/cookielaw/js/cookielaw.js&quot; type=&quot;text/javascript&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2988_5_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2988</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/cookielaw/js/cookielaw.js&quot; type=&quot;text/javascript&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>2988_5_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2993</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-primary pull-right&quot; href=&quot;javascript:Cookielaw.createCookielawCookie();&quot;&gt;Aceptar&lt;/ ...</errorSourceCode>
        
        <sequenceID>2993_13_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2993</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-primary pull-right&quot; href=&quot;javascript:Cookielaw.createCookielawCookie();&quot;&gt;Aceptar&lt;/ ...</errorSourceCode>
        
        <sequenceID>2993_13_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2993</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;btn btn-primary pull-right&quot; href=&quot;javascript:Cookielaw.createCookielawCookie();&quot;&gt;Aceptar&lt;/ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2994</lineNum>
      <columnNum>280</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.google.com/search?q=borrar+cookies+navegador&quot;&gt;Google&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2994_280_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>2994</lineNum>
      <columnNum>280</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.google.com/search?q=borrar+cookies+navegador&quot;&gt;Google&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>2994_280_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>2994</lineNum>
      <columnNum>280</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.google.com/search?q=borrar+cookies+navegador&quot;&gt;Google&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3012</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
        var $buoop = {};
        $buoop.ol = window.onload;
         ...</errorSourceCode>
        
        <sequenceID>3012_5_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3012</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
        var $buoop = {};
        $buoop.ol = window.onload;
         ...</errorSourceCode>
        
        <sequenceID>3012_5_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>3012</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
        var $buoop = {};
        $buoop.ol = window.onload;
         ...</errorSourceCode>
        
        <sequenceID>3012_5_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>3012</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
        var $buoop = {};
        $buoop.ol = window.onload;
         ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3012</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
        var $buoop = {};
        $buoop.ol = window.onload;
         ...</errorSourceCode>
        
        <sequenceID>3012_5_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3012</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
        var $buoop = {};
        $buoop.ol = window.onload;
         ...</errorSourceCode>
        
        <sequenceID>3012_5_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3052</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/js/ie_patches/jquery.h5validate.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3052_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3052</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/js/ie_patches/jquery.h5validate.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3052_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>3052</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/js/ie_patches/jquery.h5validate.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3052_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>3052</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/js/ie_patches/jquery.h5validate.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3052</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/js/ie_patches/jquery.h5validate.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3052_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3052</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/js/ie_patches/jquery.h5validate.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3052_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3053</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
    $(document).ready(function () {
        $(&#039;form&#039;).h5Validate();
    });




&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3053_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3053</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
    $(document).ready(function () {
        $(&#039;form&#039;).h5Validate();
    });




&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3053_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>3053</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
    $(document).ready(function () {
        $(&#039;form&#039;).h5Validate();
    });




&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3053_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>3053</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
    $(document).ready(function () {
        $(&#039;form&#039;).h5Validate();
    });




&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3053</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
    $(document).ready(function () {
        $(&#039;form&#039;).h5Validate();
    });




&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3053_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3053</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
    $(document).ready(function () {
        $(&#039;form&#039;).h5Validate();
    });




&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3053_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3059</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/js/ie_patches/jquery.placeholder.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3059_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3059</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/js/ie_patches/jquery.placeholder.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3059_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>3059</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/js/ie_patches/jquery.placeholder.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3059_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>3059</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/js/ie_patches/jquery.placeholder.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3059</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/js/ie_patches/jquery.placeholder.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3059_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3059</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/js/ie_patches/jquery.placeholder.min.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3059_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3060</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;$(&#039;input, textarea&#039;).placeholder();&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3060_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3060</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;$(&#039;input, textarea&#039;).placeholder();&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3060_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>3060</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;$(&#039;input, textarea&#039;).placeholder();&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3060_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>3060</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;$(&#039;input, textarea&#039;).placeholder();&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3060</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;$(&#039;input, textarea&#039;).placeholder();&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3060_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3060</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;$(&#039;input, textarea&#039;).placeholder();&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3060_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3061</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/js/jquery-ui/ui/i18n/datepicker-es.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3061_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3061</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/js/jquery-ui/ui/i18n/datepicker-es.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3061_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>3061</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/js/jquery-ui/ui/i18n/datepicker-es.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3061_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>3061</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/js/jquery-ui/ui/i18n/datepicker-es.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3061</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/js/jquery-ui/ui/i18n/datepicker-es.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3061_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3061</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script src=&quot;/static/js/jquery-ui/ui/i18n/datepicker-es.js&quot;&gt;&lt;/script&gt;</errorSourceCode>
        
        <sequenceID>3061_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3062</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
    /* ------------------------ */
    /* Generic Slide Plugin */
    /* ------------------ ...</errorSourceCode>
        
        <sequenceID>3062_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3062</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
    /* ------------------------ */
    /* Generic Slide Plugin */
    /* ------------------ ...</errorSourceCode>
        
        <sequenceID>3062_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>3062</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
    /* ------------------------ */
    /* Generic Slide Plugin */
    /* ------------------ ...</errorSourceCode>
        
        <sequenceID>3062_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>3062</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
    /* ------------------------ */
    /* Generic Slide Plugin */
    /* ------------------ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3062</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
    /* ------------------------ */
    /* Generic Slide Plugin */
    /* ------------------ ...</errorSourceCode>
        
        <sequenceID>3062_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3062</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
    /* ------------------------ */
    /* Generic Slide Plugin */
    /* ------------------ ...</errorSourceCode>
        
        <sequenceID>3062_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 

  </results>
</resultset>
