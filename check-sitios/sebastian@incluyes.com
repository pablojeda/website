<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE resultset[
<!ELEMENT resultset (summary,results)>
<!ELEMENT summary (status,sessionID,NumOfErrors,NumOfLikelyProblems,NumOfPotentialProblems,guidelines)>
<!ELEMENT status (#PCDATA)>
<!ELEMENT sessionID (#PCDATA)>
<!ELEMENT NumOfErrors (#PCDATA)>
<!ELEMENT NumOfLikelyProblems (#PCDATA)>
<!ELEMENT NumOfPotentialProblems (#PCDATA)>
<!ELEMENT guidelines (guideline)*>
<!ELEMENT guideline (#PCDATA)>
<!ELEMENT results (result)*>
<!ELEMENT result (resultType,lineNum,columnNum,errorMsg,errorSourceCode,repair*,sequenceID*,decisionPass*,decisionFail*,decisionMade*,decisionMadeDate*)>
<!ELEMENT resultType (#PCDATA)>
<!ELEMENT lineNum (#PCDATA)>
<!ELEMENT columnNum (#PCDATA)>
<!ELEMENT errorMsg (#PCDATA)>
<!ELEMENT errorSourceCode (#PCDATA)>
<!ELEMENT repair (#PCDATA)>
<!ELEMENT sequenceID (#PCDATA)>
<!ELEMENT decisionPass (#PCDATA)>
<!ELEMENT decisionFail (#PCDATA)>
<!ELEMENT decisionMade (#PCDATA)>
<!ELEMENT decisionMadeDate (#PCDATA)>
<!ENTITY lt "&#38;#60;">
<!ENTITY gt "&#62;">
<!ENTITY amp "&#38;#38;">
<!ENTITY apos "&#39;">
<!ENTITY quot "&#34;">
<!ENTITY ndash "&#8211;">
]>
<resultset>
  <summary>
    <status>FAIL</status>
    <sessionID>bf3f813990e465fa628a29d86d4ca836ae6c7c94</sessionID>
    <NumOfErrors>117</NumOfErrors>
    <NumOfLikelyProblems>64</NumOfLikelyProblems>
    <NumOfPotentialProblems>471</NumOfPotentialProblems>

    <guidelines>
      <guideline>Stanca Act</guideline>
      <guideline>WCAG 2.0 (Level AA)</guideline>

    </guidelines>
  </summary>

  <results>
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;var et_site_url=&#039;http://arcangeles.org&#039;;var et_post_id=&#039;623&#039;;function et_core_page_resource_ ...</errorSourceCode>
        
        <sequenceID>3_3_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>3</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;var et_site_url=&#039;http://arcangeles.org&#039;;var et_post_id=&#039;623&#039;;function et_core_page_resource_ ...</errorSourceCode>
        
        <sequenceID>3_3_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;var et_site_url=&#039;http://arcangeles.org&#039;;var et_post_id=&#039;623&#039;;function et_core_page_resource_ ...</errorSourceCode>
        
        <sequenceID>3_3_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;var et_site_url=&#039;http://arcangeles.org&#039;;var et_post_id=&#039;623&#039;;function et_core_page_resource_ ...</errorSourceCode>
        
        <sequenceID>3_3_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>3</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;var et_site_url=&#039;http://arcangeles.org&#039;;var et_post_id=&#039;623&#039;;function et_core_page_resource_ ...</errorSourceCode>
        
        <sequenceID>3_3_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>4</lineNum>
      <columnNum>10</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=54&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=54'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;title&lt;/code&gt; might not describe the document.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;title&gt;Inicio - Arcangeles&lt;/title&gt;</errorSourceCode>
        
        <sequenceID>4_10_54</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the document.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the document.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>17</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;application/ld+json&#039;&gt;{&quot;@context&quot;:&quot;https://schema.org&quot;,&quot;@type&quot;:&quot;WebSite&quot;,&quot;@id&quot;:&quot;http:// ...</errorSourceCode>
        
        <sequenceID>17_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>17</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;application/ld+json&#039;&gt;{&quot;@context&quot;:&quot;https://schema.org&quot;,&quot;@type&quot;:&quot;WebSite&quot;,&quot;@id&quot;:&quot;http:// ...</errorSourceCode>
        
        <sequenceID>17_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>17</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;application/ld+json&#039;&gt;{&quot;@context&quot;:&quot;https://schema.org&quot;,&quot;@type&quot;:&quot;WebSite&quot;,&quot;@id&quot;:&quot;http:// ...</errorSourceCode>
        
        <sequenceID>17_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>17</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;application/ld+json&#039;&gt;{&quot;@context&quot;:&quot;https://schema.org&quot;,&quot;@type&quot;:&quot;WebSite&quot;,&quot;@id&quot;:&quot;http:// ...</errorSourceCode>
        
        <sequenceID>17_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>17</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;application/ld+json&#039;&gt;{&quot;@context&quot;:&quot;https://schema.org&quot;,&quot;@type&quot;:&quot;WebSite&quot;,&quot;@id&quot;:&quot;http:// ...</errorSourceCode>
        
        <sequenceID>17_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>27</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
			window._wpemojiSettings = {&quot;baseUrl&quot;:&quot;https:\/\/s.w.org\/images\/ ...</errorSourceCode>
        
        <sequenceID>27_3_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>27</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
			window._wpemojiSettings = {&quot;baseUrl&quot;:&quot;https:\/\/s.w.org\/images\/ ...</errorSourceCode>
        
        <sequenceID>27_3_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>27</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
			window._wpemojiSettings = {&quot;baseUrl&quot;:&quot;https:\/\/s.w.org\/images\/ ...</errorSourceCode>
        
        <sequenceID>27_3_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>27</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
			window._wpemojiSettings = {&quot;baseUrl&quot;:&quot;https:\/\/s.w.org\/images\/ ...</errorSourceCode>
        
        <sequenceID>27_3_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>27</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
			window._wpemojiSettings = {&quot;baseUrl&quot;:&quot;https:\/\/s.w.org\/images\/ ...</errorSourceCode>
        
        <sequenceID>27_3_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>69</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-includes/js/jquery/jquery.js?ver=1.12.4 ...</errorSourceCode>
        
        <sequenceID>69_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>69</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-includes/js/jquery/jquery.js?ver=1.12.4 ...</errorSourceCode>
        
        <sequenceID>69_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>69</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-includes/js/jquery/jquery.js?ver=1.12.4 ...</errorSourceCode>
        
        <sequenceID>69_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>69</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-includes/js/jquery/jquery.js?ver=1.12.4 ...</errorSourceCode>
        
        <sequenceID>69_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>69</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-includes/js/jquery/jquery.js?ver=1.12.4 ...</errorSourceCode>
        
        <sequenceID>69_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>70</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-includes/js/jquery/jquery-migrate.min.j ...</errorSourceCode>
        
        <sequenceID>70_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>70</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-includes/js/jquery/jquery-migrate.min.j ...</errorSourceCode>
        
        <sequenceID>70_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>70</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-includes/js/jquery/jquery-migrate.min.j ...</errorSourceCode>
        
        <sequenceID>70_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>70</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-includes/js/jquery/jquery-migrate.min.j ...</errorSourceCode>
        
        <sequenceID>70_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>70</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-includes/js/jquery/jquery-migrate.min.j ...</errorSourceCode>
        
        <sequenceID>70_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>71</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/awesome-logos/js/awesom ...</errorSourceCode>
        
        <sequenceID>71_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>71</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/awesome-logos/js/awesom ...</errorSourceCode>
        
        <sequenceID>71_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>71</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/awesome-logos/js/awesom ...</errorSourceCode>
        
        <sequenceID>71_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>71</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/awesome-logos/js/awesom ...</errorSourceCode>
        
        <sequenceID>71_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>71</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/awesome-logos/js/awesom ...</errorSourceCode>
        
        <sequenceID>71_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>72</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://maps.googleapis.com/maps/api/js?sensor=false&amp;#038;ver=5. ...</errorSourceCode>
        
        <sequenceID>72_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>72</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://maps.googleapis.com/maps/api/js?sensor=false&amp;#038;ver=5. ...</errorSourceCode>
        
        <sequenceID>72_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>72</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://maps.googleapis.com/maps/api/js?sensor=false&amp;#038;ver=5. ...</errorSourceCode>
        
        <sequenceID>72_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>72</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://maps.googleapis.com/maps/api/js?sensor=false&amp;#038;ver=5. ...</errorSourceCode>
        
        <sequenceID>72_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>72</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://maps.googleapis.com/maps/api/js?sensor=false&amp;#038;ver=5. ...</errorSourceCode>
        
        <sequenceID>72_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>73</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/wild-googlemap/wild-goo ...</errorSourceCode>
        
        <sequenceID>73_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>73</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/wild-googlemap/wild-goo ...</errorSourceCode>
        
        <sequenceID>73_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>73</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/wild-googlemap/wild-goo ...</errorSourceCode>
        
        <sequenceID>73_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>73</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/wild-googlemap/wild-goo ...</errorSourceCode>
        
        <sequenceID>73_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>73</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/wild-googlemap/wild-goo ...</errorSourceCode>
        
        <sequenceID>73_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>74</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/wild-googlemap/wild-goo ...</errorSourceCode>
        
        <sequenceID>74_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>74</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/wild-googlemap/wild-goo ...</errorSourceCode>
        
        <sequenceID>74_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>74</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/wild-googlemap/wild-goo ...</errorSourceCode>
        
        <sequenceID>74_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>74</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/wild-googlemap/wild-goo ...</errorSourceCode>
        
        <sequenceID>74_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>74</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/wild-googlemap/wild-goo ...</errorSourceCode>
        
        <sequenceID>74_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>75</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js?ver=1.4 ...</errorSourceCode>
        
        <sequenceID>75_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>75</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js?ver=1.4 ...</errorSourceCode>
        
        <sequenceID>75_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>75</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js?ver=1.4 ...</errorSourceCode>
        
        <sequenceID>75_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>75</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js?ver=1.4 ...</errorSourceCode>
        
        <sequenceID>75_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>75</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js?ver=1.4 ...</errorSourceCode>
        
        <sequenceID>75_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>76</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/revslider/public/assets ...</errorSourceCode>
        
        <sequenceID>76_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>76</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/revslider/public/assets ...</errorSourceCode>
        
        <sequenceID>76_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>76</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/revslider/public/assets ...</errorSourceCode>
        
        <sequenceID>76_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>76</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/revslider/public/assets ...</errorSourceCode>
        
        <sequenceID>76_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>76</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/revslider/public/assets ...</errorSourceCode>
        
        <sequenceID>76_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>77</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/revslider/public/assets ...</errorSourceCode>
        
        <sequenceID>77_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>77</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/revslider/public/assets ...</errorSourceCode>
        
        <sequenceID>77_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>77</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/revslider/public/assets ...</errorSourceCode>
        
        <sequenceID>77_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>77</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/revslider/public/assets ...</errorSourceCode>
        
        <sequenceID>77_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>77</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/revslider/public/assets ...</errorSourceCode>
        
        <sequenceID>77_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>78</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/sliding-social-icons/js ...</errorSourceCode>
        
        <sequenceID>78_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>78</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/sliding-social-icons/js ...</errorSourceCode>
        
        <sequenceID>78_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>78</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/sliding-social-icons/js ...</errorSourceCode>
        
        <sequenceID>78_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>78</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/sliding-social-icons/js ...</errorSourceCode>
        
        <sequenceID>78_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>78</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/sliding-social-icons/js ...</errorSourceCode>
        
        <sequenceID>78_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>725</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
(function(){window.incluTabOptions={incluTabVisible:!0};var i7e_e=d ...</errorSourceCode>
        
        <sequenceID>725_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>725</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
(function(){window.incluTabOptions={incluTabVisible:!0};var i7e_e=d ...</errorSourceCode>
        
        <sequenceID>725_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>725</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
(function(){window.incluTabOptions={incluTabVisible:!0};var i7e_e=d ...</errorSourceCode>
        
        <sequenceID>725_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>725</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
(function(){window.incluTabOptions={incluTabVisible:!0};var i7e_e=d ...</errorSourceCode>
        
        <sequenceID>725_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>725</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
(function(){window.incluTabOptions={incluTabVisible:!0};var i7e_e=d ...</errorSourceCode>
        
        <sequenceID>725_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>732</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;if(typeof WebFont!=&#039;undefined&#039;){WebFont.load({google:{families:[&quot;Roboto:400,400italic,700,70 ...</errorSourceCode>
        
        <sequenceID>732_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>732</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;if(typeof WebFont!=&#039;undefined&#039;){WebFont.load({google:{families:[&quot;Roboto:400,400italic,700,70 ...</errorSourceCode>
        
        <sequenceID>732_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>732</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;if(typeof WebFont!=&#039;undefined&#039;){WebFont.load({google:{families:[&quot;Roboto:400,400italic,700,70 ...</errorSourceCode>
        
        <sequenceID>732_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>732</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;if(typeof WebFont!=&#039;undefined&#039;){WebFont.load({google:{families:[&quot;Roboto:400,400italic,700,70 ...</errorSourceCode>
        
        <sequenceID>732_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>732</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;if(typeof WebFont!=&#039;undefined&#039;){WebFont.load({google:{families:[&quot;Roboto:400,400italic,700,70 ...</errorSourceCode>
        
        <sequenceID>732_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>734</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;http://arcangeles.org/wp-content/plugins/si-captcha-for-wordpres ...</errorSourceCode>
        
        <sequenceID>734_2_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>734</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;http://arcangeles.org/wp-content/plugins/si-captcha-for-wordpres ...</errorSourceCode>
        
        <sequenceID>734_2_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>734</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;http://arcangeles.org/wp-content/plugins/si-captcha-for-wordpres ...</errorSourceCode>
        
        <sequenceID>734_2_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>734</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;http://arcangeles.org/wp-content/plugins/si-captcha-for-wordpres ...</errorSourceCode>
        
        <sequenceID>734_2_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>734</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot; src=&quot;http://arcangeles.org/wp-content/plugins/si-captcha-for-wordpres ...</errorSourceCode>
        
        <sequenceID>734_2_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>759</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;function setREVStartSize(e){
				try{ var i=jQuery(window).width(),t ...</errorSourceCode>
        
        <sequenceID>759_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>759</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;function setREVStartSize(e){
				try{ var i=jQuery(window).width(),t ...</errorSourceCode>
        
        <sequenceID>759_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>759</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;function setREVStartSize(e){
				try{ var i=jQuery(window).width(),t ...</errorSourceCode>
        
        <sequenceID>759_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>759</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;function setREVStartSize(e){
				try{ var i=jQuery(window).width(),t ...</errorSourceCode>
        
        <sequenceID>759_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>759</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;function setREVStartSize(e){
				try{ var i=jQuery(window).width(),t ...</errorSourceCode>
        
        <sequenceID>759_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>766</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
  (function(i,s,o,g,r,a,m){i[&#039;GoogleAnalyticsObject&#039;]=r;i[r]=i[r]||function(){
  (i[r].q= ...</errorSourceCode>
        
        <sequenceID>766_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>766</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
  (function(i,s,o,g,r,a,m){i[&#039;GoogleAnalyticsObject&#039;]=r;i[r]=i[r]||function(){
  (i[r].q= ...</errorSourceCode>
        
        <sequenceID>766_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>766</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
  (function(i,s,o,g,r,a,m){i[&#039;GoogleAnalyticsObject&#039;]=r;i[r]=i[r]||function(){
  (i[r].q= ...</errorSourceCode>
        
        <sequenceID>766_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>766</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
  (function(i,s,o,g,r,a,m){i[&#039;GoogleAnalyticsObject&#039;]=r;i[r]=i[r]||function(){
  (i[r].q= ...</errorSourceCode>
        
        <sequenceID>766_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>766</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
  (function(i,s,o,g,r,a,m){i[&#039;GoogleAnalyticsObject&#039;]=r;i[r]=i[r]||function(){
  (i[r].q= ...</errorSourceCode>
        
        <sequenceID>766_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>994</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=185&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=185'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;id&lt;/code&gt; attribute is not unique.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;time-4-6 home page-template page-template-custom-page page-template-custom-page-php pag ...(vozme_form_afc06142da03f8f3ef34a98958d98efb)</errorSourceCode>
        <repair>Modify the &lt;code&gt;id&lt;/code&gt; attribute value so it is unique.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>994</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=276&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=276'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Repeated components may not appear in the same relative order each time they appear.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;time-4-6 home page-template page-template-custom-page page-template-custom-page-php pag ...(vozme_form_afc06142da03f8f3ef34a98958d98efb)</errorSourceCode>
        
        <sequenceID>994_2_276</sequenceID>
        <decisionPass>Repeated components appear in the same relative order on this page.</decisionPass>
        <decisionFail>Repeated components do not appear in the same relative order on this page.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>777</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=270&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=270'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Unicode right-to-left marks or left-to-right marks may be required.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;time-4-6 home page-template page-template-custom-page page-template-custom-page-php pag ...</errorSourceCode>
        
        <sequenceID>777_2_270</sequenceID>
        <decisionPass>Unicode right-to-left marks or left-to-right marks are used whenever the HTML bidirectional algorithm produces undesirable results.</decisionPass>
        <decisionFail>Unicode right-to-left marks or left-to-right marks are not used whenever the HTML bidirectional algorithm produces undesirable results.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>777</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=250&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=250'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Text may refer to items by shape, size, or relative position alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;time-4-6 home page-template page-template-custom-page page-template-custom-page-php pag ...</errorSourceCode>
        
        <sequenceID>777_2_250</sequenceID>
        <decisionPass>There are no references to items in the document by shape, size, or relative position alone.</decisionPass>
        <decisionFail>There are references to items in the document by shape, size, or relative position alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>777</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=248&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=248'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Visual lists may not be properly marked.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;time-4-6 home page-template page-template-custom-page page-template-custom-page-php pag ...</errorSourceCode>
        
        <sequenceID>777_2_248</sequenceID>
        <decisionPass>All visual lists contain proper markup.</decisionPass>
        <decisionFail>Not all visual lists contain proper markup.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>777</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=262&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=262'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Groups of links with a related purpose are not marked.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;time-4-6 home page-template page-template-custom-page page-template-custom-page-php pag ...</errorSourceCode>
        
        <sequenceID>777_2_262</sequenceID>
        <decisionPass>All groups of links with a related purpose are marked.</decisionPass>
        <decisionFail>All groups of links with a related purpose are not marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>777</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=184&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=184'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Site missing site map.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;time-4-6 home page-template page-template-custom-page page-template-custom-page-php pag ...</errorSourceCode>
        
        <sequenceID>777_2_184</sequenceID>
        <decisionPass>Page is not part of a collection that requires a site map.</decisionPass>
        <decisionFail>Page is part of a collection that requires a site map.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>777</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=28&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=28'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Document may be missing a &quot;skip to content&quot; link.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;time-4-6 home page-template page-template-custom-page page-template-custom-page-php pag ...</errorSourceCode>
        
        <sequenceID>777_2_28</sequenceID>
        <decisionPass>Document contians a &quot;skip to content&quot; link or does not require it.</decisionPass>
        <decisionFail>Document does not contain a &quot;skip to content&quot; link and requires it.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>777</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=241&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=241'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Tabular information may be missing table markup.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;time-4-6 home page-template page-template-custom-page page-template-custom-page-php pag ...</errorSourceCode>
        
        <sequenceID>777_2_241</sequenceID>
        <decisionPass>Table markup is used for all tabular information.</decisionPass>
        <decisionFail>Table markup is not used for all tabular information.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>777</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=271&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=271'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;dir&lt;/code&gt; attribute may be required to identify changes in text direction.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;time-4-6 home page-template page-template-custom-page page-template-custom-page-php pag ...</errorSourceCode>
        
        <sequenceID>777_2_271</sequenceID>
        <decisionPass>All changes in text direction are marked using the &lt;code&gt;dir&lt;/code&gt; attribute.</decisionPass>
        <decisionFail>All changes in text direction are not marked using the &lt;code&gt;dir&lt;/code&gt; attribute.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>777</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=131&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=131'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Long quotations may not be marked using the &lt;code&gt;blockquote&lt;/code&gt; element.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;time-4-6 home page-template page-template-custom-page page-template-custom-page-php pag ...</errorSourceCode>
        
        <sequenceID>777_2_131</sequenceID>
        <decisionPass>All long quotations have been marked with the &lt;code&gt;blockquote&lt;/code&gt; element.</decisionPass>
        <decisionFail>All long quotations are not marked with the &lt;code&gt;blockquote&lt;/code&gt; element.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>789</lineNum>
      <columnNum>230</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/&quot; aria-current=&quot;page&quot;&gt;INICIO&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>789_230_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>789</lineNum>
      <columnNum>230</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/&quot; aria-current=&quot;page&quot;&gt;INICIO&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>789</lineNum>
      <columnNum>230</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/&quot; aria-current=&quot;page&quot;&gt;INICIO&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>789_230_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>790</lineNum>
      <columnNum>126</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/acerca-de-arcangeles/&quot;&gt;LA FUNDACION&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>790_126_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>790</lineNum>
      <columnNum>126</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/acerca-de-arcangeles/&quot;&gt;LA FUNDACION&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>790</lineNum>
      <columnNum>126</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/acerca-de-arcangeles/&quot;&gt;LA FUNDACION&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>790_126_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>792</lineNum>
      <columnNum>106</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/salud-ips-rehabilitacion/&quot;&gt;SALUD&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>792_106_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>792</lineNum>
      <columnNum>106</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/salud-ips-rehabilitacion/&quot;&gt;SALUD&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>792</lineNum>
      <columnNum>106</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/salud-ips-rehabilitacion/&quot;&gt;SALUD&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>792_106_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>793</lineNum>
      <columnNum>106</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/programas-y-proyectos/&quot;&gt;PROGRAMAS Y PROYECTOS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>793_106_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>793</lineNum>
      <columnNum>106</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/programas-y-proyectos/&quot;&gt;PROGRAMAS Y PROYECTOS&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>793</lineNum>
      <columnNum>106</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/programas-y-proyectos/&quot;&gt;PROGRAMAS Y PROYECTOS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>793_106_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>796</lineNum>
      <columnNum>108</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/noticias/&quot;&gt;NOTICIAS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>796_108_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>796</lineNum>
      <columnNum>108</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/noticias/&quot;&gt;NOTICIAS&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>796</lineNum>
      <columnNum>108</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/noticias/&quot;&gt;NOTICIAS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>796_108_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>797</lineNum>
      <columnNum>103</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/contactenos/&quot;&gt;CONTACTENOS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>797_103_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>797</lineNum>
      <columnNum>103</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/contactenos/&quot;&gt;CONTACTENOS&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>797</lineNum>
      <columnNum>103</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/contactenos/&quot;&gt;CONTACTENOS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>797_103_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>801</lineNum>
      <columnNum>8</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=269&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=269'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission data may not be presented to the user before final acceptance of an irreversable transaction.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form method=&quot;get&quot; action=&quot;http://arcangeles.org/&quot; class=&quot;search&quot; role=&quot;search&quot;&gt;&lt;input type=&quot;text&quot; n ...</errorSourceCode>
        
        <sequenceID>801_8_269</sequenceID>
        <decisionPass>All form submission data is presented to the user before final acceptance for all irreversable transactions.</decisionPass>
        <decisionFail>All form submission data is not presented to the user before final acceptance for all irreversable transactions.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>801</lineNum>
      <columnNum>8</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=265&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=265'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Tab order may not follow logical order.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form method=&quot;get&quot; action=&quot;http://arcangeles.org/&quot; class=&quot;search&quot; role=&quot;search&quot;&gt;&lt;input type=&quot;text&quot; n ...</errorSourceCode>
        
        <sequenceID>801_8_265</sequenceID>
        <decisionPass>Tab order follows a logical order.</decisionPass>
        <decisionFail>Tab order does not follow a logical order.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>801</lineNum>
      <columnNum>8</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=268&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=268'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission error messages may not provide assistance.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form method=&quot;get&quot; action=&quot;http://arcangeles.org/&quot; class=&quot;search&quot; role=&quot;search&quot;&gt;&lt;input type=&quot;text&quot; n ...</errorSourceCode>
        
        <sequenceID>801_8_268</sequenceID>
        <decisionPass>All form submission error messages provide assistance in correcting the error.</decisionPass>
        <decisionFail>All form submission error messages do not provide assistance in correcting the errors.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>801</lineNum>
      <columnNum>8</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=267&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=267'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission error messages may not identify empty required fields.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form method=&quot;get&quot; action=&quot;http://arcangeles.org/&quot; class=&quot;search&quot; role=&quot;search&quot;&gt;&lt;input type=&quot;text&quot; n ...</errorSourceCode>
        
        <sequenceID>801_8_267</sequenceID>
        <decisionPass>Required fields are identified in all form submission error messages.</decisionPass>
        <decisionFail>Required fields are not identified in all form submission error messages.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>801</lineNum>
      <columnNum>8</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=272&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=272'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form may delete information without allowing for recovery.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form method=&quot;get&quot; action=&quot;http://arcangeles.org/&quot; class=&quot;search&quot; role=&quot;search&quot;&gt;&lt;input type=&quot;text&quot; n ...</errorSourceCode>
        
        <sequenceID>801_8_272</sequenceID>
        <decisionPass>Information deleted using this form can be recovered.</decisionPass>
        <decisionFail>Information delted using this form can not be recovered.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>801</lineNum>
      <columnNum>8</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=246&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=246'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;All required &lt;code&gt;form&lt;/code&gt; fields may not be indicated as required.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form method=&quot;get&quot; action=&quot;http://arcangeles.org/&quot; class=&quot;search&quot; role=&quot;search&quot;&gt;&lt;input type=&quot;text&quot; n ...</errorSourceCode>
        
        <sequenceID>801_8_246</sequenceID>
        <decisionPass>All required fields are indicated to the user as required.</decisionPass>
        <decisionFail>All required fields are not indicated to the user as required.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>801</lineNum>
      <columnNum>88</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=57&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=57'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;input&lt;/code&gt; element, &lt;code&gt;type&lt;/code&gt; of &quot;text&quot;, missing an associated label.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;text&quot; name=&quot;s&quot; value=&quot;&quot; placeholder=&quot;Buscar&quot; /&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;label&lt;/code&gt; element that surrounds the control's &lt;code&gt;label&lt;/code&gt;. Set the &lt;code&gt;for&lt;/code&gt; attribute on the &lt;code&gt;label&lt;/code&gt; element to the same value as the &lt;code&gt;id&lt;/code&gt; attribute of the control. And/or add a &lt;code&gt;title&lt;/code&gt; attribute to the &lt;code&gt;input&lt;/code&gt; element. And/or create a &lt;code&gt;label&lt;/code&gt; element that contains the &lt;code&gt;input&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>801</lineNum>
      <columnNum>88</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=211&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=211'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;input&lt;/code&gt; element &lt;code&gt;label&lt;/code&gt;, &lt;code&gt;type&lt;/code&gt; of &quot;text&quot;, is not positioned close to control.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;text&quot; name=&quot;s&quot; value=&quot;&quot; placeholder=&quot;Buscar&quot; /&gt;</errorSourceCode>
        
        <sequenceID>801_88_211</sequenceID>
        <decisionPass>Label is positioned close to the control.</decisionPass>
        <decisionFail>Label is not positioned close to the control.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>801</lineNum>
      <columnNum>88</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=188&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=188'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Label text is empty.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;text&quot; name=&quot;s&quot; value=&quot;&quot; placeholder=&quot;Buscar&quot; /&gt;</errorSourceCode>
        <repair>Add text to the &lt;code&gt;label&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>801</lineNum>
      <columnNum>88</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=55&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=55'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;input&lt;/code&gt; possibly using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;text&quot; name=&quot;s&quot; value=&quot;&quot; placeholder=&quot;Buscar&quot; /&gt;</errorSourceCode>
        
        <sequenceID>801_88_55</sequenceID>
        <decisionPass>&lt;code&gt;input&lt;/code&gt; element does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;input&lt;/code&gt; element uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>801</lineNum>
      <columnNum>88</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=213&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=213'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;input&lt;/code&gt; element, &lt;code&gt;type&lt;/code&gt; of &quot;text&quot;, has no text in &lt;code&gt;label&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;text&quot; name=&quot;s&quot; value=&quot;&quot; placeholder=&quot;Buscar&quot; /&gt;</errorSourceCode>
        <repair>Add text to the &lt;code&gt;input&lt;/code&gt; element's associated label that describes the purpose or function of the control.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>801</lineNum>
      <columnNum>170</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;icon-search&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>813</lineNum>
      <columnNum>18</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#menu&quot; class=&quot;button&quot; title=&quot;Men&uacute;&quot;&gt;&lt;i class=&quot;icon-menu&quot;&gt;&lt;/i&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>813_18_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>813</lineNum>
      <columnNum>18</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#menu&quot; class=&quot;button&quot; title=&quot;Men&uacute;&quot;&gt;&lt;i class=&quot;icon-menu&quot;&gt;&lt;/i&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>813_18_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>813</lineNum>
      <columnNum>18</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#menu&quot; class=&quot;button&quot; title=&quot;Men&uacute;&quot;&gt;&lt;i class=&quot;icon-menu&quot;&gt;&lt;/i&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>813_18_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>813</lineNum>
      <columnNum>63</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;icon-menu&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>816</lineNum>
      <columnNum>18</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#search&quot; class=&quot;button&quot; title=&quot;Buscar&quot;&gt;&lt;i class=&quot;icon-search&quot;&gt;&lt;/i&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>816_18_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>816</lineNum>
      <columnNum>18</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#search&quot; class=&quot;button&quot; title=&quot;Buscar&quot;&gt;&lt;i class=&quot;icon-search&quot;&gt;&lt;/i&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>816_18_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>816</lineNum>
      <columnNum>18</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#search&quot; class=&quot;button&quot; title=&quot;Buscar&quot;&gt;&lt;i class=&quot;icon-search&quot;&gt;&lt;/i&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>816_18_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>816</lineNum>
      <columnNum>66</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;icon-search&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>821</lineNum>
      <columnNum>8</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=37&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=37'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Header nesting - header following &lt;code&gt;h1&lt;/code&gt; is incorrect.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h1 class=&quot;logo vertical-align&quot;&gt;
																&lt;a href=&quot;http://arcangeles.org/&quot; title=&quot;Arcangeles&quot; ...</errorSourceCode>
        <repair>Modify the header levels so only an &lt;code&gt;h1&lt;/code&gt; or &lt;code&gt;h2&lt;/code&gt; follows &lt;code&gt;h1&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>821</lineNum>
      <columnNum>8</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=42&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=42'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h1&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h1 class=&quot;logo vertical-align&quot;&gt;
																&lt;a href=&quot;http://arcangeles.org/&quot; title=&quot;Arcangeles&quot; ...</errorSourceCode>
        
        <sequenceID>821_8_42</sequenceID>
        <decisionPass>This &lt;code&gt;h1&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h1&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>822</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/&quot; title=&quot;Arcangeles&quot; rel=&quot;home&quot;&gt;&lt;img width=&quot;115&quot; height=&quot;74&quot; src=&quot;htt ...</errorSourceCode>
        
        <sequenceID>822_17_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>822</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/&quot; title=&quot;Arcangeles&quot; rel=&quot;home&quot;&gt;&lt;img width=&quot;115&quot; height=&quot;74&quot; src=&quot;htt ...</errorSourceCode>
        
        <sequenceID>822_17_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>822</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/&quot; title=&quot;Arcangeles&quot; rel=&quot;home&quot;&gt;&lt;img width=&quot;115&quot; height=&quot;74&quot; src=&quot;htt ...</errorSourceCode>
        
        <sequenceID>822_17_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>822</lineNum>
      <columnNum>80</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;115&quot; height=&quot;74&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2017/01/logo-arcangeles-c ...</errorSourceCode>
        
        <sequenceID>822_80_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>822</lineNum>
      <columnNum>80</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;115&quot; height=&quot;74&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2017/01/logo-arcangeles-c ...</errorSourceCode>
        
        <sequenceID>822_80_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>822</lineNum>
      <columnNum>80</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;115&quot; height=&quot;74&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2017/01/logo-arcangeles-c ...</errorSourceCode>
        
        <sequenceID>822_80_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>822</lineNum>
      <columnNum>80</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;115&quot; height=&quot;74&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2017/01/logo-arcangeles-c ...</errorSourceCode>
        
        <sequenceID>822_80_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>826</lineNum>
      <columnNum>214</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/&quot; aria-current=&quot;page&quot;&gt;INICIO&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>826_214_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>826</lineNum>
      <columnNum>214</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/&quot; aria-current=&quot;page&quot;&gt;INICIO&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>826</lineNum>
      <columnNum>214</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/&quot; aria-current=&quot;page&quot;&gt;INICIO&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>826_214_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>827</lineNum>
      <columnNum>107</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/acerca-de-arcangeles/&quot;&gt;LA FUNDACION&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>827_107_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>827</lineNum>
      <columnNum>107</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/acerca-de-arcangeles/&quot;&gt;LA FUNDACION&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>827</lineNum>
      <columnNum>107</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/acerca-de-arcangeles/&quot;&gt;LA FUNDACION&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>827_107_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>829</lineNum>
      <columnNum>86</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/salud-ips-rehabilitacion/&quot;&gt;SALUD&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>829_86_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>829</lineNum>
      <columnNum>86</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/salud-ips-rehabilitacion/&quot;&gt;SALUD&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>829</lineNum>
      <columnNum>86</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/salud-ips-rehabilitacion/&quot;&gt;SALUD&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>829_86_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>830</lineNum>
      <columnNum>86</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/programas-y-proyectos/&quot;&gt;PROGRAMAS Y PROYECTOS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>830_86_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>830</lineNum>
      <columnNum>86</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/programas-y-proyectos/&quot;&gt;PROGRAMAS Y PROYECTOS&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>830</lineNum>
      <columnNum>86</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/programas-y-proyectos/&quot;&gt;PROGRAMAS Y PROYECTOS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>830_86_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>833</lineNum>
      <columnNum>88</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/noticias/&quot;&gt;NOTICIAS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>833_88_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>833</lineNum>
      <columnNum>88</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/noticias/&quot;&gt;NOTICIAS&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>833</lineNum>
      <columnNum>88</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/noticias/&quot;&gt;NOTICIAS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>833_88_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>834</lineNum>
      <columnNum>84</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/contactenos/&quot;&gt;CONTACTENOS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>834_84_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>834</lineNum>
      <columnNum>84</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/contactenos/&quot;&gt;CONTACTENOS&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>834</lineNum>
      <columnNum>84</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/contactenos/&quot;&gt;CONTACTENOS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>834_84_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>835</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=269&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=269'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission data may not be presented to the user before final acceptance of an irreversable transaction.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form method=&quot;get&quot; action=&quot;http://arcangeles.org/&quot; class=&quot;search&quot; role=&quot;search&quot;&gt;&lt;input type=&quot;text&quot; n ...</errorSourceCode>
        
        <sequenceID>835_5_269</sequenceID>
        <decisionPass>All form submission data is presented to the user before final acceptance for all irreversable transactions.</decisionPass>
        <decisionFail>All form submission data is not presented to the user before final acceptance for all irreversable transactions.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>835</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=265&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=265'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Tab order may not follow logical order.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form method=&quot;get&quot; action=&quot;http://arcangeles.org/&quot; class=&quot;search&quot; role=&quot;search&quot;&gt;&lt;input type=&quot;text&quot; n ...</errorSourceCode>
        
        <sequenceID>835_5_265</sequenceID>
        <decisionPass>Tab order follows a logical order.</decisionPass>
        <decisionFail>Tab order does not follow a logical order.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>835</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=268&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=268'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission error messages may not provide assistance.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form method=&quot;get&quot; action=&quot;http://arcangeles.org/&quot; class=&quot;search&quot; role=&quot;search&quot;&gt;&lt;input type=&quot;text&quot; n ...</errorSourceCode>
        
        <sequenceID>835_5_268</sequenceID>
        <decisionPass>All form submission error messages provide assistance in correcting the error.</decisionPass>
        <decisionFail>All form submission error messages do not provide assistance in correcting the errors.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>835</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=267&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=267'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission error messages may not identify empty required fields.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form method=&quot;get&quot; action=&quot;http://arcangeles.org/&quot; class=&quot;search&quot; role=&quot;search&quot;&gt;&lt;input type=&quot;text&quot; n ...</errorSourceCode>
        
        <sequenceID>835_5_267</sequenceID>
        <decisionPass>Required fields are identified in all form submission error messages.</decisionPass>
        <decisionFail>Required fields are not identified in all form submission error messages.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>835</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=272&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=272'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form may delete information without allowing for recovery.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form method=&quot;get&quot; action=&quot;http://arcangeles.org/&quot; class=&quot;search&quot; role=&quot;search&quot;&gt;&lt;input type=&quot;text&quot; n ...</errorSourceCode>
        
        <sequenceID>835_5_272</sequenceID>
        <decisionPass>Information deleted using this form can be recovered.</decisionPass>
        <decisionFail>Information delted using this form can not be recovered.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>835</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=246&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=246'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;All required &lt;code&gt;form&lt;/code&gt; fields may not be indicated as required.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form method=&quot;get&quot; action=&quot;http://arcangeles.org/&quot; class=&quot;search&quot; role=&quot;search&quot;&gt;&lt;input type=&quot;text&quot; n ...</errorSourceCode>
        
        <sequenceID>835_5_246</sequenceID>
        <decisionPass>All required fields are indicated to the user as required.</decisionPass>
        <decisionFail>All required fields are not indicated to the user as required.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>835</lineNum>
      <columnNum>85</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=57&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=57'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;input&lt;/code&gt; element, &lt;code&gt;type&lt;/code&gt; of &quot;text&quot;, missing an associated label.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;text&quot; name=&quot;s&quot; value=&quot;&quot; placeholder=&quot;Buscar&quot; /&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;label&lt;/code&gt; element that surrounds the control's &lt;code&gt;label&lt;/code&gt;. Set the &lt;code&gt;for&lt;/code&gt; attribute on the &lt;code&gt;label&lt;/code&gt; element to the same value as the &lt;code&gt;id&lt;/code&gt; attribute of the control. And/or add a &lt;code&gt;title&lt;/code&gt; attribute to the &lt;code&gt;input&lt;/code&gt; element. And/or create a &lt;code&gt;label&lt;/code&gt; element that contains the &lt;code&gt;input&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>835</lineNum>
      <columnNum>85</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=211&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=211'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;input&lt;/code&gt; element &lt;code&gt;label&lt;/code&gt;, &lt;code&gt;type&lt;/code&gt; of &quot;text&quot;, is not positioned close to control.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;text&quot; name=&quot;s&quot; value=&quot;&quot; placeholder=&quot;Buscar&quot; /&gt;</errorSourceCode>
        
        <sequenceID>835_85_211</sequenceID>
        <decisionPass>Label is positioned close to the control.</decisionPass>
        <decisionFail>Label is not positioned close to the control.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>835</lineNum>
      <columnNum>85</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=188&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=188'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Label text is empty.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;text&quot; name=&quot;s&quot; value=&quot;&quot; placeholder=&quot;Buscar&quot; /&gt;</errorSourceCode>
        <repair>Add text to the &lt;code&gt;label&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>835</lineNum>
      <columnNum>85</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=55&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=55'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;input&lt;/code&gt; possibly using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;text&quot; name=&quot;s&quot; value=&quot;&quot; placeholder=&quot;Buscar&quot; /&gt;</errorSourceCode>
        
        <sequenceID>835_85_55</sequenceID>
        <decisionPass>&lt;code&gt;input&lt;/code&gt; element does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;input&lt;/code&gt; element uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>835</lineNum>
      <columnNum>85</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=213&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=213'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;input&lt;/code&gt; element, &lt;code&gt;type&lt;/code&gt; of &quot;text&quot;, has no text in &lt;code&gt;label&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;text&quot; name=&quot;s&quot; value=&quot;&quot; placeholder=&quot;Buscar&quot; /&gt;</errorSourceCode>
        <repair>Add text to the &lt;code&gt;input&lt;/code&gt; element's associated label that describes the purpose or function of the control.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>835</lineNum>
      <columnNum>167</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;icon-search&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>853</lineNum>
      <columnNum>162</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=178&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=178'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text does not convey the same information as the image.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;1920&quot; height=&quot;640&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2016/11/fnd-arcangeles- ...</errorSourceCode>
        
        <sequenceID>853_162_178</sequenceID>
        <decisionPass>Alt text conveys the same information as the image.</decisionPass>
        <decisionFail>Alt text does not convey the same information as the image.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>853</lineNum>
      <columnNum>162</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;1920&quot; height=&quot;640&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2016/11/fnd-arcangeles- ...</errorSourceCode>
        
        <sequenceID>853_162_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>853</lineNum>
      <columnNum>162</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;1920&quot; height=&quot;640&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2016/11/fnd-arcangeles- ...</errorSourceCode>
        
        <sequenceID>853_162_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>853</lineNum>
      <columnNum>162</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=4&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=4'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image has suspicious Alt text (empty string &quot;&quot;).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;1920&quot; height=&quot;640&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2016/11/fnd-arcangeles- ...</errorSourceCode>
        
        <sequenceID>853_162_4</sequenceID>
        <decisionPass>Image is decorative (null Alt text is OK).</decisionPass>
        <decisionFail>Image is not decorative (null Alt text is not OK).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>853</lineNum>
      <columnNum>162</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;1920&quot; height=&quot;640&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2016/11/fnd-arcangeles- ...</errorSourceCode>
        
        <sequenceID>853_162_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>853</lineNum>
      <columnNum>162</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;1920&quot; height=&quot;640&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2016/11/fnd-arcangeles- ...</errorSourceCode>
        
        <sequenceID>853_162_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>853</lineNum>
      <columnNum>1220</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=178&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=178'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text does not convey the same information as the image.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;100&quot; height=&quot;63&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2016/11/fnd-arcangeles-10 ...</errorSourceCode>
        
        <sequenceID>853_1220_178</sequenceID>
        <decisionPass>Alt text conveys the same information as the image.</decisionPass>
        <decisionFail>Alt text does not convey the same information as the image.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>853</lineNum>
      <columnNum>1220</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;100&quot; height=&quot;63&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2016/11/fnd-arcangeles-10 ...</errorSourceCode>
        
        <sequenceID>853_1220_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>853</lineNum>
      <columnNum>1220</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;100&quot; height=&quot;63&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2016/11/fnd-arcangeles-10 ...</errorSourceCode>
        
        <sequenceID>853_1220_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>853</lineNum>
      <columnNum>1220</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=4&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=4'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image has suspicious Alt text (empty string &quot;&quot;).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;100&quot; height=&quot;63&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2016/11/fnd-arcangeles-10 ...</errorSourceCode>
        
        <sequenceID>853_1220_4</sequenceID>
        <decisionPass>Image is decorative (null Alt text is OK).</decisionPass>
        <decisionFail>Image is not decorative (null Alt text is not OK).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>853</lineNum>
      <columnNum>1366</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.youtube.com/watch?v=3GO-6kQitdk&quot; target=&quot;_blank&quot;   class=&quot;ls-s-1&quot;  style=&quot;posit ...</errorSourceCode>
        
        <sequenceID>853_1366_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>853</lineNum>
      <columnNum>1366</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.youtube.com/watch?v=3GO-6kQitdk&quot; target=&quot;_blank&quot;   class=&quot;ls-s-1&quot;  style=&quot;posit ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>853</lineNum>
      <columnNum>1366</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.youtube.com/watch?v=3GO-6kQitdk&quot; target=&quot;_blank&quot;   class=&quot;ls-s-1&quot;  style=&quot;posit ...</errorSourceCode>
        
        <sequenceID>853_1366_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>856</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
(function(){window.incluTabOptions={incluTabVisible:!0};var i7e_e=do ...</errorSourceCode>
        
        <sequenceID>856_4_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>856</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
(function(){window.incluTabOptions={incluTabVisible:!0};var i7e_e=do ...</errorSourceCode>
        
        <sequenceID>856_4_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>856</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
(function(){window.incluTabOptions={incluTabVisible:!0};var i7e_e=do ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>856</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
(function(){window.incluTabOptions={incluTabVisible:!0};var i7e_e=do ...</errorSourceCode>
        
        <sequenceID>856_4_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>856</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
(function(){window.incluTabOptions={incluTabVisible:!0};var i7e_e=do ...</errorSourceCode>
        
        <sequenceID>856_4_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>856</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
(function(){window.incluTabOptions={incluTabVisible:!0};var i7e_e=do ...</errorSourceCode>
        
        <sequenceID>856_4_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>864</lineNum>
      <columnNum>160</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/informacion-articulo-364-5-del-estatuto-tributario-2018/&quot;&gt;Informaci&oacute; ...</errorSourceCode>
        
        <sequenceID>864_160_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>864</lineNum>
      <columnNum>160</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/informacion-articulo-364-5-del-estatuto-tributario-2018/&quot;&gt;Informaci&oacute; ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>864</lineNum>
      <columnNum>160</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/informacion-articulo-364-5-del-estatuto-tributario-2018/&quot;&gt;Informaci&oacute; ...</errorSourceCode>
        
        <sequenceID>864_160_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>865</lineNum>
      <columnNum>103</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/estados-financieros/&quot;&gt;ESTADOS FINANCIEROS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>865_103_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>865</lineNum>
      <columnNum>103</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/estados-financieros/&quot;&gt;ESTADOS FINANCIEROS&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>865</lineNum>
      <columnNum>103</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/estados-financieros/&quot;&gt;ESTADOS FINANCIEROS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>865_103_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>866</lineNum>
      <columnNum>103</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/politica-de-privacidad/&quot;&gt;POL&Iacute;TICA DE PRIVACIDAD&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>866_103_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>866</lineNum>
      <columnNum>103</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/politica-de-privacidad/&quot;&gt;POL&Iacute;TICA DE PRIVACIDAD&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>866</lineNum>
      <columnNum>103</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/politica-de-privacidad/&quot;&gt;POL&Iacute;TICA DE PRIVACIDAD&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>866_103_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>867</lineNum>
      <columnNum>105</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/texto-exoneracion-carrera-presta-pierna-11k-2017/&quot;&gt;EXONERACI&Oacute;N PREST ...</errorSourceCode>
        
        <sequenceID>867_105_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>867</lineNum>
      <columnNum>105</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/texto-exoneracion-carrera-presta-pierna-11k-2017/&quot;&gt;EXONERACI&Oacute;N PREST ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>867</lineNum>
      <columnNum>105</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/texto-exoneracion-carrera-presta-pierna-11k-2017/&quot;&gt;EXONERACI&Oacute;N PREST ...</errorSourceCode>
        
        <sequenceID>867_105_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>868</lineNum>
      <columnNum>105</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/vacantes/&quot;&gt;VACANTES&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>868_105_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>868</lineNum>
      <columnNum>105</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/vacantes/&quot;&gt;VACANTES&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>868</lineNum>
      <columnNum>105</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/vacantes/&quot;&gt;VACANTES&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>868_105_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>869</lineNum>
      <columnNum>102</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot; href=&quot;http://app.vipermed.uy/login.aspx&quot;&gt;ACCESO A TELEM ...</errorSourceCode>
        
        <sequenceID>869_102_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>869</lineNum>
      <columnNum>102</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot; href=&quot;http://app.vipermed.uy/login.aspx&quot;&gt;ACCESO A TELEM ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>869</lineNum>
      <columnNum>102</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot; href=&quot;http://app.vipermed.uy/login.aspx&quot;&gt;ACCESO A TELEM ...</errorSourceCode>
        
        <sequenceID>869_102_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>870</lineNum>
      <columnNum>102</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot; href=&quot;http://docs.arcangeles.org/&quot;&gt;ACCESO A DOCUMENTACI ...</errorSourceCode>
        
        <sequenceID>870_102_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>870</lineNum>
      <columnNum>102</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot; href=&quot;http://docs.arcangeles.org/&quot;&gt;ACCESO A DOCUMENTACI ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>870</lineNum>
      <columnNum>102</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot; href=&quot;http://docs.arcangeles.org/&quot;&gt;ACCESO A DOCUMENTACI ...</errorSourceCode>
        
        <sequenceID>870_102_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>871</lineNum>
      <columnNum>106</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/noticias/&quot;&gt;NOTICIAS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>871_106_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>871</lineNum>
      <columnNum>106</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/noticias/&quot;&gt;NOTICIAS&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>871</lineNum>
      <columnNum>106</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/noticias/&quot;&gt;NOTICIAS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>871_106_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>872</lineNum>
      <columnNum>105</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/club-deportivo-arcangeles/&quot;&gt;CLUB DEPORTIVO&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>872_105_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>872</lineNum>
      <columnNum>105</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/club-deportivo-arcangeles/&quot;&gt;CLUB DEPORTIVO&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>872</lineNum>
      <columnNum>105</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/club-deportivo-arcangeles/&quot;&gt;CLUB DEPORTIVO&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>872_105_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>873</lineNum>
      <columnNum>102</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot; href=&quot;http://sportpower2.org/&quot;&gt;SPORT POWER 2&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>873_102_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>873</lineNum>
      <columnNum>102</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot; href=&quot;http://sportpower2.org/&quot;&gt;SPORT POWER 2&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>873</lineNum>
      <columnNum>102</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot; href=&quot;http://sportpower2.org/&quot;&gt;SPORT POWER 2&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>873_102_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>874</lineNum>
      <columnNum>102</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot; href=&quot;http://maximusproject.org/&quot;&gt;MAXIMUS PROJECT&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>874_102_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>874</lineNum>
      <columnNum>102</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot; href=&quot;http://maximusproject.org/&quot;&gt;MAXIMUS PROJECT&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>874</lineNum>
      <columnNum>102</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a target=&quot;_blank&quot; rel=&quot;noopener noreferrer&quot; href=&quot;http://maximusproject.org/&quot;&gt;MAXIMUS PROJECT&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>874_102_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>880</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=100&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=100'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;blockquote&lt;/code&gt; may be used for indentation.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;blockquote&gt;&lt;p&gt;&lt;strong&gt;ARC&Aacute;NGELES se ocupa de la inclusi&oacute;n social y los derechos de las personas c ...</errorSourceCode>
        
        <sequenceID>880_1_100</sequenceID>
        <decisionPass>Text enclosed by &lt;code&gt;blockquote&lt;/code&gt; is a quotation.</decisionPass>
        <decisionFail>Text enclosed by &lt;code&gt;blockquote&lt;/code&gt; is not a quotation.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>880</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=82&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=82'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;p&lt;/code&gt; element may be misused (could be a header).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;p&gt;&lt;strong&gt;ARC&Aacute;NGELES se ocupa de la inclusi&oacute;n social y los derechos de las personas con discapaci ...</errorSourceCode>
        
        <sequenceID>880_13_82</sequenceID>
        <decisionPass>Text is not a header, current markup is OK.</decisionPass>
        <decisionFail>Text is a header, markup should change.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>883</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;zoom-hover fb&quot; href=&quot;http://arcangeles.org/blog/charly-un-capitan-que-ha-roto-paradigmas-e ...</errorSourceCode>
        
        <sequenceID>883_37_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>883</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;zoom-hover fb&quot; href=&quot;http://arcangeles.org/blog/charly-un-capitan-que-ha-roto-paradigmas-e ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>883</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;zoom-hover fb&quot; href=&quot;http://arcangeles.org/blog/charly-un-capitan-que-ha-roto-paradigmas-e ...</errorSourceCode>
        
        <sequenceID>883_37_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>883</lineNum>
      <columnNum>164</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;394&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/08/banner-charly-94 ...</errorSourceCode>
        
        <sequenceID>883_164_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>883</lineNum>
      <columnNum>164</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=7&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=7'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image used as anchor is missing valid Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;394&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/08/banner-charly-94 ...</errorSourceCode>
        <repair>Add Alt text that identifies the purpose or function of the image.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>883</lineNum>
      <columnNum>164</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;394&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/08/banner-charly-94 ...</errorSourceCode>
        
        <sequenceID>883_164_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>883</lineNum>
      <columnNum>164</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;394&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/08/banner-charly-94 ...</errorSourceCode>
        
        <sequenceID>883_164_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>883</lineNum>
      <columnNum>164</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=4&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=4'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image has suspicious Alt text (empty string &quot;&quot;).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;394&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/08/banner-charly-94 ...</errorSourceCode>
        
        <sequenceID>883_164_4</sequenceID>
        <decisionPass>Image is decorative (null Alt text is OK).</decisionPass>
        <decisionFail>Image is not decorative (null Alt text is not OK).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>883</lineNum>
      <columnNum>164</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;394&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/08/banner-charly-94 ...</errorSourceCode>
        
        <sequenceID>883_164_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>883</lineNum>
      <columnNum>164</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;394&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/08/banner-charly-94 ...</errorSourceCode>
        
        <sequenceID>883_164_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>883</lineNum>
      <columnNum>1125</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=44&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=44'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h3&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h3&gt;&lt;a href=&quot;http://arcangeles.org/blog/charly-un-capitan-que-ha-roto-paradigmas-en-el-deporte-paral ...</errorSourceCode>
        
        <sequenceID>883_1125_44</sequenceID>
        <decisionPass>This &lt;code&gt;h3&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h3&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>883</lineNum>
      <columnNum>1129</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/charly-un-capitan-que-ha-roto-paradigmas-en-el-deporte-paralimpi ...</errorSourceCode>
        
        <sequenceID>883_1129_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>883</lineNum>
      <columnNum>1129</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/charly-un-capitan-que-ha-roto-paradigmas-en-el-deporte-paralimpi ...</errorSourceCode>
        
        <sequenceID>883_1129_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>883</lineNum>
      <columnNum>1129</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/charly-un-capitan-que-ha-roto-paradigmas-en-el-deporte-paralimpi ...</errorSourceCode>
        
        <sequenceID>883_1129_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>883</lineNum>
      <columnNum>1411</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/club-deportivo/&quot; rel=&quot;tag&quot;&gt;Club Deportivo&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>883_1411_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>883</lineNum>
      <columnNum>1411</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/club-deportivo/&quot; rel=&quot;tag&quot;&gt;Club Deportivo&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>883</lineNum>
      <columnNum>1411</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/club-deportivo/&quot; rel=&quot;tag&quot;&gt;Club Deportivo&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>883_1411_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>886</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;zoom-hover fb&quot; href=&quot;http://arcangeles.org/blog/deportistas-la-fundacion-arcangeles-viajar ...</errorSourceCode>
        
        <sequenceID>886_37_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>886</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;zoom-hover fb&quot; href=&quot;http://arcangeles.org/blog/deportistas-la-fundacion-arcangeles-viajar ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>886</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;zoom-hover fb&quot; href=&quot;http://arcangeles.org/blog/deportistas-la-fundacion-arcangeles-viajar ...</errorSourceCode>
        
        <sequenceID>886_37_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>886</lineNum>
      <columnNum>176</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;800&quot; height=&quot;399&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/10/nota-tennis-sill ...</errorSourceCode>
        
        <sequenceID>886_176_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>886</lineNum>
      <columnNum>176</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=7&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=7'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image used as anchor is missing valid Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;800&quot; height=&quot;399&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/10/nota-tennis-sill ...</errorSourceCode>
        <repair>Add Alt text that identifies the purpose or function of the image.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>886</lineNum>
      <columnNum>176</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;800&quot; height=&quot;399&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/10/nota-tennis-sill ...</errorSourceCode>
        
        <sequenceID>886_176_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>886</lineNum>
      <columnNum>176</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;800&quot; height=&quot;399&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/10/nota-tennis-sill ...</errorSourceCode>
        
        <sequenceID>886_176_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>886</lineNum>
      <columnNum>176</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=4&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=4'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image has suspicious Alt text (empty string &quot;&quot;).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;800&quot; height=&quot;399&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/10/nota-tennis-sill ...</errorSourceCode>
        
        <sequenceID>886_176_4</sequenceID>
        <decisionPass>Image is decorative (null Alt text is OK).</decisionPass>
        <decisionFail>Image is not decorative (null Alt text is not OK).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>886</lineNum>
      <columnNum>176</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;800&quot; height=&quot;399&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/10/nota-tennis-sill ...</errorSourceCode>
        
        <sequenceID>886_176_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>886</lineNum>
      <columnNum>176</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;800&quot; height=&quot;399&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/10/nota-tennis-sill ...</errorSourceCode>
        
        <sequenceID>886_176_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>886</lineNum>
      <columnNum>1062</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=44&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=44'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h3&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h3&gt;&lt;a href=&quot;http://arcangeles.org/blog/deportistas-la-fundacion-arcangeles-viajaran-brasil-torneo-i ...</errorSourceCode>
        
        <sequenceID>886_1062_44</sequenceID>
        <decisionPass>This &lt;code&gt;h3&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h3&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>886</lineNum>
      <columnNum>1066</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/deportistas-la-fundacion-arcangeles-viajaran-brasil-torneo-inter ...</errorSourceCode>
        
        <sequenceID>886_1066_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>886</lineNum>
      <columnNum>1066</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/deportistas-la-fundacion-arcangeles-viajaran-brasil-torneo-inter ...</errorSourceCode>
        
        <sequenceID>886_1066_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>886</lineNum>
      <columnNum>1066</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/deportistas-la-fundacion-arcangeles-viajaran-brasil-torneo-inter ...</errorSourceCode>
        
        <sequenceID>886_1066_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>886</lineNum>
      <columnNum>1428</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/club-deportivo/&quot; rel=&quot;tag&quot;&gt;Club Deportivo&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>886_1428_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>886</lineNum>
      <columnNum>1428</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/club-deportivo/&quot; rel=&quot;tag&quot;&gt;Club Deportivo&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>886</lineNum>
      <columnNum>1428</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/club-deportivo/&quot; rel=&quot;tag&quot;&gt;Club Deportivo&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>886_1428_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>886</lineNum>
      <columnNum>1520</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/deportes/&quot; rel=&quot;tag&quot;&gt;DEPORTES&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>886_1520_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>886</lineNum>
      <columnNum>1520</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/deportes/&quot; rel=&quot;tag&quot;&gt;DEPORTES&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>886</lineNum>
      <columnNum>1520</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/deportes/&quot; rel=&quot;tag&quot;&gt;DEPORTES&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>886_1520_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>886</lineNum>
      <columnNum>1600</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/noticias/&quot; rel=&quot;tag&quot;&gt;NOTICIAS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>886_1600_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>886</lineNum>
      <columnNum>1600</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/noticias/&quot; rel=&quot;tag&quot;&gt;NOTICIAS&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>886</lineNum>
      <columnNum>1600</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/noticias/&quot; rel=&quot;tag&quot;&gt;NOTICIAS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>886_1600_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>889</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;zoom-hover fb&quot; href=&quot;http://arcangeles.org/blog/florencia-saravena-puerto-jordan-tame-y-cu ...</errorSourceCode>
        
        <sequenceID>889_37_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>889</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;zoom-hover fb&quot; href=&quot;http://arcangeles.org/blog/florencia-saravena-puerto-jordan-tame-y-cu ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>889</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;zoom-hover fb&quot; href=&quot;http://arcangeles.org/blog/florencia-saravena-puerto-jordan-tame-y-cu ...</errorSourceCode>
        
        <sequenceID>889_37_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>889</lineNum>
      <columnNum>185</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;705&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/09/Entrega-sillas-t ...</errorSourceCode>
        
        <sequenceID>889_185_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>889</lineNum>
      <columnNum>185</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=7&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=7'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image used as anchor is missing valid Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;705&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/09/Entrega-sillas-t ...</errorSourceCode>
        <repair>Add Alt text that identifies the purpose or function of the image.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>889</lineNum>
      <columnNum>185</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;705&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/09/Entrega-sillas-t ...</errorSourceCode>
        
        <sequenceID>889_185_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>889</lineNum>
      <columnNum>185</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;705&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/09/Entrega-sillas-t ...</errorSourceCode>
        
        <sequenceID>889_185_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>889</lineNum>
      <columnNum>185</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=4&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=4'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image has suspicious Alt text (empty string &quot;&quot;).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;705&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/09/Entrega-sillas-t ...</errorSourceCode>
        
        <sequenceID>889_185_4</sequenceID>
        <decisionPass>Image is decorative (null Alt text is OK).</decisionPass>
        <decisionFail>Image is not decorative (null Alt text is not OK).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>889</lineNum>
      <columnNum>185</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;705&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/09/Entrega-sillas-t ...</errorSourceCode>
        
        <sequenceID>889_185_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>889</lineNum>
      <columnNum>185</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;705&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/09/Entrega-sillas-t ...</errorSourceCode>
        
        <sequenceID>889_185_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>889</lineNum>
      <columnNum>1294</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=44&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=44'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h3&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h3&gt;&lt;a href=&quot;http://arcangeles.org/blog/florencia-saravena-puerto-jordan-tame-y-cucuta-reciben-donac ...</errorSourceCode>
        
        <sequenceID>889_1294_44</sequenceID>
        <decisionPass>This &lt;code&gt;h3&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h3&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>889</lineNum>
      <columnNum>1298</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/florencia-saravena-puerto-jordan-tame-y-cucuta-reciben-donacion- ...</errorSourceCode>
        
        <sequenceID>889_1298_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>889</lineNum>
      <columnNum>1298</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/florencia-saravena-puerto-jordan-tame-y-cucuta-reciben-donacion- ...</errorSourceCode>
        
        <sequenceID>889_1298_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>889</lineNum>
      <columnNum>1298</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/florencia-saravena-puerto-jordan-tame-y-cucuta-reciben-donacion- ...</errorSourceCode>
        
        <sequenceID>889_1298_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>889</lineNum>
      <columnNum>1653</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/noticias/&quot; rel=&quot;tag&quot;&gt;NOTICIAS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>889_1653_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>889</lineNum>
      <columnNum>1653</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/noticias/&quot; rel=&quot;tag&quot;&gt;NOTICIAS&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>889</lineNum>
      <columnNum>1653</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/noticias/&quot; rel=&quot;tag&quot;&gt;NOTICIAS&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>889_1653_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>889</lineNum>
      <columnNum>1733</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/salud/&quot; rel=&quot;tag&quot;&gt;SALUD&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>889_1733_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>889</lineNum>
      <columnNum>1733</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/salud/&quot; rel=&quot;tag&quot;&gt;SALUD&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>889</lineNum>
      <columnNum>1733</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://arcangeles.org/blog/category/salud/&quot; rel=&quot;tag&quot;&gt;SALUD&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>889_1733_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>896</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=100&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=100'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;blockquote&lt;/code&gt; may be used for indentation.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;blockquote class=&quot;alignnone bar&quot;&gt;Creemos que el mundo es para todos, por eso generamos transformaci ...</errorSourceCode>
        
        <sequenceID>896_1_100</sequenceID>
        <decisionPass>Text enclosed by &lt;code&gt;blockquote&lt;/code&gt; is a quotation.</decisionPass>
        <decisionFail>Text enclosed by &lt;code&gt;blockquote&lt;/code&gt; is not a quotation.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>899</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;button big&quot; data-button-href=&quot;http://web.arcangeles.org/acerca-de-arcangeles/&quot; data-button ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>899</lineNum>
      <columnNum>29</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;button big&quot; data-button-href=&quot;http://web.arcangeles.org/acerca-de-arcangeles/&quot; data-button ...</errorSourceCode>
        
        <sequenceID>899_29_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>899</lineNum>
      <columnNum>145</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;icon-export&quot; style=&quot;color: #000;&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>232</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a rel=&quot;gallery-4&quot; href=&quot;https://www.caffiniti.com/cafe&quot; target=&quot;_blank&quot; class=&quot;set-target no-lightb ...</errorSourceCode>
        
        <sequenceID>905_232_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>905</lineNum>
      <columnNum>232</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a rel=&quot;gallery-4&quot; href=&quot;https://www.caffiniti.com/cafe&quot; target=&quot;_blank&quot; class=&quot;set-target no-lightb ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>232</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a rel=&quot;gallery-4&quot; href=&quot;https://www.caffiniti.com/cafe&quot; target=&quot;_blank&quot; class=&quot;set-target no-lightb ...</errorSourceCode>
        
        <sequenceID>905_232_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>336</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;752&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/06/caffiniti-940x75 ...</errorSourceCode>
        
        <sequenceID>905_336_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>905</lineNum>
      <columnNum>336</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=7&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=7'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image used as anchor is missing valid Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;752&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/06/caffiniti-940x75 ...</errorSourceCode>
        <repair>Add Alt text that identifies the purpose or function of the image.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>336</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;752&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/06/caffiniti-940x75 ...</errorSourceCode>
        
        <sequenceID>905_336_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>336</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;752&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/06/caffiniti-940x75 ...</errorSourceCode>
        
        <sequenceID>905_336_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>336</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=4&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=4'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image has suspicious Alt text (empty string &quot;&quot;).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;752&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/06/caffiniti-940x75 ...</errorSourceCode>
        
        <sequenceID>905_336_4</sequenceID>
        <decisionPass>Image is decorative (null Alt text is OK).</decisionPass>
        <decisionFail>Image is not decorative (null Alt text is not OK).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>336</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;752&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/06/caffiniti-940x75 ...</errorSourceCode>
        
        <sequenceID>905_336_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>336</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;752&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2018/06/caffiniti-940x75 ...</errorSourceCode>
        
        <sequenceID>905_336_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>1336</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a rel=&quot;gallery-4&quot; href=&quot;http://arcangeles.org/club-deportivo-arcangeles/&quot; class=&quot;no-lightbox&quot;&gt;&lt;img  ...</errorSourceCode>
        
        <sequenceID>905_1336_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>905</lineNum>
      <columnNum>1336</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a rel=&quot;gallery-4&quot; href=&quot;http://arcangeles.org/club-deportivo-arcangeles/&quot; class=&quot;no-lightbox&quot;&gt;&lt;img  ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>1336</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a rel=&quot;gallery-4&quot; href=&quot;http://arcangeles.org/club-deportivo-arcangeles/&quot; class=&quot;no-lightbox&quot;&gt;&lt;img  ...</errorSourceCode>
        
        <sequenceID>905_1336_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>1431</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;630&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2016/02/club-deportivo-u ...</errorSourceCode>
        
        <sequenceID>905_1431_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>905</lineNum>
      <columnNum>1431</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=7&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=7'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image used as anchor is missing valid Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;630&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2016/02/club-deportivo-u ...</errorSourceCode>
        <repair>Add Alt text that identifies the purpose or function of the image.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>1431</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;630&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2016/02/club-deportivo-u ...</errorSourceCode>
        
        <sequenceID>905_1431_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>1431</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;630&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2016/02/club-deportivo-u ...</errorSourceCode>
        
        <sequenceID>905_1431_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>1431</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=4&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=4'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image has suspicious Alt text (empty string &quot;&quot;).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;630&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2016/02/club-deportivo-u ...</errorSourceCode>
        
        <sequenceID>905_1431_4</sequenceID>
        <decisionPass>Image is decorative (null Alt text is OK).</decisionPass>
        <decisionFail>Image is not decorative (null Alt text is not OK).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>1431</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;630&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2016/02/club-deportivo-u ...</errorSourceCode>
        
        <sequenceID>905_1431_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>1431</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;630&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2016/02/club-deportivo-u ...</errorSourceCode>
        
        <sequenceID>905_1431_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>2589</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a rel=&quot;gallery-4&quot; href=&quot;http://sportpower2.org/&quot; target=&quot;_blank&quot; class=&quot;set-target no-lightbox&quot;&gt;&lt;im ...</errorSourceCode>
        
        <sequenceID>905_2589_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>905</lineNum>
      <columnNum>2589</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a rel=&quot;gallery-4&quot; href=&quot;http://sportpower2.org/&quot; target=&quot;_blank&quot; class=&quot;set-target no-lightbox&quot;&gt;&lt;im ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>2589</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a rel=&quot;gallery-4&quot; href=&quot;http://sportpower2.org/&quot; target=&quot;_blank&quot; class=&quot;set-target no-lightbox&quot;&gt;&lt;im ...</errorSourceCode>
        
        <sequenceID>905_2589_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>2686</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;630&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2014/12/slides-05-940x63 ...</errorSourceCode>
        
        <sequenceID>905_2686_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>905</lineNum>
      <columnNum>2686</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=7&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=7'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image used as anchor is missing valid Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;630&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2014/12/slides-05-940x63 ...</errorSourceCode>
        <repair>Add Alt text that identifies the purpose or function of the image.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>2686</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;630&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2014/12/slides-05-940x63 ...</errorSourceCode>
        
        <sequenceID>905_2686_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>2686</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;630&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2014/12/slides-05-940x63 ...</errorSourceCode>
        
        <sequenceID>905_2686_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>2686</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=4&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=4'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image has suspicious Alt text (empty string &quot;&quot;).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;630&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2014/12/slides-05-940x63 ...</errorSourceCode>
        
        <sequenceID>905_2686_4</sequenceID>
        <decisionPass>Image is decorative (null Alt text is OK).</decisionPass>
        <decisionFail>Image is not decorative (null Alt text is not OK).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>2686</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;630&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2014/12/slides-05-940x63 ...</errorSourceCode>
        
        <sequenceID>905_2686_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>905</lineNum>
      <columnNum>2686</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;940&quot; height=&quot;630&quot; src=&quot;http://arcangeles.org/wp-content/uploads/2014/12/slides-05-940x63 ...</errorSourceCode>
        
        <sequenceID>905_2686_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>910</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=43&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=43'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h2&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h2 style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;Aliados&lt;/strong&gt;&lt;/h2&gt;</errorSourceCode>
        
        <sequenceID>910_1_43</sequenceID>
        <decisionPass>This &lt;code&gt;h2&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h2&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>912</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=84&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=84'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;ASCII art possibly missing a skipover link.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;pre&gt;&lt;div class=&quot;gs_logo_area slider1&quot;&gt;&lt;div class=&quot;gs_logo_container&quot; data-options=&#039;{&quot;mode&quot;:&quot;horizon ...</errorSourceCode>
        
        <sequenceID>912_1_84</sequenceID>
        <decisionPass>ASCII art has a skipover link.</decisionPass>
        <decisionFail>ASCII art does not have a skipover link.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>912</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=154&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=154'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;pre&lt;/code&gt; element may be misused to create tabular layout.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;pre&gt;&lt;div class=&quot;gs_logo_area slider1&quot;&gt;&lt;div class=&quot;gs_logo_container&quot; data-options=&#039;{&quot;mode&quot;:&quot;horizon ...</errorSourceCode>
        
        <sequenceID>912_1_154</sequenceID>
        <decisionPass>&lt;code&gt;pre&lt;/code&gt; is not used to create tabular layout.</decisionPass>
        <decisionFail>&lt;code&gt;pre&lt;/code&gt; is used to create tabular layout.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>920</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=269&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=269'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission data may not be presented to the user before final acceptance of an irreversable transaction.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form id=&quot;vozme_form_afc06142da03f8f3ef34a98958d98efb&quot; method=&quot;post&quot; name=&quot;vozme_form_afc06142da03f8 ...</errorSourceCode>
        
        <sequenceID>920_1_269</sequenceID>
        <decisionPass>All form submission data is presented to the user before final acceptance for all irreversable transactions.</decisionPass>
        <decisionFail>All form submission data is not presented to the user before final acceptance for all irreversable transactions.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>920</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=265&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=265'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Tab order may not follow logical order.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form id=&quot;vozme_form_afc06142da03f8f3ef34a98958d98efb&quot; method=&quot;post&quot; name=&quot;vozme_form_afc06142da03f8 ...</errorSourceCode>
        
        <sequenceID>920_1_265</sequenceID>
        <decisionPass>Tab order follows a logical order.</decisionPass>
        <decisionFail>Tab order does not follow a logical order.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>920</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=268&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=268'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission error messages may not provide assistance.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form id=&quot;vozme_form_afc06142da03f8f3ef34a98958d98efb&quot; method=&quot;post&quot; name=&quot;vozme_form_afc06142da03f8 ...</errorSourceCode>
        
        <sequenceID>920_1_268</sequenceID>
        <decisionPass>All form submission error messages provide assistance in correcting the error.</decisionPass>
        <decisionFail>All form submission error messages do not provide assistance in correcting the errors.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>920</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=267&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=267'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission error messages may not identify empty required fields.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form id=&quot;vozme_form_afc06142da03f8f3ef34a98958d98efb&quot; method=&quot;post&quot; name=&quot;vozme_form_afc06142da03f8 ...</errorSourceCode>
        
        <sequenceID>920_1_267</sequenceID>
        <decisionPass>Required fields are identified in all form submission error messages.</decisionPass>
        <decisionFail>Required fields are not identified in all form submission error messages.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>920</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=272&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=272'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form may delete information without allowing for recovery.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form id=&quot;vozme_form_afc06142da03f8f3ef34a98958d98efb&quot; method=&quot;post&quot; name=&quot;vozme_form_afc06142da03f8 ...</errorSourceCode>
        
        <sequenceID>920_1_272</sequenceID>
        <decisionPass>Information deleted using this form can be recovered.</decisionPass>
        <decisionFail>Information delted using this form can not be recovered.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>920</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=246&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=246'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;All required &lt;code&gt;form&lt;/code&gt; fields may not be indicated as required.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form id=&quot;vozme_form_afc06142da03f8f3ef34a98958d98efb&quot; method=&quot;post&quot; name=&quot;vozme_form_afc06142da03f8 ...</errorSourceCode>
        
        <sequenceID>920_1_246</sequenceID>
        <decisionPass>All required fields are indicated to the user as required.</decisionPass>
        <decisionFail>All required fields are not indicated to the user as required.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>920</lineNum>
      <columnNum>204</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=189&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=189'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;label&lt;/code&gt; may not describe its associated control.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input name=&quot;text&quot; type=&quot;hidden&quot; value=&quot;MAXIMUS PROJECT. [content]
[section]
[columns]
[column width ...</errorSourceCode>
        
        <sequenceID>920_204_189</sequenceID>
        <decisionPass>&lt;code&gt;label&lt;/code&gt; describes its associated control.</decisionPass>
        <decisionFail>&lt;code&gt;label&lt;/code&gt; does not describe its associated control.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>960</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=189&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=189'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;label&lt;/code&gt; may not describe its associated control.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input name=&quot;lang&quot; type=&quot;hidden&quot; value=&quot;es&quot; /&gt;</errorSourceCode>
        
        <sequenceID>960_5_189</sequenceID>
        <decisionPass>&lt;code&gt;label&lt;/code&gt; describes its associated control.</decisionPass>
        <decisionFail>&lt;code&gt;label&lt;/code&gt; does not describe its associated control.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>960</lineNum>
      <columnNum>51</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=189&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=189'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;label&lt;/code&gt; may not describe its associated control.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input name=&quot;gn&quot; type=&quot;hidden&quot; value=&quot;fm&quot; /&gt;</errorSourceCode>
        
        <sequenceID>960_51_189</sequenceID>
        <decisionPass>&lt;code&gt;label&lt;/code&gt; describes its associated control.</decisionPass>
        <decisionFail>&lt;code&gt;label&lt;/code&gt; does not describe its associated control.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>960</lineNum>
      <columnNum>95</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=189&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=189'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;label&lt;/code&gt; may not describe its associated control.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;hidden&quot; id=&quot;interface&quot; name=&quot;interface&quot; value=&quot;full&quot; /&gt;</errorSourceCode>
        
        <sequenceID>960_95_189</sequenceID>
        <decisionPass>&lt;code&gt;label&lt;/code&gt; describes its associated control.</decisionPass>
        <decisionFail>&lt;code&gt;label&lt;/code&gt; does not describe its associated control.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>963</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=59&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=59'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image used for &lt;code&gt;input&lt;/code&gt; element may have Alt text that does not identify the purpose or function as the image.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input style=&quot;float:left;&quot; type=&quot;image&quot; width=&quot;32&quot; height=&quot;32&quot; src=&quot;http://arcangeles.org/wp-content ...</errorSourceCode>
        
        <sequenceID>963_5_59</sequenceID>
        <decisionPass>Alt text identifies the purpose or function of the image.</decisionPass>
        <decisionFail>Alt text does not identify the purpose or function of the image.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>963</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=193&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=193'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submit button image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input style=&quot;float:left;&quot; type=&quot;image&quot; width=&quot;32&quot; height=&quot;32&quot; src=&quot;http://arcangeles.org/wp-content ...</errorSourceCode>
        
        <sequenceID>963_5_193</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative.</decisionPass>
        <decisionFail>Image contains text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>963</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=188&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=188'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Label text is empty.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input style=&quot;float:left;&quot; type=&quot;image&quot; width=&quot;32&quot; height=&quot;32&quot; src=&quot;http://arcangeles.org/wp-content ...</errorSourceCode>
        <repair>Add text to the &lt;code&gt;label&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>963</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=55&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=55'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;input&lt;/code&gt; possibly using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input style=&quot;float:left;&quot; type=&quot;image&quot; width=&quot;32&quot; height=&quot;32&quot; src=&quot;http://arcangeles.org/wp-content ...</errorSourceCode>
        
        <sequenceID>963_5_55</sequenceID>
        <decisionPass>&lt;code&gt;input&lt;/code&gt; element does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;input&lt;/code&gt; element uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>963</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input style=&quot;float:left;&quot; type=&quot;image&quot; width=&quot;32&quot; height=&quot;32&quot; src=&quot;http://arcangeles.org/wp-content ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>964</lineNum>
      <columnNum>53</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a style=&quot;font-size:12px;&quot; href=&quot;javascript:void(0);&quot; onclick=&quot;window.open(&#039;&#039;, &#039;afc06142da03f8f3ef34 ...</errorSourceCode>
        
        <sequenceID>964_53_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>964</lineNum>
      <columnNum>53</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a style=&quot;font-size:12px;&quot; href=&quot;javascript:void(0);&quot; onclick=&quot;window.open(&#039;&#039;, &#039;afc06142da03f8f3ef34 ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>964</lineNum>
      <columnNum>53</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a style=&quot;font-size:12px;&quot; href=&quot;javascript:void(0);&quot; onclick=&quot;window.open(&#039;&#039;, &#039;afc06142da03f8f3ef34 ...</errorSourceCode>
        
        <sequenceID>964_53_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>964</lineNum>
      <columnNum>53</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a style=&quot;font-size:12px;&quot; href=&quot;javascript:void(0);&quot; onclick=&quot;window.open(&#039;&#039;, &#039;afc06142da03f8f3ef34 ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>983</lineNum>
      <columnNum>128</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a data-fancybox-title=&quot;&quot; class=&quot;zoom-hover fb&quot; href=&quot;http://web.arcangeles.org/wp-content/uploads/2 ...</errorSourceCode>
        
        <sequenceID>983_128_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>983</lineNum>
      <columnNum>128</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a data-fancybox-title=&quot;&quot; class=&quot;zoom-hover fb&quot; href=&quot;http://web.arcangeles.org/wp-content/uploads/2 ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>983</lineNum>
      <columnNum>128</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a data-fancybox-title=&quot;&quot; class=&quot;zoom-hover fb&quot; href=&quot;http://web.arcangeles.org/wp-content/uploads/2 ...</errorSourceCode>
        
        <sequenceID>983_128_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>983</lineNum>
      <columnNum>264</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot; wp-image-752 size-full &quot; src=&quot;http://web.arcangeles.org/wp-content/uploads/2015/01/logo ...</errorSourceCode>
        
        <sequenceID>983_264_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>983</lineNum>
      <columnNum>264</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot; wp-image-752 size-full &quot; src=&quot;http://web.arcangeles.org/wp-content/uploads/2015/01/logo ...</errorSourceCode>
        
        <sequenceID>983_264_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>983</lineNum>
      <columnNum>264</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot; wp-image-752 size-full &quot; src=&quot;http://web.arcangeles.org/wp-content/uploads/2015/01/logo ...</errorSourceCode>
        
        <sequenceID>983_264_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>983</lineNum>
      <columnNum>264</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot; wp-image-752 size-full &quot; src=&quot;http://web.arcangeles.org/wp-content/uploads/2015/01/logo ...</errorSourceCode>
        
        <sequenceID>983_264_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>983</lineNum>
      <columnNum>264</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot; wp-image-752 size-full &quot; src=&quot;http://web.arcangeles.org/wp-content/uploads/2015/01/logo ...</errorSourceCode>
        
        <sequenceID>983_264_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>983</lineNum>
      <columnNum>264</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img class=&quot; wp-image-752 size-full &quot; src=&quot;http://web.arcangeles.org/wp-content/uploads/2015/01/logo ...</errorSourceCode>
        
        <sequenceID>983_264_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>984</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=269&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=269'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission data may not be presented to the user before final acceptance of an irreversable transaction.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form id=&quot;vozme_form_afc06142da03f8f3ef34a98958d98efb&quot; method=&quot;post&quot; name=&quot;vozme_form_afc06142da03f8 ...</errorSourceCode>
        
        <sequenceID>984_1_269</sequenceID>
        <decisionPass>All form submission data is presented to the user before final acceptance for all irreversable transactions.</decisionPass>
        <decisionFail>All form submission data is not presented to the user before final acceptance for all irreversable transactions.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>984</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=265&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=265'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Tab order may not follow logical order.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form id=&quot;vozme_form_afc06142da03f8f3ef34a98958d98efb&quot; method=&quot;post&quot; name=&quot;vozme_form_afc06142da03f8 ...</errorSourceCode>
        
        <sequenceID>984_1_265</sequenceID>
        <decisionPass>Tab order follows a logical order.</decisionPass>
        <decisionFail>Tab order does not follow a logical order.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>984</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=268&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=268'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission error messages may not provide assistance.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form id=&quot;vozme_form_afc06142da03f8f3ef34a98958d98efb&quot; method=&quot;post&quot; name=&quot;vozme_form_afc06142da03f8 ...</errorSourceCode>
        
        <sequenceID>984_1_268</sequenceID>
        <decisionPass>All form submission error messages provide assistance in correcting the error.</decisionPass>
        <decisionFail>All form submission error messages do not provide assistance in correcting the errors.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>984</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=267&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=267'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission error messages may not identify empty required fields.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form id=&quot;vozme_form_afc06142da03f8f3ef34a98958d98efb&quot; method=&quot;post&quot; name=&quot;vozme_form_afc06142da03f8 ...</errorSourceCode>
        
        <sequenceID>984_1_267</sequenceID>
        <decisionPass>Required fields are identified in all form submission error messages.</decisionPass>
        <decisionFail>Required fields are not identified in all form submission error messages.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>984</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=272&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=272'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form may delete information without allowing for recovery.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form id=&quot;vozme_form_afc06142da03f8f3ef34a98958d98efb&quot; method=&quot;post&quot; name=&quot;vozme_form_afc06142da03f8 ...</errorSourceCode>
        
        <sequenceID>984_1_272</sequenceID>
        <decisionPass>Information deleted using this form can be recovered.</decisionPass>
        <decisionFail>Information delted using this form can not be recovered.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>984</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=246&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=246'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;All required &lt;code&gt;form&lt;/code&gt; fields may not be indicated as required.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form id=&quot;vozme_form_afc06142da03f8f3ef34a98958d98efb&quot; method=&quot;post&quot; name=&quot;vozme_form_afc06142da03f8 ...</errorSourceCode>
        
        <sequenceID>984_1_246</sequenceID>
        <decisionPass>All required fields are indicated to the user as required.</decisionPass>
        <decisionFail>All required fields are not indicated to the user as required.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>984</lineNum>
      <columnNum>204</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=189&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=189'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;label&lt;/code&gt; may not describe its associated control.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input name=&quot;text&quot; type=&quot;hidden&quot; value=&quot;Pacto Productividad. 
&quot; /&gt;</errorSourceCode>
        
        <sequenceID>984_204_189</sequenceID>
        <decisionPass>&lt;code&gt;label&lt;/code&gt; describes its associated control.</decisionPass>
        <decisionFail>&lt;code&gt;label&lt;/code&gt; does not describe its associated control.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>985</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=189&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=189'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;label&lt;/code&gt; may not describe its associated control.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input name=&quot;lang&quot; type=&quot;hidden&quot; value=&quot;es&quot; /&gt;</errorSourceCode>
        
        <sequenceID>985_5_189</sequenceID>
        <decisionPass>&lt;code&gt;label&lt;/code&gt; describes its associated control.</decisionPass>
        <decisionFail>&lt;code&gt;label&lt;/code&gt; does not describe its associated control.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>985</lineNum>
      <columnNum>51</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=189&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=189'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;label&lt;/code&gt; may not describe its associated control.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input name=&quot;gn&quot; type=&quot;hidden&quot; value=&quot;fm&quot; /&gt;</errorSourceCode>
        
        <sequenceID>985_51_189</sequenceID>
        <decisionPass>&lt;code&gt;label&lt;/code&gt; describes its associated control.</decisionPass>
        <decisionFail>&lt;code&gt;label&lt;/code&gt; does not describe its associated control.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>985</lineNum>
      <columnNum>95</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=189&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=189'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;label&lt;/code&gt; may not describe its associated control.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;hidden&quot; id=&quot;interface&quot; name=&quot;interface&quot; value=&quot;full&quot; /&gt;</errorSourceCode>
        
        <sequenceID>985_95_189</sequenceID>
        <decisionPass>&lt;code&gt;label&lt;/code&gt; describes its associated control.</decisionPass>
        <decisionFail>&lt;code&gt;label&lt;/code&gt; does not describe its associated control.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>988</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=59&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=59'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image used for &lt;code&gt;input&lt;/code&gt; element may have Alt text that does not identify the purpose or function as the image.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input style=&quot;float:left;&quot; type=&quot;image&quot; width=&quot;32&quot; height=&quot;32&quot; src=&quot;http://arcangeles.org/wp-content ...</errorSourceCode>
        
        <sequenceID>988_5_59</sequenceID>
        <decisionPass>Alt text identifies the purpose or function of the image.</decisionPass>
        <decisionFail>Alt text does not identify the purpose or function of the image.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>988</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=193&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=193'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submit button image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input style=&quot;float:left;&quot; type=&quot;image&quot; width=&quot;32&quot; height=&quot;32&quot; src=&quot;http://arcangeles.org/wp-content ...</errorSourceCode>
        
        <sequenceID>988_5_193</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative.</decisionPass>
        <decisionFail>Image contains text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>988</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=188&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=188'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Label text is empty.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input style=&quot;float:left;&quot; type=&quot;image&quot; width=&quot;32&quot; height=&quot;32&quot; src=&quot;http://arcangeles.org/wp-content ...</errorSourceCode>
        <repair>Add text to the &lt;code&gt;label&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>988</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=55&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=55'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;input&lt;/code&gt; possibly using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input style=&quot;float:left;&quot; type=&quot;image&quot; width=&quot;32&quot; height=&quot;32&quot; src=&quot;http://arcangeles.org/wp-content ...</errorSourceCode>
        
        <sequenceID>988_5_55</sequenceID>
        <decisionPass>&lt;code&gt;input&lt;/code&gt; element does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;input&lt;/code&gt; element uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>988</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input style=&quot;float:left;&quot; type=&quot;image&quot; width=&quot;32&quot; height=&quot;32&quot; src=&quot;http://arcangeles.org/wp-content ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>989</lineNum>
      <columnNum>53</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a style=&quot;font-size:12px;&quot; href=&quot;javascript:void(0);&quot; onclick=&quot;window.open(&#039;&#039;, &#039;afc06142da03f8f3ef34 ...</errorSourceCode>
        
        <sequenceID>989_53_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>989</lineNum>
      <columnNum>53</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a style=&quot;font-size:12px;&quot; href=&quot;javascript:void(0);&quot; onclick=&quot;window.open(&#039;&#039;, &#039;afc06142da03f8f3ef34 ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>989</lineNum>
      <columnNum>53</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a style=&quot;font-size:12px;&quot; href=&quot;javascript:void(0);&quot; onclick=&quot;window.open(&#039;&#039;, &#039;afc06142da03f8f3ef34 ...</errorSourceCode>
        
        <sequenceID>989_53_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>989</lineNum>
      <columnNum>53</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=102&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=102'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;SCRIPT not keyboard accessible - &lt;code&gt;onclick&lt;/code&gt; missing &lt;code&gt;onkeypress&lt;/code&gt;.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a style=&quot;font-size:12px;&quot; href=&quot;javascript:void(0);&quot; onclick=&quot;window.open(&#039;&#039;, &#039;afc06142da03f8f3ef34 ...</errorSourceCode>
        <repair>Add a &lt;code&gt;onkeypress&lt;/code&gt; handler to your SCRIPT that performs the same function as the &lt;code&gt;onclick&lt;/code&gt; function.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>997</lineNum>
      <columnNum>61</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.facebook.com/fundacionarcangeles&quot; target=&quot;_blank&quot;&gt;&lt;i class=&quot;icon-facebook&quot;&gt;&lt;/i&gt; ...</errorSourceCode>
        
        <sequenceID>997_61_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>997</lineNum>
      <columnNum>61</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.facebook.com/fundacionarcangeles&quot; target=&quot;_blank&quot;&gt;&lt;i class=&quot;icon-facebook&quot;&gt;&lt;/i&gt; ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>997</lineNum>
      <columnNum>61</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.facebook.com/fundacionarcangeles&quot; target=&quot;_blank&quot;&gt;&lt;i class=&quot;icon-facebook&quot;&gt;&lt;/i&gt; ...</errorSourceCode>
        
        <sequenceID>997_61_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>997</lineNum>
      <columnNum>132</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;icon-facebook&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>997</lineNum>
      <columnNum>174</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/FundArcangeles&quot; target=&quot;_blank&quot;&gt;&lt;i class=&quot;icon-twitter&quot;&gt;&lt;/i&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>997_174_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>997</lineNum>
      <columnNum>174</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/FundArcangeles&quot; target=&quot;_blank&quot;&gt;&lt;i class=&quot;icon-twitter&quot;&gt;&lt;/i&gt;&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>997</lineNum>
      <columnNum>174</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/FundArcangeles&quot; target=&quot;_blank&quot;&gt;&lt;i class=&quot;icon-twitter&quot;&gt;&lt;/i&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>997_174_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>997</lineNum>
      <columnNum>234</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;icon-twitter&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>997</lineNum>
      <columnNum>275</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.youtube.com/channel/UCV0RKFEOHTdJGQEDyglNHzg&quot; target=&quot;_blank&quot;&gt;&lt;i class=&quot;icon-yo ...</errorSourceCode>
        
        <sequenceID>997_275_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>997</lineNum>
      <columnNum>275</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.youtube.com/channel/UCV0RKFEOHTdJGQEDyglNHzg&quot; target=&quot;_blank&quot;&gt;&lt;i class=&quot;icon-yo ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>997</lineNum>
      <columnNum>275</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://www.youtube.com/channel/UCV0RKFEOHTdJGQEDyglNHzg&quot; target=&quot;_blank&quot;&gt;&lt;i class=&quot;icon-yo ...</errorSourceCode>
        
        <sequenceID>997_275_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>997</lineNum>
      <columnNum>358</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;icon-youtube&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>999</lineNum>
      <columnNum>93</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;icon-twitter&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>206</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/#!/search?q=%23Fundaci&oacute;nArc&aacute;ngeles&quot; title=&quot;#Fundaci&oacute;nArc&aacute;ngeles&quot;&gt;#Fu ...</errorSourceCode>
        
        <sequenceID>999_206_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>206</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/#!/search?q=%23Fundaci&oacute;nArc&aacute;ngeles&quot; title=&quot;#Fundaci&oacute;nArc&aacute;ngeles&quot;&gt;#Fu ...</errorSourceCode>
        
        <sequenceID>999_206_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>206</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/#!/search?q=%23Fundaci&oacute;nArc&aacute;ngeles&quot; title=&quot;#Fundaci&oacute;nArc&aacute;ngeles&quot;&gt;#Fu ...</errorSourceCode>
        
        <sequenceID>999_206_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>330</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://t.co/QsefEaBYlk&quot; title=&quot;https://twitter.com/CanalCapital/status/1125899360489943045 ...</errorSourceCode>
        
        <sequenceID>999_330_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>330</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://t.co/QsefEaBYlk&quot; title=&quot;https://twitter.com/CanalCapital/status/1125899360489943045 ...</errorSourceCode>
        
        <sequenceID>999_330_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>330</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://t.co/QsefEaBYlk&quot; title=&quot;https://twitter.com/CanalCapital/status/1125899360489943045 ...</errorSourceCode>
        
        <sequenceID>999_330_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>490</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/FundArcangeles/status/1129067106039943168&quot;&gt;hace 4 d&iacute;as&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>999_490_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>999</lineNum>
      <columnNum>490</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/FundArcangeles/status/1129067106039943168&quot;&gt;hace 4 d&iacute;as&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>490</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/FundArcangeles/status/1129067106039943168&quot;&gt;hace 4 d&iacute;as&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>999_490_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>999</lineNum>
      <columnNum>594</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;icon-twitter&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>687</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/#!/search?q=%23disCAPACIDAD&quot; title=&quot;#disCAPACIDAD&quot;&gt;#disCAPACIDAD&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>999_687_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>687</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/#!/search?q=%23disCAPACIDAD&quot; title=&quot;#disCAPACIDAD&quot;&gt;#disCAPACIDAD&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>999_687_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>687</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/#!/search?q=%23disCAPACIDAD&quot; title=&quot;#disCAPACIDAD&quot;&gt;#disCAPACIDAD&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>999_687_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>784</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/#!/search?q=%23Inclusi&oacute;n&quot; title=&quot;#Inclusi&oacute;n&quot;&gt;#Inclusi&oacute;n&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>999_784_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>784</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/#!/search?q=%23Inclusi&oacute;n&quot; title=&quot;#Inclusi&oacute;n&quot;&gt;#Inclusi&oacute;n&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>999_784_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>784</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/#!/search?q=%23Inclusi&oacute;n&quot; title=&quot;#Inclusi&oacute;n&quot;&gt;#Inclusi&oacute;n&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>999_784_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>875</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://t.co/a5yTT8sEHr&quot; title=&quot;http://tinyurl.com/y3dn7l8u&quot;&gt;tinyurl.com/y3dn7l8u&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>999_875_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>875</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://t.co/a5yTT8sEHr&quot; title=&quot;http://tinyurl.com/y3dn7l8u&quot;&gt;tinyurl.com/y3dn7l8u&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>999_875_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>875</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://t.co/a5yTT8sEHr&quot; title=&quot;http://tinyurl.com/y3dn7l8u&quot;&gt;tinyurl.com/y3dn7l8u&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>999_875_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>975</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/#!/elespectador&quot;&gt;@elespectador&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>999_975_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>999</lineNum>
      <columnNum>975</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/#!/elespectador&quot;&gt;@elespectador&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>975</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/#!/elespectador&quot;&gt;@elespectador&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>999_975_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>1062</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/FundArcangeles/status/1127974614502535168&quot;&gt;hace 1 semana&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>999_1062_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>999</lineNum>
      <columnNum>1062</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/FundArcangeles/status/1127974614502535168&quot;&gt;hace 1 semana&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>1062</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://twitter.com/FundArcangeles/status/1127974614502535168&quot;&gt;hace 1 semana&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>999_1062_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>1171</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;button&quot; href=&quot;https://twitter.com/FundArcangeles&quot;&gt;follow me on twitter &amp;rsaquo;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>999_1171_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>999</lineNum>
      <columnNum>1171</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;button&quot; href=&quot;https://twitter.com/FundArcangeles&quot;&gt;follow me on twitter &amp;rsaquo;&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>999</lineNum>
      <columnNum>1171</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;button&quot; href=&quot;https://twitter.com/FundArcangeles&quot;&gt;follow me on twitter &amp;rsaquo;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>999_1171_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1029</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        jQuery(function($){ 
           
            $(&#039;.wc-tabs-wrapper&#039;).after($(&#039;#pro ...</errorSourceCode>
        
        <sequenceID>1029_5_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1029</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        jQuery(function($){ 
           
            $(&#039;.wc-tabs-wrapper&#039;).after($(&#039;#pro ...</errorSourceCode>
        
        <sequenceID>1029_5_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1029</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        jQuery(function($){ 
           
            $(&#039;.wc-tabs-wrapper&#039;).after($(&#039;#pro ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1029</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        jQuery(function($){ 
           
            $(&#039;.wc-tabs-wrapper&#039;).after($(&#039;#pro ...</errorSourceCode>
        
        <sequenceID>1029_5_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1029</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        jQuery(function($){ 
           
            $(&#039;.wc-tabs-wrapper&#039;).after($(&#039;#pro ...</errorSourceCode>
        
        <sequenceID>1029_5_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1029</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
        jQuery(function($){ 
           
            $(&#039;.wc-tabs-wrapper&#039;).after($(&#039;#pro ...</errorSourceCode>
        
        <sequenceID>1029_5_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1075</lineNum>
      <columnNum>50</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i style=&quot;font-size: 30px&quot; class=&quot;fa fa-universal-access&quot;  &gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1116</lineNum>
      <columnNum>18</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a   id=&quot;sogo_accessibility&quot;
                    href=&quot;#&quot;&gt;&lt;i aria-hidden=&quot;true&quot; class=&quot;fa fa-times-c ...</errorSourceCode>
        
        <sequenceID>1116_18_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1116</lineNum>
      <columnNum>18</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a   id=&quot;sogo_accessibility&quot;
                    href=&quot;#&quot;&gt;&lt;i aria-hidden=&quot;true&quot; class=&quot;fa fa-times-c ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1116</lineNum>
      <columnNum>18</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a   id=&quot;sogo_accessibility&quot;
                    href=&quot;#&quot;&gt;&lt;i aria-hidden=&quot;true&quot; class=&quot;fa fa-times-c ...</errorSourceCode>
        
        <sequenceID>1116_18_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1117</lineNum>
      <columnNum>30</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i aria-hidden=&quot;true&quot; class=&quot;fa fa-times-circle-o red &quot; &gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1128</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a target=&quot;_blank&quot; href=&quot;https://pluginsmarket.com/downloads/accessibility-plugin/&quot;
               t ...</errorSourceCode>
        
        <sequenceID>1128_13_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1128</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a target=&quot;_blank&quot; href=&quot;https://pluginsmarket.com/downloads/accessibility-plugin/&quot;
               t ...</errorSourceCode>
        
        <sequenceID>1128_13_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1128</lineNum>
      <columnNum>13</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a target=&quot;_blank&quot; href=&quot;https://pluginsmarket.com/downloads/accessibility-plugin/&quot;
               t ...</errorSourceCode>
        
        <sequenceID>1128_13_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1131</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;http://arcangeles.org/wp-content/plugins/sogo-accessibility/public/css/sogo-logo.png&quot; alt= ...</errorSourceCode>
        
        <sequenceID>1131_17_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1131</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;http://arcangeles.org/wp-content/plugins/sogo-accessibility/public/css/sogo-logo.png&quot; alt= ...</errorSourceCode>
        
        <sequenceID>1131_17_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1131</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;http://arcangeles.org/wp-content/plugins/sogo-accessibility/public/css/sogo-logo.png&quot; alt= ...</errorSourceCode>
        
        <sequenceID>1131_17_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1131</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;http://arcangeles.org/wp-content/plugins/sogo-accessibility/public/css/sogo-logo.png&quot; alt= ...</errorSourceCode>
        
        <sequenceID>1131_17_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1131</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;http://arcangeles.org/wp-content/plugins/sogo-accessibility/public/css/sogo-logo.png&quot; alt= ...</errorSourceCode>
        
        <sequenceID>1131_17_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1131</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;http://arcangeles.org/wp-content/plugins/sogo-accessibility/public/css/sogo-logo.png&quot; alt= ...</errorSourceCode>
        
        <sequenceID>1131_17_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1143</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=43&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=43'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h2&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h2&gt;Subscribete&lt;/h2&gt;</errorSourceCode>
        
        <sequenceID>1143_7_43</sequenceID>
        <decisionPass>This &lt;code&gt;h2&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h2&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1151</lineNum>
      <columnNum>6</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=269&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=269'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission data may not be presented to the user before final acceptance of an irreversable transaction.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form method=&quot;post&quot; class=&quot;clearfix&quot;&gt;
						&lt;div class=&quot;et_bloom_fields&quot;&gt;
							
							&lt;p class=&quot;e ...</errorSourceCode>
        
        <sequenceID>1151_6_269</sequenceID>
        <decisionPass>All form submission data is presented to the user before final acceptance for all irreversable transactions.</decisionPass>
        <decisionFail>All form submission data is not presented to the user before final acceptance for all irreversable transactions.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1151</lineNum>
      <columnNum>6</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=265&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=265'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Tab order may not follow logical order.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form method=&quot;post&quot; class=&quot;clearfix&quot;&gt;
						&lt;div class=&quot;et_bloom_fields&quot;&gt;
							
							&lt;p class=&quot;e ...</errorSourceCode>
        
        <sequenceID>1151_6_265</sequenceID>
        <decisionPass>Tab order follows a logical order.</decisionPass>
        <decisionFail>Tab order does not follow a logical order.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1151</lineNum>
      <columnNum>6</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=268&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=268'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission error messages may not provide assistance.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form method=&quot;post&quot; class=&quot;clearfix&quot;&gt;
						&lt;div class=&quot;et_bloom_fields&quot;&gt;
							
							&lt;p class=&quot;e ...</errorSourceCode>
        
        <sequenceID>1151_6_268</sequenceID>
        <decisionPass>All form submission error messages provide assistance in correcting the error.</decisionPass>
        <decisionFail>All form submission error messages do not provide assistance in correcting the errors.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1151</lineNum>
      <columnNum>6</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=267&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=267'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission error messages may not identify empty required fields.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form method=&quot;post&quot; class=&quot;clearfix&quot;&gt;
						&lt;div class=&quot;et_bloom_fields&quot;&gt;
							
							&lt;p class=&quot;e ...</errorSourceCode>
        
        <sequenceID>1151_6_267</sequenceID>
        <decisionPass>Required fields are identified in all form submission error messages.</decisionPass>
        <decisionFail>Required fields are not identified in all form submission error messages.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1151</lineNum>
      <columnNum>6</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=272&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=272'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form may delete information without allowing for recovery.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form method=&quot;post&quot; class=&quot;clearfix&quot;&gt;
						&lt;div class=&quot;et_bloom_fields&quot;&gt;
							
							&lt;p class=&quot;e ...</errorSourceCode>
        
        <sequenceID>1151_6_272</sequenceID>
        <decisionPass>Information deleted using this form can be recovered.</decisionPass>
        <decisionFail>Information delted using this form can not be recovered.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1151</lineNum>
      <columnNum>6</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=246&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=246'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;All required &lt;code&gt;form&lt;/code&gt; fields may not be indicated as required.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form method=&quot;post&quot; class=&quot;clearfix&quot;&gt;
						&lt;div class=&quot;et_bloom_fields&quot;&gt;
							
							&lt;p class=&quot;e ...</errorSourceCode>
        
        <sequenceID>1151_6_246</sequenceID>
        <decisionPass>All required fields are indicated to the user as required.</decisionPass>
        <decisionFail>All required fields are not indicated to the user as required.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1155</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=188&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=188'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Label text is empty.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input placeholder=&quot;Email&quot;&gt;</errorSourceCode>
        <repair>Add text to the &lt;code&gt;label&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1155</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=55&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=55'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;input&lt;/code&gt; possibly using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input placeholder=&quot;Email&quot;&gt;</errorSourceCode>
        
        <sequenceID>1155_9_55</sequenceID>
        <decisionPass>&lt;code&gt;input&lt;/code&gt; element does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;input&lt;/code&gt; element uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1167</lineNum>
      <columnNum>6</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=43&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=43'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h2&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h2 class=&quot;et_bloom_success_message&quot;&gt;Gracias por suscribirte&lt;/h2&gt;</errorSourceCode>
        
        <sequenceID>1167_6_43</sequenceID>
        <decisionPass>This &lt;code&gt;h2&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h2&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1173</lineNum>
      <columnNum>14</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
		var c = document.body.className;
		c = c.replace(/woocommerce-no-j ...</errorSourceCode>
        
        <sequenceID>1173_14_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1173</lineNum>
      <columnNum>14</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
		var c = document.body.className;
		c = c.replace(/woocommerce-no-j ...</errorSourceCode>
        
        <sequenceID>1173_14_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1173</lineNum>
      <columnNum>14</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
		var c = document.body.className;
		c = c.replace(/woocommerce-no-j ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1173</lineNum>
      <columnNum>14</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
		var c = document.body.className;
		c = c.replace(/woocommerce-no-j ...</errorSourceCode>
        
        <sequenceID>1173_14_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1173</lineNum>
      <columnNum>14</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
		var c = document.body.className;
		c = c.replace(/woocommerce-no-j ...</errorSourceCode>
        
        <sequenceID>1173_14_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1173</lineNum>
      <columnNum>14</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
		var c = document.body.className;
		c = c.replace(/woocommerce-no-j ...</errorSourceCode>
        
        <sequenceID>1173_14_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1180</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var countVars = {&quot;disqusShortname&quot;:&quot;arcangeles&quot;};
/* ...</errorSourceCode>
        
        <sequenceID>1180_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1180</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var countVars = {&quot;disqusShortname&quot;:&quot;arcangeles&quot;};
/* ...</errorSourceCode>
        
        <sequenceID>1180_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1180</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var countVars = {&quot;disqusShortname&quot;:&quot;arcangeles&quot;};
/* ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1180</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var countVars = {&quot;disqusShortname&quot;:&quot;arcangeles&quot;};
/* ...</errorSourceCode>
        
        <sequenceID>1180_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1180</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var countVars = {&quot;disqusShortname&quot;:&quot;arcangeles&quot;};
/* ...</errorSourceCode>
        
        <sequenceID>1180_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1180</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var countVars = {&quot;disqusShortname&quot;:&quot;arcangeles&quot;};
/* ...</errorSourceCode>
        
        <sequenceID>1180_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1185</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/disqus-comment-system/p ...</errorSourceCode>
        
        <sequenceID>1185_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1185</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/disqus-comment-system/p ...</errorSourceCode>
        
        <sequenceID>1185_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1185</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/disqus-comment-system/p ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1185</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/disqus-comment-system/p ...</errorSourceCode>
        
        <sequenceID>1185_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1185</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/disqus-comment-system/p ...</errorSourceCode>
        
        <sequenceID>1185_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1185</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/disqus-comment-system/p ...</errorSourceCode>
        
        <sequenceID>1185_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1186</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/gs-logo-slider/gsl-file ...</errorSourceCode>
        
        <sequenceID>1186_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1186</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/gs-logo-slider/gsl-file ...</errorSourceCode>
        
        <sequenceID>1186_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1186</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/gs-logo-slider/gsl-file ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1186</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/gs-logo-slider/gsl-file ...</errorSourceCode>
        
        <sequenceID>1186_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1186</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/gs-logo-slider/gsl-file ...</errorSourceCode>
        
        <sequenceID>1186_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1186</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/gs-logo-slider/gsl-file ...</errorSourceCode>
        
        <sequenceID>1186_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1187</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/gs-logo-slider/gsl-file ...</errorSourceCode>
        
        <sequenceID>1187_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1187</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/gs-logo-slider/gsl-file ...</errorSourceCode>
        
        <sequenceID>1187_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1187</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/gs-logo-slider/gsl-file ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1187</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/gs-logo-slider/gsl-file ...</errorSourceCode>
        
        <sequenceID>1187_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1187</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/gs-logo-slider/gsl-file ...</errorSourceCode>
        
        <sequenceID>1187_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1187</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/gs-logo-slider/gsl-file ...</errorSourceCode>
        
        <sequenceID>1187_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1188</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/gs-logo-slider/gsl-file ...</errorSourceCode>
        
        <sequenceID>1188_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1188</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/gs-logo-slider/gsl-file ...</errorSourceCode>
        
        <sequenceID>1188_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1188</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/gs-logo-slider/gsl-file ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1188</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/gs-logo-slider/gsl-file ...</errorSourceCode>
        
        <sequenceID>1188_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1188</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/gs-logo-slider/gsl-file ...</errorSourceCode>
        
        <sequenceID>1188_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1188</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/gs-logo-slider/gsl-file ...</errorSourceCode>
        
        <sequenceID>1188_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1189</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/sogo-accessibility/publ ...</errorSourceCode>
        
        <sequenceID>1189_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1189</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/sogo-accessibility/publ ...</errorSourceCode>
        
        <sequenceID>1189_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1189</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/sogo-accessibility/publ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1189</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/sogo-accessibility/publ ...</errorSourceCode>
        
        <sequenceID>1189_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1189</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/sogo-accessibility/publ ...</errorSourceCode>
        
        <sequenceID>1189_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1189</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/sogo-accessibility/publ ...</errorSourceCode>
        
        <sequenceID>1189_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1190</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/sogo-accessibility/publ ...</errorSourceCode>
        
        <sequenceID>1190_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1190</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/sogo-accessibility/publ ...</errorSourceCode>
        
        <sequenceID>1190_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1190</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/sogo-accessibility/publ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1190</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/sogo-accessibility/publ ...</errorSourceCode>
        
        <sequenceID>1190_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1190</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/sogo-accessibility/publ ...</errorSourceCode>
        
        <sequenceID>1190_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1190</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/sogo-accessibility/publ ...</errorSourceCode>
        
        <sequenceID>1190_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1191</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/j ...</errorSourceCode>
        
        <sequenceID>1191_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1191</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/j ...</errorSourceCode>
        
        <sequenceID>1191_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1191</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/j ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1191</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/j ...</errorSourceCode>
        
        <sequenceID>1191_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1191</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/j ...</errorSourceCode>
        
        <sequenceID>1191_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1191</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/j ...</errorSourceCode>
        
        <sequenceID>1191_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1192</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var wc_add_to_cart_params = {&quot;ajax_url&quot;:&quot;\/wp-admin\ ...</errorSourceCode>
        
        <sequenceID>1192_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1192</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var wc_add_to_cart_params = {&quot;ajax_url&quot;:&quot;\/wp-admin\ ...</errorSourceCode>
        
        <sequenceID>1192_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1192</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var wc_add_to_cart_params = {&quot;ajax_url&quot;:&quot;\/wp-admin\ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1192</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var wc_add_to_cart_params = {&quot;ajax_url&quot;:&quot;\/wp-admin\ ...</errorSourceCode>
        
        <sequenceID>1192_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1192</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var wc_add_to_cart_params = {&quot;ajax_url&quot;:&quot;\/wp-admin\ ...</errorSourceCode>
        
        <sequenceID>1192_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1192</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var wc_add_to_cart_params = {&quot;ajax_url&quot;:&quot;\/wp-admin\ ...</errorSourceCode>
        
        <sequenceID>1192_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1197</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/f ...</errorSourceCode>
        
        <sequenceID>1197_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1197</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/f ...</errorSourceCode>
        
        <sequenceID>1197_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1197</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/f ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1197</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/f ...</errorSourceCode>
        
        <sequenceID>1197_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1197</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/f ...</errorSourceCode>
        
        <sequenceID>1197_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1197</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/f ...</errorSourceCode>
        
        <sequenceID>1197_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1198</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/j ...</errorSourceCode>
        
        <sequenceID>1198_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1198</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/j ...</errorSourceCode>
        
        <sequenceID>1198_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1198</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/j ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1198</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/j ...</errorSourceCode>
        
        <sequenceID>1198_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1198</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/j ...</errorSourceCode>
        
        <sequenceID>1198_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1198</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/j ...</errorSourceCode>
        
        <sequenceID>1198_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1199</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var woocommerce_params = {&quot;ajax_url&quot;:&quot;\/wp-admin\/ad ...</errorSourceCode>
        
        <sequenceID>1199_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1199</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var woocommerce_params = {&quot;ajax_url&quot;:&quot;\/wp-admin\/ad ...</errorSourceCode>
        
        <sequenceID>1199_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1199</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var woocommerce_params = {&quot;ajax_url&quot;:&quot;\/wp-admin\/ad ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1199</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var woocommerce_params = {&quot;ajax_url&quot;:&quot;\/wp-admin\/ad ...</errorSourceCode>
        
        <sequenceID>1199_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1199</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var woocommerce_params = {&quot;ajax_url&quot;:&quot;\/wp-admin\/ad ...</errorSourceCode>
        
        <sequenceID>1199_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1199</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var woocommerce_params = {&quot;ajax_url&quot;:&quot;\/wp-admin\/ad ...</errorSourceCode>
        
        <sequenceID>1199_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1204</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/f ...</errorSourceCode>
        
        <sequenceID>1204_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1204</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/f ...</errorSourceCode>
        
        <sequenceID>1204_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1204</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/f ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1204</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/f ...</errorSourceCode>
        
        <sequenceID>1204_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1204</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/f ...</errorSourceCode>
        
        <sequenceID>1204_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1204</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/f ...</errorSourceCode>
        
        <sequenceID>1204_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1205</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var wc_cart_fragments_params = {&quot;ajax_url&quot;:&quot;\/wp-adm ...</errorSourceCode>
        
        <sequenceID>1205_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1205</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var wc_cart_fragments_params = {&quot;ajax_url&quot;:&quot;\/wp-adm ...</errorSourceCode>
        
        <sequenceID>1205_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1205</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var wc_cart_fragments_params = {&quot;ajax_url&quot;:&quot;\/wp-adm ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1205</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var wc_cart_fragments_params = {&quot;ajax_url&quot;:&quot;\/wp-adm ...</errorSourceCode>
        
        <sequenceID>1205_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1205</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var wc_cart_fragments_params = {&quot;ajax_url&quot;:&quot;\/wp-adm ...</errorSourceCode>
        
        <sequenceID>1205_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1205</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var wc_cart_fragments_params = {&quot;ajax_url&quot;:&quot;\/wp-adm ...</errorSourceCode>
        
        <sequenceID>1205_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1210</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/f ...</errorSourceCode>
        
        <sequenceID>1210_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1210</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/f ...</errorSourceCode>
        
        <sequenceID>1210_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1210</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/f ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1210</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/f ...</errorSourceCode>
        
        <sequenceID>1210_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1210</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/f ...</errorSourceCode>
        
        <sequenceID>1210_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1210</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/woocommerce/assets/js/f ...</errorSourceCode>
        
        <sequenceID>1210_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1211</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
		jQuery( &#039;body&#039; ).bind( &#039;wc_fragments_refreshed&#039;, function() {
			j ...</errorSourceCode>
        
        <sequenceID>1211_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1211</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
		jQuery( &#039;body&#039; ).bind( &#039;wc_fragments_refreshed&#039;, function() {
			j ...</errorSourceCode>
        
        <sequenceID>1211_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1211</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
		jQuery( &#039;body&#039; ).bind( &#039;wc_fragments_refreshed&#039;, function() {
			j ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1211</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
		jQuery( &#039;body&#039; ).bind( &#039;wc_fragments_refreshed&#039;, function() {
			j ...</errorSourceCode>
        
        <sequenceID>1211_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1211</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
		jQuery( &#039;body&#039; ).bind( &#039;wc_fragments_refreshed&#039;, function() {
			j ...</errorSourceCode>
        
        <sequenceID>1211_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1211</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
		jQuery( &#039;body&#039; ).bind( &#039;wc_fragments_refreshed&#039;, function() {
			j ...</errorSourceCode>
        
        <sequenceID>1211_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1217</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/themes/time/data/js/3rd-party.m ...</errorSourceCode>
        
        <sequenceID>1217_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1217</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/themes/time/data/js/3rd-party.m ...</errorSourceCode>
        
        <sequenceID>1217_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1217</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/themes/time/data/js/3rd-party.m ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1217</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/themes/time/data/js/3rd-party.m ...</errorSourceCode>
        
        <sequenceID>1217_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1217</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/themes/time/data/js/3rd-party.m ...</errorSourceCode>
        
        <sequenceID>1217_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1217</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/themes/time/data/js/3rd-party.m ...</errorSourceCode>
        
        <sequenceID>1217_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1218</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/themes/time/data/js/time.min.js ...</errorSourceCode>
        
        <sequenceID>1218_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1218</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/themes/time/data/js/time.min.js ...</errorSourceCode>
        
        <sequenceID>1218_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1218</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/themes/time/data/js/time.min.js ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1218</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/themes/time/data/js/time.min.js ...</errorSourceCode>
        
        <sequenceID>1218_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1218</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/themes/time/data/js/time.min.js ...</errorSourceCode>
        
        <sequenceID>1218_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1218</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/themes/time/data/js/time.min.js ...</errorSourceCode>
        
        <sequenceID>1218_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1219</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var drone_social_media_api = {&quot;locale&quot;:&quot;es_ES&quot;};
/*  ...</errorSourceCode>
        
        <sequenceID>1219_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1219</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var drone_social_media_api = {&quot;locale&quot;:&quot;es_ES&quot;};
/*  ...</errorSourceCode>
        
        <sequenceID>1219_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1219</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var drone_social_media_api = {&quot;locale&quot;:&quot;es_ES&quot;};
/*  ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1219</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var drone_social_media_api = {&quot;locale&quot;:&quot;es_ES&quot;};
/*  ...</errorSourceCode>
        
        <sequenceID>1219_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1219</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var drone_social_media_api = {&quot;locale&quot;:&quot;es_ES&quot;};
/*  ...</errorSourceCode>
        
        <sequenceID>1219_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1219</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var drone_social_media_api = {&quot;locale&quot;:&quot;es_ES&quot;};
/*  ...</errorSourceCode>
        
        <sequenceID>1219_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1224</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/themes/time/drone/js/social-med ...</errorSourceCode>
        
        <sequenceID>1224_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1224</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/themes/time/drone/js/social-med ...</errorSourceCode>
        
        <sequenceID>1224_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1224</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/themes/time/drone/js/social-med ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1224</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/themes/time/drone/js/social-med ...</errorSourceCode>
        
        <sequenceID>1224_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1224</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/themes/time/drone/js/social-med ...</errorSourceCode>
        
        <sequenceID>1224_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1224</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/themes/time/drone/js/social-med ...</errorSourceCode>
        
        <sequenceID>1224_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1225</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/wp-gallery-custom-links ...</errorSourceCode>
        
        <sequenceID>1225_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1225</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/wp-gallery-custom-links ...</errorSourceCode>
        
        <sequenceID>1225_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1225</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/wp-gallery-custom-links ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1225</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/wp-gallery-custom-links ...</errorSourceCode>
        
        <sequenceID>1225_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1225</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/wp-gallery-custom-links ...</errorSourceCode>
        
        <sequenceID>1225_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1225</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/wp-gallery-custom-links ...</errorSourceCode>
        
        <sequenceID>1225_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1226</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1226_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1226</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1226_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1226</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1226</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1226_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1226</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1226_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1226</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1226_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1227</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1227_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1227</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1227_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1227</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1227</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1227_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1227</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1227_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1227</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1227_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1228</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1228_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1228</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1228_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1228</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1228</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1228_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1228</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1228_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1228</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1228_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1229</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var simcal_default_calendar = {&quot;ajax_url&quot;:&quot;\/wp-admi ...</errorSourceCode>
        
        <sequenceID>1229_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1229</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var simcal_default_calendar = {&quot;ajax_url&quot;:&quot;\/wp-admi ...</errorSourceCode>
        
        <sequenceID>1229_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1229</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var simcal_default_calendar = {&quot;ajax_url&quot;:&quot;\/wp-admi ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1229</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var simcal_default_calendar = {&quot;ajax_url&quot;:&quot;\/wp-admi ...</errorSourceCode>
        
        <sequenceID>1229_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1229</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var simcal_default_calendar = {&quot;ajax_url&quot;:&quot;\/wp-admi ...</errorSourceCode>
        
        <sequenceID>1229_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1229</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var simcal_default_calendar = {&quot;ajax_url&quot;:&quot;\/wp-admi ...</errorSourceCode>
        
        <sequenceID>1229_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1234</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1234_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1234</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1234_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1234</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1234</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1234_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1234</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1234_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1234</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1234_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1235</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/core/admin/js/com ...</errorSourceCode>
        
        <sequenceID>1235_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1235</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/core/admin/js/com ...</errorSourceCode>
        
        <sequenceID>1235_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1235</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/core/admin/js/com ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1235</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/core/admin/js/com ...</errorSourceCode>
        
        <sequenceID>1235_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1235</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/core/admin/js/com ...</errorSourceCode>
        
        <sequenceID>1235_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1235</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/core/admin/js/com ...</errorSourceCode>
        
        <sequenceID>1235_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1236</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1236_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1236</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1236_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1236</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1236</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1236_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1236</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1236_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1236</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/google-calendar-events/ ...</errorSourceCode>
        
        <sequenceID>1236_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1237</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-includes/js/wp-embed.min.js?ver=5.2&#039;&gt;&lt;/ ...</errorSourceCode>
        
        <sequenceID>1237_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1237</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-includes/js/wp-embed.min.js?ver=5.2&#039;&gt;&lt;/ ...</errorSourceCode>
        
        <sequenceID>1237_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1237</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-includes/js/wp-embed.min.js?ver=5.2&#039;&gt;&lt;/ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1237</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-includes/js/wp-embed.min.js?ver=5.2&#039;&gt;&lt;/ ...</errorSourceCode>
        
        <sequenceID>1237_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1237</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-includes/js/wp-embed.min.js?ver=5.2&#039;&gt;&lt;/ ...</errorSourceCode>
        
        <sequenceID>1237_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1237</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-includes/js/wp-embed.min.js?ver=5.2&#039;&gt;&lt;/ ...</errorSourceCode>
        
        <sequenceID>1237_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1238</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/js/jquery.uniform ...</errorSourceCode>
        
        <sequenceID>1238_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1238</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/js/jquery.uniform ...</errorSourceCode>
        
        <sequenceID>1238_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1238</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/js/jquery.uniform ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1238</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/js/jquery.uniform ...</errorSourceCode>
        
        <sequenceID>1238_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1238</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/js/jquery.uniform ...</errorSourceCode>
        
        <sequenceID>1238_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1238</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/js/jquery.uniform ...</errorSourceCode>
        
        <sequenceID>1238_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1239</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var bloomSettings = {&quot;ajaxurl&quot;:&quot;http:\/\/arcangeles. ...</errorSourceCode>
        
        <sequenceID>1239_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1239</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var bloomSettings = {&quot;ajaxurl&quot;:&quot;http:\/\/arcangeles. ...</errorSourceCode>
        
        <sequenceID>1239_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1239</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var bloomSettings = {&quot;ajaxurl&quot;:&quot;http:\/\/arcangeles. ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1239</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var bloomSettings = {&quot;ajaxurl&quot;:&quot;http:\/\/arcangeles. ...</errorSourceCode>
        
        <sequenceID>1239_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1239</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var bloomSettings = {&quot;ajaxurl&quot;:&quot;http:\/\/arcangeles. ...</errorSourceCode>
        
        <sequenceID>1239_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1239</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var bloomSettings = {&quot;ajaxurl&quot;:&quot;http:\/\/arcangeles. ...</errorSourceCode>
        
        <sequenceID>1239_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1244</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/js/custom.js?ver= ...</errorSourceCode>
        
        <sequenceID>1244_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1244</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/js/custom.js?ver= ...</errorSourceCode>
        
        <sequenceID>1244_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1244</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/js/custom.js?ver= ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1244</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/js/custom.js?ver= ...</errorSourceCode>
        
        <sequenceID>1244_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1244</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/js/custom.js?ver= ...</errorSourceCode>
        
        <sequenceID>1244_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1244</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/js/custom.js?ver= ...</errorSourceCode>
        
        <sequenceID>1244_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1245</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/js/idle-timer.min ...</errorSourceCode>
        
        <sequenceID>1245_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>1245</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/js/idle-timer.min ...</errorSourceCode>
        
        <sequenceID>1245_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>1245</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/js/idle-timer.min ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1245</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/js/idle-timer.min ...</errorSourceCode>
        
        <sequenceID>1245_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1245</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/js/idle-timer.min ...</errorSourceCode>
        
        <sequenceID>1245_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>1245</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://arcangeles.org/wp-content/plugins/bloom/js/idle-timer.min ...</errorSourceCode>
        
        <sequenceID>1245_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 

  </results>
</resultset>
