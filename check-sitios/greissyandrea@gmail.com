<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE resultset[
<!ELEMENT resultset (summary,results)>
<!ELEMENT summary (status,sessionID,NumOfErrors,NumOfLikelyProblems,NumOfPotentialProblems,guidelines)>
<!ELEMENT status (#PCDATA)>
<!ELEMENT sessionID (#PCDATA)>
<!ELEMENT NumOfErrors (#PCDATA)>
<!ELEMENT NumOfLikelyProblems (#PCDATA)>
<!ELEMENT NumOfPotentialProblems (#PCDATA)>
<!ELEMENT guidelines (guideline)*>
<!ELEMENT guideline (#PCDATA)>
<!ELEMENT results (result)*>
<!ELEMENT result (resultType,lineNum,columnNum,errorMsg,errorSourceCode,repair*,sequenceID*,decisionPass*,decisionFail*,decisionMade*,decisionMadeDate*)>
<!ELEMENT resultType (#PCDATA)>
<!ELEMENT lineNum (#PCDATA)>
<!ELEMENT columnNum (#PCDATA)>
<!ELEMENT errorMsg (#PCDATA)>
<!ELEMENT errorSourceCode (#PCDATA)>
<!ELEMENT repair (#PCDATA)>
<!ELEMENT sequenceID (#PCDATA)>
<!ELEMENT decisionPass (#PCDATA)>
<!ELEMENT decisionFail (#PCDATA)>
<!ELEMENT decisionMade (#PCDATA)>
<!ELEMENT decisionMadeDate (#PCDATA)>
<!ENTITY lt "&#38;#60;">
<!ENTITY gt "&#62;">
<!ENTITY amp "&#38;#38;">
<!ENTITY apos "&#39;">
<!ENTITY quot "&#34;">
<!ENTITY ndash "&#8211;">
]>
<resultset>
  <summary>
    <status>FAIL</status>
    <sessionID>7732ea8ddcdf167189fd0f5f4dea1043754b60e6</sessionID>
    <NumOfErrors>26</NumOfErrors>
    <NumOfLikelyProblems>15</NumOfLikelyProblems>
    <NumOfPotentialProblems>146</NumOfPotentialProblems>

    <guidelines>
      <guideline>Stanca Act</guideline>
      <guideline>WCAG 2.0 (Level AA)</guideline>

    </guidelines>
  </summary>

  <results>
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>4</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=54&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=54'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;title&lt;/code&gt; might not describe the document.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;title&gt;PLANETA RADIO ESTACI&Oacute;N WEB | &iexcl;Girando al ritmo de tu vida!&lt;/title&gt;</errorSourceCode>
        
        <sequenceID>4_2_54</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the document.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the document.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>15</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
			window._wpemojiSettings = {&quot;baseUrl&quot;:&quot;https:\/\/s.w.org\/images\/ ...</errorSourceCode>
        
        <sequenceID>15_3_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>15</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
			window._wpemojiSettings = {&quot;baseUrl&quot;:&quot;https:\/\/s.w.org\/images\/ ...</errorSourceCode>
        
        <sequenceID>15_3_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>15</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
			window._wpemojiSettings = {&quot;baseUrl&quot;:&quot;https:\/\/s.w.org\/images\/ ...</errorSourceCode>
        
        <sequenceID>15_3_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>15</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
			window._wpemojiSettings = {&quot;baseUrl&quot;:&quot;https:\/\/s.w.org\/images\/ ...</errorSourceCode>
        
        <sequenceID>15_3_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>15</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
			window._wpemojiSettings = {&quot;baseUrl&quot;:&quot;https:\/\/s.w.org\/images\/ ...</errorSourceCode>
        
        <sequenceID>15_3_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>42</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-includes/js/jquery/jquery.js?ver=1.12. ...</errorSourceCode>
        
        <sequenceID>42_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>42</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-includes/js/jquery/jquery.js?ver=1.12. ...</errorSourceCode>
        
        <sequenceID>42_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>42</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-includes/js/jquery/jquery.js?ver=1.12. ...</errorSourceCode>
        
        <sequenceID>42_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>42</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-includes/js/jquery/jquery.js?ver=1.12. ...</errorSourceCode>
        
        <sequenceID>42_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>42</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-includes/js/jquery/jquery.js?ver=1.12. ...</errorSourceCode>
        
        <sequenceID>42_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>43</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-includes/js/jquery/jquery-migrate.min. ...</errorSourceCode>
        
        <sequenceID>43_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>43</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-includes/js/jquery/jquery-migrate.min. ...</errorSourceCode>
        
        <sequenceID>43_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>43</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-includes/js/jquery/jquery-migrate.min. ...</errorSourceCode>
        
        <sequenceID>43_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>43</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-includes/js/jquery/jquery-migrate.min. ...</errorSourceCode>
        
        <sequenceID>43_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>43</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-includes/js/jquery/jquery-migrate.min. ...</errorSourceCode>
        
        <sequenceID>43_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>44</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/facebook-page-promoter ...</errorSourceCode>
        
        <sequenceID>44_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>44</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/facebook-page-promoter ...</errorSourceCode>
        
        <sequenceID>44_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>44</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/facebook-page-promoter ...</errorSourceCode>
        
        <sequenceID>44_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>44</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/facebook-page-promoter ...</errorSourceCode>
        
        <sequenceID>44_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>44</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/facebook-page-promoter ...</errorSourceCode>
        
        <sequenceID>44_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>45</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var lb_l_ret = {&quot;width&quot;:&quot;400&quot;,&quot;height&quot;:&quot;255&quot;,&quot;delay&quot; ...</errorSourceCode>
        
        <sequenceID>45_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>45</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var lb_l_ret = {&quot;width&quot;:&quot;400&quot;,&quot;height&quot;:&quot;255&quot;,&quot;delay&quot; ...</errorSourceCode>
        
        <sequenceID>45_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>45</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var lb_l_ret = {&quot;width&quot;:&quot;400&quot;,&quot;height&quot;:&quot;255&quot;,&quot;delay&quot; ...</errorSourceCode>
        
        <sequenceID>45_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>45</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var lb_l_ret = {&quot;width&quot;:&quot;400&quot;,&quot;height&quot;:&quot;255&quot;,&quot;delay&quot; ...</errorSourceCode>
        
        <sequenceID>45_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>45</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var lb_l_ret = {&quot;width&quot;:&quot;400&quot;,&quot;height&quot;:&quot;255&quot;,&quot;delay&quot; ...</errorSourceCode>
        
        <sequenceID>45_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>50</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/facebook-page-promoter ...</errorSourceCode>
        
        <sequenceID>50_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>50</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/facebook-page-promoter ...</errorSourceCode>
        
        <sequenceID>50_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>50</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/facebook-page-promoter ...</errorSourceCode>
        
        <sequenceID>50_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>50</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/facebook-page-promoter ...</errorSourceCode>
        
        <sequenceID>50_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>50</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/facebook-page-promoter ...</errorSourceCode>
        
        <sequenceID>50_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>69</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=250&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=250'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Text may refer to items by shape, size, or relative position alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home page-template-default page page-id-23 custom-background header-image full-width sl ...</errorSourceCode>
        
        <sequenceID>69_1_250</sequenceID>
        <decisionPass>There are no references to items in the document by shape, size, or relative position alone.</decisionPass>
        <decisionFail>There are references to items in the document by shape, size, or relative position alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>69</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=248&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=248'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Visual lists may not be properly marked.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home page-template-default page page-id-23 custom-background header-image full-width sl ...</errorSourceCode>
        
        <sequenceID>69_1_248</sequenceID>
        <decisionPass>All visual lists contain proper markup.</decisionPass>
        <decisionFail>Not all visual lists contain proper markup.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>69</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=262&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=262'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Groups of links with a related purpose are not marked.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home page-template-default page page-id-23 custom-background header-image full-width sl ...</errorSourceCode>
        
        <sequenceID>69_1_262</sequenceID>
        <decisionPass>All groups of links with a related purpose are marked.</decisionPass>
        <decisionFail>All groups of links with a related purpose are not marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>69</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=184&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=184'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Site missing site map.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home page-template-default page page-id-23 custom-background header-image full-width sl ...</errorSourceCode>
        
        <sequenceID>69_1_184</sequenceID>
        <decisionPass>Page is not part of a collection that requires a site map.</decisionPass>
        <decisionFail>Page is part of a collection that requires a site map.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>69</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=241&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=241'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Tabular information may be missing table markup.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home page-template-default page page-id-23 custom-background header-image full-width sl ...</errorSourceCode>
        
        <sequenceID>69_1_241</sequenceID>
        <decisionPass>Table markup is used for all tabular information.</decisionPass>
        <decisionFail>Table markup is not used for all tabular information.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>69</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=271&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=271'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;dir&lt;/code&gt; attribute may be required to identify changes in text direction.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home page-template-default page page-id-23 custom-background header-image full-width sl ...</errorSourceCode>
        
        <sequenceID>69_1_271</sequenceID>
        <decisionPass>All changes in text direction are marked using the &lt;code&gt;dir&lt;/code&gt; attribute.</decisionPass>
        <decisionFail>All changes in text direction are not marked using the &lt;code&gt;dir&lt;/code&gt; attribute.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>69</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=131&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=131'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Long quotations may not be marked using the &lt;code&gt;blockquote&lt;/code&gt; element.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home page-template-default page page-id-23 custom-background header-image full-width sl ...</errorSourceCode>
        
        <sequenceID>69_1_131</sequenceID>
        <decisionPass>All long quotations have been marked with the &lt;code&gt;blockquote&lt;/code&gt; element.</decisionPass>
        <decisionFail>All long quotations are not marked with the &lt;code&gt;blockquote&lt;/code&gt; element.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>69</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=28&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=28'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Document may be missing a &quot;skip to content&quot; link.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home page-template-default page page-id-23 custom-background header-image full-width sl ...</errorSourceCode>
        
        <sequenceID>69_1_28</sequenceID>
        <decisionPass>Document contians a &quot;skip to content&quot; link or does not require it.</decisionPass>
        <decisionFail>Document does not contain a &quot;skip to content&quot; link and requires it.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>69</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=276&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=276'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Repeated components may not appear in the same relative order each time they appear.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home page-template-default page page-id-23 custom-background header-image full-width sl ...</errorSourceCode>
        
        <sequenceID>69_1_276</sequenceID>
        <decisionPass>Repeated components appear in the same relative order on this page.</decisionPass>
        <decisionFail>Repeated components do not appear in the same relative order on this page.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>69</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=270&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=270'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Unicode right-to-left marks or left-to-right marks may be required.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body class=&quot;home page-template-default page page-id-23 custom-background header-image full-width sl ...</errorSourceCode>
        
        <sequenceID>69_1_270</sequenceID>
        <decisionPass>Unicode right-to-left marks or left-to-right marks are used whenever the HTML bidirectional algorithm produces undesirable results.</decisionPass>
        <decisionFail>Unicode right-to-left marks or left-to-right marks are not used whenever the HTML bidirectional algorithm produces undesirable results.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>72</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/&quot; rel=&quot;home&quot;&gt;
			&lt;img src=&quot;http://planetaradio.co/wp-content/uploads ...</errorSourceCode>
        
        <sequenceID>72_3_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>72</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/&quot; rel=&quot;home&quot;&gt;
			&lt;img src=&quot;http://planetaradio.co/wp-content/uploads ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>72</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/&quot; rel=&quot;home&quot;&gt;
			&lt;img src=&quot;http://planetaradio.co/wp-content/uploads ...</errorSourceCode>
        
        <sequenceID>72_3_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>73</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;http://planetaradio.co/wp-content/uploads/Logo-Planeta-Radio.png&quot; width=&quot;478&quot; height=&quot;294&quot; ...</errorSourceCode>
        
        <sequenceID>73_4_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>73</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;http://planetaradio.co/wp-content/uploads/Logo-Planeta-Radio.png&quot; width=&quot;478&quot; height=&quot;294&quot; ...</errorSourceCode>
        
        <sequenceID>73_4_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>73</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;http://planetaradio.co/wp-content/uploads/Logo-Planeta-Radio.png&quot; width=&quot;478&quot; height=&quot;294&quot; ...</errorSourceCode>
        
        <sequenceID>73_4_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>73</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;http://planetaradio.co/wp-content/uploads/Logo-Planeta-Radio.png&quot; width=&quot;478&quot; height=&quot;294&quot; ...</errorSourceCode>
        
        <sequenceID>73_4_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>73</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;http://planetaradio.co/wp-content/uploads/Logo-Planeta-Radio.png&quot; width=&quot;478&quot; height=&quot;294&quot; ...</errorSourceCode>
        
        <sequenceID>73_4_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>73</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;http://planetaradio.co/wp-content/uploads/Logo-Planeta-Radio.png&quot; width=&quot;478&quot; height=&quot;294&quot; ...</errorSourceCode>
        
        <sequenceID>73_4_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>79</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=42&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=42'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h1&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h1 class=&quot;site-title&quot;&gt;&lt;a href=&quot;http://planetaradio.co/&quot; rel=&quot;home&quot;&gt;PLANETA RADIO ESTACI&Oacute;N WEB&lt;/a&gt;&lt; ...</errorSourceCode>
        
        <sequenceID>79_4_42</sequenceID>
        <decisionPass>This &lt;code&gt;h1&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h1&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>79</lineNum>
      <columnNum>27</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/&quot; rel=&quot;home&quot;&gt;PLANETA RADIO ESTACI&Oacute;N WEB&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>79_27_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>79</lineNum>
      <columnNum>27</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/&quot; rel=&quot;home&quot;&gt;PLANETA RADIO ESTACI&Oacute;N WEB&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>79</lineNum>
      <columnNum>27</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/&quot; rel=&quot;home&quot;&gt;PLANETA RADIO ESTACI&Oacute;N WEB&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>79_27_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>82</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#search-container&quot; class=&quot;screen-reader-text&quot; aria-expanded=&quot;false&quot; aria-controls=&quot;search-c ...</errorSourceCode>
        
        <sequenceID>82_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>82</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#search-container&quot; class=&quot;screen-reader-text&quot; aria-expanded=&quot;false&quot; aria-controls=&quot;search-c ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>82</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#search-container&quot; class=&quot;screen-reader-text&quot; aria-expanded=&quot;false&quot; aria-controls=&quot;search-c ...</errorSourceCode>
        
        <sequenceID>82_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>87</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;screen-reader-text skip-link&quot; href=&quot;#content&quot;&gt;Ir al contenido&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>87_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>87</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;screen-reader-text skip-link&quot; href=&quot;#content&quot;&gt;Ir al contenido&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>87</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;screen-reader-text skip-link&quot; href=&quot;#content&quot;&gt;Ir al contenido&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>87_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>88</lineNum>
      <columnNum>235</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/&quot; aria-current=&quot;page&quot;&gt;Inicio&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>88_235_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>88</lineNum>
      <columnNum>235</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/&quot; aria-current=&quot;page&quot;&gt;Inicio&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>88</lineNum>
      <columnNum>235</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/&quot; aria-current=&quot;page&quot;&gt;Inicio&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>88_235_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>89</lineNum>
      <columnNum>175</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/&quot; aria-current=&quot;page&quot;&gt;Esc&uacute;chanos En Vivo&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>89_175_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>89</lineNum>
      <columnNum>175</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/&quot; aria-current=&quot;page&quot;&gt;Esc&uacute;chanos En Vivo&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>89</lineNum>
      <columnNum>175</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/&quot; aria-current=&quot;page&quot;&gt;Esc&uacute;chanos En Vivo&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>89_175_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>90</lineNum>
      <columnNum>103</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/programacion/&quot;&gt;Programaci&oacute;n&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>90_103_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>90</lineNum>
      <columnNum>103</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/programacion/&quot;&gt;Programaci&oacute;n&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>90</lineNum>
      <columnNum>103</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/programacion/&quot;&gt;Programaci&oacute;n&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>90_103_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>91</lineNum>
      <columnNum>101</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/quienes-somos/&quot;&gt;Qui&eacute;nes somos, misi&oacute;n y visi&oacute;n&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>91_101_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>91</lineNum>
      <columnNum>101</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/quienes-somos/&quot;&gt;Qui&eacute;nes somos, misi&oacute;n y visi&oacute;n&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>91</lineNum>
      <columnNum>101</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/quienes-somos/&quot;&gt;Qui&eacute;nes somos, misi&oacute;n y visi&oacute;n&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>91_101_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>92</lineNum>
      <columnNum>101</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/contactanos/&quot;&gt;Cont&aacute;ctanos&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>92_101_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>92</lineNum>
      <columnNum>101</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/contactanos/&quot;&gt;Cont&aacute;ctanos&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>92</lineNum>
      <columnNum>101</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/contactanos/&quot;&gt;Cont&aacute;ctanos&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>92_101_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>93</lineNum>
      <columnNum>103</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/audios/&quot;&gt;Audios&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>93_103_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>93</lineNum>
      <columnNum>103</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/audios/&quot;&gt;Audios&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>93</lineNum>
      <columnNum>103</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/audios/&quot;&gt;Audios&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>93_103_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>99</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=267&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=267'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission error messages may not identify empty required fields.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form role=&quot;search&quot; method=&quot;get&quot; class=&quot;search-form&quot; action=&quot;http://planetaradio.co/&quot;&gt;
				&lt;label&gt;
	 ...</errorSourceCode>
        
        <sequenceID>99_5_267</sequenceID>
        <decisionPass>Required fields are identified in all form submission error messages.</decisionPass>
        <decisionFail>Required fields are not identified in all form submission error messages.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>99</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=272&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=272'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form may delete information without allowing for recovery.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form role=&quot;search&quot; method=&quot;get&quot; class=&quot;search-form&quot; action=&quot;http://planetaradio.co/&quot;&gt;
				&lt;label&gt;
	 ...</errorSourceCode>
        
        <sequenceID>99_5_272</sequenceID>
        <decisionPass>Information deleted using this form can be recovered.</decisionPass>
        <decisionFail>Information delted using this form can not be recovered.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>99</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=246&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=246'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;All required &lt;code&gt;form&lt;/code&gt; fields may not be indicated as required.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form role=&quot;search&quot; method=&quot;get&quot; class=&quot;search-form&quot; action=&quot;http://planetaradio.co/&quot;&gt;
				&lt;label&gt;
	 ...</errorSourceCode>
        
        <sequenceID>99_5_246</sequenceID>
        <decisionPass>All required fields are indicated to the user as required.</decisionPass>
        <decisionFail>All required fields are not indicated to the user as required.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>99</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=269&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=269'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission data may not be presented to the user before final acceptance of an irreversable transaction.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form role=&quot;search&quot; method=&quot;get&quot; class=&quot;search-form&quot; action=&quot;http://planetaradio.co/&quot;&gt;
				&lt;label&gt;
	 ...</errorSourceCode>
        
        <sequenceID>99_5_269</sequenceID>
        <decisionPass>All form submission data is presented to the user before final acceptance for all irreversable transactions.</decisionPass>
        <decisionFail>All form submission data is not presented to the user before final acceptance for all irreversable transactions.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>99</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=265&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=265'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Tab order may not follow logical order.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form role=&quot;search&quot; method=&quot;get&quot; class=&quot;search-form&quot; action=&quot;http://planetaradio.co/&quot;&gt;
				&lt;label&gt;
	 ...</errorSourceCode>
        
        <sequenceID>99_5_265</sequenceID>
        <decisionPass>Tab order follows a logical order.</decisionPass>
        <decisionFail>Tab order does not follow a logical order.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>99</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=268&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=268'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission error messages may not provide assistance.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form role=&quot;search&quot; method=&quot;get&quot; class=&quot;search-form&quot; action=&quot;http://planetaradio.co/&quot;&gt;
				&lt;label&gt;
	 ...</errorSourceCode>
        
        <sequenceID>99_5_268</sequenceID>
        <decisionPass>All form submission error messages provide assistance in correcting the error.</decisionPass>
        <decisionFail>All form submission error messages do not provide assistance in correcting the errors.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>102</lineNum>
      <columnNum>6</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=188&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=188'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Label text is empty.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;search&quot; class=&quot;search-field&quot; placeholder=&quot;Buscar &amp;hellip;&quot; value=&quot;&quot; name=&quot;s&quot; /&gt;</errorSourceCode>
        <repair>Add text to the &lt;code&gt;label&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>102</lineNum>
      <columnNum>6</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=55&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=55'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;input&lt;/code&gt; possibly using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;search&quot; class=&quot;search-field&quot; placeholder=&quot;Buscar &amp;hellip;&quot; value=&quot;&quot; name=&quot;s&quot; /&gt;</errorSourceCode>
        
        <sequenceID>102_6_55</sequenceID>
        <decisionPass>&lt;code&gt;input&lt;/code&gt; element does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;input&lt;/code&gt; element uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>104</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=55&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=55'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;input&lt;/code&gt; possibly using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;submit&quot; class=&quot;search-submit&quot; value=&quot;Buscar&quot; /&gt;</errorSourceCode>
        
        <sequenceID>104_5_55</sequenceID>
        <decisionPass>&lt;code&gt;input&lt;/code&gt; element does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;input&lt;/code&gt; element uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>104</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=189&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=189'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;label&lt;/code&gt; may not describe its associated control.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;submit&quot; class=&quot;search-submit&quot; value=&quot;Buscar&quot; /&gt;</errorSourceCode>
        
        <sequenceID>104_5_189</sequenceID>
        <decisionPass>&lt;code&gt;label&lt;/code&gt; describes its associated control.</decisionPass>
        <decisionFail>&lt;code&gt;label&lt;/code&gt; does not describe its associated control.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>118</lineNum>
      <columnNum>31</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=42&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=42'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h1&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h1 class=&quot;entry-title&quot;&gt;Esc&uacute;chanos En Vivo&lt;/h1&gt;</errorSourceCode>
        
        <sequenceID>118_31_42</sequenceID>
        <decisionPass>This &lt;code&gt;h1&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h1&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>120</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=43&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=43'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h2&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h2 style=&quot;text-align: justify;&quot;&gt;&iexcl;BIENVENIDO A TU PLANETA!&lt;/h2&gt;</errorSourceCode>
        
        <sequenceID>120_3_43</sequenceID>
        <decisionPass>This &lt;code&gt;h2&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h2&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>122</lineNum>
      <columnNum>413</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;mailto:oyentes@planetaradio.co?Subject=Interesado%20en%20el%20curso&quot;&gt;Nuestro e:mail, oyente ...</errorSourceCode>
        
        <sequenceID>122_413_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>122</lineNum>
      <columnNum>413</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;mailto:oyentes@planetaradio.co?Subject=Interesado%20en%20el%20curso&quot;&gt;Nuestro e:mail, oyente ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>122</lineNum>
      <columnNum>413</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;mailto:oyentes@planetaradio.co?Subject=Interesado%20en%20el%20curso&quot;&gt;Nuestro e:mail, oyente ...</errorSourceCode>
        
        <sequenceID>122_413_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>124</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=43&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=43'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h2&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h2 style=&quot;text-align: justify;&quot;&gt;Puedes escucharnos  directamente desde aqu&iacute;, o tambi&eacute;n usar nuest ...</errorSourceCode>
        
        <sequenceID>124_1_43</sequenceID>
        <decisionPass>This &lt;code&gt;h2&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h2&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>125</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://www.colombia.com/radio/i3488/crossover/planeta-radio-colombia&quot; target=&quot;mainFrame&quot; re ...</errorSourceCode>
        
        <sequenceID>125_4_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>125</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://www.colombia.com/radio/i3488/crossover/planeta-radio-colombia&quot; target=&quot;mainFrame&quot; re ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>125</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://www.colombia.com/radio/i3488/crossover/planeta-radio-colombia&quot; target=&quot;mainFrame&quot; re ...</errorSourceCode>
        
        <sequenceID>125_4_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/pls/playlist.pls&quot;&gt;&lt;img title=&quot;Winamp, iTunes&quot; align=&quot;absmiddle&quot; alt= ...</errorSourceCode>
        
        <sequenceID>126_4_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>126</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/pls/playlist.pls&quot;&gt;&lt;img title=&quot;Winamp, iTunes&quot; align=&quot;absmiddle&quot; alt= ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/pls/playlist.pls&quot;&gt;&lt;img title=&quot;Winamp, iTunes&quot; align=&quot;absmiddle&quot; alt= ...</errorSourceCode>
        
        <sequenceID>126_4_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>54</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Winamp, iTunes&quot; align=&quot;absmiddle&quot; alt=&quot;Winamp, iTunes&quot; src=&quot;http://superstream.co:2199/s ...</errorSourceCode>
        
        <sequenceID>126_54_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>54</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Winamp, iTunes&quot; align=&quot;absmiddle&quot; alt=&quot;Winamp, iTunes&quot; src=&quot;http://superstream.co:2199/s ...</errorSourceCode>
        
        <sequenceID>126_54_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>54</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Winamp, iTunes&quot; align=&quot;absmiddle&quot; alt=&quot;Winamp, iTunes&quot; src=&quot;http://superstream.co:2199/s ...</errorSourceCode>
        
        <sequenceID>126_54_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>54</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Winamp, iTunes&quot; align=&quot;absmiddle&quot; alt=&quot;Winamp, iTunes&quot; src=&quot;http://superstream.co:2199/s ...</errorSourceCode>
        
        <sequenceID>126_54_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>54</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=239&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=239'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; has &lt;code&gt;title&lt;/code&gt; attribute and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Winamp, iTunes&quot; align=&quot;absmiddle&quot; alt=&quot;Winamp, iTunes&quot; src=&quot;http://superstream.co:2199/s ...</errorSourceCode>
        
        <sequenceID>126_54_239</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>54</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Winamp, iTunes&quot; align=&quot;absmiddle&quot; alt=&quot;Winamp, iTunes&quot; src=&quot;http://superstream.co:2199/s ...</errorSourceCode>
        
        <sequenceID>126_54_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>54</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Winamp, iTunes&quot; align=&quot;absmiddle&quot; alt=&quot;Winamp, iTunes&quot; src=&quot;http://superstream.co:2199/s ...</errorSourceCode>
        
        <sequenceID>126_54_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>202</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/pls/playlist.asx&quot;&gt;&lt;img title=&quot;Windows Media Player&quot; align=&quot;absmiddle ...</errorSourceCode>
        
        <sequenceID>126_202_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>126</lineNum>
      <columnNum>202</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/pls/playlist.asx&quot;&gt;&lt;img title=&quot;Windows Media Player&quot; align=&quot;absmiddle ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>202</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/pls/playlist.asx&quot;&gt;&lt;img title=&quot;Windows Media Player&quot; align=&quot;absmiddle ...</errorSourceCode>
        
        <sequenceID>126_202_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>252</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Windows Media Player&quot; align=&quot;absmiddle&quot; alt=&quot;Windows Media Player&quot; src=&quot;http://superstre ...</errorSourceCode>
        
        <sequenceID>126_252_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>252</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Windows Media Player&quot; align=&quot;absmiddle&quot; alt=&quot;Windows Media Player&quot; src=&quot;http://superstre ...</errorSourceCode>
        
        <sequenceID>126_252_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>252</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Windows Media Player&quot; align=&quot;absmiddle&quot; alt=&quot;Windows Media Player&quot; src=&quot;http://superstre ...</errorSourceCode>
        
        <sequenceID>126_252_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>252</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Windows Media Player&quot; align=&quot;absmiddle&quot; alt=&quot;Windows Media Player&quot; src=&quot;http://superstre ...</errorSourceCode>
        
        <sequenceID>126_252_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>252</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=239&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=239'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; has &lt;code&gt;title&lt;/code&gt; attribute and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Windows Media Player&quot; align=&quot;absmiddle&quot; alt=&quot;Windows Media Player&quot; src=&quot;http://superstre ...</errorSourceCode>
        
        <sequenceID>126_252_239</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>252</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Windows Media Player&quot; align=&quot;absmiddle&quot; alt=&quot;Windows Media Player&quot; src=&quot;http://superstre ...</errorSourceCode>
        
        <sequenceID>126_252_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>252</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Windows Media Player&quot; align=&quot;absmiddle&quot; alt=&quot;Windows Media Player&quot; src=&quot;http://superstre ...</errorSourceCode>
        
        <sequenceID>126_252_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>412</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/pls/playlist.ram&quot;&gt;&lt;img title=&quot;Real Player&quot; align=&quot;absmiddle&quot; alt=&quot;Re ...</errorSourceCode>
        
        <sequenceID>126_412_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>412</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=20&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=20'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link to multimedia may be missing a text transcript.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/pls/playlist.ram&quot;&gt;&lt;img title=&quot;Real Player&quot; align=&quot;absmiddle&quot; alt=&quot;Re ...</errorSourceCode>
        
        <sequenceID>126_412_20</sequenceID>
        <decisionPass>Link to multimedia file has an associated text transcript.</decisionPass>
        <decisionFail>Link to multimedia file does not have an associate text transcript.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>126</lineNum>
      <columnNum>412</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/pls/playlist.ram&quot;&gt;&lt;img title=&quot;Real Player&quot; align=&quot;absmiddle&quot; alt=&quot;Re ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>412</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=145&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=145'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link to multimedia may be missing a link to text alternative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/pls/playlist.ram&quot;&gt;&lt;img title=&quot;Real Player&quot; align=&quot;absmiddle&quot; alt=&quot;Re ...</errorSourceCode>
        
        <sequenceID>126_412_145</sequenceID>
        <decisionPass>Link to multimedia file has a link to a text alternative.</decisionPass>
        <decisionFail>Link to multimedia file does not have a link to a text alternative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>412</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://planetaradio.co/pls/playlist.ram&quot;&gt;&lt;img title=&quot;Real Player&quot; align=&quot;absmiddle&quot; alt=&quot;Re ...</errorSourceCode>
        
        <sequenceID>126_412_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>462</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Real Player&quot; align=&quot;absmiddle&quot; alt=&quot;Real Player&quot; src=&quot;http://superstream.co:2199/system/ ...</errorSourceCode>
        
        <sequenceID>126_462_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>462</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Real Player&quot; align=&quot;absmiddle&quot; alt=&quot;Real Player&quot; src=&quot;http://superstream.co:2199/system/ ...</errorSourceCode>
        
        <sequenceID>126_462_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>462</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Real Player&quot; align=&quot;absmiddle&quot; alt=&quot;Real Player&quot; src=&quot;http://superstream.co:2199/system/ ...</errorSourceCode>
        
        <sequenceID>126_462_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>462</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Real Player&quot; align=&quot;absmiddle&quot; alt=&quot;Real Player&quot; src=&quot;http://superstream.co:2199/system/ ...</errorSourceCode>
        
        <sequenceID>126_462_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>462</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=239&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=239'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; has &lt;code&gt;title&lt;/code&gt; attribute and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Real Player&quot; align=&quot;absmiddle&quot; alt=&quot;Real Player&quot; src=&quot;http://superstream.co:2199/system/ ...</errorSourceCode>
        
        <sequenceID>126_462_239</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>462</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Real Player&quot; align=&quot;absmiddle&quot; alt=&quot;Real Player&quot; src=&quot;http://superstream.co:2199/system/ ...</errorSourceCode>
        
        <sequenceID>126_462_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>462</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img title=&quot;Real Player&quot; align=&quot;absmiddle&quot; alt=&quot;Real Player&quot; src=&quot;http://superstream.co:2199/system/ ...</errorSourceCode>
        
        <sequenceID>126_462_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>591</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://tunein.com/radio/Planeta-Radio-Colombia-s202439&quot; target=&quot;mainFrame&quot; rel=&quot;noopener no ...</errorSourceCode>
        
        <sequenceID>126_591_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>126</lineNum>
      <columnNum>591</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://tunein.com/radio/Planeta-Radio-Colombia-s202439&quot; target=&quot;mainFrame&quot; rel=&quot;noopener no ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>126</lineNum>
      <columnNum>591</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;http://tunein.com/radio/Planeta-Radio-Colombia-s202439&quot; target=&quot;mainFrame&quot; rel=&quot;noopener no ...</errorSourceCode>
        
        <sequenceID>126_591_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>129</lineNum>
      <columnNum>32</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;http://planetaradio.co/wp-content/uploads/1fotoRadio-1.jpg&quot; alt=&quot;Dos personas transmitiend ...</errorSourceCode>
        
        <sequenceID>129_32_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>129</lineNum>
      <columnNum>32</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;http://planetaradio.co/wp-content/uploads/1fotoRadio-1.jpg&quot; alt=&quot;Dos personas transmitiend ...</errorSourceCode>
        
        <sequenceID>129_32_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>129</lineNum>
      <columnNum>32</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;http://planetaradio.co/wp-content/uploads/1fotoRadio-1.jpg&quot; alt=&quot;Dos personas transmitiend ...</errorSourceCode>
        
        <sequenceID>129_32_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>129</lineNum>
      <columnNum>32</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;http://planetaradio.co/wp-content/uploads/1fotoRadio-1.jpg&quot; alt=&quot;Dos personas transmitiend ...</errorSourceCode>
        
        <sequenceID>129_32_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>129</lineNum>
      <columnNum>32</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;http://planetaradio.co/wp-content/uploads/1fotoRadio-1.jpg&quot; alt=&quot;Dos personas transmitiend ...</errorSourceCode>
        
        <sequenceID>129_32_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>132</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=43&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=43'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h2&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h2 style=&quot;text-align: justify;&quot;&gt;Copyright&lt;/h2&gt;</errorSourceCode>
        
        <sequenceID>132_1_43</sequenceID>
        <decisionPass>This &lt;code&gt;h2&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h2&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>133</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://infoinnova.co&quot; target=&quot;mainFrame&quot; rel=&quot;noopener noreferrer&quot;&gt;Copyright &ndash; Corporaci ...</errorSourceCode>
        
        <sequenceID>133_4_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>133</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://infoinnova.co&quot; target=&quot;mainFrame&quot; rel=&quot;noopener noreferrer&quot;&gt;Copyright &ndash; Corporaci ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>133</lineNum>
      <columnNum>4</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://infoinnova.co&quot; target=&quot;mainFrame&quot; rel=&quot;noopener noreferrer&quot;&gt;Copyright &ndash; Corporaci ...</errorSourceCode>
        
        <sequenceID>133_4_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>145</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=43&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=43'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h2&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h2 class=&quot;site-description&quot;&gt;&iexcl;Girando al ritmo de tu vida!&lt;/h2&gt;</errorSourceCode>
        
        <sequenceID>145_3_43</sequenceID>
        <decisionPass>This &lt;code&gt;h2&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h2&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>161</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a id=&quot;inline&quot; href=&quot;#arvlbdata&quot; style=&quot;display: none;overflow:hidden;&quot;&gt;Show&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>161_2_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>161</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a id=&quot;inline&quot; href=&quot;#arvlbdata&quot; style=&quot;display: none;overflow:hidden;&quot;&gt;Show&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>161</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a id=&quot;inline&quot; href=&quot;#arvlbdata&quot; style=&quot;display: none;overflow:hidden;&quot;&gt;Show&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>161_2_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>179</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/contact-form-7/include ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>179</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/contact-form-7/include ...</errorSourceCode>
        
        <sequenceID>179_7_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>179</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/contact-form-7/include ...</errorSourceCode>
        
        <sequenceID>179_7_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>179</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/contact-form-7/include ...</errorSourceCode>
        
        <sequenceID>179_7_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>179</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/contact-form-7/include ...</errorSourceCode>
        
        <sequenceID>179_7_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>179</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/contact-form-7/include ...</errorSourceCode>
        
        <sequenceID>179_7_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>180</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var _wpcf7 = {&quot;loaderUrl&quot;:&quot;http:\/\/planetaradio.co\ ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>180</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var _wpcf7 = {&quot;loaderUrl&quot;:&quot;http:\/\/planetaradio.co\ ...</errorSourceCode>
        
        <sequenceID>180_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>180</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var _wpcf7 = {&quot;loaderUrl&quot;:&quot;http:\/\/planetaradio.co\ ...</errorSourceCode>
        
        <sequenceID>180_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>180</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var _wpcf7 = {&quot;loaderUrl&quot;:&quot;http:\/\/planetaradio.co\ ...</errorSourceCode>
        
        <sequenceID>180_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>180</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var _wpcf7 = {&quot;loaderUrl&quot;:&quot;http:\/\/planetaradio.co\ ...</errorSourceCode>
        
        <sequenceID>180_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>180</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var _wpcf7 = {&quot;loaderUrl&quot;:&quot;http:\/\/planetaradio.co\ ...</errorSourceCode>
        
        <sequenceID>180_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>185</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/contact-form-7/include ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>185</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/contact-form-7/include ...</errorSourceCode>
        
        <sequenceID>185_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>185</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/contact-form-7/include ...</errorSourceCode>
        
        <sequenceID>185_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>185</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/contact-form-7/include ...</errorSourceCode>
        
        <sequenceID>185_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>185</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/contact-form-7/include ...</errorSourceCode>
        
        <sequenceID>185_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>185</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/plugins/contact-form-7/include ...</errorSourceCode>
        
        <sequenceID>185_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>186</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var featuredSliderDefaults = {&quot;prevText&quot;:&quot;Anterior&quot;, ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>186</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var featuredSliderDefaults = {&quot;prevText&quot;:&quot;Anterior&quot;, ...</errorSourceCode>
        
        <sequenceID>186_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>186</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var featuredSliderDefaults = {&quot;prevText&quot;:&quot;Anterior&quot;, ...</errorSourceCode>
        
        <sequenceID>186_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>186</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var featuredSliderDefaults = {&quot;prevText&quot;:&quot;Anterior&quot;, ...</errorSourceCode>
        
        <sequenceID>186_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>186</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var featuredSliderDefaults = {&quot;prevText&quot;:&quot;Anterior&quot;, ...</errorSourceCode>
        
        <sequenceID>186_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>186</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
/* &lt;![CDATA[ */
var featuredSliderDefaults = {&quot;prevText&quot;:&quot;Anterior&quot;, ...</errorSourceCode>
        
        <sequenceID>186_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>191</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/themes/twentyfourteen/js/slide ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>191</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/themes/twentyfourteen/js/slide ...</errorSourceCode>
        
        <sequenceID>191_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>191</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/themes/twentyfourteen/js/slide ...</errorSourceCode>
        
        <sequenceID>191_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>191</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/themes/twentyfourteen/js/slide ...</errorSourceCode>
        
        <sequenceID>191_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>191</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/themes/twentyfourteen/js/slide ...</errorSourceCode>
        
        <sequenceID>191_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>191</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/themes/twentyfourteen/js/slide ...</errorSourceCode>
        
        <sequenceID>191_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>192</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/themes/twentyfourteen/js/funct ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>192</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/themes/twentyfourteen/js/funct ...</errorSourceCode>
        
        <sequenceID>192_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>192</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/themes/twentyfourteen/js/funct ...</errorSourceCode>
        
        <sequenceID>192_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>192</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/themes/twentyfourteen/js/funct ...</errorSourceCode>
        
        <sequenceID>192_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>192</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/themes/twentyfourteen/js/funct ...</errorSourceCode>
        
        <sequenceID>192_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>192</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-content/themes/twentyfourteen/js/funct ...</errorSourceCode>
        
        <sequenceID>192_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>193</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=90&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=90'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; must have a &lt;code&gt;noscript&lt;/code&gt; section.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-includes/js/wp-embed.min.js?ver=5.2.5&#039; ...</errorSourceCode>
        <repair>Add a &lt;code&gt;noscript&lt;/code&gt; section immediately following the &lt;code&gt;script&lt;/code&gt; that provides the same functionality as the &lt;code&gt;script&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>193</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-includes/js/wp-embed.min.js?ver=5.2.5&#039; ...</errorSourceCode>
        
        <sequenceID>193_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>193</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-includes/js/wp-embed.min.js?ver=5.2.5&#039; ...</errorSourceCode>
        
        <sequenceID>193_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>193</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-includes/js/wp-embed.min.js?ver=5.2.5&#039; ...</errorSourceCode>
        
        <sequenceID>193_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>193</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-includes/js/wp-embed.min.js?ver=5.2.5&#039; ...</errorSourceCode>
        
        <sequenceID>193_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>193</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;http://planetaradio.co/wp-includes/js/wp-embed.min.js?ver=5.2.5&#039; ...</errorSourceCode>
        
        <sequenceID>193_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 

  </results>
</resultset>
