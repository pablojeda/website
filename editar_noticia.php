<!DOCTYPE html>
<html lang="es">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad.">
    <meta name="author" content="Prime Developers Chile">
    
    <!-- Facebook Metadatos | Diseño web accesible -->
    <meta property="og:title" content="Fundación Comparlante | Arte para promover la empatía"/>
    <meta property="og:site_name" content="Arte para promover la empatía"/>
    <meta property="og:description" content="Fundación Comparlante brinda servicios de impacto social para la accesibilidad de las personas con discapacidad" />
    <!-- <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/services/2/diseno-web.jpg"/> -->
    <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/home/logo-fundacion-2.jpg"/>

    <!-- <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/diseno-web.php"/> -->

    
    
    <title>Fundación Comparlante</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
    <link href="css/main.css" rel="stylesheet">
    <link href="css/noticias.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head><!--/head-->

<body>
  <?php include("header.php"); 
  
// get passed parameter value, in this case, the record ID
                // isset() is a PHP function used to verify if a value is there or not
                $id=isset($_GET['id']) ? $_GET['id'] : die('ERROR: Record ID not found.');
                
                //include database connection
                include 'config_news/database.php';
                
                // read current record's data
                try {
                    // prepare select query
                    $query = "SELECT id, titulo, publica, imagen, altText, contenido, idioma, fechaorigen FROM noticias WHERE id = ? LIMIT 0,1";
                    $stmt = $con->prepare( $query );
                    
                    // this is the first question mark
                    $stmt->bindParam(1, $id);
                    
                    // execute our query
                    $stmt->execute();
                    
                    // store retrieved row to a variable
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    
                    // values to fill up our form
                    $titulo = $row['titulo'];
                    $publica = $row['publica'];
                    $imagen = $row['imagen'];
                    $altText = $row['altText'];
                    $contenido = $row['contenido'];
                    $idioma = $row['idioma'];
                    $fechaorigen = $row['fechaorigen'];

                }
                
                // show error
                catch(PDOException $exception){
                    die('ERROR: ' . $exception->getMessage());
                }
                ?>

  

<section id="noticias">
    <div class="container">
        <div class="row">
            <div tabindex="30">

               <div class="col-sm-12  text-justify">
               <article>
               <?php
 
                // check if form was submitted
                if($_POST){
                    
                    try{
                    
                        // write update query
                        // in this case, it seemed like we have so many fields to pass and 
                        // it is better to label them and not use question marks
                        $query = "UPDATE noticias 
                                    SET titulo=:titulo, idioma=:idioma, imagen=:imagen,fechaorigen=:fechaorigen, altText=:altText, publica=:publica, contenido=:contenido 
                                    WHERE id = :id";
                
                        // prepare query for excecution
                        $stmt = $con->prepare($query);
                
                        // posted values
                        $titulo=$_POST['titulo'];
                        $idioma=$_POST['idioma'];
                        $imagen=$_POST['imagen'];
                        $altText=$_POST['altText'];
                        $publica=$_POST['publica'];
                        $contenido=$_POST['contenido'];
                        $fechaorigen=$_POST['fechaorigen'];
                
                        // bind the parameters
                        $stmt->bindParam(':titulo', $titulo);
                        $stmt->bindParam(':idioma', $idioma);
                        $stmt->bindParam(':imagen', $imagen);
                        $stmt->bindParam(':altText', $altText);
                        $stmt->bindParam(':publica', $publica);
                        $stmt->bindParam(':contenido', $contenido);
                        $stmt->bindParam(':fechaorigen', $fechaorigen);
                        $stmt->bindParam(':id', $id);
                        
                        // Execute the query
                        if($stmt->execute()){
                            echo "<div class='alert alert-success'>Noticia actualizada.</div>";
                        }else{
                            echo "<div class='alert alert-danger'>Mmm error, revisar la información.</div>";
                        }
                        
                    }
                    
                    // show errors
                    catch(PDOException $exception){
                        die('ERROR: ' . $exception->getMessage());
                    }
                }
                ?>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"] . "?id={$id}");?>" method="post">
                <table class='table table-hover table-responsive table-bordered'>
                    <tr>
                        <td>Publico?</td>
                        <td><input type='text' name='publica' value="<?php echo htmlspecialchars($publica, ENT_QUOTES);  ?>" class='form-control' /></td>
                    </tr>
                    <tr>
                        <td>Fecha: (YYYY-MM-DD)</td>
                        <td><input type='text' name='fechaorigen' value="<?php echo htmlspecialchars($fechaorigen, ENT_QUOTES);  ?>" class='form-control' /></td>
                    </tr>
                    <tr>
                        <td>Título:</td>
                        <td><input type='text' name='titulo' value="<?php echo html_entity_decode($titulo);  ?>" class='form-control' /></td>
                    </tr>
                    <tr>
                        <td>Título en Inglés:</td>
                        <td><input type='text' name='idioma' value="<?php echo htmlspecialchars($idioma, ENT_QUOTES);  ?>" class='form-control' /></td>
                    </tr>
                    
                    <tr>
                        <td>Imagen</td>
                        <td><input type='text' name='imagen' value="<?php echo htmlspecialchars($imagen, ENT_QUOTES);  ?>" class='form-control' /></td>
                    </tr>
                    <tr>
                        <td>Texto alternativo</td>
                        <td><input type='text' name='altText' value="<?php echo html_entity_decode($altText);  ?>" class='form-control' /></td>
                    </tr>
                    
                    <tr>
                        <td>Link:</td>
                        <td><input type='text' name='contenido' value="<?php echo $contenido   ?>" class='form-control' /></td>
                       
                    </tr>
                    
                    <tr>
                        <td></td>
                        <td>
                            <input type='submit' value='Guardar' class='btn btn-primary' />
                            <a href='lista_noticias.php' class='btn btn-secondary'>Ver todas las noticias</a>
                        </td>
                    </tr>
                </table>
            </form     
                
                            

                
               </article>
                
            </div>






        </div>
    </div>

</div>
</section>


<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">

            </div>

                   <!--  <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
                  
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                    <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/main_es.js"></script>   
<script src="./tinymce/tinymce.min.js" referrerpolicy="origin"></script>
<script>tinymce.init({selector:'textarea'});</script> 
</body>


</html>
