<!DOCTYPE html>
<html lang="es">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad.">
    <meta name="author" content="Prime Developers Chile">
    
    <!-- Facebook Metadatos | Diseño web accesible -->
    <meta property="og:title" content="Fundación Comparlante | Programas: Accesibilidad"/>
    <meta property="og:site_name" content="Programas"/>
    <meta property="og:description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad" />
    <meta property="og:image" content="http://www.comparlante.com/images/services/2/asesoria-accesibilidad.jpg"/>
    <meta property="og:url" content="http://www.comparlante.com/accesibilidad-que-transforma.php"/>
    <meta property="og:image:alt" content="Logo de Asesoría en accesibilidad"/>
    
    
    <title>Fundación Comparlante</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head><!--/head-->

<body>
  <?php include("header.php"); ?>

  <section id="titulo">
    <div class="row  padding" style="background-color: #f6f6f6;" tabindex="10">
        <div class="container" >
            <div class="row">
                <div class="wow scaleIn " >
                    <center>
                        <img src="images/programas/accesibilidadquetransforma_sf.png" alt="Cuatro manos sobrepuestas que representan el trabajo en equipo">
                    </center>
                </div>

                <div class="col-sm-12 padding-top-index text-justify">

                   <center>
                       <h2><b>Accesiblidad que transforma</b></h2>
                   </center>
               </div>
           </div>
       </div>
   </div>

</section>

<section id="portfolio-information" >
    <div class="container" tabindex="12"> 
        <br><br>
        <!-- asesoria en accesibilidad -->
        <div class="row">
            <div class="project-info overflow">
                <center>
                    <img src="images/services/asesoria-accesibilidad.png" class="img-responsive" width="40%" alt="Logo de Asesoría en accesibilidad">
                </center>
            </div>
            <br>
            <div class="row">
                <center>
                    <h2><b>Asesoría en accesibilidad</b></h2>
                </center>
            </div>
            <div class="row">

                <div class="col-sm-12">

                    <div  class="project-info overflow " style="text-align:justify">

                        <h2>En un mundo tan globalizado como en el que vivimos, no es posible que nuestros productos, nuestros servicios, nuestras prácticas educativas, sociales, comunicacionales y culturales sigan siendo tan poco accesibles. No se trata simplemente de "incluir", la accesibilidad es equidad de oportunidades para el desarrollo de todos, más allá de nuestras características y capacidades. </h2>
                         <p><center><a type="button" href="accesibilidad_es.php" class="btn btn-info">Más información</a></center>  </p>

                    </div>
                    <br>
                </div>
            </div>
        </div>

        <!-- fin asesoria accesibilidad -->
        

        <!-- desarrollo web -->
        <div class="row" tabindex="14">
            <div class="project-info overflow">
                <center>
                    <img src="images/services/diseno-web.png" class="img-responsive" width="40%" alt="Logo de Desarrollo web.">
                </center>
            </div>
            <br>
            <div class="row">
                <center>
                    <h2><b>Desarrollo web</b></h2>
                </center>
            </div>
            <div class="row">

                <div class="col-sm-12">

                    <div  class="project-info overflow " style="text-align:justify">

                        <h2>Actualmente el 90% de los contenidos disponibles en internet no es accesible para personas con discapacidad. Comparlante ofrece asesoría y desarrollo de sitios web para que estos sean visitados por más personas y que la información llegue a todos. Hacemos que sea accesible, navegable e intuitivo. Ofrecemos un Verificador de Accesibilidad gratuito y desarrollamos propuestas acorde a las necesidades de los clientes. 
                        </h2>
                         <p><center><a type="button" href="verifica.php" class="btn btn-info">Más información</a></center>  </p>

                    </div>
                    <br>
                </div>
            </div>
        </div>

        <!-- diseño universal -->

         <div class="row" tabindex="15">
            <div class="project-info overflow">
                <center>
                    <img src="images/services/diseno-accesible.png" class="img-responsive" width="40%" alt="Logo de Diseño universal, accesibilidad creativa.">
                </center>
            </div>
            <br>
            <div class="row">
                <center>
                    <h2><b>Diseño universal, accesibilidad creativa</b></h2>
                </center>
            </div>
            <div class="row">

                <div class="col-sm-12">

                    <div  class="project-info overflow " style="text-align:justify">

                        <h2>El propósito del diseño es crear algo útil. La forma, color, textura y demás características junto a la función de esa creación deben atender las diversas necesidades del entorno. Este servicio promueve que organizaciones, empresas y el sector gubernamental ofrezcan materiales bajo estándares de diseño universal: Identidad, sitios web, documentos, presentaciones, todo accesible para las personas con discapacidad. 
                        </h2>
                         <p><center><a type="button" href="diseno-grafico.php" class="btn btn-info">Más información</a></center>  </p>
                    </div>
                    <br>
                </div>
            </div>
        </div>
        <!-- audioteca -->
        <div class="row" tabindex="16">
            <div class="project-info overflow">
                <center>
                    <img src="images/services/audiolibros.png" class="img-responsive" width="40%" alt="Logo de Audioteca Comparlante.">
                </center>
            </div>
            <br>
            <div class="row">
                <center>
                    <h2><b>Audioteca Comparlante</b></h2>
                </center>
            </div>
            <div class="row">

                <div class="col-sm-12">

                    <div tabindex="" class="project-info overflow " style="text-align:justify">

                        <h2>Comparlante ofrece un banco de audiolibros accesibles en 18 idiomas, de acceso libre y construcción colaborativa. A la fecha, contamos con más de 400 títulos.
                        </h2>
                         <p><center><a type="button" href="http://www.comparlante.com/biblioteca_audiolibros/" class="btn btn-info">Más información</a></center>  </p>

                    </div>
                    <br>
                </div>
            </div>
        </div>

    </div>
</section>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">

            </div>

                   <!--  <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
                  <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                     <h2>Envíanos un mensaje</h2>
                     <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php">
                        <div class="form-group">
                            <input tabindex="51" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Nombre">
                        </div>
                        <div class="form-group">
                            <input tabindex="52" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <textarea tabindex="53" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Tu mensaje"></textarea>
                        </div>            
                        <div style="display:none"> 
                            <input id="cc" value="sebastian@comparlante.com" placeholder="E-mail"> 
                        </div>             
                        <div class="form-group">
                            <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Enviar mensaje</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                    <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/main_es.js"></script>   
</body>


</html>
