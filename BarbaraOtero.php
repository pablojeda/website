<!DOCTYPE html>
<html lang="es">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad.">
    <meta name="author" content="Prime Developers Chile">
    
    <!-- Facebook Metadatos | Diseño web accesible -->
    <meta property="og:title" content="Fundación Comparlante | Equipo | Bárbara Otero"/>
    <meta property="og:site_name" content="Perfil de Bárbara Otero"/>
    <meta property="og:description" content="Biografía y contacto de Bárbara Otero, pasante en el área de desarrollo Web de Fundación Comparlante" />
    <!-- <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/services/2/diseno-web.jpg"/> -->
    <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/home/logo-fundacion-2.jpg"/>

    <!-- <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/diseno-web.php"/> -->

    
    
    <title>Fundación Comparlante</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head><!--/head-->

<body>
  <?php include("header.php"); ?>

  <section id="page-breadcrumb">
      <div class="vertical-center sun">
       <div class="container">
        <div class="row">
          <div class="action">
            <div tabindex="10" class="col-sm-12">
              <h1 class="title text-center">Bárbara Otero</h1>
              <p></p>
            </div>
          </div>
        </div>
       </div>
      </div>
  </section>

<section id="PerfilBO">
<div class="container">
        <div class="row">
            <div tabindex="30">

               <div class="col-sm-12">

                <h2><b>Biografía</b></h2>
                <p>Bárbara Otero es una estudiante de Licenciatura en Informática, nacida en Pehuajó, Argentina el 25 de Enero de 1996. <br>
                Desde 2016 hasta la actualidad, Bárbara trabajó como administrativa en un consultorio psicopedagógico ubicado en su ciudad natal y fue allí donde comenzó a aprender sobre discapacidad y a entender la importancia de crear un mundo accesible e inclusivo para todas las personas. <br>
                Actualmente se desempeña como pasante en el área de desarrollo Web de Fundación Comparlante, contenta de poder ser agente de cambio y aportar su granito de arena en esta gran tarea que realiza la fundación para mejorar la calidad de vida de millones de personas.<br>
                <br> <br>

                </div>
            </div>

            <div tabindex="31">

               <div class="col-sm-12">

                <h2><b>Contacto</b></h2>
                <p>Dirección: Pío XI número 135 - Pehuajó - Buenos Aires - Argentina <br>
                <a title="Perfil de Linkedin de Bárbara" href="https://www.linkedin.com/in/botero-lp-arg/">Haz click aquí para contactar con Bárbara Otero por Linkedin</a></p>
                <br> <br>
                </div>

            </div>

            <div tabindex="32">
                <div class="col-sm-12 text-center"> 
                    <figure>
                        <img src="images\TareaPerfilBarbara\FotoParaTarea.jpeg" alt="Imagen de un paisaje invernal en Caviahue, Neuquén, Argentina. La foto está tomada desde una pista de sky y se puede ver el cielo y las aerosillas en la parte superior, montañas nevadas a los lados con araucarias en sus laderas y un lago en la parte inferior. La pista de sky y las montañas están cubiertas de nieve.">
                    </figure>
                </div>
            </div>

        </div>
</section>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">

            </div>

                   <!--  <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
                  <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                       <h2>Envíanos un mensaje</h2>
                       <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php">
                        <div class="form-group">
                            <input tabindex="51" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Nombre">
                        </div>
                        <div class="form-group">
                            <input tabindex="52" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <textarea tabindex="53" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Tu mensaje"></textarea>
                        </div>            
                        <div style="display:none"> 
                            <input id="cc" value="sebastian@comparlante.com" placeholder="E-mail"> 
                        </div>             
                        <div class="form-group">
                            <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Enviar mensaje</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                    <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/main_es.js"></script>   
</body>


</html>