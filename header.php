  <header id="header">      

    <div class="navbar navbar-inverse" role="banner">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Barras de navegación</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index_es.php">
            <h1><img class="logo-a" src="images/home/logofundacionhorizontal_act.png" width="80%" alt="Logo Comparlante"></h1>
          </a>

        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
           <li><a href="index_es.php" tabindex="86">INICIO</a></li>
           <li><a href="equipo.php" tabindex="87">Quiénes somos</a></li>

           <li class="dropdown"><a href="#" aria-haspopup="true" aria-expanded="false">Programas <i class="fa fa-angle-down"></i></a>
            <ul role="menu" class="sub-menu">

              <li><a href="accesibilidad-que-transforma.php">Accesibilidad</a></li>
              <li><a href="arte-empatia.php">Arte</a></li>
              <li><a href="emprendimiento.php">Emprendimiento </a></li>
               <li><a href="https://www.incluyes.com">IncluYes</a></li>
               <li><a href="http://www.comparlante.com/biblioteca_audiolibros/comparlante/index.php?r=BuscadorLibros/biblioteca">Biblioteca de audiolibros</a></li>   


            </ul>
          </li>   
          <li><a href="donar.php">Donar</a></li>

          <li><a href="index_es.php#involucrate">Involúcrate</a></li>
          <!-- <li><a href="index_es.php#equipo">Equipo</a></li> -->
          <li><a href="contacto_es.php">Contacto</a></li>         


        </ul>
      </div>
      <div class="col-sm-12 overflow media-movil">
       <div class="social-icons pull-right">
        <div style="display:none" id="tp1" role="tooltip"><p>Facebook.</p> </div>  
        <div style="display:none" id="tp2" role="tooltip"><p>Twitter. </p></div>
        <div style="display:none" id="tp3" role="tooltip"><p>Instagram. </p></div>
        <div style="display:none" id="tp4" role="tooltip"><p>YouTube.</p> </div>
        <div style="display:none" id="tp5" role="tooltip"><p>LinkedIn.</p> </div>

        <ul class="nav nav-pills">
          <li><a tabindex="90" alt="Facebook" title="Facebook" aria-labelledby="tp1" href="https://www.facebook.com/pages/Comparlante/1537630623166015?ref=aymt_homepage_panel" target="_blank" ><i class="fa fa-facebook"></i></a></li>
          <li><a tabindex="91" title="Twitter" alt="Twitter" aria-labelledby="tp2" href="https://twitter.com/comparlante" target="_blank" ><i class="fa fa-twitter"></i></a></li>
          <li><a tabindex="92" title="Instagram" alt="Instagram" aria-labelledby="tp3" href="https://instagram.com/comparlante?igshid=hjvgvukjtfyl" target="_blank" ><i class="fa fa-instagram"></i></a></li>
          <li><a tabindex="93" title="YouTube" alt="YouTube" aria-labelledby="tp4" href="http://youtube.com/channel/UCTYBW1pJ5TcsCuMAYSm_10Q" target="_blank" ><i class="fa fa-youtube"></i></a></li>
          <li><a title="Linkedin" alt="LinkedIn" tabindex="94" aria-labelledby="tp5" href="https://www.linkedin.com/in/fundaci%C3%B3n-comparlante-207b88bb" target="_blank" ><i class="fa fa-linkedin"></i></a></li>
          <li class="aumentar-redes"><a href="#" id="zoom_in" title="Aumentar tamaño texto" tabindex="95" onclick="aumentar()"  aria-label="Aumentar tamaño del texto"><b>A+</b></i></a></li>
          <li class="aumentar-redes"><a href="#" id="zoom_out" title="Disminuir tamaño texto"  tabindex="96" onclick="disminuir()"  aria-label="Disminuir tamaño del texto"><b>A-</b></a></li>
        </ul>
      </div> 
    </div>
  </div>
        <!-- <div class="search">
            <form role="form">
                <i class="fa fa-search">Acceder/Registrarse</i>
                <div class="field-toggle">
                    <input type="text" class="search-form" autocomplete="off" placeholder="Search">
                </div>
            </form>
          </div> -->
        </div>
      </div>
    </header>

    <div class="letras">
     <a href="#" id="zoom_in" alt="Disminuir tamaño del texto" title="Disminuir tamaño del texto" tabindex="95" onclick="aumentar()" aria-label="Aumentar tamaño del texto"><h3 ><b>A+</b></h3></i></a>
     <br>
     <a href="#" id="zoom_out" alt="Disminuir tamaño del texto" title="Disminuir tamaño del texto"  tabindex="96" onclick="disminuir()" aria-label="Disminuir tamaño del texto"><h3 ><b>A-</b></h3></a>



   </div>

   <div class="donar">
     <a href="donar.php" id="zoom_in" alt="Contribuye con tu donación" title="Contribuye con tu donación" tabindex="99" aria-label="Contribuye con tu donación"><h3><b>Donar</b></h3></i></a>
   </div>


   <style type="text/css">
   @media (max-width: 740px) and (min-width: 50px) {
    .letras, .donar   {
      display: none !important;
    }
    .media-movil{
      position: fixed;
      background-color: #fff;
      z-index: 99;
     
    }

    .aumentar-redes{
       display: inherit!important;
    }
  }

  .aumentar-redes{
    display: none!important;
  }
  .letras{
   margin-left: 0%;
   border-color: #f9c741;
   background-color: #f9c741;
   border-width: 1px;
   border-style: solid;
   position: fixed;
   padding-left: 1%;
   z-index: 99;
   width: 4%;
   border-top-right-radius: 2em;
   border-bottom-right-radius: 2em;
 }

 .donar{
   right: 0%;
    border-color: #f9c741;
    background-color: #f9c741;
    border-width: 1px;
    border-style: solid;
    position: fixed;
    padding-left: 1%;
    z-index: 99;
    width: 7%;
    border-top-left-radius: 2em;
    border-bottom-left-radius: 2em;
 }

/* .donar{
       margin-left: 0%;
    border-color: #f9c741;
    background-color: #f9c741;
    border-width: 1px;
    border-style: solid;
    position: fixed;
    padding-left: 1%;
    z-index: 99;
    width: 8%;
    border-top-right-radius: 2em;
    border-bottom-right-radius: 2em;
    margin-top: 15%;
    height: 59px;
 }*/

</style>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75127504-2', 'auto');
  ga('send', 'pageview');

</script>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WRVC32B"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript)-->




  <script type="text/javascript">
    function aumentar(){
      if(window.parent.document.body.style.zoom!=0) window.parent.document.body.style.zoom*=1.2; else window.parent.document.body.style.zoom=1.2;
        // var fontSize;
        //  fontSize += 1;
        //     document.body.style.fontSize = fontSize + "px";
      }

      function disminuir(){
        if(window.parent.document.body.style.zoom!=0) window.parent.document.body.style.zoom*=0.8; else window.parent.document.body.style.zoom=0.8;
    //     var fontSize;
    //      fontSize -= 0.1;
    //         document.body.style.fontSize = fontSize + "px";
  }
</script>


