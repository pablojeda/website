function realizaProceso(){

	nombre=document.getElementById("nombre");
	email = document.getElementById("email");
	web=document.getElementById("web");
	valor= true;

	if( nombre.value == "" || email.value == "" || web.value == ""){
		swal("Lo siento", "Debes completar tus datos", "error");
		valor = false;
	}

	var strExpReg = /^(www\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/gi;
	var sitio_web = web.value;
	if (sitio_web.match(strExpReg)){
		
		if (valor == true) {
			parametros = {	
				"nombre":nombre.value,
				"email":email.value,
				"web":web.value};
				$.ajax({
					data:parametros,
					url:'achecker.php',
					type:'post',
					cache: false,
					beforeSend: function(){
						$('#loading').css("visibility", "visible");
					},
					
					success: function (response) {
						
						setTimeout(function () {
							swal("Gracias","Ya enviamos los resultados a tu email, revisa tu bandeja de entrada." ,"success");
						}, 2000);
						
					},
					complete: function(){
						$('#loading').hide();
					},
					error: function () {
						swal({ 
							title: "Ha ocurrido un error",
							text: "Lo sentimos, no hemos podido verificar tu sitio :(",
							type: "error",
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Intentar nuevamente",
							closeOnConfirm: false },

							function(isConfirm){ 
								if (isConfirm) {
									location.reload();
								} 
							});
					}
					}); //fin ajax
			}
		} else {
			swal("Sitio web no válido", "prueba con formato ej: www.tusitio.com", "error");
		}
		
	}