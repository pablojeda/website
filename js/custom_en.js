function realizaProceso(){

	nombre=document.getElementById("nombre");
	email = document.getElementById("email");
	web=document.getElementById("web");
	valor= true;

	if( nombre.value == "" || email.value == "" || web.value == ""){
		swal("Sorry", "You must complete your data", "error");
		valor = false;
	}

	var strExpReg = /^(www\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/gi;
	var sitio_web = web.value;
	if (sitio_web.match(strExpReg)){
		
		if (valor == true) {
			parametros = {	
				"nombre":nombre.value,
				"email":email.value,
				"web":web.value};
				$.ajax({
					data:parametros,
					url:'achecker.php',
					type:'post',
					cache: false,
					beforeSend: function(){
						$('#loading').css("visibility", "visible");
					},
					
					success: function (response) {
						
						setTimeout(function () {
							swal("Thanks","We already send the results to your email, check your inbox." ,"success");
						}, 2000);
						
					},
					complete: function(){
						$('#loading').hide();
					},
					error: function () {
						swal({ 
							title: "An error has occurred",
							text: "Sorry, we were unable to verify your site :(",
							type: "error",
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Try again",
							closeOnConfirm: false },

							function(isConfirm){ 
								if (isConfirm) {
									location.reload();
								} 
							});
					}
					}); //fin ajax
			}
		} else {
			swal("Invalid website", "Try with a formatted test example: www.yourSite.com", "error");
		}
		
	}