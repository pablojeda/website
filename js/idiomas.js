
function readCookie(name) {

  var nameEQ = name + "="; 
  var ca = document.cookie.split(';');

  for(var i=0;i < ca.length;i++) {

    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) {
      return decodeURIComponent( c.substring(nameEQ.length,c.length) );
    }

  }

  return null;

}

function espanol(){
	document.cookie = "comparlante=es;";
	
	// alert( miCookie );
	window.location= "index_es.php"
}

function ingles(){
	document.cookie = "comparlante=en;";
	// var miCookie = readCookie( "comparlante" );
	// alert( miCookie );
	window.location= "english/index_en.php"
}

function redireccionar(){
	var miCookie = readCookie( "comparlante" );
	if (miCookie.length > 0){
		if(miCookie == "es"){
			window.location= "index_es.php"
		}
		if(miCookie == "en"){
			window.location= "english/index_en.php"
		}
	}
}
