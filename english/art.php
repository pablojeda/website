<!DOCTYPE html>
<html lang="en">

<head><meta charset="gb18030">
 
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <meta name="description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities.">
 <meta name="author" content="Prime Developers Chile">

 <!-- Facebook Metadatos -->
 <!--  Inicio -->
 <meta property="og:title" content="Comparlante Foundation | HOME"/>
 <meta property="og:site_name" content="Comparlante Foundation"/>
 <meta property="og:description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities." />
 <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/home/logo-fundacion-2.jpg"/>
 <!--   <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/index_es.php"/> -->




 <title>Fundación Comparlante</title>
 <link href="../css/bootstrap.min.css" rel="stylesheet">
 <link href="../css/font-awesome.min.css" rel="stylesheet">
 <link href="../css/animate.min.css" rel="stylesheet"> 
 <link href="../css/lightbox.css" rel="stylesheet"> 
 <link href="../css/main.css" rel="stylesheet">
 <link href="../css/responsive.css" rel="stylesheet">


 <link rel="shortcut icon" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" href="../images/ico/logo-icon.png">

 <!-- Google Tag Manager -->
 <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
 j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head>

<body>
  <?php include("header.php"); ?>

  <section id="titulo" tabindex="10">
    <div class="row  padding" style="background-color: #f6f6f6;">
      <div class="container" >
        <div class="row">
          <div class="wow scaleIn " >
            <center>
              <img src="../images/programas/arte.png" alt="A plane, a pencil and a square, tools used in art. ">
            </center>
          </div>

          <div class="col-sm-12 padding-top-index text-justify">

           <center>
             <h2><b>Art to Promote Empathy </b></h2>
           </center>
         </div>
       </div>
     </div>
   </div>

 </section>

 <section id="portfolio-information" >
  <div class="container"> 
    <br><br>
    <!-- asesoria en accesibilidad -->
    <div class="row" tabindex="15">
      <div class="project-info overflow">
        <center>
          <a href="accesibleart.php">
            <img src="../images/services/wefeel.jpg" class="img-responsive" width="40%" alt="Accessible Art Logo">
          </a>
        </center>
      </div>
      <br>
      <div class="row">
        <center>
          <h2><b>Accessible Art</b></h2>
        </center>
      </div>
      <div class="row">

        <div class="col-sm-12">

          <div  class="project-info overflow " style="text-align:justify">

            <h2>It is an artistic exhibition that aims to promote empathy and critical thinking in decision makers. Sculpture, audiovisual art, photography and interventions invite visitors to feel and put themselves in the shoes of persons with disabilities to change the attitude from society and, instead of just looking at the disability, begin to focus on the capacity and possibilities.</h2>
            <p> <center><a tabindex="16" type="button" href="accesibleart.php" class="btn btn-info">Read more</a> </center> </p>
          </div>
          <br>
        </div>
      </div>
    </div>

    <!-- fin asesoria accesibilidad -->


    <!-- desarrollo web -->
    <div class="row" tabindex="20">
      <div class="project-info overflow">
        <center>
          <img src="../images/programas/we-feel.png" class="img-responsive" width="40%" alt="We Feel Logo">
        </center>
      </div>
      <br>
      <div class="row">
        <center>
          <h2><b>We Feel</b></h2>
        </center>
      </div>
      <div class="row">

        <div class="col-sm-12">

          <div  class="project-info overflow " style="text-align:justify">

            <h2>A photographic exhibition that portrays persons with disabilities carrying out different activities in full. This awareness campaign offers a real approach to the daily life of persons with disabilities when they love, enjoy, create, dance, paint, teach, ... live.
            </h2>
                    <p><center><a tabindex="16" type="button" href="wefeel.php" class="btn btn-info">Read more</a></center>  </p>

          </div>
          <br>
        </div>
      </div>
    </div>

    <!-- diseño universal -->

    <div class="row" tabindex="25">
      <div class="project-info overflow">
        <center>
          <a href="contest.php">
            <img src="../images/concurso/personajes2020.png" class="img-responsive" width="40%" alt="The 4 animated characters of the Contest: boy with visual disability, girl with motor disability, girl with Down syndrome and boy with hearing disability, playing together with the earth globe as a ball.">
          </a>
        </center>
      </div>
      <br>
      <div class="row">
        <center>
          <h2><b>My World My Way
          </b></h2>
        </center>
      </div>
      <div class="row">

        <div class="col-sm-12">

          <div  class="project-info overflow " style="text-align:justify">

            <h2>"My World a My Way" is an International Literary Contest that promotes awareness of people with disabilities in the school environment. It proposes, through literature, to offer a space for reflection in society.
            </h2>
             <p><center><a tabindex="16" type="button" href="my-world-my-way-2020.php" class="btn btn-info">Read more</a></center>  </p>
          </div>
          <br>
        </div>
      </div>
    </div>


  </div>
</section>

<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center bottom-separator">

      </div>
      <div class="col-md-12 col-sm-12">
        <div id="contacto" class="contact-form bottom">
         <h2>Send Us a Message</h2>
         <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php"
         <div class="form-group">
          <input type="text" name="name" class="form-control" required="required" placeholder="Name">
        </div>
        <div class="form-group">
          <input type="email" name="email" class="form-control" required="required" placeholder="E-mail">
        </div>
        <div class="form-group">
          <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Your message"></textarea>
        </div>
        <div style="display:none"> 
          <input id="cc" value="" placeholder="E-mail"> 
        </div>                         
        <div class="form-group">
          <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Send</button>
        </div>
      </form>
    </div>
  </div>
  <div class="col-sm-12">
    <div class="copyright-text text-center">
      <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
      <p>Developed by <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
    </div>
  </div>
</div>
</div>
</footer>
<!--/#footer-->
<!--/#footer-->
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/lightbox.min.js"></script>
<script type="text/javascript" src="../js/wow.min.js"></script>
<script type="text/javascript" src="../js/main_es.js"></script>   
</body>


</html>
