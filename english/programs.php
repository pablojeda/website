<!DOCTYPE html>
<html lang="es">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad.">
    <meta name="author" content="Prime Developers Chile">
    
    <!-- Facebook Metadatos | Diseño web accesible -->
    <meta property="og:title" content="Fundación Comparlante | Programas"/>
    <meta property="og:site_name" content="Programas"/>
    <meta property="og:description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad" />
    <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/services/2/diseno-web.jpg"/>
    <!-- <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/diseno-web.php"/> -->

    
    
   <title>Comparlante Fundation</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet"> 
    <link href="../css/lightbox.css" rel="stylesheet"> 
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">


    <link rel="shortcut icon" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" href="../images/ico/logo-icon.png">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head><!--/head-->

<body>
  <?php include("header.php"); ?>

  <section id="page-breadcrumb">
    <div class="vertical-center sun">
       <div class="container">
        <div class="row">
            <div class="action">
                <div tabindex="10" class="col-sm-12">
                    <h1 class="title">Programs</h1>
                    <p>From our effort are born different proposals to make the world, a more inclusive and better place to live.</p>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!--/#page-breadcrumb-->

<section id="portfolio-information" >
    <div class="container"> 
        <br><br>
        <div class="row">
            <h2 class="title">Incluyes - MarketPlace</h2>
        </div>
        <div class="project-info overflow">
            <center>
                <img src="../images/programas/incluyes-logo-morado.png" class="img-responsive" width="40%" alt="Representative logo of incluYes MarketPlace.">
            </center>
        </div>
        <br>
        <div class="row">

            <div class="col-sm-12">

                <div tabindex="11" class="project-info overflow " style="text-align:justify">

                    <h2>Accessible platform of supply and demand of products and services for people with disabilities. The best opportunities in Latin America and the world in one place. </h2>

                </div>
                <br>
                <center>
                    <div taindex="13" class="live-preview" data-wow-duration="500ms" data-wow-delay="300ms">


                        <a tabindex="14" target="_blank" href="http://www.incluyes.com" class="btn btn-lg btn-info" >Visit IncluYes</a>

                    </div>
                </center>


            </div>
        </div>
        <hr>
        <!-- arte accesible -->
  <!--       <div class="row">
            <h2 class="title">Accessible Art</h2>
        </div>
        <div class="project-info overflow">
            <center>
                <img src="../images/programas/arte-accesible.PNG" class="img-responsive" width="40%" alt="Logo de programa arte accesible.">
            </center>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-12">
                <div tabindex="11" class="project-info overflow " style="text-align:justify">

                    <h2>Campaña de concientización que usa el arte como herramienta para sensibilizar sobre la importancia de generar entornos accesibles. </h2>

                </div>
                <br>
                <center>
                    <div taindex="13" class="live-preview" data-wow-duration="500ms" data-wow-delay="300ms">


                        <a tabindex="14" target="_blank" href="arteaccesible.php" class="btn btn-lg btn-info" >Ir a Arte Accesible</a>

                    </div>
                </center>
            </div>
        </div>
        <hr> -->
        <!-- mi mundo a mi manera -->
        <div class="row">
            <h2 class="title">My World My Way</h2>
        </div>
        <div class="project-info overflow">
            <center>
                <img src="../images/programas/mi-mundo-a-mi-manera.png" class="img-responsive" width="40%" alt="The 4 animated characters of the Contest: boy with visual disability, girl with motor disability, girl with Down syndrome and boy with hearing disability, playing together with the earth globe as a ball.">
            </center>
        </div>
        <br>
        <div class="row">

            <div class="col-sm-12">

                <div tabindex="11" class="project-info overflow " style="text-align:justify">

                    <h2>International literary contest for children from 6 to 13 years old. In its first edition, in 2017, we have participating students from 5 countries. </h2>

                </div>
                <br>
                <center>
                    <div taindex="13" class="live-preview" data-wow-duration="500ms" data-wow-delay="300ms">


                        <a tabindex="14" target="_blank" href="my-world-my-way.php" class="btn btn-lg btn-info" >Visit My World My Way</a>

                    </div>
                </center>
                

            </div>
        </div>



    </div>
</section>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">

            </div>
            <div class="col-md-12 col-sm-12">
                <div id="contacto" class="contact-form bottom">
                   <h2>Send Us a Message</h2>
                   <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php"
                   <div class="form-group">
                    <input type="text" name="name" class="form-control" required="required" placeholder="Name">
                </div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" required="required" placeholder="E-mail">
                </div>
                <div class="form-group">
                    <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Your message"></textarea>
                </div>
                <div style="display:none"> 
                    <input id="cc" value="" placeholder="E-mail"> 
                </div>                         
                <div class="form-group">
                    <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Send</button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="copyright-text text-center">
            <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
            <p>Developed by <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
        </div>
    </div>
</div>
</div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/lightbox.min.js"></script>
<script type="text/javascript" src="../js/wow.min.js"></script>
<script type="text/javascript" src="../js/main_es.js"></script>   
</body>


</html>
