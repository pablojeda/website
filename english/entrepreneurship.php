<!DOCTYPE html>
<html lang="en">

<head><meta charset="gb18030">
 
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <meta name="description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities.">
 <meta name="author" content="Prime Developers Chile">

 <!-- Facebook Metadatos -->
 <!--  Inicio -->
 <meta property="og:title" content="Comparlante Foundation | HOME"/>
 <meta property="og:site_name" content="Comparlante Foundation"/>
 <meta property="og:description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities." />
 <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/home/logo-fundacion-2.jpg"/>
 <!--   <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/index_es.php"/> -->




 <title>Fundación Comparlante</title>
 <link href="../css/bootstrap.min.css" rel="stylesheet">
 <link href="../css/font-awesome.min.css" rel="stylesheet">
 <link href="../css/animate.min.css" rel="stylesheet"> 
 <link href="../css/lightbox.css" rel="stylesheet"> 
 <link href="../css/main.css" rel="stylesheet">
 <link href="../css/responsive.css" rel="stylesheet">


 <link rel="shortcut icon" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" href="../images/ico/logo-icon.png">

 <!-- Google Tag Manager -->
 <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
 j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head>

<body>
  <?php include("header.php"); ?>

  <section id="titulo" tabindex="10">
    <div class="row  padding" style="background-color: #f6f6f6;">
      <div class="container" >
        <div class="row">
          <div class="wow scaleIn " >
            <center>
              <img src="../images/programas/emprendimiento.png" alt="A light bulb representing ideas">
            </center>
          </div>

          <div class="col-sm-12 padding-top-index text-justify">

           <center>
             <h2><b>Entrepreneurship, Independence and Dignity </b></h2>
           </center>
         </div>
       </div>
     </div>
   </div>

 </section>

 <section id="portfolio-information" >
  <div class="container"> 
    <br><br>
    <div class="row" tabindex="13">
      <div  class="project-info overflow " style="text-align:justify">

        <h2> <center> We all have an idea!  </center><br> 

          Encourage your entrepreneurial spirit and boost your business skills with us.
          <br>
          <br>
          Here you will find: 
          <br>
          <br>

          Mentoring tailored to the development of your business idea or its scaling.
          <br>
          Creation and strengthening of skills in project management, planning, fundraising, marketing and communication, among others.
          <br>
          <br>
          <b>One-on-one consulting:</b> If you are looking for a mentor to accompany your business idea development process or you need to scale it but do not have access to financial tools, we want to support you! Our consultancies are offered in two modalities: Online and Home Visits, it can be tailored to your needs!
          <br>
          <br>
          <b>Workshops: </b> Receive professional, dynamic and accessible training in entrepreneurship and innovation through group workshops to catalyze the best of you. These are in three stages:
          <br> <br>
          <ul>
            <li><b>Entrepreneur 1.0.</b> At this stage, the conditions in which the participants start their business are defined. They should not start in their project without defining their entrepreneurial qualities.</li>
            <li>
              <br>
              <b>I start my business.</b> This stage is the first approach of the entrepreneur with the definition of an enterprise, its main needs, including "the client".
              <br>

            </li>
            <li>
              <br>
              <b>My first business.</b> Here, the value proposition of an entrepreneur is strengthened. The entrepreneur must be able to know the market, their needs and identify their client. In this way, it deals with the ability to develop solid and sustainable value propositions.
            </li>
          </ul>
          <br>
       

        </h2>

      </div>
    </div>
    <!-- asesoria en accesibilidad -->
<!--     <div class="row" tabindex="15">
      <div class="project-info overflow">
        <center>
          <img src="../images/services/asesoramiento-productivo.png" class="img-responsive" width="40%" alt="Logo de Asesorías individuales.">
        </center>
      </div>
      <br>
      <div class="row">
        <center>
          <h2><b>Individual Counseling</b></h2>
        </center>
      </div>
      <div class="row">

        <div class="col-sm-12">

          <div  class="project-info overflow " style="text-align:justify">

            <h2>Under the commitment assumed by Comparlante to promote more and better opportunities for the dignifying economic and social development of persons with disabilities, this space seeks to improve access to business information, training, technology, consulting services and technical and financial resources. We offer group workshops and one to one mentoring focused on the promotion of productive agents prepared to lead the new era of universal accessibility and entrepreneurship. </h2>

          </div>
          <br>
        </div>
      </div>
    </div> -->

    <!-- fin asesoria accesibilidad -->
    
    <!-- desarrollo web -->
  <!--   <div class="row padding" tabindex="18">

        <br>
        <div class="row">
            <center>
                <h2><b>Workshops</b></h2>
            </center>
        </div>
        <div class="row">

            <div class="col-sm-12">

                <div  class="project-info overflow " style="text-align:justify">

                    <h2>
                      Receive a professional, dynamic and accessible training in entrepreneurship and innovation through group workshops designed to catalyze the best of you.


                    </h2>

                </div>
                <br>
            </div>
        </div>
      </div> -->



    </div>
  </section>

  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center bottom-separator">

        </div>
        <div class="col-md-12 col-sm-12">
          <div id="contacto" class="contact-form bottom">
           <h2>To request consulting  or learn about our upcoming activities, leave us a message!</h2>
           <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php"
           <div class="form-group">
            <input type="text" name="name" class="form-control" required="required" placeholder="Name">
          </div>
          <div class="form-group">
            <input type="email" name="email" class="form-control" required="required" placeholder="E-mail">
          </div>
          <div class="form-group">
            <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Your message"></textarea>
          </div>
          <div style="display:none"> 
            <input id="cc" value="" placeholder="E-mail"> 
          </div>                         
          <div class="form-group">
            <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Send</button>
          </div>
        </form>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="copyright-text text-center">
        <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
        <p>Developed by <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
      </div>
    </div>
  </div>
</div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/lightbox.min.js"></script>
<script type="text/javascript" src="../js/wow.min.js"></script>
<script type="text/javascript" src="../js/main_es.js"></script>   
</body>


</html>
