<!DOCTYPE html>
<html lang="en">


<head><meta charset="gb18030">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities.">
    <meta name="author" content="Prime Developers Chile">
    
     <!-- Facebook Metadatos | Contact -->
    <meta property="og:title" content="Fundación Comparlante | Contact  "/>
    <meta property="og:site_name" content="Contact"/>
    <meta property="og:description" content="Send Us a Message" />
    <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/services/2/contacto.jpg"/>
  <!--  <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/contacto_es.php"/> -->

    
    
    
    <title>Fundación Comparlante</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet"> 
    <link href="../css/lightbox.css" rel="stylesheet"> 
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">

        <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
    </head><!--/head-->

    <body>
      <?php include("header.php"); ?>

      <section id="page-breadcrumb">
        <div class="vertical-center sun">
           <div class="container">
            <div class="row">
                <div class="action">
                    <div tabindex="10" class="col-sm-12">
                        <h1 class="title">Send Us a Message </h1>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#page-breadcrumb-->

<section id="portfolio-information" >
    <div class="container"> <br><br>
                 <div  class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                        
                        <form id="main-contact-form" name="contact-form" method="post" action="../contacto2.php">
                        <div class="form-group">
                            <input tabindex="12" id="nombre-formulario" type="text" name="name" class="form-control" required="required" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <input tabindex="13" id="email-formulario" type="email" name="email" class="form-control" required="required" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <textarea tabindex="13" id="mensaje-formulario" name="message" id="message" required="required" class="form-control" rows="8" placeholder="Your message"></textarea>
                        </div>         
                        <div style="display:none"> 
                            <input id="cc" value="" placeholder="E-mail"> 
                        </div>                 
                        <div class="form-group">
                            <input tabindex="13" type="submit" name="submit" class="btn btn-submit" value="Send">
                        </div>
                    </form>
                </div>
            </div>
            </div>
        </section>

        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center bottom-separator">
                        <img src="../images/home/under.png" class="img-responsive inline" alt="">
                    </div>

         
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                    <p>Developed by <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/lightbox.min.js"></script>
<script type="text/javascript" src="../js/wow.min.js"></script>
<script type="text/javascript" src="../js/main_en.js"></script>   
</body>


</html>
