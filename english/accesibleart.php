<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <meta name="description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities.">
 <meta name="author" content="Prime Developers Chile">

 <!-- Facebook Metadatos -->
 <!--  Inicio -->
 <meta property="og:title" content="Comparlante Foundation | HOME"/>
 <meta property="og:site_name" content="Comparlante Foundation"/>
 <meta property="og:description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities." />
 <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/home/logo-fundacion-2.jpg"/>
 <!--   <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/index_es.php"/> -->




 <title>Fundación Comparlante</title>
 <link href="../css/bootstrap.min.css" rel="stylesheet">
 <link href="../css/font-awesome.min.css" rel="stylesheet">
 <link href="../css/animate.min.css" rel="stylesheet"> 
 <link href="../css/lightbox.css" rel="stylesheet"> 
 <link href="../css/main.css" rel="stylesheet">
 <link href="../css/responsive.css" rel="stylesheet">


 <link rel="shortcut icon" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" href="../images/ico/logo-icon.png">

 <!-- Google Tag Manager -->
 <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
 j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head>

<body>

  <!--#include file="header.html"-->
  <?php include("header.php"); ?>

  <section id="page-breadcrumb">
    <div class="vertical-center sun">
     <div class="container">
      <div class="row">
        <div class="action">
          <div tabindex="10" class="col-sm-12">
            <h1 class="title">Accessible Art</h1>
            <p></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--/#page-breadcrumb-->

<section id="portfolio-information" >
  <div class="container">
    <br><br>
        <!-- <div class="project-info overflow">
            <center>
                <img src="images/services/1/diseno-accesible.png" class="img-responsive " alt="Diseño Accesible – Accesibilidad Creativa"></center>
              </div> -->
              <br>
              <div class="row">

                <div class="col-sm-12">

                  <div tabindex="11" class="project-info overflow " style="text-align:justify">
                    <h2>Accessible Arte is an awareness campaign that uses art as a tool to create awareness of the importance of generating accessible environments.
                    </h2>
                    <br>
                    <h2>Sculptural objects, photography, installations and audiovisual material are the supports with which we want to generate a mindset shift in non-disabled people. Using reverse psychology we will put decision makers in the shoes of those who suffer exclusion.

                    </h2>
                    <br>
                    <h2>
                    Accessible Art will be exhibited in the Congresses of different countries, because there is where legislations are created and where it is needed  to sensitize the effectors of the laws. The pilot will be implemented in 2019 in Argentina, Ecuador and the headquarters of the Organization of American States in Washington DC.



                    </h2>
                    <br>
                    <h2>
                     The project includes the presentation of the exhibition for a month in each location, which would give the opportunity to contact other artists and generate community. Together with other non-governmental organizations and foundations, develop parallel networks that promote accessibility throughout the hemisphere.


                   </h2>
                   <br>

                   <br>
                   <h2>
                    We are currently in the process of finding sponsors to carry out this proposal. For this, we seek the support of the private and public sector. If you want to be a sponsor contact us via <a href="mailto:info@comparlante.com">info@comparlante.com</a>
                  </h2>
                  <br>
                  <h2>


                    <br><br>
                    <a tabindex="" style="background-color: #f39917!important; width: 400px!important;
                    border-color: #f39917!important;"  alt="Download the pieces catalogue"   type="button" href="accesible-art.pdf" class="btn btn-info"><h4>Click here to access the pieces catalogue</h4></a>

                    <br><br>
                  </h2>

                </div>


              </div>
            </div>
          </div>
        </section>


<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">

            </div>

        <div class="col-sm-12">
            <div class="copyright-text text-center">
                <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                <p>Developed by <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
            </div>
        </div>
    </div>
</div>
</footer>
<!--/#footer-->
<!--/#footer-->

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/lightbox.min.js"></script>
<script type="text/javascript" src="../js/wow.min.js"></script>
<script type="text/javascript" src="../js/main_es.js"></script>   
</body>


          </html>
