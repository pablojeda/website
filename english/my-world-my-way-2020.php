<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities.">
    <meta name="author" content="Prime Developers Chile">
    
    <!-- Facebook Metadatos | Diseño web accesible -->
    <meta property="og:title" content="I Literary contest for girls and boys in elementary school 'My world, My way'"/>
    <meta property="og:site_name" content="I Literary contest for girls and boys in elementary school 'My world, My way'"/>
    <meta property="og:description" content="From Comparlante Foundation we want all the little dreamers to have the opportunity to develop a story that involves at least one of the four characters of our Foundation. That's why we are pleased to invite children and writers to give life to one of our four characters in a story." />
    <meta property="og:image" content="http://comparlante.com/images/concurso/personajes.png"/>
    <!-- <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/diseno-web.php"/> -->

    
    
    <title>Comparlante Foundation</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet"> 
    <link href="../css/lightbox.css" rel="stylesheet"> 
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" href="../images/ico/logo-icon.png">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head><!--/head-->

<body>
  <?php include("header.php"); ?>

  <section id="page-breadcrumb">
    <div class="vertical-center sun">
       <div class="container">
        <div class="row">
          <center>
            <img width="50%" src="../images/concurso/winners.png" class="img-responsive " alt="Illustration of the 3 animated characters in My World A My Way Two: girl with cerebral palsy, girl with amputated arm and boy with Autism Spectrum Disorders."></center>
        </div>
        <div class="row">
            <div class="action">
                <div tabindex="10" class="col-sm-12">
                    <h2 style="font-size: 20pt; font-weight: 400" class="title text-center">Second Literary contest for girls and boys in elementary school "My world, My way II"
                    </h2>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!--/#page-breadcrumb-->
<div id="zooming">

    <section id="concurso" >
        <div class="container"> <br><br>
            <div class="project-info overflow">

            </div><br>
            <div class="row">

                <div class="col-sm-12">

                    <div tabindex="11" class="project-info overflow "style="text-align:justify">

                        <h2>We understand that raising awareness in society about different disabilities 
                            promotes equity and literature is a very powerful tool that allows us to work with children. <br>
                         <br>
                         In this second edition of the contest "My world in my way", the children between 6 and 13 years old had the opportunity to develop a story that 
                         involves at least one of the three characters proposed by Fundación Comparlante. </h2>

                 </div>

                <br>
                <!-- primer personaje -->
                <div tabindex="15" class="project-info overflow" style="text-align:justify">
                    <h2>Our 1st character is a child with autism:</h2>
                    <h3>
                        <div  class="col-sm-3">
                            <img src="../images/concurso/1p.png"  class="img-responsive" alt="Child with autism"> 
                        </div> 
                        <br> <br><h3>
                        Autism Spectrum Disorder (ASD) is a neurodevelopmental condition characterized by the presence of alterations in 3 areas: 
                        communication, social interaction and sensory perception.<br><br>
                        People with ASD can present behavior patterns such as restricted interests and repetitive activities, 
                        with difficulties coping with unexpected changes.
                        </h3>
                    </div> 
                    <!-- segundo personaje -->
                    <div tabindex="16" class="project-info overflow" style="text-align:justify">
                        <h2>Our 2nd character is a girl with cerebral palsy:</h2>
                        <h3>
                            <div  class="col-sm-3">
                                <img src="../images/concurso/2p.png"  class="img-responsive" alt="Girl with cerebral palsy"> 
                            </div> 
                            <br> <br>
                            Infantile cerebral palsy (thus defined by the stage of human development where it originates) is a group of disorders that affect a person's 
                            ability to voluntarily move in a coordinated way, maintaining balance and posture. It is the most common motor disability in childhood.

                        </h3>
                        <h3>Depending on the degree of cerebral palsy, some people could make use of implements or assistive devices to assist them in mobility or other 
                            motor functions, and even in some cases, these could be totally reduced.
                        </h3>
                    </div> 

                    <!-- tercer personaje -->
                    <div tabindex="17" class="project-info overflow" style="text-align:justify">
                        <h2>Our 3rd character is a girl with amputated arm or stump:</h2>
                        <h3>
                            <div  class="col-sm-3">
                                <img src="../images/concurso/3p.png" class="img-responsive" alt="Girl with amputated arm"> 
                            </div> 
                            <br> <br>
                            Amputation is the procedure by which a part or limbs of the body (either upper: such as arms or hands, or lower: 
                            such as legs or feet) are removed through one or more bones.
                            <br><br>
                            There are several causes that can lead to amputation, such as traffic, domestic or work accidents, serious gunshot or explosion wounds, 
                            and those related to health conditions or congenital affections, among which diabetes stands out.
                            <br><br>
                            The end of the amputated limb is called a stump. Generally, people with limb loss use a prosthesis: an artificial extension that replaces 
                            the missing body part, supporting the person in carrying out daily activities and for cosmetic purposes.

                        </h3>
                    </div> 

                </div>
            </div>
        </div>
    </section>
    <hr>

                <br>
    <section id="ganadores">
            <div class="container">
                <div class="row">
                    <div tabindex="18" class="project-info overflow " style="text-align:justify">
                        <center><h2 class="center">Meet the winners!</h2></center>
                        <h2>
                        Access the winning stories of the Second Edition and be amazed with the creation of the little writers who with their imagination are already shaping a more accessible world and equity for all.
                        </h2>
                        <br>
                        <h3> <b>First place: Diago Rotman</b></h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Story: Abrazándonos con el corazón.</li>
                            <li><i class="fa fa-angle-right"></i> País: Perú.</li>
                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/ABRAZANDONOS-CON-EL-CORAZON.pdf">See story in digital version.</a></li>

                        </ul>

                    </div> 
                    <div tabindex="22" class="project-info overflow " style="text-align:justify">

                        <h3><b>Second place: Violeta Parra</b> </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Story: El susurrador mágico.</li>
                            <li><i class="fa fa-angle-right"></i> Country: Argentina.</li>
                            
                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/EL-SUSURRADOR-MAGICO.pdf">See story in digital version.</a></li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->

                        </ul>
                    </div> 
                    <div tabindex="25" class="project-info overflow " style="text-align:justify">

                        <h3><b>Third place: Alejandro Valenzuela</b> </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Story: Unos lentes para Sara.</li>
                            <li><i class="fa fa-angle-right"></i> Country: Ecuador.</li>

                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/UNOS-LENTES-PARA-SARA.pdf">See story in digital version.</a></li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->

                        </ul>
                    </div> 
                    
                    <div tabindex="29" class="project-info overflow " style="text-align:justify">

                        <h3><b>Special recognition - Rotary: </b> </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Story: Una oportunidad para Rocío.</li>
                            <li><i class="fa fa-angle-right"></i> Author: Valentín Humberto Chavarría Márquez.</li>
                            <li><i class="fa fa-angle-right"></i> Country: El Salvador.</li>
                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/UNA-OPORTUNIDAD-PARA-ROCIO.pdf">See story in digital version.</a></li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->
                        </ul>
                        
                        <h3><b>Special recognition - JCI: </b> </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Story: Aviones de papel.</li>
                            <li><i class="fa fa-angle-right"></i> Author: Ambar Virginia Carolina Palacios Gómez.</li>
                            <li><i class="fa fa-angle-right"></i> Country: Argentina</li>
                            
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->
                        </ul>
                        <br>
                        <ul class="elements">

                    </div> 
                </div>
            </div>

        </section>

 </section>

</section>


<section id="informacion-concurso">
    <div class="container">
        <div class="row">
            <div tabindex="30" class="project-info overflow " style="text-align:justify">
            <center>
                <br>
                <a tabindex="29" type="button" href="../concurso/mimundoamimanera2.pdf" class="btn btn-info"><h4>Contest rules</h4></a>
                <br>
                <br>
                <br>
                <br>
            </center>
                <br>
                <h3>
                In 2017 we developed the 1st Edition of this children's literary contest. We work hand in hand with schools and families to break down stereotypes in society and promote equity in 5 Latin American countries. KNOW THE WINNING STORIES!

              </h3>
              <br>
              <center>
                <a tabindex="31" type="button" href="my-world-my-way.php" class="btn btn-info"><h4>Winners of the first edition</h4></a>
            </center>
        </div> 


    </div>
</div>
<br>
<!-- seccion de auspiciantes del concurso-->
<section id="auspiciantes">
  <div class="container">
      <div class="row">
        <h3>
        With the support of:    
        </h3>
        <br>
        <center>
          <div  class="col-xs-12 col-sm-3">
            <a title="riadis" href="https://www.riadis.org/"><img src="../images/auspiciantes/Logo+RIADIS+transparente.png" class="img-responsive" alt="Riadis logo, Latin American Network of Non-Governmental Organizations of people with disabilities and their families. It will direct you to the Riadis website" /></a>
          </div> 
            
          <div class="col-xs-12 col-sm-3"> 
            <a title="rotary" href="https://www.rotary.org/es"><img src="../images/auspiciantes/rotary-transparente.png" class="img-responsive" alt="Rotary logo, Second Bicentennial of Pedro Luro. It will direct you to the Rotary website"/></a>
          </div>
          
          <div class="col-xs-12 col-sm-3">
            <a title="jci" href="https://jci.cc/"><img src="../images/auspiciantes/logo+jci.png" class="img-responsive" alt="Logo of JCI Salta, Argentina. Will direct you to the official JCI website." /></a>
          </div> 

          <div class="col-xs-12 col-sm-3">
            <a title="Municipio de Villarino" href="http://www.villarino.gob.ar/"><img src="../images\auspiciantes\Logo+MunicipioVillarino.png" class="img-responsive" alt="Logo of the Municipality of Villarino, Province of Buenos Aires, Argentina. It will direct you to the official site of the Municipality of Villarino, Buenos Aires, Argentina" /></a>
          </div> 
        </center>
      </div>
  </div>

</section>

</section>
</body>
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center bottom-separator">

                    </div>

                   <!--  <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
                  <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                     <h2>Send Us a Message</h2>
                     <form id="main-contact-form" name="contact-form" method="post" action="../contacto2.php">
                        <div class="form-group">
                            <input tabindex="51" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <input tabindex="52" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <textarea tabindex="53" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Your message"></textarea>
                        </div>            
                        <div style="display:none"> 
                            <input id="cc" value="" placeholder="E-mail"> 
                        </div>             
                        <div class="form-group">
                            <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Send</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                    <p>Developed by <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/lightbox.min.js"></script>
<script type="text/javascript" src="../js/wow.min.js"></script>
<script type="text/javascript" src="../js/main_es.js"></script>   
</body>


</html>
