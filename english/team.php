<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <meta name="description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities.">
 <meta name="author" content="Prime Developers Chile">

 <!-- Facebook Metadatos -->
 <!--  Inicio -->
 <meta property="og:title" content="Comparlante Foundation | HOME"/>
 <meta property="og:site_name" content="Comparlante Foundation"/>
 <meta property="og:description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities." />
 <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/home/logo-fundacion-2.jpg"/>
 <!--   <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/index_es.php"/> -->




 <title>Fundación Comparlante</title>
 <link href="../css/bootstrap.min.css" rel="stylesheet">
 <link href="../css/font-awesome.min.css" rel="stylesheet">
 <link href="../css/animate.min.css" rel="stylesheet"> 
 <link href="../css/lightbox.css" rel="stylesheet"> 
 <link href="../css/main.css" rel="stylesheet">
 <link href="../css/responsive.css" rel="stylesheet">


 <link rel="shortcut icon" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" href="../images/ico/logo-icon.png">

 <!-- Google Tag Manager -->
 <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
 j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head>

<body>
  <?php include("header.php"); ?>

  <section id="page-breadcrumb">
      <div class="vertical-center sun">
         <div class="container">
            <div class="row">
              <div class="action">
                <div tabindex="10" class="col-sm-12">
                  <h1 class="title">Who we are</h1>
                  <p></p>
              </div>
          </div>
      </div>
  </div>
</div>
</section>

<section id="equipo">
    <div class="container">
        <div class="row">
            <div tabindex="30">
                <div class="col-sm-12  text-justify">

                    Fundación Comparlante is a non-profit organization that promotes the development and inclusion of persons with disabilities in Latin America. From technological tools, products and services, we promote accessibility, entrepreneurship and equity. Our goal is to ensure that this vulnerable sector of society has full enjoyment of their rights. <br> <br>
                    Fundación Comparlante was created in 2015 in Argentina with the objective to make technological tools which generate accessibility to people with visual impairment. Today, it is a collective project which operates from Latin America and United States (Washington DC) integrated by young entrepreneurs from Argentina, Chile, Costa Rica, Ecuador, El Salvador, Uruguay and Venezuela.

                    <br> <br>
                    <p>
                    <b>About the Founders</b> <br> <br></p>
                    <img class="col-sm-6 img-responsive" src="../images/team/lorenaJulio.jpg" alt="Lorena Julio, Co- Founder & President">
                    <div class="col-sm-6">
                        <p class="text-justify">
                            <b>Lorena Julio - Co- Founder & Director </b> <br>
                            Lorena is a Human Rights advocate with a Master in Compared Public Policies and holds a degree in Social Communication. She has worked for the last ten years on promotion and protection of Human Right in Latin America and the United States.
                            She has worked for the Government of Argentina as Head of Press in the Human Rights Secretariat of the Province of Buenos Aires as well as in the Coordination Team of the Vulnerable Groups Assistance Program. In the international arena, Lorena worked in the Organization of American States (OAS) as Program Officer of the Young Forum of the Americas in the framework of the Presidents Summit. After that, she took functions as Communications Officer at the Young Americas Business Trust, leading the youth networks from the Forum. She did a course on entrepreneurship in Israel and last year she took leadership course for social change makers at Kanthari in India.
                            In 2015 Lorena founded Fundación Comparlante in Argentina with the aim of shortening the gap to access education and information for visually impaired persons. Currently, Comparlante promotes equity through universal accessibility.
                            <br><br></p>
                    </div>       
                    <div class="col-sm-12 text-justify">
                    <img class="col-sm-6 img-responsive" src="../images/team/sebastianFlores.jpg" alt="Sebastian Flores, Co- Founder & Director">
                    <div class="col-sm-6">
                        <p class="text-justify">
                        <b>Sebastián Flores - Co- Founder & Director</b>
                        <br>
                        He is a partially sighted change maker graduated in Diplomacy and International Affairs. Sebastian has developed his professional training focused on Diplomacy, International Politics, Law, democracy and citizen participation, information and communication technologies in the context of Electronic Government and Digital Diplomacy. He is a role model of youth in the global process of sustainable development, humanitarian issues ranging from Migration and Human Mobility, Human Rights, to consultation on accessibility and equity for people with disabilities, both locally and at regional and international levels. Sebastian's activity as a young leader has led him to key positions in projects with social impact by the hand of international organizations such as the UN, OAS, and youth advocacy networks worldwide. Sebastian is a TEDx Speaker, motivator, and mentor, a role that combines with his activity as a delegate in international summits and as Co-Founder of Comparlante.
                    </p> 

                    <br> <br>
                </div> 
                
            </div>

        </div>
        <div class="row">
            
            <h1 class="title text-center wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 500ms; animation-delay: 300ms; animation-name: fadeInDown;">Meet the team</h1>
            <p class="text-center wow fadeInDown animated" data-wow-duration="400ms" data-wow-delay="400ms" style="visibility: visible; animation-duration: 400ms; animation-delay: 400ms; animation-name: fadeInDown;">Our team is made up of many professionals from different fields who share the same aim: to work every single day for a more accessible world. </p> 
            
            <div id="team-carousel" class="carousel slide wow fadeIn animated" data-ride="carousel" data-wow-duration="400ms" data-wow-delay="400ms" style="visibility: visible; animation-duration: 400ms; animation-delay: 400ms; animation-name: fadeIn;">
                <!-- Indicators -->
                <ol class="carousel-indicators visible-xs">
                    <li data-target="#team-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#team-carousel" data-slide-to="1" class=""></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                   <div class="item active">
                    <div tabindex="31" class="col-sm-2 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="../images/team/ana-maidana.jpg" class="img-responsive" alt="Ana Maidana, Administrative area">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Ana Maidana</h2><br>
                                    <p>Secretary</p>
                                    <p>Argentina</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="32" class="col-sm-2 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="../images/team/NairRodriguez.jpeg" class="img-responsive" alt="Nair Rodriguez, treaurer">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Nair Rodriguez</h2>
                                    <p>Treasurer</p>
                                    <p>Argentina</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="33" class="col-sm-2 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="../images/team/roberto-jaramillo.jpg" class="img-responsive" alt="Roberto Jaramillo, Entrepreneurship Consulting ">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Roberto Jaramillo</h2><br>
                                    <p>Entrepreneurship Consulting</p>
                                    <p>Ecuador</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="34" class="col-sm-2 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="../images/team/martinRoyo.jpg" class="img-responsive" alt="Martin Royo, Grant Seeker">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Martin Royo</h2><br>
                                    <p>Grant Seeker</p>
                                    <p>Argentina</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="35" class="col-sm-2 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="../images/team/pablo-ojeda.jpg" class="img-responsive" alt="Pablo Ojeda, Software engineer">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Pablo Ojeda</h2><br>
                                    <p>Software engineer </p>
                                    <p>Chile</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="36" class="col-sm-2 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="../images/team/beatriz-calvo.jpg" class="img-responsive" alt="Beatriz Calvo, Design and Illustrations">
                                    </div>

                                </div>
                                <div class="person-info">
                                <h2>Beatriz Calvo</h2><br>
                                <p>Design and Illustrations</p>
                                <p>Costa Rica</p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- segundo item -->
                    <div class="item">
                    <div tabindex="37" class="col-sm-2 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                    <img src="../images/team/CelesteBenitez.jpg" class="img-responsive" alt="Celeste Benitez, Volunteer, Content Manager ">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>CelesteBenitez</h2><br>
                                    <p>Volunteer - Content Manager </p>
                                    <p>Argentina</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="38" class="col-sm-2 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="../images/team/LudmilaAyelenGuidi.jpg" class="img-responsive" alt="Ludmila Ayelen Guidi, Volunteer -  Rights Advocate">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Ludmila Ayelen Guidi</h2><br>
                                    <p>Volunteer -  Rights Advocate</p>
                                    <p>Argentina</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="39" class="col-sm-2 col-xs-6">
                            <div class="team-single-wrapper">
                                 <div class="team-single">
                                     <div class="person-thumb">
                                         <img src="../images/team/mayraAnabelaLuengo.png" class="img-responsive" alt="Mayra Anabela Luengo, Intern – Web Development">
                                     </div>

                                </div>
                                <div class="person-info">
                                    <h2>Mayra Anabela Luengo</h2><br>
                                    <p>Intern – Web Development</p>
                                    <p>Argentina</p>
                                </div>
                             </div>
                        </div>
                        <div tabindex="40" class="col-sm-2 col-xs-6">
                             <div class="team-single-wrapper">
                                 <div class="team-single">
                                     <div class="person-thumb">
                                         <img src="../images/team/barbaraOtero.jpg" class="img-responsive" alt="Bárbara Otero, Intern – Web Development">
                                     </div>

                                 </div>
                                 <div class="person-info">
                                    <h2>Bárbara Otero</h2><br>
                                    <p>Intern – Web Development</p>
                                   <p>Argentina</p>
                                 </div>
                             </div>
                         </div>
                         <div tabindex="40" class="col-sm-2 col-xs-6">
                             <div class="team-single-wrapper">
                                 <div class="team-single">
                                      <div class="person-thumb">
                                         <img src="../images/team/camila-aprea.png" class="img-responsive" alt="Camila Aprea, Intern, Design">
                                     </div>

                                 </div>
                                 <div class="person-info">
                                    <h2>Camila Aprea</h2><br>
                                    <p>Intern – Design</p>
                                   <p>Argentina</p>
                                 </div>
                             </div>
                         </div>
                         <div tabindex="40" class="col-sm-2 col-xs-6">
                             <div class="team-single-wrapper">
                                 <div class="team-single">
                                      <div class="person-thumb">
                                         <img src="../images/team/PilarRoldan.jpg" class="img-responsive" alt="Pilar Roldán, Intern, Design">
                                     </div>

                                 </div>
                                 <div class="person-info">
                                    <h2>Pilar Roldán</h2><br>
                                    <p>Intern – Design</p>
                                   <p>Argentina</p>
                                 </div>
                             </div>
                         </div>                         
                        </div>

                   </div>

                
                <!-- Controls -->
                <a aria-hidden="true" class="left team-carousel-control hidden-xs" href="#team-carousel" data-slide="prev">left</a>
                <a aria-hidden="true" class="right team-carousel-control hidden-xs" href="#team-carousel" data-slide="next">right</a>
            </div>
        </div>
    </div>
</section>

<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center bottom-separator">

      </div>
      <div class="col-md-12 col-sm-12">
        <div id="contacto" class="contact-form bottom">
         <h2>Send Us a Message</h2>
         <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php"
         <div class="form-group">
          <input type="text" name="name" class="form-control" required="required" placeholder="Name">
      </div>
      <div class="form-group">
          <input type="email" name="email" class="form-control" required="required" placeholder="E-mail">
      </div>
      <div class="form-group">
          <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Your message"></textarea>
      </div>
      <div style="display:none"> 
          <input id="cc" value="" placeholder="E-mail"> 
      </div>                         
      <div class="form-group">
          <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Send</button>
      </div>
  </form>
</div>
</div>
<div class="col-sm-12">
    <div class="copyright-text text-center">
      <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
      <p>Developed by <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
  </div>
</div>
</div>
</div>
</footer>
<!--/#footer-->
<!--/#footer-->
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/lightbox.min.js"></script>
<script type="text/javascript" src="../js/wow.min.js"></script>
<script type="text/javascript" src="../js/main_es.js"></script>   
</body>


</html>
