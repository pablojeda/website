<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities.">
    <meta name="author" content="Prime Developers Chile">
    
    <!-- Facebook Metadatos | Diseño web accesible -->
    <meta property="og:title" content="I Literary contest for girls and boys in elementary school 'My world, My way'"/>
    <meta property="og:site_name" content="I Literary contest for girls and boys in elementary school 'My world, My way'"/>
    <meta property="og:description" content="From Comparlante Foundation we want all the little dreamers to have the opportunity to develop a story that involves at least one of the four characters of our Foundation. That's why we are pleased to invite children and writers to give life to one of our four characters in a story." />
    <meta property="og:image" content="http://comparlante.com/images/concurso/personajes.png"/>
    <!-- <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/diseno-web.php"/> -->

    
    
    <title>Comparlante Foundation</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet"> 
    <link href="../css/lightbox.css" rel="stylesheet"> 
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" href="../images/ico/logo-icon.png">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head><!--/head-->

<body>
  <?php include("header.php"); ?>

  <section id="page-breadcrumb">
    <div class="vertical-center sun">
     <div class="container">
        <div class="row">
            <div class="action">
                <div tabindex="10" class="col-sm-12">
                    <h2 style="font-size: 20pt; font-weight: 400" class="title text-center">I Literary contest for girls and boys in elementary school "My world, My way"</h2>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!--/#page-breadcrumb-->
<div id="zooming">

    <section id="concurso" >
        <div class="container"> <br><br>
            <div class="project-info overflow">
                <center>
                    <img width="50%" src="../images/concurso/personajes.png" class="img-responsive " alt="Characters of contest, a boy with visual impairment appears, to his right a girl with motor disability, a boy with hearing impairment and a girl with down syndrome. Enter for contest information">
                </center>
            </div>
            <br>
            <div class="row">

                <div class="col-sm-12">

                    <div tabindex="11" class="project-info overflow "style="text-align:justify">

                        <h2>Children are the present and the future. Fundación Comparlante understands that raising awareness about the different disabilities promotes equity. In 2017 we developed this children's literary contest to work from schools and with the family to break down stereotypes in society.
                            <br>
                            This international contest for children between 6 to 13 years of age counted in its first edition with the participation of children from Argentina, Ecuador, Costa Rica, Mexico, Peru and Colombia. In their stories they had to involved the characters proposed by Fundación Comparlante.
                        </h2>

                    </div>

                    <br>
                    <!-- primer personaje -->
                    <!-- primer personaje -->
                        <div tabindex="15" class="project-info overflow" style="text-align:justify">
                            <h2>Our 1st character is a child with visual impairment:</h2>
                            <h3>
                                <div  class="col-sm-3">
                                    <img src="../images/concurso/discapacidad-visual.png" width="75%" class="img-responsive" alt="boy with visual impairment"> 
                                </div> 
                                <br> <br>
                                Visual impairment refers to the deficiency of the vision system which affects acuity and visual field, ocular motility and perception of colors and depth, resulting in diagnoses such as low vision or blindness.
                            </h3>
                            <h3>He is accompanied by his guide dog: an animal specially trained to provide assistance for the mobility and independence of the visually impaired.  </h3>
                        </div> 
                    <!-- segundo personaje -->
                      <!-- segundo personaje -->
                        <div tabindex="16" class="project-info overflow" style="text-align:justify">
                            <h2>Our 2nd character is a girl with motor disability:</h2>
                            <h3>
                                <div  class="col-sm-3">
                                    <img src="../images/concurso/discapacidad-motriz.png" width="60%" class="img-responsive" alt="girl with motor disability"> 
                                </div> 
                                <br> <br>
                                Motor disability refers to a physical condition which influences the body’s ability to control and move, characterized by disturbances in the person’s movement, balance, speech, and breathing.
                            </h3>
                            <h3>To facilitate her movement and autonomy, she has a wheelchair that adapts and responds to her needs.</h3>
                        </div> 

                    <!-- tercer personaje -->
                     <div tabindex="17" class="project-info overflow" style="text-align:justify">
                            <h2>Our 3rd character is a girl with Down Syndrome:</h2>
                            <h3>
                                <div  class="col-sm-3">
                                    <img src="../images/concurso/sindrome-down.png" width="60%" class="img-responsive" alt="girl with Down Syndrome"> 
                                </div> 
                                <br> <br>
                                Down syndrome is a congenital disorder derived from total or partial triplication of the chromosome 21, a cognitive spectrum disability that results in mental retardation and growth as part of certain physical changes.  
                            </h3>
                        </div> 
                    <!-- cuarto personaje -->
                    <div tabindex="18" class="project-info overflow " style="text-align:justify">
                            <h2>Our 4th character is a child with hearing impairment:</h2>
                            <h3>
                                <div  class="col-sm-3">
                                    <img src="../images/concurso/discapacidad-auditiva.png" width="60%" class="img-responsive" alt="child with hearing impairment"> 
                                </div> 
                                <br> <br>
                                Hearing impairment or deafness refers to the impossibility or difficulty of making use of the sense of hearing due to a loss of partial
                                (hearing loss) or total hearing loss unilaterally or bilaterally. Like other physical disabilities, deafness can originate at birth or be  acquired over the years of life.
                            </h3>
                            <h3>In order to communicate, deaf people use Sign Language: a complete system of communication which, just as the spoken language allows to transmit ideas and feelings, transforming words into gestures carried out mainly with hands.
                            </h3>
                        </div> 
                </div>
            </div>

            <div class="project-info overflow">
                <center>
                    <br> 
                    <br> 
                    <img width="60%" src="../images/concurso/infografico_en.PNG" class="img-responsive " alt="Infografía con los resultados del concurso literario 'Mi mundo a mi manera'. 5 países participando, 3 premios y 3 menciones especiales."></center>
                </div>

            </div>
        </section>
        <br>

        <section id="ganadores">
            <div class="container">
                <div class="row">
                    <div tabindex="18" class="project-info overflow " style="text-align:justify">
                        <center><h2 class="center">Meet the winners! </h2></center>
                        <h2>
                            Access the winning stories of the First Edition and be amazed with the creation of the little writers who with their imagination are already shaping a more accessible world and equity for all.
                        </h2>

                        <h3> <b>First place: Agustina Irene Abdo Valdiviezo </b></h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Cuento: Los colores de Tom.</li>
                            <li><i class="fa fa-angle-right"></i> Colegio: Liceo Internacional, Quito.</li>
                            <li><i class="fa fa-angle-right"></i> Edad: 11 Años.</li>
                            <li><i class="fa fa-angle-right"></i> País: Ecuador.</li>
                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/los-colores-de-tom.pdf">Read digital version.</a></li>
                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/biblioteca_audiolibros/comparlante/index.php?r=libros/view&id=215">Audiobook.</a></li>

                        </ul>

                    </div> 
                    <div tabindex="22" class="project-info overflow " style="text-align:justify">

                        <h3><b>Second place: Ariana Valeria Valenzuela Muñoz</b> </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Cuento: Mi mundo se llamaba silencio.</li>
                            <li><i class="fa fa-angle-right"></i> Edad: 12 Años.</li>
                            <li><i class="fa fa-angle-right"></i> País: Ecuador.</li>
                            <li><i class="fa fa-angle-right"></i> Colegio: Colegio Ecuatoriano-Español América Latina,Quito.</li>
                            
                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/mi-mundo-se-llama-silencio.pdf">Read digital version.</a></li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->

                        </ul>
                    </div> 
                    <div tabindex="25" class="project-info overflow " style="text-align:justify">

                        <h3><b>Third place: David Rodolfo Jiménez Caamaño</b> </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Cuento: Felipe.</li>
                            <li><i class="fa fa-angle-right"></i> Edad: 11 Años.</li>
                            <li><i class="fa fa-angle-right"></i> País: Costa Rica.</li>
                            <li><i class="fa fa-angle-right"></i> Colegio: Saint Gregory School, San José.</li>

                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/felipe-spreads.pdf">Read digital version.</a></li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->

                        </ul>
                    </div> 
                    <div tabindex="29" class="project-info overflow " style="text-align:justify">

                        <h3><b>Special recognitions: </b> </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Cuento: Un breve relato sobre mis super-poderes.</li>
                            <li><i class="fa fa-angle-right"></i> Autor: Galo Dana.</li>
                            <li><i class="fa fa-angle-right"></i> Edad: 13 años.</li>
                            <li><i class="fa fa-angle-right"></i> País: Argentina</li>
                            <li><i class="fa fa-angle-right"></i> Colegio: Santo Tomás Moro, La Plata.</li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->
                        </ul>
                        <br>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Cuento: Una gran amistad.</li>
                            <li><i class="fa fa-angle-right"></i> Autor: Melina Bogarín Monge</li>
                            <li><i class="fa fa-angle-right"></i> Edad: 11 años.</li>
                            <li><i class="fa fa-angle-right"></i> País: Costa Rica.</li>
                            <li><i class="fa fa-angle-right"></i> Colegio: Saint Gregory School, San José.</li>
                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/una-gran-amistad.pdf">Read digital version.</a></li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->
                        </ul>
                        <br>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Cuento: This is me.</li>
                            <li><i class="fa fa-angle-right"></i> Autor: Estefania Alexandra Moreno Marin</li>
                            <li><i class="fa fa-angle-right"></i> Edad: 10 años.</li>
                            <li><i class="fa fa-angle-right"></i> País: Ecuador.</li>
                            <li><i class="fa fa-angle-right"></i> Colegio: Liceo Internacional, Quito.</li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->
                        </ul>
                    </div> 
                </div>
            </div>

        </section>



        <center>
            <br>
            <a tabindex="9" type="button" href="../concurso/basesen.pdf" class="btn btn-info"><h4>Download the contest rules</h4></a>
            
        </center>
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center bottom-separator">

                    </div>

                   <!--  <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
                  <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                     <h2>Send Us a Message</h2>
                     <form id="main-contact-form" name="contact-form" method="post" action="../contacto2.php">
                        <div class="form-group">
                            <input tabindex="51" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <input tabindex="52" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <textarea tabindex="53" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Your message"></textarea>
                        </div>            
                        <div style="display:none"> 
                            <input id="cc" value="" placeholder="E-mail"> 
                        </div>             
                        <div class="form-group">
                            <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Send</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                    <p>Developed by <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/lightbox.min.js"></script>
<script type="text/javascript" src="../js/wow.min.js"></script>
<script type="text/javascript" src="../js/main_es.js"></script>   
</body>


</html>
