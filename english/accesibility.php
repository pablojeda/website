<!DOCTYPE html>
<html lang="en">


<head><meta charset="gb18030">
   
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta name="description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities.">
   <meta name="author" content="Prime Developers Chile">

   <!-- Facebook Metadatos -->
   <!--  Inicio -->
   <meta property="og:title" content="Comparlante Foundation | HOME"/>
   <meta property="og:site_name" content="Comparlante Foundation"/>
   <meta property="og:description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities." />
   <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/home/logo-fundacion-2.jpg"/>
   <!--   <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/index_es.php"/> -->




   <title>Fundación Comparlante</title>
   <link href="../css/bootstrap.min.css" rel="stylesheet">
   <link href="../css/font-awesome.min.css" rel="stylesheet">
   <link href="../css/animate.min.css" rel="stylesheet"> 
   <link href="../css/lightbox.css" rel="stylesheet"> 
   <link href="../css/main.css" rel="stylesheet">
   <link href="../css/responsive.css" rel="stylesheet">


   <link rel="shortcut icon" href="../images/ico/logo-icon.png">
   <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/logo-icon.png">
   <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/logo-icon.png">
   <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/ico/logo-icon.png">
   <link rel="apple-touch-icon-precomposed" href="../images/ico/logo-icon.png">

   <!-- Google Tag Manager -->
   <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
   j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
   'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head>

<body>
  <?php include("header.php"); ?>

  <section id="titulo">
    <div class="row  padding" style="background-color: #f6f6f6;" tabindex="10">
        <div class="container" >
            <div class="row">
                <div class="wow scaleIn " >
                    <center>
                        <img src="../images/programas/accesibilidadquetransforma.png" alt="Four overlapping hands representing teamwork">
                    </center>
                </div>

                <div class="col-sm-12 padding-top-index text-justify">

                 <center>
                     <h2><b>Accessibility to Transform</b></h2>
                 </center>
             </div>
         </div>
     </div>
 </div>

</section>

<section id="portfolio-information" >
    <div class="container" tabindex="12"> 
        <br><br>
        <!-- asesoria en accesibilidad -->
        <div class="row">
            <div class="project-info overflow">
                <center>
                    <img src="../images/services/asesoria-accesibilidad.png" class="img-responsive" width="40%" alt="Accessibility Consulting Logo">
                </center>
            </div>
            <br>
            <div class="row">
                <center>
                    <h2><b>Accessibility Consulting</b></h2>
                </center>
            </div>
            <div class="row">

                <div class="col-sm-12">

                    <div  class="project-info overflow " style="text-align:justify">

                        <h2>In a world as globalized as the one we live in, it is not possible that our products, our services, our educational, social, communication and cultural practices remain so inaccessible. It is not simply about "including", accessibility is equity of opportunities for the development of all, beyond our characteristics and capabilities. </h2>
                         <p><center><a type="button" href="accessibility-consulting.php" class="btn btn-info">Read more</a></center>  </p>

                    </div>
                    <br>
                </div>
            </div>
        </div>

        <!-- fin asesoria accesibilidad -->
        

        <!-- desarrollo web -->
        <div class="row" tabindex="14">
            <div class="project-info overflow">
                <center>
                    <img src="../images/services/diseno-web.png" class="img-responsive" width="40%" alt="Web Development Logo.">
                </center>
            </div>
            <br>
            <div class="row">
                <center>
                    <h2><b>Web Development</b></h2>
                </center>
            </div>
            <div class="row">

                <div class="col-sm-12">

                    <div  class="project-info overflow " style="text-align:justify">

                        <h2>Currently, 90% of the content available on the Internet is not accessible to persons with disabilities. Comparlante offers advising and development of websites that can be visited by more people and where information can be reached by everyone. We make it accessible, navigable and intuitive. We offer a free Accessibility Checker and we develop proposals according to the needs of our clients.

                        </h2>
                         <p><center><a type="button" href="check.php" class="btn btn-info">Read more</a></center>  </p>

                    </div>
                    <br>
                </div>
            </div>
        </div>

        <!-- diseño universal -->

        <div class="row" tabindex="15">
            <div class="project-info overflow">
                <center>
                    <img src="../images/services/diseno-accesible.png" class="img-responsive" width="40%" alt="Universal Design, Creative Accessibility Logo">
                </center>
            </div>
            <br>
            <div class="row">
                <center>
                    <h2><b>Universal Design, Creative Accessibility</b></h2>
                </center>
            </div>
            <div class="row">

                <div class="col-sm-12">

                    <div  class="project-info overflow " style="text-align:justify">

                        <h2>The purpose of design is to create something useful. The shape, color, texture and other characteristics along with the function of that creation must meet the diverse needs of the environment. This service promotes organizations, companies and the government sector to offer materials under universal design standards: Identity, websites, documents, presentations, all accessible to persons with disabilities.
                        </h2>
                         <p><center><a type="button" href="accesible-designs.php" class="btn btn-info">Read more</a></center>  </p>

                    </div>
                    <br>
                </div>
            </div>
        </div>
        <!-- audioteca -->
        <div class="row" tabindex="16">
            <div class="project-info overflow">
                <center>
                    <img src="../images/services/audiolibros.png" class="img-responsive" width="40%" alt="Comparlante Audiobooks Library Logo.">
                </center>
            </div>
            <br>
            <div class="row">
                <center>
                    <h2><b>Comparlante Audiobooks Library</b></h2>
                </center>
            </div>
            <div class="row">

                <div class="col-sm-12">

                    <div tabindex="" class="project-info overflow " style="text-align:justify">

                        <h2>Comparlante offers a bank of accessible audiobooks in 18 languages of free access and collaborative construction. Currently we count on more than 400 titles.
                        </h2>
                         <p><center><a type="button" href="http://www.comparlante.com/biblioteca_audiolibros/english/" class="btn btn-info">Read more</a></center>  </p>
                        
                    </div>
                    <br>
                </div>
            </div>
        </div>

    </div>
</section>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">

            </div>

                   <!--  <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
                  <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                       <h2>Send Us a Message</h2>
                       <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php"
                       <div class="form-group">
                        <input type="text" name="name" class="form-control" required="required" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" required="required" placeholder="E-mail">
                    </div>
                    <div class="form-group">
                        <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Your message"></textarea>
                    </div>
                    <div style="display:none"> 
                        <input id="cc" value="pablo@primedevelopers.cl" placeholder="E-mail"> 
                    </div>                         
                    <div class="form-group">
                        <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Send</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="copyright-text text-center">
                <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                <p>Developed by <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
            </div>
        </div>
    </div>
</div>
</footer>
<!--/#footer-->
<!--/#footer-->

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/lightbox.min.js"></script>
<script type="text/javascript" src="../js/wow.min.js"></script>
<script type="text/javascript" src="../js/main_es.js"></script>   
</body>


</html>
