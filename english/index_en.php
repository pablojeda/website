<!DOCTYPE html>
<html lang="en">


<script type="text/javascript">
  window.onload = setTimeout(function() {
    document.getElementById("instrucciones-de-uso").focus();

  },1500);

</script>

<head>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <meta name="description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities.">
 <meta name="author" content="Prime Developers Chile">

 <!-- Facebook Metadatos -->
 <!--  Inicio -->
 <meta property="og:title" content="Comparlante Foundation | HOME"/>
 <meta property="og:site_name" content="Comparlante Foundation"/>
 <meta property="og:description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities." />
 <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/home/logo-fundacion-2.jpg"/>
 <!--   <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/index_es.php"/> -->




 <title>Fundación Comparlante</title>
 <link href="../css/bootstrap.min.css" rel="stylesheet">
 <link href="../css/font-awesome.min.css" rel="stylesheet">
 <link href="../css/animate.min.css" rel="stylesheet"> 
 <link href="../css/lightbox.css" rel="stylesheet"> 
 <link href="../css/main.css" rel="stylesheet">
 <link href="../css/responsive.css" rel="stylesheet">


 <link rel="shortcut icon" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" href="../images/ico/logo-icon.png">

 <!-- Google Tag Manager -->
 <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
 j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head>

<body>
  <?php include("header.php"); ?>

  <section id="home-slider">
    <div class="container">
      <div class="row">
        <div class="main-slider">
          <div class="slide-text" tabindex="4">
            <h1>Comparlante Foundation</h1>
            <h2>Welcome to our community!</h2>
            <!-- <a href="#" class="btn btn-common">SIGN UP</a> -->
          </div>
          <img src="../images/home/slider/img1.png" class="slider-hill" alt="Landscape of a mountain in the background, a sun, and colorful birds in the clouds.">
          <img src="../images/home/slider/img2.png" class="slider-house" alt="A building surrounded by trees, and in the left corner a person in a wheelchair and another person using a cane.">
          <img src="../images/home/slider/img3.png" class="slider-sun" alt="Four people smiling in front of a building, a person in a wheelchair, another person using a cane, accompanied by an adult man and an adult woman.">

        </div>
      </div>
    </div>
    <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
  </section>
  <!--/#home-slider-->

  <section id="quienes-somos" tabindex="5" class="padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
    <div class="container">
      <div class="row">
        <h1 class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Who we are</h1>

        <div class="col-sm-12 padding-top-index text-justify">

         Fundación Comparlante is a non-profit organization that promotes the development and inclusion of persons with disabilities in Latin America. From technological tools, products and services, we promote accessibility, entrepreneurship and equity. Our goal is to ensure that this vulnerable sector of society has full enjoyment of their rights.

       </div>
     </div>
   </div>
   <br>
 </section>

 <!-- banner mi mundo a mi manera 2 -->
 <section id="quienes-somos"  class="padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
  <div class="container">
    <div class="row">

      <div  tabindex="8" class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
        <center>
          <a href="my-world-my-way-2020.php">
            <img width="90%" src="../images/concurso/winners.png" alt='The three characters of the competition with the following text. Winners: first place, Diago Rotman, “Abrazándonos con el corazón” PERU.
            Second place, Violeta Parra, "El susurrador mágico", ARGENTINA. Third place, Alejandro Valenzuela, "Unos lentes para Sara", ECUADOR. It will direct you to the contest page'>
          </a>
        </center>
      </div>
      
    </div>
  </div>
  <br>
</section>

 <!-- sección de las 3 lineas de comparlante -->
 <section id="programas" tabindex="6" class=" wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">.
  <div class="container">
    <div class="row">
     <!-- accesibilidad que transforma -->
     <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms">
      <a href="accesibility.php" style="color:#000;">

        <div tabindex="7"  style="background-color: #f6f6f6; height: 500px;">
          <br>
          <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
            <img src="../images/programas/accesibilidadquetransforma_sf.png" alt="Four overlapping hands representing teamwork">
          </div>
          <div class="row" style="margin-left: 8%; margin-right: 8%;">
            <div class="row" style="height: 65px;">
              <h2><b>Accessibility to Transform
              </b></h2>
            </div>
            <br>
            <div class="row">
              <p>To achieve a barrier free world, it is required the commitment of all. We help companies, governments, NGOs and private companies to develop and adapt their practices and services under international accessibility standards.</p> 
            </div>

          </div>

        </div>
      </a>
    </div>
    <!-- arte para promover la empatía -->
    <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms" style="padding-left: 15px;">
      <a href="art.php" style="color:#000;">

        <div tabindex="8"  style="background-color: #f6f6f6; height: 500px;">
          <br>
          <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
            <img src="../images/programas/arte_sf.png" alt="A plane, a pencil and a square, tools used in art.">
          </div>
          <div class="row" style="margin-left: 8%; margin-right: 8%;">
            <div class="row" style="height: 65px;">

              <h2><b>Art to promote empathy</b></h2>
            </div>
            <br>
            <div class="row">
              <p>A piece of art can shake our senses and make us understand how persons with disabilities live in the world, which are their rights and needs, and which are the barriers they face every day.</p> 
            </div>



          </div>

        </div>
      </a>
    </div>
    <!-- emprendimiento, independencia y dignidad -->
    <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms" style="padding-left: 15px;">
      <a href="entrepreneurship.php" style="color:#000;">
        <div tabindex="9"  style="background-color: #f6f6f6; height: 500px;">
          <br>
          <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
            <img src="../images/programas/emprendimiento_sf.png" alt="A light bulb representing ideas">
          </div>
          <div class="row" style="margin-left: 8%; margin-right: 8%;">
            <div class="row" style="height: 65px;">

              <h2><b>Entrepreneurship, independence and dignity </b></h2>
            </div>
            <br>
            <div class="row">
              <p>Entrepreneurship is a tool that promotes social welfare, economic development and empowerment of persons with disabilities. In addition, it reinforces the sense of dignity and promotes their independence.</p> 
            </div>

          </div>

        </div>
      </a>  
    </div>
    <!-- fin emprendimiento -->
  </div>  
</div>
</section>


<section>

  <div class="container">
    <div class="col-sm-12">
      <div class="col-sm-6">
       <b>Globalization and local Action - Globalization y Acción Local | Sebastian Flores | TEDxQuito</b>
       <iframe width="100%" height="315" src="https://www.youtube.com/embed/myeooeE6ack" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
     </div>
    
     <div class="col-sm-6">
       <b>“Accessibility is in our hands” | kanthari TALKS 2018 | Lorena Julio | Argentina</b>
       <iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/euC31Kw-92Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
     </div>
   </div>
 </div>

</section>

<!-- Noticias -->
<section id="noticias">
  <div class="container">
    <div class="row">
      <div tabindex="10">
        <h1 class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">News and Announcements</h1>
        
      </div>
      <?php
                // include database connection
                include '../config_news/database.php';
                
                
                // select all data
                $query = "SELECT id,idioma, imagen, titulo,altText, publica, fechaorigen, contenido FROM noticias WHERE publica = 1 ORDER BY fechaorigen DESC LIMIT 3";
                $stmt = $con->prepare($query);
                $stmt->execute();
                
                // this is how to get number of rows returned
                $num = $stmt->rowCount();
               
                
                //check if more than 0 record found
                if($num>0){
                     
                        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                            
                            extract($row);
                            setlocale(LC_TIME,"es_ES.UTF-8");
                            echo "
                            <div class='col-sm-4 text-center padding wow fadeIn' data-wow-duration='1000ms' data-wow-delay='300ms'>
                            <div tabindex='32' class=''>
                              
                    
                              <div tabindex='33' class='wow ' data-wow-duration='500ms' data-wow-delay='300ms' style='height: 250px'>
                              <a tabindex='34' href='https://{$contenido}' target='_blank'>
                                <img class='noticias' src='../news_img/${imagen}' alt='{$idioma}'>
                              </a>
                              </div>
                              
                    
                              <p style='height: 120px'>{$idioma} </p>
                              <p> <a tabindex='34' type='button' href='https://{$contenido}' class='btn btn-info' target='_blank'>View more</a> </p>
                            </div>
                          </div>"            ;
                            
                             
                            
                        }
                    
                    // end table
                    echo "</table>";
                    
                }

                // if no records found
                else{
                    echo "<div class='alert alert-danger'>No encontramos noticias.</div>";
                }
                ?>



    
     


      </div>
      <h3 style="text-align: center;"><a href="http://www.comparlante.com/english/news.php" class="btn btn-warning btn-lg">More NEWS</a></h3>
      <br>
      <br>

    </div>


      
        

      
  </section>


   <!-- newsletter -->
  <section>
    <div class="container">
     
        <p> 
          <b>Access to our Newsletters  :</b>   
          <a tabindex="42" type="button" href="https://mailchi.mp/53f22650d11f/nuestro-increble-2018-our-amazing-2018
          " class="btn btn-info" style="background-color: #252983;">DECEMBER  2018</a>  <a tabindex="42" type="button" href="https://mailchi.mp/d0150d241b4e/comenzamos-el-ao-con-buenas-noticias
          " class="btn btn-info" style="background-color: #252983;">APRIL  2019</a> 
          <a tabindex="42" type="button" href="https://mailchi.mp/d0150d241b4e/comenzamos-el-ao-con-buenas-noticias
        " class="btn btn-info" style="background-color: #252983;" target="_blank">APRIL 2019</a> 
        <a tabindex="42" type="button" href="https://mailchi.mp/0eace2fd861d/comparlante-newsletter-2019
        " class="btn btn-info" style="background-color: #252983;" target="_blank">DECEMBER 2019</a>
        <a tabindex="42" type="button" href="https://mailchi.mp/7da75590ed4d/comparlante-newsletter-2734852" class="btn btn-info" style="background-color: #252983;" target="_blank">JUNE 2019</a> 
        </p>
     
     </div>

  <!--  -->
  <section id="incluyes"  class="padding">
    <div class="row  padding" style="background-color: #f6f6f6;">
      <div class="container" tabindex="19" >
        <div class="row">
          <div class="wow scaleIn " data-wow-duration="500ms" data-wow-delay="900ms">
            <center>
              <a href="https://www.incluyes.com">
               <img src="../images/programas/incluyes-morado.png" width="40%" alt="Platform logo IncluYes, the word Includes purple and Yes yellow.">
             </a>
           </center>
         </div>

         <div class="col-sm-12 padding-top-index text-justify">

           New technologies must be at the service of all. For that reason we created this accessible platform for the supply and demand of products and services for persons with disabilities. We promote alternatives in professional services which methodologies, formats and processes of implementation are developed under international accessibility standards generating equity in the access to opportunities.

         </div>
       </div>
     </div>
   </div>

 </section>





 <!-- involucrate -->
 <section id="involucrate">
  <div class="container">
    <div class="row">
      <div tabindex="20">
        <h1 class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Get Involved!</h1>
        <p class="text-center wow fadeInDown" data-wow-duration="400ms" data-wow-delay="400ms">Comparlante is a collective project encouraged and held thanks to people who want to live in a more accessible world innovating and being part of the change.</p>
      </div>
      <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
        <div tabindex="21" class="single-service">
          <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
            <img src="../images/services/unete.png" alt="Join us">
          </div>
          <h2>Join us</h2>
          <p> Are you interested in being part of the team?</p>

          <p> <a tabindex="22" type="button" href="contact.php" class="btn btn-info">Write us</a> </p>
        </div>
      </div>
      <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
        <div  tabindex="24"class="single-service">
          <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
            <img src="../images/services/apoyanos.png" alt="">
          </div>
          <h2>Support our project</h2>
          <p>With your donation you help us to build a more accessible world.</p>

          <a href="donate.php" target="_blank">
           <img alt="" border="0" src="../images/services/donar.jpg"  >

         </a> 

       </div>
     </div>
     <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms">
      <div tabindex="27" class="single-service">
        <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
          <img src="../images/services/idea.png" alt="">
        </div>
        <h2>Do you have an idea?</h2>
        <p>Let's build together a world with more equity.</p> <p><a tabindex="28" type="button" href="contact.php" class="btn btn-info">Write us
        </a> </p>
      </div>
    </div>
  </div>
</div>
</section>
<!--/#services--> 


<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center bottom-separator">

      </div>
      <div class="col-md-12 col-sm-12">
        <div id="contacto" class="contact-form bottom">
         <h2>Send Us a Message</h2>
         <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php"
         <div class="form-group">
          <input type="text" name="name" class="form-control" required="required" placeholder="Name">
        </div>
        <div class="form-group">
          <input type="email" name="email" class="form-control" required="required" placeholder="E-mail">
        </div>
        <div class="form-group">
          <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Your message"></textarea>
        </div>
        <div style="display:none"> 
          <input id="cc" value="" placeholder="E-mail"> 
        </div>                         
        <div class="form-group">
          <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Send</button>
        </div>
      </form>
    </div>
  </div>
  <div class="col-sm-12">
    <div class="copyright-text text-center">
      <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
      <p>Developed by <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
    </div>
  </div>
</div>
</div>
</footer>
<!--/#footer-->


<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/lightbox.min.js"></script>
<script type="text/javascript" src="../js/wow.min.js"></script>
<script type="text/javascript" src="../js/main_en.js"></script>   
</body>


</html>
