<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <meta name="description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities.">
 <meta name="author" content="Prime Developers Chile">

 <!-- Facebook Metadatos -->
 <!--  Inicio -->
 <meta property="og:title" content="Comparlante Foundation | HOME"/>
 <meta property="og:site_name" content="Comparlante Foundation"/>
 <meta property="og:description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities." />
 <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/home/logo-fundacion-2.jpg"/>
 <!--   <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/index_es.php"/> -->




 <title>Fundación Comparlante</title>
 <link href="../css/bootstrap.min.css" rel="stylesheet">
 <link href="../css/font-awesome.min.css" rel="stylesheet">
 <link href="../css/animate.min.css" rel="stylesheet"> 
 <link href="../css/lightbox.css" rel="stylesheet"> 
 <link href="../css/main.css" rel="stylesheet">
 <link href="../css/noticias.css" rel="stylesheet">
 <link href="../css/responsive.css" rel="stylesheet">


 <link rel="shortcut icon" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" href="../images/ico/logo-icon.png">

 <!-- Google Tag Manager -->
 <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
 j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head>

<body>
  <?php include("header.php"); ?>

  

<section id="noticias">
   
<div class="container">
  <div class="row">
  <?php
                // include database connection
                include '../config_news/database.php';
                
                
                // select all data
                $query = "SELECT id,idioma, imagen, titulo,altText, publica, fechaorigen, contenido FROM noticias WHERE publica = 1 ORDER BY fechaorigen DESC";
                $stmt = $con->prepare($query);
                $stmt->execute();
                
                // this is how to get number of rows returned
                $num = $stmt->rowCount();
               
                
                //check if more than 0 record found
                if($num>0){
                     
                        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                            
                            extract($row);
                            setlocale(LC_TIME,"es_ES.UTF-8");
                            echo "<div class='col-sm-4 listaNoticias'>";
                            echo "
                            <a href='https://{$contenido}' target='_blank'>
                            <img src='../news_img/${imagen}' alt='{$idioma}'>
                            </a>
                            <br>
                            <br>
                            <small>";
                            echo strftime('%e/%m/%Y', strtotime($fechaorigen));
                            echo "</small>
                            <a href='https://{$contenido}' target='_blank'>
                                <p>{$idioma}</p>
                            </a>
                        </div>"; 
                             
                            
                        }
                    
                    // end table
                    echo "</table>";
                    
                }

                // if no records found
                else{
                    echo "<div class='alert alert-danger'>No encontramos noticias.</div>";
                }
                ?>










 

  
</div>
</section>


<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">

            </div>

                  
                  
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                    <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/main_es.js"></script>   
</body>


</html>
