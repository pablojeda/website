<?php 

// used to connect to the database

$host = "localhost";
$username = "root";
$password = "root";
$db_name = "comparla_noticias";
  
try {
    $con = new PDO("mysql:host={$host};dbname={$db_name}", $username, $password);
}
  
// show error
catch(PDOException $exception){
    echo "Connection error: " . $exception->getMessage();
}
?>

