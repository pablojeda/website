<!DOCTYPE html>
<html lang="es">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad.">
    <meta name="author" content="Prime Developers Chile">
    
    <!-- Facebook Metadatos | Diseño web accesible -->
    <meta property="og:title" content="Fundación Comparlante | Arte para promover la empatía"/>
    <meta property="og:site_name" content="Arte para promover la empatía"/>
    <meta property="og:description" content="Fundación Comparlante brinda servicios de impacto social para la accesibilidad de las personas con discapacidad" />
    <!-- <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/services/2/diseno-web.jpg"/> -->
    <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/home/logo-fundacion-2.jpg"/>

    <!-- <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/diseno-web.php"/> -->

    
    
    <title>Fundación Comparlante</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
    <link href="css/main.css" rel="stylesheet">
    <link href="css/noticias.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">
    <!-- Google Tag Manager -->
   
<!-- End Google Tag Manager -->
</head><!--/head-->

<body>
  <?php include("header.php"); ?>

  

<section id="noticias">
    <div class="container">
        <div class="row">
            <div tabindex="30">

               <div class="col-sm-12  text-justify">
               <article>
               <h3 class="title"> <strong>Noticias:</strong></h3>
               
               <?php
                // include database connection
                include 'config_news/database.php';
                
                
                // select all data
                $query = "SELECT id,idioma, titulo,publica FROM noticias ORDER BY id DESC";
                $stmt = $con->prepare($query);
                $stmt->execute();
                
                // this is how to get number of rows returned
                $num = $stmt->rowCount();
                
                // link to create record form
                echo "<p><a href='add_new.php' style='color:white;' type='button' class='btn btn-primary'>Agregar nueva noticia</a><p>";
                
                //check if more than 0 record found
                if($num>0){
                
                    echo "<table class='table table-hover table-responsive table-bordered'>";//start table
 
                        //creating our table heading
                        echo "<tr>";
                            echo "<th>Publico?</th>";
                            echo "<th>Título:</th>";
                        echo "</tr>";
                        
                        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                            // extract row
                            // this will make $row['firstname'] to
                            // just $firstname only
                            extract($row);
                             
                            // creating new table row per record
                            echo "<tr>";
                                if($publica == 1) 
                                {echo "<td>Si</td>";} 
                            else {
                                echo "<td>No</td>";
                                };
                                echo "<td>{$titulo}</td>";
                                echo "<td>";
                                    
                                    // we will use this links on next part of this post
                                    echo "<a href='editar_noticia.php?id={$id}' style='color:white;' class='btn btn-primary m-r-1em'>Edit</a>";
                         
                                echo "</td>";
                            echo "</tr>";
                        }
                    
                    // end table
                    echo "</table>";
                    
                }

                // if no records found
                else{
                    echo "<div class='alert alert-danger'>No encontramos noticias.</div>";
                }
                ?>
                
               </article>
                
            </div>






        </div>
    </div>

</div>
</section>


<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">

            </div>

                  
                  
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                    <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/main_es.js"></script>   
</body>


</html>
