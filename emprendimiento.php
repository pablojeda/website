<!DOCTYPE html>
<html lang="es">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Fundación Comparlante brinda servicios de impacto social para la accesibilidad de las personas con discapacidad.">
    <meta name="author" content="Prime Developers Chile">
    
    <!-- Facebook Metadatos | Diseño web accesible -->
    <meta property="og:title" content="Fundación Comparlante | Emprendimiento, independencia y dignidad "/>
    <meta property="og:site_name" content="Emprendimiento, independencia y dignidad "/>
    <meta property="og:description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad" />
    <meta property="og:image" content="http://comparlante.com/images/home/logo-fundacion-2.jpg"/>
    <meta prperty="og:image:alt" content= "Logo de Fundación Comparlante, auriculares/audífonos unidos formando una flor, en naranja, rojo, celeste, azúl y verde"/>
    <meta property="og:url" content="http://www.comparlante.com/emprendimiento.php"/>

    
    
    <title>Fundación Comparlante</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head><!--/head-->

<body>
  <?php include("header.php"); ?>

  <section id="titulo" tabindex="10">
    <div class="row  padding" style="background-color: #f6f6f6;">
        <div class="container" >
            <div class="row">
                <div class="wow scaleIn " >
                    <center>
                        <img src="images/programas/emprendimiento_sf.png" alt="Una ampolleta que representa las ideas">
                    </center>
                </div>

                <div class="col-sm-12 padding-top-index text-justify">

                 <center>
                     <h2><b>Emprendimiento, independencia y dignidad </b></h2>
                 </center>
             </div>
         </div>
     </div>
 </div>

</section>

<section id="portfolio-information" >
    <div class="container"> 
        <br><br>

        <div class="row" tabindex="13">
          <div  class="project-info overflow " style="text-align:justify">

            <h2> <center> ¡Todos tenemos una idea!  </center><br> 

         Dale alas a tu espíritu emprendedor y potencia tus habilidades de negocio con nosotros. Nuestros especialistas en emprendimiento ofrecen tutorías a tu medida para el desarrollo de tu idea de negocio o su escalamiento y talleres para grupos focalizados.
          <br>
          <br>
          <b>Asesorías individuales:</b> Si lo que buscas es un mentor que te acompañe en la construcción de tu idea de negocio o ya tiene un startup funcionando y necesitas escalarla pero no cuentas con herramientas, este espacio busca mejorar el acceso a información sobre negocios, entrenamiento, innovación, servicios de consultoría y recursos técnicos. Ofrecemos asesorías bajo dos modalidades: Online y visitas domiciliarias, así nos  ajustamos a tus necesidades. Nuestras mentorías 1 a 1 están enfocadas en el impulso de agentes productivos preparados para liderar la nueva era del emprendimiento y la accesibilidad universal.
          <br>
          <br>
          <b>Talleres: </b>Recibe formación profesional, dinámica y accesible en emprendimientos e innovación a través de talleres grupales diseñados para catalizar lo mejor de ti. Estos se desarrollan en tres fases:
          <br> <br>
          <ul>
            <li><b>Emprendedor 1.0.</b> En esa fase se definen las condiciones en las que se encuentran los participantes antes de empezar con su negocio ya que un emprendedor no debe empezar su proyecto sin definir sus cualidades y capacidades.</li>
            <li>
              <br>
              <b>Yo emprendo.</b> TEsta fase es el primer acercamiento del emprendedor con la definición de un emprendimiento y sus principales necesidades, en ellas incluida “el cliente”.
              <br>

            </li>
            <li>
              <br>
              <b>Mi primer negocio.</b> Es la fase de fortalecimiento de la propuesta de valor de un emprendedor. Aquí el emprendedor adquiere herramientas y metodologías para conocer el mercado, sus necesidades e identificar a su cliente. De este modo se encontrará en capacidad de desarrollar propuestas de valor sólidas y sostenibles.
            </li>
          </ul>
          <br>
       

        </h2>

        </div>
    </div>
    <!-- asesoria en accesibilidad -->
<!--     <div class="row" tabindex="15">
        <div class="project-info overflow">
            <center>
                <img src="images/services/asesoramiento-productivo.png" class="img-responsive" width="40%" alt="Logo de Asesorías individuales.">
            </center>
        </div>
        <br>
        <div class="row">
            <center>
                <h2><b>Asesorías individuales</b></h2>
            </center>
        </div>
        <div class="row">

            <div class="col-sm-12">

                <div  class="project-info overflow " style="text-align:justify">

                    <h2>Bajo el compromiso asumido por Comparlante de promover más y mejores oportunidades para el digno desarrollo económico y social de las personas con discapacidad, este espacio busca mejorar el acceso a información sobre negocios, entrenamiento, tecnología, servicios de consultoría y recursos tanto técnicos como financieros. Ofrecemos talleres grupales y mentorías 1 a 1 enfocadas en el impulso de agentes productivos preparados para liderar la nueva era de la accesibilidad universal y el emprendimiento. </h2>

                </div>
                <br>
            </div>
        </div>
    </div> -->

    <!-- fin asesoria accesibilidad -->


    <!-- desarrollo web -->
<!--     <div class="row padding" tabindex="18">

        <br>
        <div class="row">
            <center>
                <h2><b>Workshops</b></h2>
            </center>
        </div>
        <div class="row">

            <div class="col-sm-12">

                <div  class="project-info overflow " style="text-align:justify">

                    <h2>
                       Recibe formación profesional, dinámica y accesible en emprendimiento e innovación a través de talleres grupales diseñados para catalizar lo mejor de ti.

                    </h2>

                </div>
                <br>
            </div>
        </div>
    </div>
 -->


</div>
</section>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">

            </div>

                   <!--  <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
                  <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                       <h2>Para solicitar asesoría o conocer nuestras próximas actividades ¡déjanos tu mensaje!</h2>
                       <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php">
                        <div class="form-group">
                            <input tabindex="51" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Nombre">
                        </div>
                        <div class="form-group">
                            <input tabindex="52" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <textarea tabindex="53" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Tu mensaje"></textarea>
                        </div>            
                        <div style="display:none"> 
                            <input id="cc" value="sebastian@comparlante.com" placeholder="E-mail"> 
                        </div>             
                        <div class="form-group">
                            <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Enviar mensaje</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                    <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/main_es.js"></script>   
</body>


</html>
