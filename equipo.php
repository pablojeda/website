<!DOCTYPE html>
<html lang="es">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad.">
    <meta name="author" content="Prime Developers Chile">
    
    <!-- Facebook Metadatos | Diseño web accesible -->
    <meta property="og:title" content="Fundación Comparlante | Equipo"/>
    <meta property="og:site_name" content="Fundación Comparlante"/>
    <meta property="og:description" content="Fundación Comparlante brinda servicios de impacto social para la accesibilidad de las personas con discapacidad" />
    <meta property="og:image" content="http://comparlante.com/images/home/logo-fundacion-2.jpg"/>
    <meta prperty="og:image:alt" content= "Logo de Fundación Comparlante, auriculares/audífonos unidos formando una flor, en naranja, rojo, celeste, azúl y verde"/>
    <meta property="og:url" content="http://www.comparlante.com/equipo.php"/>


    
    
    <title>Fundación Comparlante</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head><!--/head-->

<body>
  <?php include("header.php"); ?>

  <section id="page-breadcrumb">
      <div class="vertical-center sun">
       <div class="container">
        <div class="row">
          <div class="action">
            <div tabindex="10" class="col-sm-12">
              <h1 class="title">Quienes somos</h1>
              <p></p>
          </div>
      </div>
  </div>
</div>
</div>
</section>

<section id="equipo">
    <div class="container">
        <div class="row">
            <div tabindex="30">
                <div class="col-sm-12  text-justify">

                Fundación Comparlante es una organización sin fines de lucro que promueve el desarrollo y la inclusión de las personas con discapacidad en América Latina. A partir de herramientas tecnológicas, productos y servicios promovemos la accesibilidad, el emprendimiento y la equidad. Nuestro objetivo es lograr que este sector vulnerable de la sociedad tenga pleno goce de sus derechos. <br> <br>
                Fundación Comparlante surge en el año 2015 en Argentina con el objetivo de generar herramientas tecnológicas para la accesibilidad de las personas con discapacidad visual. Hoy es un proyecto colectivo el cual opera desde Latinoamérica y Estados Unidos (Washington DC) integrado por jóvenes emprendedores de Argentina, Chile, Costa Rica, Ecuador, El Salvador, Uruguay y Venezuela.
                <br> <br>
                    <p>
                    <b>Acerca de los Fundadores</b> <br> <br></p>
                    <img class="col-sm-6 img-responsive" src="images/team/lorenaJulio.jpg" alt="Lorena Julio, Co–Fundadora y Presidente">
                    <div class="col-sm-6">
                        <p class="text-justify">
                        <b> Lorena Julio - CoFundadora y Presidente</b> <br>
                        Lorena es una defensora de derechos humanos. Posee un Máster en 
                        Políticas Públicas Comparadas y tiene una Licenciatura en 
                        Comunicación Social. Ha trabajado durante los últimos quince años en la 
                        promoción y protección de los derechos humanos en América Latina 
                        y los Estados Unidos. Ha trabajado para el Gobierno de Argentina 
                        como Jefe de Prensa en la Secretaría de Derechos Humanos de la 
                        Provincia de Buenos Aires, así como en el Equipo de Coordinación del 
                        Programa de Asistencia para Grupos Vulnerables. En el ámbito 
                        internacional, Lorena trabajó en la Organización de los Estados 
                        Americanos (OEA) como Oficial de Programas del Foro de Jóvenes de 
                        las Américas en el marco de la Cumbre de Presidentes. Después de 
                        eso, asumió funciones como Oficial de Comunicaciones en Young 
                        Americas Business Trust, liderando además las redes de jóvenes del Foro. 
                        En 2015, Lorena fundó la Fundación Comparlante en Argentina con el objetivo de reducir la brecha en el acceso a la educación y la información para las personas con discapacidad visual. Actualmente, Comparlante promueve la equidad a través de la accesibilidad universal.
                        Se capacitó en emprendimiento en Israel gracias a una beca de la Agencia Israelí de Cooperación Internacional MASHAV, y en 2018 fue entrenada en liderazgo durante 12 meses por Kanthari en la India. En 2020 participó representando a América Latina en Jai Jagat, una marcha global por la paz, llevando las banderas de la no discriminación y la no violencia.
                        <br> <br></p>
                    </div>       
                    <div class="col-sm-12 text-justify">
                    <img class="col-sm-6 img-responsive" src="images/team/sebastianFlores.jpg" alt="Sebastián Flores, Co – Fundador & Director">
                    <div class="col-sm-6">
                        <p class="text-justify">
                        <b>Sebastián Flores - Cofundador y Director</b>
                        <br>
                            Es un agente de cambio con discapacidad visual graduado en Diplomacia y Asuntos Internacionales. Sebastián ha desarrollado su formación profesional centrada en Diplomacia, Política Internacional, Derecho, democracia y participación ciudadana, tecnologías de la información y la comunicación en el contexto del Gobierno Electrónico y la Diplomacia Digital. Es un joven modelo en el proceso global de desarrollo sostenible, temas humanitarios que van desde la migración y la movilidad humana, los derechos humanos, hasta consultas sobre accesibilidad y equidad para personas con discapacidades, tanto a nivel local como a nivel regional e internacional. La actividad de Sebastian como joven líder lo ha llevado a ocupar puestos clave en proyectos con impacto social de la mano de organizaciones internacionales como la ONU, la OEA y las redes de defensa de la juventud en todo el mundo. Sebastian es un orador, motivador y mentor de TEDx, un papel que se combina con su actividad como delegado en cumbres internacionales y como co-fundador de Comparlante.
                    </p> 

                    <br> <br>
                </div> 
                
            </div>

        </div>
        <div class="row">
            
            <h1 class="title text-center wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 500ms; animation-delay: 300ms; animation-name: fadeInDown;">Conoce al equipo</h1>
            <p class="text-center wow fadeInDown animated" data-wow-duration="400ms" data-wow-delay="400ms" style="visibility: visible; animation-duration: 400ms; animation-delay: 400ms; animation-name: fadeInDown;">Nuestro equipo se compone de profesionales de diferentes áreas que comparten un objetivo común: Trabajar día a día para lograr un mundo más accesible.</p> 
            
            <div id="team-carousel" class="carousel slide wow fadeIn animated" data-ride="carousel" data-wow-duration="400ms" data-wow-delay="400ms" style="visibility: visible; animation-duration: 400ms; animation-delay: 400ms; animation-name: fadeIn;">
                <!-- Indicators -->
                <ol class="carousel-indicators visible-xs">
                    <li data-target="#team-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#team-carousel" data-slide-to="1" class=""></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                   <div class="item active">
                    <div tabindex="31" class="col-sm-2 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="images/team/ana-maidana.jpg" class="img-responsive" alt="Ana Maidana, Área Administrativa">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Ana Maidana</h2><br>
                                    <p>Secretaria</p>
                                    <p>Argentina</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="32" class="col-sm-2 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="images/team/NairRodriguez.jpeg" class="img-responsive" alt="Nair Rodriguez, tesorera">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Nair Rodriguez</h2>
                                    <p>Tesorera</p>
                                    <p>Argentina</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="33" class="col-sm-2 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="images/team/roberto-jaramillo.jpg" class="img-responsive" alt="Roberto Jaramillo, Asesoría en Emprendimiento">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Roberto Jaramillo</h2><br>
                                    <p>Asesoría en Emprendimiento</p>
                                    <p>Ecuador</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="34" class="col-sm-2 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="images/team/martinRoyo.jpg" class="img-responsive" alt="Martin Royo, Desarrollador de fondos">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Martin Royo</h2><br>
                                    <p>Desarrollador de fondos</p>
                                    <p>Argentina</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="35" class="col-sm-2 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="images/team/pablo-ojeda.jpg" class="img-responsive" alt="Pablo Ojeda, Desarrollador de Software">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Pablo Ojeda</h2><br>
                                    <p>Desarrollador de Software</p>
                                    <p>Chile</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="36" class="col-sm-2 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="images/team/beatriz-calvo.jpg" class="img-responsive" alt="Beatriz Calvo, Diseño e Ilustación">
                                    </div>

                                </div>
                                <div class="person-info">
                                <h2>Beatriz Calvo</h2><br>
                                <p>Diseño e Ilustación</p>
                                <p>Costa Rica</p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- segundo item -->
                    <div class="item">
                    <div tabindex="37" class="col-sm-2 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                    <img src="images/team/CelesteBenitez.jpg" class="img-responsive" alt="Celeste Benitez, Voluntaria - Creadora de contenidos">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>CelesteBenitez</h2><br>
                                    <p>Voluntaria - Creadora de contenidos</p>
                                    <p>Argentina</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="38" class="col-sm-2 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="images/team/LudmilaAyelenGuidi.jpg" class="img-responsive" alt="Ludmila Ayelen Guidi, Voluntaria - Promotora de derechos">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Ludmila Ayelen Guidi</h2><br>
                                    <p>Voluntaria - Promotora de derechos</p>
                                    <p>Argentina</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="39" class="col-sm-2 col-xs-6">
                            <div class="team-single-wrapper">
                                 <div class="team-single">
                                     <div class="person-thumb">
                                         <img src="images/team/mayraAnabelaLuengo.png" class="img-responsive" alt="Mayra Anabela Luengo, Pasante - Desarrollo web">
                                     </div>

                                </div>
                                <div class="person-info">
                                    <h2>Mayra Anabela Luengo</h2><br>
                                    <p>Pasante - Desarrollo web</p>
                                    <p>Argentina</p>
                                </div>
                             </div>
                        </div>
                        <div tabindex="40" class="col-sm-2 col-xs-6">
                             <div class="team-single-wrapper">
                                 <div class="team-single">
                                     <div class="person-thumb">
                                         <img src="images/team/barbaraOtero.jpg" class="img-responsive" alt="Bárbara Otero, Pasante - Desarrollo web">
                                     </div>

                                 </div>
                                 <div class="person-info">
                                    <h2>Bárbara Otero</h2><br>
                                    <p>Pasante - Desarrollo web</p>
                                   <p>Argentina</p>
                                 </div>
                             </div>
                         </div>
                         <div tabindex="40" class="col-sm-2 col-xs-6">
                             <div class="team-single-wrapper">
                                 <div class="team-single">
                                      <div class="person-thumb">
                                         <img src="images/team/camila-aprea.png" class="img-responsive" alt="Pasante - Diseño">
                                     </div>

                                 </div>
                                 <div class="person-info">
                                    <h2>Camila Aprea</h2><br>
                                    <p>Pasante - Diseño</p>
                                   <p>Argentina</p>
                                 </div>
                             </div>
                         </div>
                         <div tabindex="40" class="col-sm-2 col-xs-6">
                             <div class="team-single-wrapper">
                                 <div class="team-single">
                                      <div class="person-thumb">
                                         <img src="images/team/PilarRoldan.jpg" class="img-responsive" alt="Pilar Roldán, Pasante - Diseño">
                                     </div>

                                 </div>
                                 <div class="person-info">
                                    <h2>Pilar Roldán</h2><br>
                                    <p>Pasante - Diseño</p>
                                   <p>Argentina</p>
                                 </div>
                             </div>
                         </div>                         
                        </div>

                   </div>

                
                <!-- Controls -->
                <a aria-hidden="true" class="left team-carousel-control hidden-xs" href="#team-carousel" data-slide="prev">left</a>
                <a aria-hidden="true" class="right team-carousel-control hidden-xs" href="#team-carousel" data-slide="next">right</a>
            </div>
        </div>
    </div>
</section>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">

            </div>

                   <!--  <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
                  <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                       <h2>Envíanos un mensaje</h2>
                       <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php">
                        <div class="form-group">
                            <input tabindex="51" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Nombre">
                        </div>
                        <div class="form-group">
                            <input tabindex="52" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <textarea tabindex="53" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Tu mensaje"></textarea>
                        </div>            
                        <div style="display:none"> 
                            <input id="cc" value="sebastian@comparlante.com" placeholder="E-mail"> 
                        </div>             
                        <div class="form-group">
                            <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Enviar mensaje</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                    <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/main_es.js"></script>   
</body>


</html>
