<!DOCTYPE html>
<html lang="es">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad.">
    <meta name="author" content="Prime Developers Chile">
    
<!-- Facebook Metadatos | Asesoramiento productivo para emprendedores -->
    <meta property="og:title" content="Fundación Comparlante | Asesoramiento productivo para emprendedores"/>
    <meta property="og:site_name" content="Asesoramiento productivo para emprendedores"/>
    <meta property="og:description" content="El entorno emprendedor a nivel mundial guarda importantes oportunidades, pero al mismo tiempo enormes desafíos." />
    <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/services/2/asesoramiento-productivo.jpg"/>
  <!--  <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/asesorias-emprendimiento.php"/> -->
    
    
    
    <title>Fundación Comparlante</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">

        <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->

    </head><!--/head-->

    <body>
        <?php include("header.php"); ?>

        <section id="page-breadcrumb">
            <div class="vertical-center sun">
             <div class="container">
                <div class="row">
                    <div class="action">
                        <div tabindex="15" class="col-sm-12">
                            <h1 class="title">Asesoramiento productivo para emprendedores</h1>
                            <p>El entorno emprendedor a nivel mundial guarda importantes oportunidades, pero al mismo tiempo enormes desafíos.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/#page-breadcrumb-->

    <section id="portfolio-information" >
     <div class="container"><br><br>
        <div class="project-info overflow">
            <center>
                <img src="images/services/1/asesoramiento-productivo.png" class="img-responsive " alt="Asesoramiento productivo para emprendedores"></center>
            </div><br>
            <div class="row">

                <div class="col-sm-12">

                    <div tabindex="16" class="project-info overflow" style="text-align:justify">

                        <h2>El entorno emprendedor a nivel mundial guarda importantes oportunidades, pero al mismo tiempo enormes desafíos para los cientos de emprendedores que tomaron la decisión de hacer realidad sus sueños a través de la puesta en marcha de sus proyectos. </h2>

                        <h2>
                            Con la misión de constituirnos en el soporte adecuado y oportuno para este grupo de futuros empresarios, COMPARLANTE pone a su disposición un servicio de Asesoramiento Productivo, en el cual profesionales con amplia experiencia en Procesos de Incubación de Emprendimientos, Fortalecimiento de Ideas de Negocio, Análisis de Mercados, Confirmación de Viabilidad Financiera y desarrollo de habilidades blandas, buscan orientar a los Emprendedores en el mundo para que puedan contar con las herramientas necesarias y aprovechar al máximo cada oportunidad que se les presente. </h2> 
                            
                            <h2>
                                Poner en práctica la caja de herramientas necesarias para el desarrollo de una idea de negocio es clave para la futura puesta en marcha de la misma.</h2>

                                <h2>
                                    En este programa conoceremos qué contiene esa caja de herramientas, cómo y por qué animarse a utilizarla para crear un emprendimiento. Además, facilitaremos pautas para que los alumnos puedan recorrer el camino emprendedor validando, aprendiendo y generando valor para sus clientes.

                                </h2>

                            </div>
                            <center>
                                <div tabindex="17" class="live-preview data-wow-duration="500ms" data-wow-delay="300ms"">

                                   <h2>¿Eres emprendedor y necesitas apoyo?</h2>
                                   <br>
                                   <a tabindex="18" href="#etapas" class="btn btn-lg btn-info">¡Asesórate!</a>

                               </div></center>
                               <br>

                               <div class="project-info overflow padding-top " style="text-align:justify">
                                <div tabindex="20" class="col-sm-6">
                                    <p> 
                                        <div class="col-sm-3">
                                            <img src="images/team/roberto-jaramillo.jpg" class="img-responsive" alt="Roberto Jaramillo"> 
                                        </div> 
                                        Roberto Jaramillo, es un joven emprendedor ecuatoriano con  más de 5 años de experiencia en el área de emprendimientos y desarrollo de metodologías para el diseño, construcción, seguimiento e implementación de proyectos productivos. 
                                    </p>
                                    <p>En su perfil académico registra un Masterado en Relaciones Internacionales con Mención en Negociación y Manejo de Conflictos; Diplomado por la Universidad Andina Simón Bolívar del Ecuador en Economía y Finanzas e Ingeniero en Negocios Internacionales de la Universidad Internacional del Ecuador.</p>
                                    <p>Ha participado como representante de la República del Ecuador en ponencias nacionales e internacionales sobre la implementación de políticas públicas y programas productivos orientados en la atención de personas con discapacidad, como son: 17° Cumbre de Micro Crédito desarrollada en México; Expositor Internacional en el I Foro Internacional de Expertos en Materia de Inserción Laboral desarrollado en Perú, Expositor en el Seminario de Apoyo a la Micro Empresa desarrollado en Israél a través de la Agencia Israelí de Cooperación Internacional para el Desarrollo MASHAV, Centro Internacional de Capacitación Golda Meir y Young Americas Business Trust de la OEA, entre otros.
                                    </p>
                                    <p>Su orientación social y su búsqueda del bienestar para las persnoas que lo necesiten, lo hace parte de este prestigioso grupo de profesionales que tienen como objetivo entregarles lo mejor de su experiencia, para hacer de sus proyectos, una realidad.</p>
                                </div>
                                <div tabindex="30" class="col-sm-6">
                                    <p> 
                                        <div class="col-sm-3">
                                            <img src="images/team/nicolas-remedi.jpg" class="img-responsive" alt="Nicolás Remedi"> 
                                        </div> 
                                        Nicolás Remedi Rumi  es Licenciado en Dirección de Empresas, graduado en la Universidad Católica del Uruguay y asesor de micro, pequeñas y medianas empresas certificado por el Laboratorio Tecnológico del Uruguay. Se ha perfeccionado con estudios en emprendimiento e innovación en MASHAV International Training Center, Israel.
                                    </p>
                                    <p>Ha creado la asignatura emprendimiento e innovación en algunas instituciones educativas, es profesor e investigador de la Facultad de Ciencias Empresariales de la Universidad Católica del Uruguay. </p>
                                    <p>Actualmente es asesor del programa de apoyo a emprendedores Salto Emprende y tutor de Idea Open de Gepian.</p>
                                    <p>Es co-autor del Código de Ética de la Facultad de Ciencias Empresariales de la Universidad Católica del Uruguay y de Todos somos emprendedores, éste último apoyado por la Red de Apoyo a Futuros Empresarios de la Agencia Nacional de Investigación e Innovación.</p>
                                    <p>Fue co-refundador de la Junior Chamber International en Salto, ha participado del programa Jóvenes emprendedores le hablan sistema político organizado por la Fundación Konrad Adenauer y la Asociación Cristina de Dirigentes de Empresas.</p>
                                    <p>Es autor de decenas de artículos relacionados con el emprendimiento y la innovación, publicados por medios escritos en Uruguay y España.</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <br>
            <section id="etapas">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div tabindex="40" class="clients text-center wow fadeInUp" data-wow-duration="300ms" data-wow-delay="300ms">

                                <h2> <b>Conviertete en emprendedor </b></h2> <br>
                                <h2 style="text-align:justify">¡No esperes ni un segundo más, sólo los gigantes como tú saben dar pasos gigantes, así que pon toda tu creatividad y tus ganas de hacer la diferencia en tu equipaje y únete al maravilloso viaje del emprendimiento de la mano de nuestros profesionales!</h2>
                                <h2>¡A la accesibilidad y más allá!</h2>
                            </div>
                            <!-- Tabuladores -->
                            <div class="col-md-12">
                                <ul id="tab1" class="nav nav-tabs ">
                                    <li class="active"><a tabindex="41" href="#tab1-item1" data-toggle="tab"  aria-labelledby="f1"><i class="fa fa-circle-o fa-3x" style="vertical-align:middle;" aria-hidden="true"></i> Fase 1</a></li>
                                    <li><a href="#tab1-item2"  tabindex="43" data-toggle="tab"  aria-labelledby="f2"><i class="fa fa-circle-o fa-3x" style="vertical-align:middle;" aria-hidden="true"></i> Fase 2</a></li>
                                    <li><a href="#tab1-item3" tabindex="45" data-toggle="tab"  aria-labelledby="f3"><i class="fa fa-circle-o fa-3x" style="vertical-align:middle;"aria-hidden="true"></i> Fase 3</a></li>
                                    <li><a href="#tab1-item4" tabindex="47" data-toggle="tab"  aria-labelledby="f4"><i class="fa fa-circle-o fa-3x" style="vertical-align:middle;"aria-hidden="true"></i> Fase 4</a></li>
                                    <li><a data-toggle="tab" tabindex="49"  aria-labelledby="f5"><i class="fa fa-bullseye fa-3x" style="vertical-align:middle;"aria-hidden="true"></i> Plenitud</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div tabindex="42" class="tab-pane fade active in" id="tab1-item1" style="text-align:justify">
                                        <h3><b>Título del curso: Desarrollo de habilidades empresariales</b></h3> <br>
                                            <p>La Fase 1 del proceso de capacitación consiste en el desarrollo de habilidades blandas para los emprendedores, donde a través de la IDEACIÓN se busca descartar las ideas que no se acoplen a la experiencia del emprendedor ni a su entorno, para terminar en la selección de aquella idea que cumple con todas sus expectativas. Esta fase, fortalece la ideación con el aterrizaje del emprendimiento a través de la implementación de un Plan de Negocio, en el cual plasmaremos de forma ordenada la razón de ser de la empresa, el análisis de su mercado, sus proyecciones hacia el futuro, sus fortalezas y debilidades. El resultado: Generar la guía del emprendedor para la toma de decisiones oportuna.</p>
                                       
                                    </div>
                                    <div tabindex="44" class="tab-pane fade" id="tab1-item2" style="text-align:justify">
                                        <h3><b>Título del curso: Modelo de negocios y valor diferencial</b></h3> <br>
                                        <p>La Fase 2 consiste en el desarrollo de herramientas que permitan entender con claridad cuál es el modelo de negocios más apropiado para la idea seleccionada; es decir: Aprenderemos como enlazar nuestra idea con los cliente, proveedores, mercados y recursos con los que contamos, para generar un valor agregado que nos diferencie de nuestros competidores y que nos permita obtener las ganancias esperadas de nuestra inversión. La innovación y el valor diferencial definen el éxito de nuestra empresa y nos otorgan ventajas que mantendrán a nuestro proyecto en la cima del mercado.</p>
                                    </div>
                                    <div tabindex="46" class="tab-pane fade" id="tab1-item3" style="text-align:justify">
                                        <h3><b>Título del curso: Proceso de validación</b></h3> <br>
                                        <p>La fase 3 permitirá que los emprendedores y futuros empresarios validen su idea de negocio en el mercado, a través de la implementación de procesos que bajo la metodología "Prueba - Error" <u>permitirán aterrizar aún más el modelo</u>, el producto y su valor agregado. Analizaremos las principales herramientas para probar nuestro producto en el mercado, obtener una retroalimentación de nuestros potenciales clientes y generar un producto innovador y <u>de alto impacto</u>. Esta fase se fortalece a través del análisis y sistematización de Gestión de procesos empresariales, donde los tutores expondrán a los potenciales empresarios las principales alternativas <u>para el manejo ordenado</u> de una futura nueva empresa. </p>
                                    </div>
                                    <div  tabindex="48" class="tab-pane fade" id="tab1-item4" style="text-align:justify">
                                        <h3><b>Título del curso: Factibilidad económica-financiera</b></h3> <br>
                                        <p>La fase 4 de nuestro proceso de capacitación busca entregar a nuestros futuros empresarios las mejores prácticas para definir la factibilidad económica y financiera del proyecto, a través del desarrollo de flujos de caja, análisis de indicadores de rentabilidad financiera y definición de puntos de equilibrio. Estos conceptos son fundamentales en el desarrollo de un emprendimiento y nos ayudan a tener una radiografía de los ingresos esperados para definir la rentabilidad de nuestra inversión. Adicionalmente los tutores presentarán procesos ordenados para la ejecución exitosa de nuestras futuras empresas.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- Fin tabuladores -->
                            <!-- aria descripciones -->
                            <div style="display:none" id="f1" role="tooltip"><p>Fase 1.</p> </div>  
                            <div style="display:none" id="f2" role="tooltip"><p>Fase 2. </p></div>
                            <div style="display:none" id="f3" role="tooltip"><p>Fase 3.</p> </div>
                            <div style="display:none" id="f4" role="tooltip"><p>Fase 4.</p> </div>
                            <div style="display:none" id="f4" role="tooltip"><p>Plenitud.</p> </div>
                            
                            
                            <br>


                            <br>
                        </div>


                    </div>

                    <!-- inicio botón contacto -->
                    <center>
                        <div style="margin-top:50px" class="live-preview data-wow-duration="500ms" data-wow-delay="300ms"">


                           <br>
                           <a tabindex="50" href="#contacto" class="btn btn-lg btn-info">Contáctanos para respuesta personalizada</a>

                       </div>
                   </center>
               </div>
           </section>




           <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center bottom-separator">

                    </div>

           <!--  <div class="col-md-5 col-sm-6">
                <div class="contact-info bottom">
                  <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

              </div>
          </div> -->
          <div class="col-md-12 col-sm-12">
            <div id="contacto" class="contact-form bottom">
             <h2>Envíanos un mensaje</h2>
             <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php">
                <div class="form-group">
                    <input tabindex="51" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Nombre">
                </div>
                <div class="form-group">
                    <input tabindex="52" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                </div>
                <div class="form-group">
                    <textarea tabindex="53" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Tu mensaje"></textarea>
                </div>            
                <div style="display:none"> 
                    <input id="cc" value="emprendimiento@comparlante.com" placeholder="E-mail"> 
                </div>             
                <div class="form-group">
                    <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Enviar mensaje</button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="copyright-text text-center">
            <p>&copy; Fundación Comparlante 2016.</p>
            <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
        </div>
    </div>
</div>
</div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/main_es.js"></script>   
</body>


</html>