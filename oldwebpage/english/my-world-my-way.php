<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities.">
    <meta name="author" content="Prime Developers Chile">
    
    <!-- Facebook Metadatos | Diseño web accesible -->
    <meta property="og:title" content="First Literary contest for children's My world My Way "/>
    <meta property="og:site_name" content="My world My Way"/>
    <meta property="og:description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities" />
    <meta property="og:image" content="http://comparlante.com/images/concurso/personajes.png"/>


    
    
    <title>Comparlante Fundation</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet"> 
    <link href="../css/lightbox.css" rel="stylesheet"> 
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">


    <link rel="shortcut icon" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" href="../images/ico/logo-icon.png">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head><!--/head-->

<body>
  <?php include("header.php"); ?>

  <section id="page-breadcrumb">
    <div class="vertical-center sun">
     <div class="container">
        <div class="row">
            <div class="action">
                <div tabindex="10" class="col-sm-12">
                    <h2 style="font-size: 20pt; font-weight: 400" class="title text-center">My World My Way</h2>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!--/#page-breadcrumb-->
<div id="zooming">

    <section id="concurso" >
        <div class="container"> <br><br>
            <div class="project-info overflow">
                <center>
                    <img width="50%" src="../images/concurso/personajes.png" class="img-responsive " alt="The 4 animated characters of the Contest: boy with visual disability, girl with motor disability, girl with Down syndrome and boy with hearing disability, playing together with the earth globe as a ball."></center>
                </div><br>
                <div class="row">

                    <div class="col-sm-12">

                        <div tabindex="11" class="project-info overflow "style="text-align:justify">

                            <h2>Children are the present and the future. Fundación Comparlante understands that raising awareness about the different disabilities promotes equity. In 2017 we developed this children's literary contest to work from schools and with the family to break down stereotypes in society.

                                <br>

                            This international contest for children between 6 to 13 years of age counted in its first edition with the participation of children from Argentina, Ecuador, Costa Rica, Mexico, Peru and Colombia. In their stories they had to involved the characters proposed by Fundación Comparlante.</h2>

                        </div>

                        <br>
                        <!-- primer personaje -->
                        <div tabindex="15" class="project-info overflow" style="text-align:justify">
                            <h2>Our 1st character is a boy with visual disability:</h2>
                            <h3>
                                <div  class="col-sm-3">
                                    <img src="../images/concurso/discapacidad-visual.png" width="75% class="img-responsive" alt="character with visual disability"> 
                                </div> 
                                <br> <br>
                                Visual impairment refers to the deficiency of the vision system which affects the acuity and visual field, ocular motility and the perception of colors and depth, resulting in diagnoses such as low vision or blindness.
                            </h3>
                            <h3>At his side, the boy is accompanied by his guide-dog: an animal specially trained to provide assistance to the mobility and independence of persons with visual impairment. </h3>
                        </div> 
                        <!-- segundo personaje -->
                        <div tabindex="16" class="project-info overflow" style="text-align:justify">
                            <h2>Our 2nd character is a girl with motor disability:</h2>
                            <h3>
                                <div  class="col-sm-3">
                                    <img src="../images/concurso/discapacidad-motriz.png" width="60% class="img-responsive" alt="character  with motor disability"> 
                                </div> 
                                <br> <br>
                                Motor disability refers to a physical condition which influences the ability of control and mobility of the body characterized by alterations in the displacement, balance, speech and breathing of the person.
                            </h3>
                            <h3>To facilitate her movement and autonomy she has a wheelchair which adapts and responds to her needs. </h3>
                        </div> 

                        <!-- tercer personaje -->
                        <div tabindex="17" class="project-info overflow" style="text-align:justify">
                            <h2>Our 3rd character is a girl with Down Syndrome:</h2>
                            <h3>
                                <div  class="col-sm-3">
                                    <img src="../images/concurso/sindrome-down.png" width="60% class="img-responsive" alt="character  with Down Syndrome"> 
                                </div> 
                                <br> <br>
                                Down Syndrome is a congenital disorder derived from the total or partial triplication of chromosome 21, cognitive spectrum disability that results in mental retardation and growth as part of certain physical alterations.  
                            </h3>
                        </div> 
                        <!-- cuarto personaje -->
                        <div tabindex="18" class="project-info overflow " style="text-align:justify">
                            <h2>Our 4th character is a boy with hearing impairment:</h2>
                            <h3>
                                <div  class="col-sm-3">
                                    <img src="../images/concurso/discapacidad-auditiva.png" width="60%" class="img-responsive" alt="character  with hearing impairment"> 
                                </div> 
                                <br> <br>
                                Hearing disability or deafness refers to the impossibility or difficulty of making use of the hearing sense due to a loss of partial hearing (hearing loss) or total (cofosis) ability unilaterally or bilaterally. As happens with other physical disabilities, deafness can originates at birth or be acquired throughout the years of life.


                            </h3>
                            <h3>To communicate, deaf persons use Sign Language: a complete communication system which in the same way that the phonetic or spoken language allows to transmit ideas and feelings by transforming words into gestures made mainly with the hands. 
                            </h3>
                        </div> 
                    </div>
                </div>
            </div>
        </section>
        <br>

        <section id="ganadores">
            <div class="container">
                <div class="row">
                    <div tabindex="18" class="project-info overflow " style="text-align:justify">
                        <center><h2 class="center">Meet the winners! </h2></center>
                        <h3>
                            Access the winning stories of the First Edition and be amazed by the creation of the little writers who, with their imagination, are already shaping a more accessible and equitable world for everyone.
                        </h3>

                        <h3> <b>First place: Agustina Irene Abdo Valdiviezo </b></h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Story: Los colores de Tom.</li>
                            <li><i class="fa fa-angle-right"></i> School: Liceo Internacional, Quito.</li>
                            <li><i class="fa fa-angle-right"></i> Age: 11 years old.</li>
                            <li><i class="fa fa-angle-right"></i> Country: Ecuador.</li>
                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/los-colores-de-tom.pdf">Read the online version. (Only Available in Spanish)</a></li>
                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/biblioteca_audiolibros/comparlante/index.php?r=libros/view&id=215">AudioBook. (Only Available in Spanish)</a></li>

                        </ul>

                    </div> 
                    <div tabindex="22" class="project-info overflow " style="text-align:justify">

                        <h3><b>Second Place: Ariana Valeria Valenzuela Muñoz</b> </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Story: Mi mundo se llamaba silencio.</li>
                            <li><i class="fa fa-angle-right"></i> Age: 12 years old.</li>
                            <li><i class="fa fa-angle-right"></i> Country: Ecuador.</li>
                            <li><i class="fa fa-angle-right"></i> School: Colegio Ecuatoriano-Español América Latina,Quito.</li>

                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/mi-mundo-se-llama-silencio.pdf">Read the online version. (Only Available in Spanish)</a></li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->

                        </ul>
                    </div> 
                    <div tabindex="25" class="project-info overflow " style="text-align:justify">

                        <h3><b>Third   place: David Rodolfo Jiménez Caamaño</b> </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Story: Felipe.</li>
                            <li><i class="fa fa-angle-right"></i> Age: 11 years old.</li>
                            <li><i class="fa fa-angle-right"></i> Country: Costa Rica.</li>
                            <li><i class="fa fa-angle-right"></i> School: Saint Gregory School, San José.</li>

                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/felipe-spreads.pdf">Read the online version. (Only Available in Spanish)</a></li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->

                        </ul>
                    </div> 
                    <div tabindex="29" class="project-info overflow " style="text-align:justify">

                        <h3><b>Menciones especiales: </b> </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Story: Un breve relato sobre mis super-poderes.</li>
                            <li><i class="fa fa-angle-right"></i> Author: Galo Dana.</li>
                            <li><i class="fa fa-angle-right"></i> Age: 13 years old.</li>
                            <li><i class="fa fa-angle-right"></i> Country: Argentina</li>
                            <li><i class="fa fa-angle-right"></i> School: Santo Tomás Moro, La Plata.</li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->
                        </ul>
                        <br>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Story: Una gran amistad.</li>
                            <li><i class="fa fa-angle-right"></i> Author: Melina Bogarín Monge</li>
                            <li><i class="fa fa-angle-right"></i> Age: 11 years old.</li>
                            <li><i class="fa fa-angle-right"></i> Country: Costa Rica.</li>
                            <li><i class="fa fa-angle-right"></i> School: Saint Gregory School, San José.</li>
                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/una-gran-amistad.pdf">Ver cuento en versión online. (Only Available in Spanish)</a></li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->
                        </ul>
                        <br>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Story: This is me.</li>
                            <li><i class="fa fa-angle-right"></i> Author: Estefania Alexandra Moreno Marin</li>
                            <li><i class="fa fa-angle-right"></i> Age: 10 years old.</li>
                            <li><i class="fa fa-angle-right"></i> Country: Ecuador.</li>
                            <li><i class="fa fa-angle-right"></i> School: Liceo Internacional, Quito.</li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->
                        </ul>
                    </div> 
                </div>
            </div>

        </section>


<!--         <section id="informacion-concurso">
            <div class="container">
                <div class="row">
                    <div tabindex="18" class="project-info overflow " style="text-align:justify">
                        <h3>
                            Descubre lo mágico e interesante que es el mundo que nos rodea atreviéndote a conocer más allá investigando junto a tu maestra, tus compañeros, tu familia y amigos. 
                        </h3>

                        <h3>Los invitamos a ser parte de esta propuesta a partir del 1 de marzo de 2017 presentando su cuento el cual debe cumplir con las siguientes bases:
                        </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Género: Cuento, infantil y juvenil.</li>
                            <li><i class="fa fa-angle-right"></i> Premio: Publicación de la obra y diploma.</li>
                            <li><i class="fa fa-angle-right"></i> Abierto a: Niñas y niños de entre 6 y 13 años.</li>
                            <li><i class="fa fa-angle-right"></i> Entidad convocante: Fundación Comparlante.</li>
                            <li><i class="fa fa-angle-right"></i> País de la entidad convocante: Argentina.</li>
                            <li><i class="fa fa-angle-right"></i>  Alcance del concurso: Internacional.</li>
                            <li><i class="fa fa-angle-right"></i>  Fecha de cierre: 15 de junio de 2017, 23.59 hora Argentina.</li>
                        </ul>
                        <br>
                        <br>
                    </div> 
                </div>
            </div>

        </section> -->
<!--         <section id="bases">
            <div class="container">
                <div class="row">

                    <h3><b>Bases:</b></h3>

                </div>
                <div class="row">

                    <div tabindex="18" class="project-info overflow " style="text-align:justify">
                        <h3>
                            Las presentes bases tienen por objeto reglamentar el I Concurso literario para niñasy niñosde educación básica “Mi mundo a mi manera” el cual se realiza por compromiso de Fundación Comparlante para incentivar la creación literaria de los pequeños talentos de nuestra región buscando generar conciencia acerca de las capacidades y fortalezas de los niños con discapacidad y las ventajas de vivir en una sociedad accesible y con equidad. 
                        </h3>
                        <br>
                        <h3> <b> Participantes:</b></h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Podrán participar niñas y niños entre 6 y 13 años de edad.</li>

                        </ul>
                        <br>
                        <br>
                        <h3> <b> Categoría:</b></h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Género de la obra: Cuento infantil y juvenil.</li>
                            <li><i class="fa fa-angle-right"></i> Las obras presentadas deberán ser de un solo autor, originales e inéditas, incluso en internet.</li>
                            <li><i class="fa fa-angle-right"></i> Los autores tendrán plena libertad de expresión y enfoque del tema, siempre que se ciñan a la temática establecida.</li>
                            <li><i class="fa fa-angle-right"></i> El cuento debe incorporar al menos uno (1) de los cuatro (4) personajes propuestos por Fundación Comparlante.</li>
                            <li><i class="fa fa-angle-right"></i> El cuento deberá estar escrito en español o inglés, con una extensión máxima de 12 hojas Word usando letra Times New Roman 12 e interlineado de 1.5. Si correspondiera, adjuntar en formato PDF o escaneado el original del manuscrito.</li>

                        </ul>
                        <br>
                        <h3> <b> Formato de entrega:</b></h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Los participantes debenenviar susobras en formato digital (Formato Word o PDF) a info@comparlante.com, indicando en el asunto del correo: Título del cuento, nombre del autor.</li>
                            <li><i class="fa fa-angle-right"></i> Seguidamente consignar los siguientes datos personales del autor en el cuerpo del correo.</li>
                            <ul>
                             <li><i class="fa fa-angle-double-right"></i> Título de la obra.</li>
                             <li><i class="fa fa-angle-double-right"></i> Nombre completo del autor.</li>
                             <li><i class="fa fa-angle-double-right"></i> Cédula de identidad.</li>
                             <li><i class="fa fa-angle-double-right"></i> Fecha de nacimiento.</li>
                             <li><i class="fa fa-angle-double-right"></i> Domicilio.</li>
                             <li><i class="fa fa-angle-double-right"></i> País.</li>
                             <li><i class="fa fa-angle-double-right"></i> Edad.</li>
                             <li><i class="fa fa-angle-double-right"></i> Colegio.</li>
                             <li><i class="fa fa-angle-double-right"></i> Curso.</li>
                             <li><i class="fa fa-angle-double-right"></i> Teléfono.</li>
                             <li><i class="fa fa-angle-double-right"></i> Correo electrónico.</li>
                         </ul>

                     </ul>
                     Los coordinadores del concurso podrán solicitar a los colegios la información para corroborar los datos de los concursantes.
                     <br>
                     <br>
                     <h3> <b> Premios:</b></h3>
                     <h3> Se premiarán a los (3) mejores trabajos:</h3>
                     <ul class="elements">
                        <li><i class="fa fa-angle-right"></i> 1° Premio: Publicación digital y papel (cien (100) ejemplares), promoción online durante 3 meses, diploma de reconocimiento.</li>
                        <li><i class="fa fa-angle-right"></i> 2º Premio: Publicación digital, promoción online durante 3 meses, diploma de reconocimiento. </li>
                        <li><i class="fa fa-angle-right"></i> 3º Premio: Publicación digital, promoción online durante 3 meses, diploma de reconocimiento.  </li>
                    </ul>
                    Los autores de los cuentos que cumplan todos los requisitos, aún sin ser ganadores, recibirán un diploma por su participación en el concurso.
                    <br>
                    <br>
                    <h3> <b> Jurado:</b></h3>
                    
                    <ul class="elements">
                        <li><i class="fa fa-angle-right"></i> Las obras participantes serán evaluadas y seleccionadas por un Jurado integrado por 3 personas de trayectoria en el ámbito.</li>
                        <li><i class="fa fa-angle-right"></i> El jurado levantará un acta firmada que designará a los ganadores de este concurso el 30 de julio de 2017 y se darán a conocer los resultados declarando a los ganadores del concurso, para luego ser notificados todos los participantes de la presente convocatoria.</li>
                        <li><i class="fa fa-angle-right"></i>  La resolución referida en el acta que declare a los ganadores será inapelable por los participantes, quienes no podrán pretender indemnización alguna por ningún concepto.</li>
                    </ul>
                    <br>

                    <br>
                    <h3> <b> Plazos:</b></h3>
                    <ul class="elements">
                        <li><i class="fa fa-angle-right"></i>  El Plazo de recepción de las obras participantes será desde el 1 de marzo al 15 de junio del año 2017.</li>
                        <li><i class="fa fa-angle-right"></i>  Los trabajos se deben enviar en formato digital (Formato Word o PDF) a info@comparlante.com hasta el 15 de junio, 23.59 hora Argentina.</li>
                        <li><i class="fa fa-angle-right"></i> Los ganadores se darán a conocer el 30 de julio a los participantes y al público en general en las redes sociales de Fundación Comparlante: Facebook y Twitter.</li>
                        <li><i class="fa fa-angle-right"></i>  Fundación Comparlante, posterior al cumplimiento de los requisitos legales por parte del ganador hará entrega oficial del premio.</li>
                        <li><i class="fa fa-angle-right"></i>   La participación en este certamen supone la total aceptación de las presentes bases las cuales rigen bajo leyes de la República Argentina.</li>


                    </ul>
                    <br>
                </div> 
            </div>
        </div>

    </section>
-->
<center>
    <br>
    <a tabindex="9" type="button" href="concurso/bases.pdf" class="btn btn-info"><h4>Download Contest Bases</h4></a>
</center>
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">

            </div>

                   <!--  <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
                  <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                     <h2>¿Tienes alguna duda? Envíanos un mensaje</h2>
                     <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php">
                        <div class="form-group">
                            <input tabindex="51" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Nombre">
                        </div>
                        <div class="form-group">
                            <input tabindex="52" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <textarea tabindex="53" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Tu mensaje"></textarea>
                        </div>            
                        <div style="display:none"> 
                            <input id="cc" value="" placeholder="E-mail"> 
                        </div>             
                        <div class="form-group">
                            <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Enviar mensaje</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Fundación Comparlante 2016.</p>
                    <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/main_es.js"></script>   
</body>


</html>
