  <header id="header">      

    <div class="navbar navbar-inverse" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation bar</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index_en.php">
                    <h1><img class="logo-a" src="../images/home/logofundacionhorizontal.png" width="80%" alt="Comparlante's logo."></h1>
                </a>
                
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                 <li><a href="index_en.php" tabindex="96">HOME</a></li>
                 <li><a href="index_en.php#quienes-somos" tabindex="6">WHO WE ARE</a></li>
                 <li><a href="programs.php">PROGRAMS</a></li>   

                 <li class="dropdown"><a href="#servicios">SERVICES OFFERED <i class="fa fa-angle-down"></i></a>
                    <ul role="menu" class="sub-menu">
                        <li><a href="http://www.comparlante.com/biblioteca_audiolibros/english/">Comparlante Audiobooks Library</a></li>
                        <li><a href="accessibility-consulting.php">Accessibility consulting</a></li>
                        <li><a href="web-design.php">Accessible websites design</a></li>
                        <li><a href="reading-for-you.php">Readings for you</a></li>
                        <li><a href="consulting-for-entrepreneurs.php">Consulting for Social Entrepreneurs</a></li>
                        <li><a href="accesible-designs.php">Accessible Designs</a></li>
                    </ul>
                </li>   
                <li><a href="index_en.php#involucrate">GET INVOLVED</a></li>
                <!-- <li><a href="index_en.php#equipo">TEAM</a></li> -->
                <li><a href="contact.php">CONTACT</a></li>         

            </ul>
        </div>
        <div class="col-sm-12 overflow">
         <div class="social-icons pull-right">
            <div style="display:none" id="tp1" role="tooltip"><p>Facebook.</p> </div>  
            <div style="display:none" id="tp2" role="tooltip"><p>Twitter. </p></div>
            <div style="display:none" id="tp4" role="tooltip"><p>YouTube.</p> </div>
            <div style="display:none" id="tp5" role="tooltip"><p>LinkedIn.</p> </div>
            <ul class="nav nav-pills">
                <li><a tabindex="90" alt="Facebook" title="Facebook" aria-labelledby="tp1" href="https://www.facebook.com/pages/Comparlante/1537630623166015?ref=aymt_homepage_panel" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                <li><a tabindex="91" title="Twitter" alt="Twitter" aria-labelledby="tp2" href="https://twitter.com/comparlante" target="_blank" ><i class="fa fa-twitter"></i></a></li>
                <li><a tabindex="93" title="YouTube" alt="YouTube" aria-labelledby="tp4" href="http://youtube.com/channel/UCTYBW1pJ5TcsCuMAYSm_10Q" target="_blank" ><i class="fa fa-youtube"></i></a></li>

                <li>
                    <a title="Linkedin" alt="LinkedIn" tabindex="94" aria-labelledby="tp5" href="https://www.linkedin.com/in/fundaci%C3%B3n-comparlante-207b88bb" target="_blank" ><i class="fa fa-linkedin"></i></a></li>
                    <li>
                        <a href="#" id="zoom_in" title="Increase Font size" tabindex="95" onclick="aumentar()"><b>A+</b></i></a>
                    </li>
                    <li>
                        <a href="#" id="zoom_out" title="Decrease Font size"  tabindex="96" onclick="disminuir()"><b>A-</b></a>
                    </li>
                </ul>
            </div> 
        </div>
    </div>

</div>
</div>
</header>

<div class="letras">
 <a href="#" id="zoom_in" alt="Increase Font size" title="Increase Font size" tabindex="95" onclick="aumentar()"><h3><b>A+</b></h3></i></a>
 <br>
 <a href="#" id="zoom_out" alt="Decrease Font size" title="Decrease Font size"  tabindex="96" onclick="disminuir()"><h3><b>A-</b></h3></a>



</div>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75127504-2', 'auto');
  ga('send', 'pageview');

</script>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WRVC32B"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!--/#header-->

    <script type="text/javascript">
      function aumentar(){
        if(window.parent.document.body.style.zoom!=0) window.parent.document.body.style.zoom*=1.2; else window.parent.document.body.style.zoom=1.2;
        // var fontSize;
        //  fontSize += 1;
        //     document.body.style.fontSize = fontSize + "px";
    }

    function disminuir(){
        if(window.parent.document.body.style.zoom!=0) window.parent.document.body.style.zoom*=0.8; else window.parent.document.body.style.zoom=0.8;
    //     var fontSize;
    //      fontSize -= 0.1;
    //         document.body.style.fontSize = fontSize + "px";
}
</script>

<style type="text/css">
@media (max-width: 1000px) and (min-width: 50px) {
  .letras  {
    display: none !important;
}
.media-movil{
    position: fixed;
    background-color: #fff;
    z-index: 99;
}
}
.letras{
   margin-left: 0%;
   border-color: #f9c741;
   background-color: #f9c741;
   border-width: 1px;
   border-style: solid;
   position: fixed;
   padding-left: 1%;
   z-index: 99;
   width: 4%;
   border-top-right-radius: 2em;
   border-bottom-right-radius: 2em;
}

</style>