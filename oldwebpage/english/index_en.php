<!DOCTYPE html>
<html lang="en">


<script type="text/javascript">
    window.onload = setTimeout(function() {
        document.getElementById("instrucciones-de-uso").focus();

    },1500);

</script>

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta name="description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities.">
   <meta name="author" content="Prime Developers Chile">

   <!-- Facebook Metadatos -->
   <!--  Inicio -->
   <meta property="og:title" content="Comparlante Foundation | Inicio"/>
   <meta property="og:site_name" content="Comparlante Foundation"/>
   <meta property="og:description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities." />
   <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/home/logo-fundacion-2.jpg"/>
   <!--   <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/index_es.php"/> -->




   <title>Fundación Comparlante</title>
   <link href="../css/bootstrap.min.css" rel="stylesheet">
   <link href="../css/font-awesome.min.css" rel="stylesheet">
   <link href="../css/animate.min.css" rel="stylesheet"> 
   <link href="../css/lightbox.css" rel="stylesheet"> 
   <link href="../css/main.css" rel="stylesheet">
   <link href="../css/responsive.css" rel="stylesheet">


   <link rel="shortcut icon" href="../images/ico/logo-icon.png">
   <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/logo-icon.png">
   <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/logo-icon.png">
   <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/ico/logo-icon.png">
   <link rel="apple-touch-icon-precomposed" href="../images/ico/logo-icon.png">

   <!-- Google Tag Manager -->
   <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
   j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
   'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head>

<body>
  <?php include("header.php"); ?>
  <div style="display:none" id="mensaje-inicial"><p id="instrucciones-de-uso">La página de fundación Comparlante tiene una navegación vertical, En dispositivos móviles debes deslizarte con el dedo hacia la derecha o izquierda.</p> </div>  
  <section id="home-slider">
    <div class="container">
        <div class="row">
            <div class="main-slider">
                <div class="slide-text" tabindex="4">
                    <h1>Comparlante Foundation</h1>
                    <h2>Welcome to our community!</h2>
                    <!-- <a href="#" class="btn btn-common">SIGN UP</a> -->
                </div>
                <img src="../images/home/slider/1.png" class="slider-hill" alt="Landscape of a mountain in the background, a sun, and colorful birds in the clouds.">
                <img src="../images/home/slider/2.png" class="slider-house" alt="A building surrounded by trees, and in the left corner a person in a wheelchair and another person using a cane.">
                <img src="../images/home/slider/3.png" class="slider-sun" alt="Four people smiling in front of a building, a person in a wheelchair, another person using a cane, accompanied by an adult man and an adult woman.">

            </div>
        </div>
    </div>
    <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
</section>
<!--/#home-slider-->

<section id="quienes-somos" tabindex="5" class="padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
    <div class="container">
        <div class="row">
            <h1 class="title text-left wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Who we are</h1>

            <div class="col-sm-12 padding-top-index text-justify">
                <p>Fundación Comparlante was created in 2015 in Argentina with the objective to make technological tools which generate accessibility to people with visual impairment. Today, it is a collective project which operates from Latin America and United States (Washington DC) integrated by young entrepreneurs from Argentina, Chile, Costa Rica, Ecuador, El Salvador, Uruguay and Venezuela.</p>
                <p>Nowadays, Comparlante wants to establish itself as a net which offers products and services adapted to the different needs that people with disabilities have. In a collective construction, accessibility, entrepreneurship and social impact are the basis of this project and its proposals.</p>
                <p>From innovation, Comparlante is an open space for developing tools which help to do this world less disabled.</p>
            </div>
        </div>
    </div>
    <br>
</section>


<!-- verificador -->
<section id="verificador" tabindex="6" class="padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
    <div class="container">
        <div class="row">
            <h2 style="font-size: 20pt; font-weight: 400" class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Verify the accessibility of your website</h2>

            <div class="col-sm-12 padding-top-index text-justify">
                <center>
                    <a href="verifica.php">
                       <img width=" 35%" src="../images/achecker/achecker.png" alt="Logo que representa el servicio de verificación de accesibilidad web">
                   </a>
                   <br> <br>
                   <a tabindex="9" style="background-color: #f39917!important;
                   border-color: #f39917!important;"  alt="ingresa al verificador de accesibilidad web"   type="button" href="check.php" class="btn btn-info"><h4>Verify your site!</h4></a>
               </center>
           </div>
       </div>
   </div>

</section> 

<!-- concurso -->
<!-- <section id="concurso" tabindex="6" class="padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
    <div class="container">
        <div class="row">
            <h2 style="font-size: 20pt; font-weight: 400" class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">I Literary contest for girls and boys in elementary school "My world, My way"</h2>

            <div class="col-sm-12 padding-top-index text-justify">
                <center>
                    <a href="contest.php">
                       <img width=" 35%" src="../images/concurso/personajes.png" alt="Characters of contest, a boy with visual impairment appears, to his right a girl with motor disability, a boy with hearing impairment and a girl with down syndrome. Enter for contest information">
                   </a>
                   <br> <br>
                   <a tabindex="9" style="background-color: #f39917!important;
                   border-color: #f39917!important;"  alt="Enter to the contest of Comparlante Foundation "   type="button" href="contest.php" class="btn btn-info"><h4>Join in!</h4></a>
               </center>
           </div>
       </div>
   </div>

</section> -->

<!-- lo que hacemos -->
<section id="servicios">
    <div class="container">
        <div class="row">
            <h1 tabindex="7" class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Our Services</h1>
            <p class="text-center wow fadeInDown" data-wow-duration="400ms" data-wow-delay="400ms"></p>
            <div class="single-features">
                <div class="col-sm-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                    <img src="../images/services/audiolibros.png" class="img-responsive" alt="Comparlante Audiobooks Library">
                </div>
                <div  tabindex="8" class="col-sm-6 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                    <h2>Comparlante Audiobooks Library</h2>
                    <P>Comparlante offers an accessible audiobook bank in 18 languages. It is collaboratively constructed and freely available.</P>
                    <a tabindex="9" type="button" href="http://www.comparlante.com/biblioteca_audiolibros/english/" class="btn btn-info">Go to the Library</a>
                </div>
            </div>

            <div class="single-features">

                <div tabindex="10" class="col-sm-6 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                    <h2>Accessibility Consulting</h2>
                    <P>Aimed at companies, governments, NGOs and private entities that want to develop and adapt their work and services according to international accessibility standards.</P>
                    <a tabindex="11" type="button" href="accessibility-consulting.php" class="btn btn-info">Go to Accessibility Consulting</a>
                </div>
                <div class="col-sm-5 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                    <img src="../images/services/asesoria-accesibilidad.png" class="img-responsive" alt="Accessibility Consulting">
                </div>
            </div>

            <div class="single-features">
             <div class="col-sm-5 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                <img src="../images/services/diseno-web.png" class="img-responsive" alt="Accessible Websites Design ">
            </div>
            <div tabindex="12" class="col-sm-6  wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                <h2>Accessible Websites Design </h2>
                <P>We develop websites under criteria of accessibility, usability and navigability.  </P>
                <a  tabindex="13" type="button" href="web-design.php" class="btn btn-info">Go to the service</a>
            </div>

        </div>
        <div class="single-features">

            <div tabindex="14" class="col-sm-6 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                <h2>Readings for you  </h2>
                <P>If you have a book you want or need, Comparlante offers you the reading of it on demand.</P>
                <a tabindex="15" type="button" href="reading-for-you.php" class="btn btn-info">Go to Readings For You</a>
            </div>
            <div class="col-sm-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                <img src="../images/services/lectura-a-medida.png" class="img-responsive" alt="Readings for you">
            </div>
        </div>
        <div class="single-features">
            <div class="col-sm-5 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                <img src="../images/services/asesoramiento-productivo.png" class="img-responsive" alt="Consulting for Social Entrepreneurs">
            </div>
            <div tabindex="16" class="col-sm-6  wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                <h2>Consulting for Social Entrepreneurs</h2>
                <P>We give consulting oriented to the development of entrepreneurships for disabled people.</P>
                <a tabindex="17" type="button" href="consulting-for-entrepreneurs.php" class="btn btn-info">Go to the Service</a>
            </div>

        </div>
        <div class="single-features">

            <div tabindex="18" class="col-sm-6 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                <h2>Accessible Designs</h2>
                <P>We develop designs and illustrations of material, products, campaigns and the like in an accessible format for your institution or project.</P>
                <a tabindex="19" type="button" href="accesible-designs.php" class="btn btn-info">Go to Accessible Designs Service</a>
            </div>
            <div class="col-sm-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                <img src="../images/services/diseno-accesible.png" class="img-responsive" alt="Accessible Designs">
            </div>
        </div>



    </div>
</div>
</section>
<!--/#features-->

<!-- servicios -->
<section id="involucrate">
    <div class="container">
        <div class="row">
            <div tabindex="20">
                <h1 class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Get Involved!</h1>
                <p class="text-center wow fadeInDown" data-wow-duration="400ms" data-wow-delay="400ms">Comparlante is a collective project encouraged and held thanks to people who want to live in a more accessible world innovating and being part of the change.</p>
            </div>
            <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
                <div tabindex="21" class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
                        <img src="../images/services/unete.png" alt="Join Us!">
                    </div>
                    <h2>Join Us!</h2>
                    <p> Do you want to join the team? </p>
                    <p>Do you think you have some skill to provide some of our services?</p>
                    <p> <a tabindex="22" type="button" href="contact.php" class="btn btn-info">Contact us</a> </p>
                </div>
            </div>
            <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
                <div  tabindex="24"class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
                        <img src="../images/services/apoyanos.png" alt="Support Our Project">
                    </div>
                    <h2>Support Our Project</h2>
                    <p>With your donation, you help us to build a more accessible world.</p>
                    <a href="donate.php" target="_blank">
                        <img alt="" border="0" src="../images/services/donar.jpg"  >
                    </a> 
                </div>
            </div>
            <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms">
                <div tabindex="27" class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                        <img src="../images/services/idea.png" alt="">
                    </div>
                    <h2>Do you have an idea?</h2>
                    <p>If you have a proposal that can give accessibility to disabled people, and you would like to have a team that supports your idea, here we are! </p> <p><a tabindex="28" type="button" href="contact.php" class="btn btn-info">Contact us </a> </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#services--> 

<!-- TEAM -->
<section id="equipo">
    <div class="container">
        <div class="row">
            <div tabindex="30">
                <h1 class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Meet the team!</h1>
                <p class="text-center wow fadeInDown" data-wow-duration="400ms" data-wow-delay="400ms">Our team is made up of many professionals from different fields who share the same aim: to work every single day for a more accessible world. </p> 
            </div>
            <div id="team-carousel" class="carousel slide wow fadeIn" data-ride="carousel" data-wow-duration="400ms" data-wow-delay="400ms">
                <!-- Indicators -->
                <ol class="carousel-indicators visible-xs">
                    <li data-target="#team-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#team-carousel" data-slide-to="1"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div tabindex="31" class="col-sm-3 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="../images/team/lorena-julio.jpg" class="img-responsive" alt="Lorena Julio, Co-founder">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Lorena Julio</h2>
                                    <p>Co-founder</p>
                                    <p>Argentina</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="32" class="col-sm-3 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="../images/team/sebastian-flores.jpg" class="img-responsive" alt="Sebastian Flores, Co-founder and Accessibility Consultant">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Sebastian Flores</h2>
                                    <p>Co-founder and Accessibility Consultant</p>
                                    <p>Ecuador</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="33" class="col-sm-3 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="../images/team/ana-maidana.jpg" class="img-responsive" alt="Ana Maidana, Administrative area">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Ana Maidana</h2>
                                    <p>Administrative area</p>
                                    <p>Argentina</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="34" class="col-sm-3 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="../images/team/guido-munoz.jpg" class="img-responsive" alt="Guido Muñoz, Finance Department ">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Guido Muñoz</h2>
                                    <p>Finance Department </p>
                                    <p>Argentina</p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- segundo item -->
                    <div class="item">
                        <div tabindex="35" class="col-sm-3 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="../images/team/jose-clautier.jpg" class="img-responsive" alt="Jose Clautier, Institutional Relationships">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Jose Clautier</h2>
                                    <p>Institutional Relationships</p>
                                    <p>El Salvador</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="36" class="col-sm-3 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="../images/team/beatriz-calvo.jpg" class="img-responsive" alt="Beatriz Calvo, Design and Illustrations">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Beatriz Calvo</h2>
                                    <p>Design and Illustrations</p>
                                    <p>Costa Rica</p>
                                </div>
                            </div>
                        </div>


                        <div tabindex="37" class="col-sm-3 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="../images/team/roberto-jaramillo.jpg" class="img-responsive" alt="Roberto Jaramillo, Entrepreneurship Consulting ">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Roberto Jaramillo</h2>
                                    <p>Entrepreneurship Consulting</p>
                                    <p>Ecuador</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="38" class="col-sm-3 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="../images/team/pablo-ojeda.jpg" class="img-responsive" alt="Pablo Ojeda, Software engineer">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Pablo Ojeda</h2>
                                    <p>Software engineer </p>
                                    <p>Chile</p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <!-- Controls -->
                <a aria-hidden="true" class="left team-carousel-control hidden-xs" href="#team-carousel" data-slide="prev">left</a>
                <a aria-hidden="true" class="right team-carousel-control hidden-xs" href="#team-carousel" data-slide="next">right</a>
            </div>
        </div>
    </div>
</section>
<!--/#team-->

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center ">
                <img src="../images/home/under.png" class="img-responsive inline" alt="">
            </div>

            <div class="col-sm-2" style="text-align: center;"> 
                <a href="index_en.php" tabindex="85">HOME</a>
            </div>
            <div class="col-sm-2" style="text-align: center;"> 
                <a href="index_es.php#quienes-somos" tabindex="86">WHO WE ARE</a>
            </div>
            <div class="col-sm-2" style="text-align: center;"> 
                <a href="index_en.php#servicios" tabindex="87">SERVICES OFFERED</a>
            </div>
            <div class="col-sm-2" style="text-align: center;"> 
               <a href="index_en.php#involucrate" tabindex="88">GET INVOLVED</a>
           </div>
           <div class="col-sm-2" style="text-align: center;"> 
            <a href="index_en.php#equipo" tabindex="89">TEAM</a>
        </div>
        <div class="col-sm-2" style="text-align: center;"> 
            <a href="contacto_en.php" tabindex="90">CONTACT</a>
        </div>

        <div class="col-sm-12">
            <div class="copyright-text text-center">
                <p>&copy; Fundación Comparlante 2016.</p>
                <p>Developed by <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
            </div>
        </div>
    </div>
</div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/lightbox.min.js"></script>
<script type="text/javascript" src="../js/wow.min.js"></script>
<script type="text/javascript" src="../js/main_en.js"></script>   
</body>


</html>
