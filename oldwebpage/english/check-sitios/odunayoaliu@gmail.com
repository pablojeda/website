<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE resultset[
<!ELEMENT resultset (summary,results)>
<!ELEMENT summary (status,sessionID,NumOfErrors,NumOfLikelyProblems,NumOfPotentialProblems,guidelines)>
<!ELEMENT status (#PCDATA)>
<!ELEMENT sessionID (#PCDATA)>
<!ELEMENT NumOfErrors (#PCDATA)>
<!ELEMENT NumOfLikelyProblems (#PCDATA)>
<!ELEMENT NumOfPotentialProblems (#PCDATA)>
<!ELEMENT guidelines (guideline)*>
<!ELEMENT guideline (#PCDATA)>
<!ELEMENT results (result)*>
<!ELEMENT result (resultType,lineNum,columnNum,errorMsg,errorSourceCode,repair*,sequenceID*,decisionPass*,decisionFail*,decisionMade*,decisionMadeDate*)>
<!ELEMENT resultType (#PCDATA)>
<!ELEMENT lineNum (#PCDATA)>
<!ELEMENT columnNum (#PCDATA)>
<!ELEMENT errorMsg (#PCDATA)>
<!ELEMENT errorSourceCode (#PCDATA)>
<!ELEMENT repair (#PCDATA)>
<!ELEMENT sequenceID (#PCDATA)>
<!ELEMENT decisionPass (#PCDATA)>
<!ELEMENT decisionFail (#PCDATA)>
<!ELEMENT decisionMade (#PCDATA)>
<!ELEMENT decisionMadeDate (#PCDATA)>
<!ENTITY lt "&#38;#60;">
<!ENTITY gt "&#62;">
<!ENTITY amp "&#38;#38;">
<!ENTITY apos "&#39;">
<!ENTITY quot "&#34;">
<!ENTITY ndash "&#8211;">
]>
<resultset>
  <summary>
    <status>FAIL</status>
    <sessionID>88c2595bfe7062edcb0600f65892a482069e7755</sessionID>
    <NumOfErrors>42</NumOfErrors>
    <NumOfLikelyProblems>16</NumOfLikelyProblems>
    <NumOfPotentialProblems>168</NumOfPotentialProblems>

    <guidelines>
      <guideline>Stanca Act</guideline>
      <guideline>WCAG 2.0 (Level AA)</guideline>

    </guidelines>
  </summary>

  <results>
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>9</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;application/ld+json&#039;&gt;{&quot;@context&quot;:&quot;http:\/\/schema.org&quot;,&quot;@type&quot;:&quot;WebSite&quot;,&quot;@id&quot;:&quot;#websi ...</errorSourceCode>
        
        <sequenceID>9_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>9</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;application/ld+json&#039;&gt;{&quot;@context&quot;:&quot;http:\/\/schema.org&quot;,&quot;@type&quot;:&quot;WebSite&quot;,&quot;@id&quot;:&quot;#websi ...</errorSourceCode>
        
        <sequenceID>9_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>9</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;application/ld+json&#039;&gt;{&quot;@context&quot;:&quot;http:\/\/schema.org&quot;,&quot;@type&quot;:&quot;WebSite&quot;,&quot;@id&quot;:&quot;#websi ...</errorSourceCode>
        
        <sequenceID>9_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>9</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;application/ld+json&#039;&gt;{&quot;@context&quot;:&quot;http:\/\/schema.org&quot;,&quot;@type&quot;:&quot;WebSite&quot;,&quot;@id&quot;:&quot;#websi ...</errorSourceCode>
        
        <sequenceID>9_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>9</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;application/ld+json&#039;&gt;{&quot;@context&quot;:&quot;http:\/\/schema.org&quot;,&quot;@type&quot;:&quot;WebSite&quot;,&quot;@id&quot;:&quot;#websi ...</errorSourceCode>
        
        <sequenceID>9_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>16</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
			window._wpemojiSettings = {&quot;baseUrl&quot;:&quot;https:\/\/s.w.org\/images\/ ...</errorSourceCode>
        
        <sequenceID>16_3_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>16</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
			window._wpemojiSettings = {&quot;baseUrl&quot;:&quot;https:\/\/s.w.org\/images\/ ...</errorSourceCode>
        
        <sequenceID>16_3_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>16</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
			window._wpemojiSettings = {&quot;baseUrl&quot;:&quot;https:\/\/s.w.org\/images\/ ...</errorSourceCode>
        
        <sequenceID>16_3_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>16</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
			window._wpemojiSettings = {&quot;baseUrl&quot;:&quot;https:\/\/s.w.org\/images\/ ...</errorSourceCode>
        
        <sequenceID>16_3_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>16</lineNum>
      <columnNum>3</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
			window._wpemojiSettings = {&quot;baseUrl&quot;:&quot;https:\/\/s.w.org\/images\/ ...</errorSourceCode>
        
        <sequenceID>16_3_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>53</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
      if (document.location.protocol != &quot;https:&quot;) {
          document.location = document. ...</errorSourceCode>
        
        <sequenceID>53_7_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>53</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
      if (document.location.protocol != &quot;https:&quot;) {
          document.location = document. ...</errorSourceCode>
        
        <sequenceID>53_7_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>53</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
      if (document.location.protocol != &quot;https:&quot;) {
          document.location = document. ...</errorSourceCode>
        
        <sequenceID>53_7_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>53</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
      if (document.location.protocol != &quot;https:&quot;) {
          document.location = document. ...</errorSourceCode>
        
        <sequenceID>53_7_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>53</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script&gt;
      if (document.location.protocol != &quot;https:&quot;) {
          document.location = document. ...</errorSourceCode>
        
        <sequenceID>53_7_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>58</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-includes/js/jquery/jquery.js?ver=1 ...</errorSourceCode>
        
        <sequenceID>58_7_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>58</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-includes/js/jquery/jquery.js?ver=1 ...</errorSourceCode>
        
        <sequenceID>58_7_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>58</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-includes/js/jquery/jquery.js?ver=1 ...</errorSourceCode>
        
        <sequenceID>58_7_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>58</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-includes/js/jquery/jquery.js?ver=1 ...</errorSourceCode>
        
        <sequenceID>58_7_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>58</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-includes/js/jquery/jquery.js?ver=1 ...</errorSourceCode>
        
        <sequenceID>58_7_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>59</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-includes/js/jquery/jquery-migrate. ...</errorSourceCode>
        
        <sequenceID>59_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>59</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-includes/js/jquery/jquery-migrate. ...</errorSourceCode>
        
        <sequenceID>59_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>59</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-includes/js/jquery/jquery-migrate. ...</errorSourceCode>
        
        <sequenceID>59_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>59</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-includes/js/jquery/jquery-migrate. ...</errorSourceCode>
        
        <sequenceID>59_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>59</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-includes/js/jquery/jquery-migrate. ...</errorSourceCode>
        
        <sequenceID>59_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>60</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/modula-best-grid-g ...</errorSourceCode>
        
        <sequenceID>60_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>60</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/modula-best-grid-g ...</errorSourceCode>
        
        <sequenceID>60_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>60</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/modula-best-grid-g ...</errorSourceCode>
        
        <sequenceID>60_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>60</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/modula-best-grid-g ...</errorSourceCode>
        
        <sequenceID>60_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>60</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/modula-best-grid-g ...</errorSourceCode>
        
        <sequenceID>60_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>61</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/so-widgets-bundle/ ...</errorSourceCode>
        
        <sequenceID>61_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>61</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/so-widgets-bundle/ ...</errorSourceCode>
        
        <sequenceID>61_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>61</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/so-widgets-bundle/ ...</errorSourceCode>
        
        <sequenceID>61_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>61</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/so-widgets-bundle/ ...</errorSourceCode>
        
        <sequenceID>61_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>61</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/so-widgets-bundle/ ...</errorSourceCode>
        
        <sequenceID>61_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>62</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/so-widgets-bundle/ ...</errorSourceCode>
        
        <sequenceID>62_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>62</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/so-widgets-bundle/ ...</errorSourceCode>
        
        <sequenceID>62_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>62</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/so-widgets-bundle/ ...</errorSourceCode>
        
        <sequenceID>62_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>62</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/so-widgets-bundle/ ...</errorSourceCode>
        
        <sequenceID>62_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>62</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/so-widgets-bundle/ ...</errorSourceCode>
        
        <sequenceID>62_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>63</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/livemesh-siteorigi ...</errorSourceCode>
        
        <sequenceID>63_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>63</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/livemesh-siteorigi ...</errorSourceCode>
        
        <sequenceID>63_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>63</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/livemesh-siteorigi ...</errorSourceCode>
        
        <sequenceID>63_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>63</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/livemesh-siteorigi ...</errorSourceCode>
        
        <sequenceID>63_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>63</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/livemesh-siteorigi ...</errorSourceCode>
        
        <sequenceID>63_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>64</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/ml-slider/assets/s ...</errorSourceCode>
        
        <sequenceID>64_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>64</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/ml-slider/assets/s ...</errorSourceCode>
        
        <sequenceID>64_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>64</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/ml-slider/assets/s ...</errorSourceCode>
        
        <sequenceID>64_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>64</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/ml-slider/assets/s ...</errorSourceCode>
        
        <sequenceID>64_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>64</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039; src=&#039;https://loveletterscsi.org/wp-content/plugins/ml-slider/assets/s ...</errorSourceCode>
        
        <sequenceID>64_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>65</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
var metaslider_465 = function($) {$(&#039;#metaslider_465&#039;).addClass(&#039;fle ...</errorSourceCode>
        
        <sequenceID>65_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>65</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
var metaslider_465 = function($) {$(&#039;#metaslider_465&#039;).addClass(&#039;fle ...</errorSourceCode>
        
        <sequenceID>65_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>65</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
var metaslider_465 = function($) {$(&#039;#metaslider_465&#039;).addClass(&#039;fle ...</errorSourceCode>
        
        <sequenceID>65_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>65</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
var metaslider_465 = function($) {$(&#039;#metaslider_465&#039;).addClass(&#039;fle ...</errorSourceCode>
        
        <sequenceID>65_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>65</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&#039;text/javascript&#039;&gt;
var metaslider_465 = function($) {$(&#039;#metaslider_465&#039;).addClass(&#039;fle ...</errorSourceCode>
        
        <sequenceID>65_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>118</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=87&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=87'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may cause screen flicker.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|Word ...</errorSourceCode>
        
        <sequenceID>118_1_87</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not cause screen flicker.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; causes screen flicker.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>118</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=88&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=88'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content may not be accessible when &lt;code&gt;script&lt;/code&gt; is disabled.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|Word ...</errorSourceCode>
        
        <sequenceID>118_1_88</sequenceID>
        <decisionPass>Content is accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionPass>
        <decisionFail>Content is not accessible when &lt;code&gt;script&lt;/code&gt; is disabled.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>118</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=86&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=86'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; may use color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|Word ...</errorSourceCode>
        
        <sequenceID>118_1_86</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>118</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=94&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=94'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Content possibly not readable when stylesheets are removed (SCRIPT).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|Word ...</errorSourceCode>
        
        <sequenceID>118_1_94</sequenceID>
        <decisionPass>Page is readable when stylesheets are removed.</decisionPass>
        <decisionFail>Page is not readable when stylesheets are removed.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>118</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=89&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=89'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;script&lt;/code&gt; user interface may not be accessible.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;script type=&quot;text/javascript&quot;&gt;
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|Word ...</errorSourceCode>
        
        <sequenceID>118_1_89</sequenceID>
        <decisionPass>&lt;code&gt;script&lt;/code&gt; user interface is accessible.</decisionPass>
        <decisionFail>&lt;code&gt;script&lt;/code&gt; user interface is not accessible.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>174</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=248&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=248'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Visual lists may not be properly marked.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body data-rsssl=1 class=&quot;home page-template-default page page-id-241 wp-custom-logo siteorigin-pane ...</errorSourceCode>
        
        <sequenceID>174_1_248</sequenceID>
        <decisionPass>All visual lists contain proper markup.</decisionPass>
        <decisionFail>Not all visual lists contain proper markup.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>174</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=262&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=262'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Groups of links with a related purpose are not marked.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body data-rsssl=1 class=&quot;home page-template-default page page-id-241 wp-custom-logo siteorigin-pane ...</errorSourceCode>
        
        <sequenceID>174_1_262</sequenceID>
        <decisionPass>All groups of links with a related purpose are marked.</decisionPass>
        <decisionFail>All groups of links with a related purpose are not marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>174</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=184&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=184'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Site missing site map.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body data-rsssl=1 class=&quot;home page-template-default page page-id-241 wp-custom-logo siteorigin-pane ...</errorSourceCode>
        
        <sequenceID>174_1_184</sequenceID>
        <decisionPass>Page is not part of a collection that requires a site map.</decisionPass>
        <decisionFail>Page is part of a collection that requires a site map.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>174</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=28&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=28'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Document may be missing a &quot;skip to content&quot; link.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body data-rsssl=1 class=&quot;home page-template-default page page-id-241 wp-custom-logo siteorigin-pane ...</errorSourceCode>
        
        <sequenceID>174_1_28</sequenceID>
        <decisionPass>Document contians a &quot;skip to content&quot; link or does not require it.</decisionPass>
        <decisionFail>Document does not contain a &quot;skip to content&quot; link and requires it.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>174</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=241&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=241'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Tabular information may be missing table markup.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body data-rsssl=1 class=&quot;home page-template-default page page-id-241 wp-custom-logo siteorigin-pane ...</errorSourceCode>
        
        <sequenceID>174_1_241</sequenceID>
        <decisionPass>Table markup is used for all tabular information.</decisionPass>
        <decisionFail>Table markup is not used for all tabular information.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>174</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=271&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=271'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;dir&lt;/code&gt; attribute may be required to identify changes in text direction.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body data-rsssl=1 class=&quot;home page-template-default page page-id-241 wp-custom-logo siteorigin-pane ...</errorSourceCode>
        
        <sequenceID>174_1_271</sequenceID>
        <decisionPass>All changes in text direction are marked using the &lt;code&gt;dir&lt;/code&gt; attribute.</decisionPass>
        <decisionFail>All changes in text direction are not marked using the &lt;code&gt;dir&lt;/code&gt; attribute.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>174</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=131&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=131'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Long quotations may not be marked using the &lt;code&gt;blockquote&lt;/code&gt; element.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body data-rsssl=1 class=&quot;home page-template-default page page-id-241 wp-custom-logo siteorigin-pane ...</errorSourceCode>
        
        <sequenceID>174_1_131</sequenceID>
        <decisionPass>All long quotations have been marked with the &lt;code&gt;blockquote&lt;/code&gt; element.</decisionPass>
        <decisionFail>All long quotations are not marked with the &lt;code&gt;blockquote&lt;/code&gt; element.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>174</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=276&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=276'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Repeated components may not appear in the same relative order each time they appear.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body data-rsssl=1 class=&quot;home page-template-default page page-id-241 wp-custom-logo siteorigin-pane ...</errorSourceCode>
        
        <sequenceID>174_1_276</sequenceID>
        <decisionPass>Repeated components appear in the same relative order on this page.</decisionPass>
        <decisionFail>Repeated components do not appear in the same relative order on this page.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>174</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=110&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=110'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Words or phrases that are not in the document's primary language may not be identified.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body data-rsssl=1 class=&quot;home page-template-default page page-id-241 wp-custom-logo siteorigin-pane ...</errorSourceCode>
        
        <sequenceID>174_1_110</sequenceID>
        <decisionPass>All words outside the primary language are identified.</decisionPass>
        <decisionFail>Words outside the primary language are not identified.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>174</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=270&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=270'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Unicode right-to-left marks or left-to-right marks may be required.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body data-rsssl=1 class=&quot;home page-template-default page page-id-241 wp-custom-logo siteorigin-pane ...</errorSourceCode>
        
        <sequenceID>174_1_270</sequenceID>
        <decisionPass>Unicode right-to-left marks or left-to-right marks are used whenever the HTML bidirectional algorithm produces undesirable results.</decisionPass>
        <decisionFail>Unicode right-to-left marks or left-to-right marks are not used whenever the HTML bidirectional algorithm produces undesirable results.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>174</lineNum>
      <columnNum>1</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=250&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=250'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Text may refer to items by shape, size, or relative position alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;body data-rsssl=1 class=&quot;home page-template-default page page-id-241 wp-custom-logo siteorigin-pane ...</errorSourceCode>
        
        <sequenceID>174_1_250</sequenceID>
        <decisionPass>There are no references to items in the document by shape, size, or relative position alone.</decisionPass>
        <decisionFail>There are references to items in the document by shape, size, or relative position alone.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>176</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;skip-link screen-reader-text&quot; href=&quot;#content&quot;&gt;Skip to content&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>176</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;skip-link screen-reader-text&quot; href=&quot;#content&quot;&gt;Skip to content&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>176_2_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>176</lineNum>
      <columnNum>2</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a class=&quot;skip-link screen-reader-text&quot; href=&quot;#content&quot;&gt;Skip to content&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>176_2_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>185</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/&quot; class=&quot;custom-logo-link&quot; rel=&quot;home&quot; itemprop=&quot;url&quot;&gt;&lt;img width= ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>185</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/&quot; class=&quot;custom-logo-link&quot; rel=&quot;home&quot; itemprop=&quot;url&quot;&gt;&lt;img width= ...</errorSourceCode>
        
        <sequenceID>185_7_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>185</lineNum>
      <columnNum>7</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/&quot; class=&quot;custom-logo-link&quot; rel=&quot;home&quot; itemprop=&quot;url&quot;&gt;&lt;img width= ...</errorSourceCode>
        
        <sequenceID>185_7_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>185</lineNum>
      <columnNum>96</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=16&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=16'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text is not empty and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;100&quot; height=&quot;52&quot; src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/06/love-letter2 ...</errorSourceCode>
        
        <sequenceID>185_96_16</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>185</lineNum>
      <columnNum>96</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;100&quot; height=&quot;52&quot; src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/06/love-letter2 ...</errorSourceCode>
        
        <sequenceID>185_96_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>185</lineNum>
      <columnNum>96</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;100&quot; height=&quot;52&quot; src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/06/love-letter2 ...</errorSourceCode>
        
        <sequenceID>185_96_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>185</lineNum>
      <columnNum>96</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;100&quot; height=&quot;52&quot; src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/06/love-letter2 ...</errorSourceCode>
        
        <sequenceID>185_96_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>189</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-facebook&quot;&gt;&lt;/i&gt;&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>189</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-facebook&quot;&gt;&lt;/i&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>189_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>189</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-facebook&quot;&gt;&lt;/i&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>189_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>189</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;fa fa-facebook&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>190</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-twitter&quot;&gt;&lt;/i&gt;&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>190</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-twitter&quot;&gt;&lt;/i&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>190_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>190</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-twitter&quot;&gt;&lt;/i&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>190_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>190</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;fa fa-twitter&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>191</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-envelope&quot;&gt;&lt;/i&gt;&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>191</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-envelope&quot;&gt;&lt;/i&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>191_5_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>191</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-envelope&quot;&gt;&lt;/i&gt;&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>191_5_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>191</lineNum>
      <columnNum>17</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;fa fa-envelope&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>198</lineNum>
      <columnNum>78</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;fa fa-align-justify fa-2x&quot; aria-hidden=&quot;true&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>199</lineNum>
      <columnNum>255</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/&quot;&gt;Home&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>199</lineNum>
      <columnNum>255</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/&quot;&gt;Home&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>199_255_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>199</lineNum>
      <columnNum>255</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/&quot;&gt;Home&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>199_255_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>200</lineNum>
      <columnNum>126</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/&quot;&gt;ABOUT&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>200</lineNum>
      <columnNum>126</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/&quot;&gt;ABOUT&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>200_126_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>200</lineNum>
      <columnNum>126</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/&quot;&gt;ABOUT&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>200_126_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>202</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/founder/&quot;&gt;Our founder&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>202</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/founder/&quot;&gt;Our founder&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>202_104_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>202</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/founder/&quot;&gt;Our founder&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>202_104_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>203</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/our-mission-and-vision/&quot;&gt;Our Mission and Vision&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>203</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/our-mission-and-vision/&quot;&gt;Our Mission and Vision&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>203_104_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>203</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/our-mission-and-vision/&quot;&gt;Our Mission and Vision&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>203_104_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>204</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/team/&quot;&gt;Team&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>204</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/team/&quot;&gt;Team&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>204_104_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>204</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/team/&quot;&gt;Team&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>204_104_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>205</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/advisory-board/&quot;&gt;Advisory Board&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>205</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/advisory-board/&quot;&gt;Advisory Board&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>205_104_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>205</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/advisory-board/&quot;&gt;Advisory Board&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>205_104_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>206</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/partners/&quot;&gt;Partners&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>206</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/partners/&quot;&gt;Partners&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>206_104_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>206</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/partners/&quot;&gt;Partners&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>206_104_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>207</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/contact-us/&quot;&gt;Contact us&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>207</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/contact-us/&quot;&gt;Contact us&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>207_104_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>207</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/contact-us/&quot;&gt;Contact us&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>207_104_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>210</lineNum>
      <columnNum>126</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/&quot;&gt;WHAT WE DO&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>210</lineNum>
      <columnNum>126</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/&quot;&gt;WHAT WE DO&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>210_126_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>210</lineNum>
      <columnNum>126</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/&quot;&gt;WHAT WE DO&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>210_126_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>212</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/partners/&quot;&gt;Bramble&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>212</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/partners/&quot;&gt;Bramble&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>212_104_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>212</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/partners/&quot;&gt;Bramble&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>212_104_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>213</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/jeans-for-love/&quot;&gt;Jeans for Love&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>213</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/jeans-for-love/&quot;&gt;Jeans for Love&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>213_104_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>213</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/jeans-for-love/&quot;&gt;Jeans for Love&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>213_104_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>214</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/medical-outreach/&quot;&gt;Medical Outreach&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>214</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/medical-outreach/&quot;&gt;Medical Outreach&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>214_104_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>214</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/medical-outreach/&quot;&gt;Medical Outreach&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>214_104_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>215</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/scholarship-program/&quot;&gt;Scholarship program&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>215</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/scholarship-program/&quot;&gt;Scholarship program&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>215_104_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>215</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/scholarship-program/&quot;&gt;Scholarship program&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>215_104_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>216</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/street-children/&quot;&gt;Street Children&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>216</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/street-children/&quot;&gt;Street Children&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>216_104_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>216</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/street-children/&quot;&gt;Street Children&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>216_104_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>217</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/advocacy/&quot;&gt;Advocacy&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>217</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/advocacy/&quot;&gt;Advocacy&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>217_104_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>217</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/advocacy/&quot;&gt;Advocacy&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>217_104_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>218</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/research/&quot;&gt;Research&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>218</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/research/&quot;&gt;Research&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>218_104_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>218</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/what-we-do/research/&quot;&gt;Research&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>218_104_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>221</lineNum>
      <columnNum>126</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/founder/&quot;&gt;GET INVOLVED&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>221</lineNum>
      <columnNum>126</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/founder/&quot;&gt;GET INVOLVED&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>221_126_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>221</lineNum>
      <columnNum>126</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/founder/&quot;&gt;GET INVOLVED&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>221_126_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>223</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/get-involved/membership/&quot;&gt;Membership&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>223</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/get-involved/membership/&quot;&gt;Membership&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>223_104_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>223</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/get-involved/membership/&quot;&gt;Membership&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>223_104_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>224</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/get-involved/volunteer/&quot;&gt;Volunteer&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>224</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/get-involved/volunteer/&quot;&gt;Volunteer&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>224_104_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>224</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/get-involved/volunteer/&quot;&gt;Volunteer&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>224_104_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>225</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/team/&quot;&gt;Donate&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>225</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/team/&quot;&gt;Donate&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>225_104_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>225</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/about/team/&quot;&gt;Donate&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>225_104_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>226</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/get-involved/media-download/&quot;&gt;Media download&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>226</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/get-involved/media-download/&quot;&gt;Media download&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>226_104_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>226</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/get-involved/media-download/&quot;&gt;Media download&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>226_104_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>227</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/get-involved/faq/&quot;&gt;FAQ&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>227</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/get-involved/faq/&quot;&gt;FAQ&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>227_104_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>227</lineNum>
      <columnNum>104</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/get-involved/faq/&quot;&gt;FAQ&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>227_104_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>230</lineNum>
      <columnNum>103</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/blog/&quot;&gt;BLOG&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>230</lineNum>
      <columnNum>103</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/blog/&quot;&gt;BLOG&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>230_103_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>230</lineNum>
      <columnNum>103</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/blog/&quot;&gt;BLOG&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>230_103_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>235</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=272&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=272'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form may delete information without allowing for recovery.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form role=&quot;search&quot; method=&quot;get&quot; class=&quot;search-form&quot; action=&quot;https://loveletterscsi.org/&quot;&gt;
				&lt;labe ...</errorSourceCode>
        
        <sequenceID>235_9_272</sequenceID>
        <decisionPass>Information deleted using this form can be recovered.</decisionPass>
        <decisionFail>Information delted using this form can not be recovered.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>235</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=246&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=246'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;All required &lt;code&gt;form&lt;/code&gt; fields may not be indicated as required.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form role=&quot;search&quot; method=&quot;get&quot; class=&quot;search-form&quot; action=&quot;https://loveletterscsi.org/&quot;&gt;
				&lt;labe ...</errorSourceCode>
        
        <sequenceID>235_9_246</sequenceID>
        <decisionPass>All required fields are indicated to the user as required.</decisionPass>
        <decisionFail>All required fields are not indicated to the user as required.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>235</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=269&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=269'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission data may not be presented to the user before final acceptance of an irreversable transaction.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form role=&quot;search&quot; method=&quot;get&quot; class=&quot;search-form&quot; action=&quot;https://loveletterscsi.org/&quot;&gt;
				&lt;labe ...</errorSourceCode>
        
        <sequenceID>235_9_269</sequenceID>
        <decisionPass>All form submission data is presented to the user before final acceptance for all irreversable transactions.</decisionPass>
        <decisionFail>All form submission data is not presented to the user before final acceptance for all irreversable transactions.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>235</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=265&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=265'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Tab order may not follow logical order.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form role=&quot;search&quot; method=&quot;get&quot; class=&quot;search-form&quot; action=&quot;https://loveletterscsi.org/&quot;&gt;
				&lt;labe ...</errorSourceCode>
        
        <sequenceID>235_9_265</sequenceID>
        <decisionPass>Tab order follows a logical order.</decisionPass>
        <decisionFail>Tab order does not follow a logical order.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>235</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=268&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=268'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission error messages may not provide assistance.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form role=&quot;search&quot; method=&quot;get&quot; class=&quot;search-form&quot; action=&quot;https://loveletterscsi.org/&quot;&gt;
				&lt;labe ...</errorSourceCode>
        
        <sequenceID>235_9_268</sequenceID>
        <decisionPass>All form submission error messages provide assistance in correcting the error.</decisionPass>
        <decisionFail>All form submission error messages do not provide assistance in correcting the errors.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>235</lineNum>
      <columnNum>9</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=267&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=267'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Form submission error messages may not identify empty required fields.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;form role=&quot;search&quot; method=&quot;get&quot; class=&quot;search-form&quot; action=&quot;https://loveletterscsi.org/&quot;&gt;
				&lt;labe ...</errorSourceCode>
        
        <sequenceID>235_9_267</sequenceID>
        <decisionPass>Required fields are identified in all form submission error messages.</decisionPass>
        <decisionFail>Required fields are not identified in all form submission error messages.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>238</lineNum>
      <columnNum>6</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=188&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=188'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Label text is empty.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;search&quot; class=&quot;search-field&quot; placeholder=&quot;Search &amp;hellip;&quot; value=&quot;&quot; name=&quot;s&quot; /&gt;</errorSourceCode>
        <repair>Add text to the &lt;code&gt;label&lt;/code&gt; element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>238</lineNum>
      <columnNum>6</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=55&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=55'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;input&lt;/code&gt; possibly using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;search&quot; class=&quot;search-field&quot; placeholder=&quot;Search &amp;hellip;&quot; value=&quot;&quot; name=&quot;s&quot; /&gt;</errorSourceCode>
        
        <sequenceID>238_6_55</sequenceID>
        <decisionPass>&lt;code&gt;input&lt;/code&gt; element does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;input&lt;/code&gt; element uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>240</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=55&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=55'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;input&lt;/code&gt; possibly using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;submit&quot; class=&quot;search-submit&quot; value=&quot;Search&quot; /&gt;</errorSourceCode>
        
        <sequenceID>240_5_55</sequenceID>
        <decisionPass>&lt;code&gt;input&lt;/code&gt; element does not use color alone.</decisionPass>
        <decisionFail>&lt;code&gt;input&lt;/code&gt; element uses color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>240</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=189&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=189'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;label&lt;/code&gt; may not describe its associated control.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;input type=&quot;submit&quot; class=&quot;search-submit&quot; value=&quot;Search&quot; /&gt;</errorSourceCode>
        
        <sequenceID>240_5_189</sequenceID>
        <decisionPass>&lt;code&gt;label&lt;/code&gt; describes its associated control.</decisionPass>
        <decisionFail>&lt;code&gt;label&lt;/code&gt; does not describe its associated control.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>260</lineNum>
      <columnNum>69</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=4&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=4'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image has suspicious Alt text (empty string &quot;&quot;).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/dfhdsfhdhf-1170x360.jpg&quot; height=&quot;360 ...</errorSourceCode>
        
        <sequenceID>260_69_4</sequenceID>
        <decisionPass>Image is decorative (null Alt text is OK).</decisionPass>
        <decisionFail>Image is not decorative (null Alt text is not OK).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>260</lineNum>
      <columnNum>69</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/dfhdsfhdhf-1170x360.jpg&quot; height=&quot;360 ...</errorSourceCode>
        
        <sequenceID>260_69_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>260</lineNum>
      <columnNum>69</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/dfhdsfhdhf-1170x360.jpg&quot; height=&quot;360 ...</errorSourceCode>
        
        <sequenceID>260_69_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>260</lineNum>
      <columnNum>69</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=178&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=178'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text does not convey the same information as the image.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/dfhdsfhdhf-1170x360.jpg&quot; height=&quot;360 ...</errorSourceCode>
        
        <sequenceID>260_69_178</sequenceID>
        <decisionPass>Alt text conveys the same information as the image.</decisionPass>
        <decisionFail>Alt text does not convey the same information as the image.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>260</lineNum>
      <columnNum>69</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=239&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=239'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; has &lt;code&gt;title&lt;/code&gt; attribute and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/dfhdsfhdhf-1170x360.jpg&quot; height=&quot;360 ...</errorSourceCode>
        
        <sequenceID>260_69_239</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>260</lineNum>
      <columnNum>69</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/dfhdsfhdhf-1170x360.jpg&quot; height=&quot;360 ...</errorSourceCode>
        
        <sequenceID>260_69_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>260</lineNum>
      <columnNum>69</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/dfhdsfhdhf-1170x360.jpg&quot; height=&quot;360 ...</errorSourceCode>
        
        <sequenceID>260_69_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>265</lineNum>
      <columnNum>68</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=4&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=4'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image has suspicious Alt text (empty string &quot;&quot;).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/IMG_20170809_140418-1170x360.jpg&quot; he ...</errorSourceCode>
        
        <sequenceID>265_68_4</sequenceID>
        <decisionPass>Image is decorative (null Alt text is OK).</decisionPass>
        <decisionFail>Image is not decorative (null Alt text is not OK).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>265</lineNum>
      <columnNum>68</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/IMG_20170809_140418-1170x360.jpg&quot; he ...</errorSourceCode>
        
        <sequenceID>265_68_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>265</lineNum>
      <columnNum>68</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/IMG_20170809_140418-1170x360.jpg&quot; he ...</errorSourceCode>
        
        <sequenceID>265_68_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>265</lineNum>
      <columnNum>68</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=178&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=178'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text does not convey the same information as the image.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/IMG_20170809_140418-1170x360.jpg&quot; he ...</errorSourceCode>
        
        <sequenceID>265_68_178</sequenceID>
        <decisionPass>Alt text conveys the same information as the image.</decisionPass>
        <decisionFail>Alt text does not convey the same information as the image.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>265</lineNum>
      <columnNum>68</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=239&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=239'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; has &lt;code&gt;title&lt;/code&gt; attribute and image may be decorative.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/IMG_20170809_140418-1170x360.jpg&quot; he ...</errorSourceCode>
        
        <sequenceID>265_68_239</sequenceID>
        <decisionPass>Image is not decorative.</decisionPass>
        <decisionFail>Image is decorative.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>265</lineNum>
      <columnNum>68</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/IMG_20170809_140418-1170x360.jpg&quot; he ...</errorSourceCode>
        
        <sequenceID>265_68_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>265</lineNum>
      <columnNum>68</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/IMG_20170809_140418-1170x360.jpg&quot; he ...</errorSourceCode>
        
        <sequenceID>265_68_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>270</lineNum>
      <columnNum>303</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=44&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=44'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h3&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h3&gt;&lt;i class=&quot;fa fa-money&quot; style=&quot;color: red; padding-right:20px; font-size: 40px;&quot;&gt;&lt;/i&gt;Education&lt;/h ...</errorSourceCode>
        
        <sequenceID>270_303_44</sequenceID>
        <decisionPass>This &lt;code&gt;h3&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h3&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>270</lineNum>
      <columnNum>307</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;fa fa-money&quot; style=&quot;color: red; padding-right:20px; font-size: 40px;&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>271</lineNum>
      <columnNum>467</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=44&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=44'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h3&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h3&gt;&lt;i class=&quot;fa fa-heart&quot; style=&quot;padding-right: 30px; color: red;&quot;&gt;&lt;/i&gt;Health&lt;/h3&gt;</errorSourceCode>
        
        <sequenceID>271_467_44</sequenceID>
        <decisionPass>This &lt;code&gt;h3&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h3&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>271</lineNum>
      <columnNum>471</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;fa fa-heart&quot; style=&quot;padding-right: 30px; color: red;&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>274</lineNum>
      <columnNum>246</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=44&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=44'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h3&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h3&gt;&lt;i class=&quot;fa fa-align-right&quot; style=&quot;padding-right: 30px; color: red;&quot;&gt;&lt;/i&gt;Research&lt;/h3&gt;</errorSourceCode>
        
        <sequenceID>274_246_44</sequenceID>
        <decisionPass>This &lt;code&gt;h3&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h3&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>274</lineNum>
      <columnNum>250</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=117&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=117'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;i&lt;/code&gt; (italic) element used.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;i class=&quot;fa fa-align-right&quot; style=&quot;padding-right: 30px; color: red;&quot;&gt;&lt;/i&gt;</errorSourceCode>
        <repair>Replace your &lt;code&gt;i&lt;/code&gt; elements with &lt;code&gt;em&lt;/code&gt; or &lt;code&gt;strong&lt;/code&gt;.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>280</lineNum>
      <columnNum>5</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=43&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=43'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h2&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h2 style=&quot;text-align: center&quot;&gt;OUR STORY&lt;/h2&gt;</errorSourceCode>
        
        <sequenceID>280_5_43</sequenceID>
        <decisionPass>This &lt;code&gt;h2&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h2&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>284</lineNum>
      <columnNum>12</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=81&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=81'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;List item used to format text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;ol class=&quot;sow-slider-pagination&quot;&gt;
											&lt;li&gt;&lt;a href=&quot;#&quot; data-goto=&quot;0&quot;&gt;1&lt;/a&gt;&lt;/li&gt;
									&lt;/ol ...</errorSourceCode>
        
        <sequenceID>284_12_81</sequenceID>
        <decisionPass>List item is used appropriately.</decisionPass>
        <decisionFail>List item is not used appropriately (its used just to format text).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>285</lineNum>
      <columnNum>16</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; data-goto=&quot;0&quot;&gt;1&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>285</lineNum>
      <columnNum>16</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; data-goto=&quot;0&quot;&gt;1&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>285_16_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>285</lineNum>
      <columnNum>16</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; data-goto=&quot;0&quot;&gt;1&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>285_16_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>289</lineNum>
      <columnNum>6</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; data-goto=&quot;next&quot; data-action=&quot;next&quot;&gt;
						&lt;em class=&quot;sow-sld-icon-thin-right&quot;&gt;&lt;/em&gt;
			 ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>289</lineNum>
      <columnNum>6</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; data-goto=&quot;next&quot; data-action=&quot;next&quot;&gt;
						&lt;em class=&quot;sow-sld-icon-thin-right&quot;&gt;&lt;/em&gt;
			 ...</errorSourceCode>
        
        <sequenceID>289_6_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>289</lineNum>
      <columnNum>6</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; data-goto=&quot;next&quot; data-action=&quot;next&quot;&gt;
						&lt;em class=&quot;sow-sld-icon-thin-right&quot;&gt;&lt;/em&gt;
			 ...</errorSourceCode>
        
        <sequenceID>289_6_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>295</lineNum>
      <columnNum>6</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; data-goto=&quot;previous&quot; data-action=&quot;prev&quot;&gt;
						&lt;em class=&quot;sow-sld-icon-thin-left&quot;&gt;&lt;/em&gt;
 ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>295</lineNum>
      <columnNum>6</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; data-goto=&quot;previous&quot; data-action=&quot;prev&quot;&gt;
						&lt;em class=&quot;sow-sld-icon-thin-left&quot;&gt;&lt;/em&gt;
 ...</errorSourceCode>
        
        <sequenceID>295_6_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>295</lineNum>
      <columnNum>6</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;#&quot; data-goto=&quot;previous&quot; data-action=&quot;prev&quot;&gt;
						&lt;em class=&quot;sow-sld-icon-thin-left&quot;&gt;&lt;/em&gt;
 ...</errorSourceCode>
        
        <sequenceID>295_6_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>299</lineNum>
      <columnNum>426</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=44&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=44'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h3&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h3 class=&quot;widget-title&quot;&gt;Latest Update&lt;/h3&gt;</errorSourceCode>
        
        <sequenceID>299_426_44</sequenceID>
        <decisionPass>This &lt;code&gt;h3&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h3&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>312</lineNum>
      <columnNum>33</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/2018/06/another-post/&quot;&gt; &lt;img width=&quot;640&quot; height=&quot;480&quot; src=&quot;https ...</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>312</lineNum>
      <columnNum>33</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/2018/06/another-post/&quot;&gt; &lt;img width=&quot;640&quot; height=&quot;480&quot; src=&quot;https ...</errorSourceCode>
        
        <sequenceID>312_33_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>312</lineNum>
      <columnNum>33</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/2018/06/another-post/&quot;&gt; &lt;img width=&quot;640&quot; height=&quot;480&quot; src=&quot;https ...</errorSourceCode>
        
        <sequenceID>312_33_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Likely Problem</resultType>
      <lineNum>312</lineNum>
      <columnNum>93</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=4&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=4'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image has suspicious Alt text (empty string &quot;&quot;).&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;640&quot; height=&quot;480&quot; src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/IMG_2017080 ...</errorSourceCode>
        
        <sequenceID>312_93_4</sequenceID>
        <decisionPass>Image is decorative (null Alt text is OK).</decisionPass>
        <decisionFail>Image is not decorative (null Alt text is not OK).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>312</lineNum>
      <columnNum>93</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=14&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=14'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may be using color alone.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;640&quot; height=&quot;480&quot; src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/IMG_2017080 ...</errorSourceCode>
        
        <sequenceID>312_93_14</sequenceID>
        <decisionPass>There are no references to this image using color alone.</decisionPass>
        <decisionFail>There is a reference to this image using color alone.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>312</lineNum>
      <columnNum>93</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=8&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=8'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;img&lt;/code&gt; element may require a long description.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;640&quot; height=&quot;480&quot; src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/IMG_2017080 ...</errorSourceCode>
        
        <sequenceID>312_93_8</sequenceID>
        <decisionPass>Image does not require a long description or a long description is provided.</decisionPass>
        <decisionFail>Image requires a long description and is not properly marked.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>312</lineNum>
      <columnNum>93</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=15&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=15'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Alt text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;640&quot; height=&quot;480&quot; src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/IMG_2017080 ...</errorSourceCode>
        
        <sequenceID>312_93_15</sequenceID>
        <decisionPass>Alt text identifies the link destination.</decisionPass>
        <decisionFail>Alt text does not identify the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>312</lineNum>
      <columnNum>93</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=7&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=7'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image used as anchor is missing valid Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;640&quot; height=&quot;480&quot; src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/IMG_2017080 ...</errorSourceCode>
        <repair>Add Alt text that identifies the purpose or function of the image.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>312</lineNum>
      <columnNum>93</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=251&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=251'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text with poor contrast.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;640&quot; height=&quot;480&quot; src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/IMG_2017080 ...</errorSourceCode>
        
        <sequenceID>312_93_251</sequenceID>
        <decisionPass>Image does not contain text with a poor contrast ratio.</decisionPass>
        <decisionFail>Image contains text with poor contrast ratio.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>312</lineNum>
      <columnNum>93</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=11&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=11'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Image may contain text that is not in Alt text.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;img width=&quot;640&quot; height=&quot;480&quot; src=&quot;https://loveletterscsi.org/wp-content/uploads/2018/05/IMG_2017080 ...</errorSourceCode>
        
        <sequenceID>312_93_11</sequenceID>
        <decisionPass>Any text in image is also in Alt text or text in image is decorative or redundant.</decisionPass>
        <decisionFail>Image contains some text that is not in Alt text.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>319</lineNum>
      <columnNum>37</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=44&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=44'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h3&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h3 class=&quot;lsow-post-title&quot;&gt;&lt;a href=&quot;https://loveletterscsi.org/2018/06/another-post/&quot; title=&quot;Anothe ...</errorSourceCode>
        
        <sequenceID>319_37_44</sequenceID>
        <decisionPass>This &lt;code&gt;h3&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h3&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>319</lineNum>
      <columnNum>65</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/2018/06/another-post/&quot; title=&quot;Another Post&quot;
                     ...</errorSourceCode>
        
        <sequenceID>319_65_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>319</lineNum>
      <columnNum>65</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/2018/06/another-post/&quot; title=&quot;Another Post&quot;
                     ...</errorSourceCode>
        
        <sequenceID>319_65_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>319</lineNum>
      <columnNum>65</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/2018/06/another-post/&quot; title=&quot;Another Post&quot;
                     ...</errorSourceCode>
        
        <sequenceID>319_65_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Error</resultType>
      <lineNum>321</lineNum>
      <columnNum>62</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=190&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=190'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor element missing a &lt;code&gt;title&lt;/code&gt; attribute.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/category/uncategorized/&quot;&gt;Uncategorized&lt;/a&gt;</errorSourceCode>
        <repair>Add a &lt;code&gt;title&lt;/code&gt; attribute to your &lt;code&gt;a&lt;/code&gt; (anchor) element.</repair>
        
    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>321</lineNum>
      <columnNum>62</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/category/uncategorized/&quot;&gt;Uncategorized&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>321_62_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>321</lineNum>
      <columnNum>62</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/category/uncategorized/&quot;&gt;Uncategorized&lt;/a&gt;</errorSourceCode>
        
        <sequenceID>321_62_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>333</lineNum>
      <columnNum>33</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=44&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=44'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;&lt;code&gt;h3&lt;/code&gt; may be used for formatting.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;h3 class=&quot;entry-title&quot;&gt;&lt;a href=&quot;https://loveletterscsi.org/2018/06/another-post/&quot; title=&quot;Another Po ...</errorSourceCode>
        
        <sequenceID>333_33_44</sequenceID>
        <decisionPass>This &lt;code&gt;h3&lt;/code&gt; element is really a section header.</decisionPass>
        <decisionFail>This &lt;code&gt;h3&lt;/code&gt; element is used to format text (not really a section header).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>333</lineNum>
      <columnNum>57</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=191&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=191'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor's &lt;code&gt;title&lt;/code&gt; may not describe the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/2018/06/another-post/&quot; title=&quot;Another Post&quot;
                     ...</errorSourceCode>
        
        <sequenceID>333_57_191</sequenceID>
        <decisionPass>&lt;code&gt;title&lt;/code&gt; describes the link destination.</decisionPass>
        <decisionFail>&lt;code&gt;title&lt;/code&gt; does not describe the link destination.</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>333</lineNum>
      <columnNum>57</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=19&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=19'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Link text may not be meaningful.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/2018/06/another-post/&quot; title=&quot;Another Post&quot;
                     ...</errorSourceCode>
        
        <sequenceID>333_57_19</sequenceID>
        <decisionPass>Link text is meaningful when read alone (out of context).</decisionPass>
        <decisionFail>Link text is not meaningful when read alone (out of context).</decisionFail>

    </result> 
    <result>
      <resultType>Potential Problem</resultType>
      <lineNum>333</lineNum>
      <columnNum>57</columnNum>
      <errorMsg>&lt;a href=&quot;https://achecker.ca/checker/suggestion.php?id=197&quot;
               onclick=&quot;AChecker.popup('https://achecker.ca/checker/suggestion.php?id=197'); return false;&quot; 
               title=&quot;Suggest improvements on this error message&quot; target=&quot;_new&quot;&gt;Anchor text may not identify the link destination.&lt;/a&gt;
      </errorMsg>
      <errorSourceCode>&lt;a href=&quot;https://loveletterscsi.org/2018/06/another-post/&quot; title=&quot;Another Post&quot;
                     ...</errorSourceCode>
        
        <sequenceID>333_57_197</sequenceID>
        <decisionPass>Anchor has text that identifies the link destination.</decisionPass>
        <decisionFail>Anchor does not have text that identifies the link destination.</decisionFail>

    </result> 

  </results>
</resultset>
