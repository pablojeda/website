<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad.">
    <meta name="author" content="Prime Developers Chile">
    
    <!-- Facebook Metadatos | Diseño web accesible -->
    <meta property="og:title" content="Fundación Comparlante | Accessibility website checker"/>
    <meta property="og:site_name" content="Accessibility website checker"/>
    <meta property="og:description" content="Verify the Accessibility of your website" />
    <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/achecker/achecker.png"/>
    <!-- <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/diseno-web.php"/> -->

    
    
    <title>Fundación Comparlante</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet"> 
    <link href="../css/lightbox.css" rel="stylesheet"> 
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" href="../images/ico/logo-icon.png">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head><!--/head-->

<body>
  <?php include("header.php"); ?>

  <section id="page-breadcrumb">
    <div class="vertical-center sun">
     <div class="container">
        <div class="row">
            <div class="action">
                <div tabindex="10" class="col-sm-12">
                    <h1 class="title">Accessibility checker</h1>
                    
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!--/#page-breadcrumb-->

<section id="portfolio-information" >
    <div class="container"> <br><br>
        <div class="project-info overflow">
            <center>
                <img src="../images/achecker/achecker.png" class="img-responsive" width="50%" alt="Verificador web accesible"></center>
            </div><br>
            <div class="row">

                <div class="col-sm-12">

                    <div tabindex="11" class="project-info overflow"style="text-align:justify">

                        <h2>Currently 90% of the content available on the Internet is not accessible to people with disabilities. <br> <br>
Be socially responsible, gain competitiveness and improve your positioning on the Internet.</h2>

                    </div>

                </div>
            </div>
        </div>
    </section>

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center bottom-separator">

                </div>

                   <!--  <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
                  <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                        <center> <h2>¡Verify your site!</h2></center>
                        <form id="main-contact-form" name="contact-form" method="post" action="achecker.php">
                            <div class="form-group">
                                <input tabindex="51" id="nombre" type="text" name="name"  class="form-control" required="required" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <input tabindex="52" id="email" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                            </div>
                            <div class="form-group">
                                <input tabindex="53" id="web"  name="text" required="required" class="form-control" rows="8" placeholder="Website"></input>
                            </div>            
                            <div class="form-group">
                                <button tabindex="54" id="send-button" name="submit" onclick="realizaProceso()" class="btn btn-submit">Verify</button>
                            </div>

                        </form>
                    </div>
                </div>
                <div id="loading" class="col-md-12 col-sm-12" style="visibility: hidden;">
                    <center>
                    <div class="row">
                        <div class="col-sm-12">
                            <h2 style="vertical-align: middle;"> We are checking the accessibility of your website, wait just a second
.</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 ">
                            <img style="vertical-align: middle;" alt="Clockwise turning spheres as an indication of loading
" src="../images/_preloader.gif">
                        </div>
                    </div>
                    </center>
                </div>
                <div class="col-sm-12">
                    <div class="copyright-text text-center">
                        <p>&copy; Fundación Comparlante 2016.</p>
                        <p>Developed by <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/#footer-->

    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/lightbox.min.js"></script>
    <script type="text/javascript" src="../js/wow.min.js"></script>
    <script type="text/javascript" src="../js/main_es.js"></script>   
    <script type="text/javascript" src="../js/custom_en.js"></script> 
    <!-- Sweet Alert Script -->
    <script src="../js/sweetalert.min.js"></script>
    <!-- Sweet Alert Styles -->
    <link href="../css/sweetalert.css" rel="stylesheet">
</body>


</html>
