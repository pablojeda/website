<!DOCTYPE html>
<html lang="en">


<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities.">
  <meta name="author" content="Prime Developers Chile">

 <!-- Facebook Metadatos | Consulting for Social Entrepreneurs -->
    <meta property="og:title" content="Consulting for Social Entrepreneurs"/>
    <meta property="og:site_name" content="Consulting for Social Entrepreneurs"/>
    <meta property="og:description" content="Entrepreneurial environment around the world is full of opportunities and, at the same time, there are big challenges." />
    <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/services/2/asesoramiento-productivo.jpg"/>
  <!--  <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/asesorias-emprendimiento.php"/> -->
    


  <title>Fundación Comparlante</title>
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/font-awesome.min.css" rel="stylesheet">
  <link href="../css/animate.min.css" rel="stylesheet"> 
  <link href="../css/lightbox.css" rel="stylesheet"> 
  <link href="../css/main.css" rel="stylesheet">
  <link href="../css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="../images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" href="../images/ico/logo-icon.png">

        <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
    </head><!--/head-->

    <body>
        <?php include("header.php"); ?>

        <section id="page-breadcrumb">
            <div class="vertical-center sun">
               <div class="container">
                <div class="row">
                    <div class="action">
                        <div tabindex="15" class="col-sm-12">
                            <h1 class="title">Consulting for Social Entrepreneurs</h1>
                            <p>Entrepreneurial environment around the world is full of opportunities and, at the same time, there are big challenges. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/#page-breadcrumb-->

    <section id="portfolio-information" >
       <div class="container"><br><br>
        <div class="project-info overflow">
            <center>
                <img src="../images/services/1/asesoramiento-productivo.png" class="img-responsive " alt="Consulting for Social Entrepreneurs"></center>
            </div><br>
            <div class="row">

                <div class="col-sm-12">

                    <div tabindex="16" class="project-info overflow" style="text-align:justify">

                        <h2>Entrepreneurial environment around the world is full of opportunities and, at the same time, there are big challenges for entrepreneurs that make the decision to make their dreams come true starting up their projects.</h2>
                        
                        <h2>
                            Comparlante’s mission is to become a suitable support for this group of future businessman, and for that reason, it offers the Consulting Service, in which experienced professionals in the fields of Entrepreneurship Incubation Processes, Strengthening of Business Ideas, Market Analysis, Financial Viability and development of soft skills, can orient entrepreneurs in order to find the necessary tools to take full advantages of every opportunity they may have.</h2> 
                            
                            <h2>
                                To put into practice the necessary tools for the development of a business idea is the key for the starting up of it.</h2>
                                
                                <h2>
                                    In this program, we will find out which are those tools, how and why we should used them in order to create an entrepreneurship. Also, we will give the student certain guidelines to go through the entrepreneurial way learning, proving and generating value for their clients. 

                                </h2>

                            </div>
                            <center>
                                <div tabindex="17" class="live-preview data-wow-duration="500ms" data-wow-delay="300ms"">

                                 <h2>Are you an entrepreneur and need help?</h2>
                                 <br>
                                 <a tabindex="18" href="#etapas" class="btn btn-lg btn-info">Seek advice from our experts!</a>

                             </div></center>
                             <br>

                             <div class="project-info overflow padding-top " style="text-align:justify">
                                <div tabindex="20" class="col-sm-6">
                                    <p> 
                                        <div class="col-sm-3">
                                            <img src="../images/team/roberto-jaramillo.jpg" class="img-responsive" alt="Roberto Jaramillo"> 
                                        </div> 
                                        Roberto Jaramillo is a young Ecuadorian entrepreneur with more than 5 years of experience in the field of entrepreneurship and methodologies development for designing, construction, monitoring and implementation of social projects. 
                                    </p>
                                    <p>He has a Master of International Relationships with Mention in Negotiation and Conflicts Management. He also holds a diploma from the Universidad Andina Simón Bolívar from Ecuador in Economics and Finance, and he got the degree in International Business Engineer in Universidad Internacional del Ecuador. </p>
                                    <p>He has participated as a representative of his country in national and international conferences about the implementation of public polices and social programs aim at disabled people, such as: the 17th Microcredit Summit in Mexico, the I International Forum of Experts in Labor Insertion in Peru, the Seminar in Micro Business Entrepreneurs Support in Israel through the Israeli Agency of International Cooperation for Development MASHAV, International Training Center Golda Meir and Young Americas Business Trust (OAS), among others.
                                    </p>
                                    <p>His direct social focus and his persistent search for the people in need welfare make him part of this prestigious group of professionals whose aim is to give their best in order to your projects come true. </p>
                                </div>
                                <div tabindex="30" class="col-sm-6">
                                    <p> 
                                        <div class="col-sm-3">
                                            <img src="../images/team/nicolas-remedi.jpg" class="img-responsive" alt="Nicolás Remedi"> 
                                        </div> 
                                        Nicolás Remedi Rumi obtained his Bachelor’s Degree in Business Administration in Universidad Católica del Uruguay. His Micro, Small and Medium-Size Company Consultancy Degree was certified by the Laboratorio Technológico in Uruguay. He has improved his entrepreneurship and innovation studies in MASHAV International Training Center, Israel.
                                    </p>
                                    <p>In some educational institution, he has incorporated the subject “Entrepreneurship and Innovation”. He is a profesor and resercher in Facultad de Ciencias Empresariales in Universidad Católica del Uruguay.</p>
                                    <p>Today, he is the consultant in the program which gives support to enptrepreneurs in “Salto emprende”, and he is the tutor in “Idea Open” in Gepian. </p>
                                    <p>He is Co-author of the Ethics Code of Facultad de Ciencias Empresariales of Universidad Católica del Uruguay and of “Todos somos emprendedores”. In the latter, he was supported by “Red de Apoyo a Futuros Empresarios” from the National Agency of Investigation and innovation.</p>
                                    <p>He co-founder of la Junior Chamber International in Salto. He participated in the “Jóvenes emprendedores le hablan sistema político” program which was organised by Konrad Adenauer Fundation and the Asociación Cristina de Dirigentes de Empresas.</p>
                                    <p>He wrote many articles about entrepreneurship and innovation which were published in Uruguay and Spain.</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section id="etapas">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <br>
                            <div tabindex="40" class="clients text-center wow fadeInUp" data-wow-duration="300ms" data-wow-delay="300ms">

                                <h2>Become an entrepreneur</h2>
                                <h2 style="text-align:justify">Do not wait any longer, only giants like you know how to take giant steps, so put all your creativity and desire to make the difference in your luggage and join the wonderful journey of entrepreneurship in the hands of our professionals!</h2>
                                <h2>To the accessibility and beyond!</h2>
                            </div>
                            <!-- tabuladores -->
                               <div class="col-md-12">
                                <ul id="tab1" class="nav nav-tabs ">
                                    <li class="active"><a tabindex="41" href="#tab1-item1" data-toggle="tab"  aria-labelledby="f1"><i class="fa fa-circle-o fa-3x" style="vertical-align:middle;" aria-hidden="true"></i> Stage 1</a></li>
                                    <li><a href="#tab1-item2"  tabindex="43" data-toggle="tab"  aria-labelledby="f2"><i class="fa fa-circle-o fa-3x" style="vertical-align:middle;" aria-hidden="true"></i> Stage 2</a></li>
                                    <li><a href="#tab1-item3" tabindex="45" data-toggle="tab"  aria-labelledby="f3"><i class="fa fa-circle-o fa-3x" style="vertical-align:middle;"aria-hidden="true"></i> Stage 3</a></li>
                                    <li><a href="#tab1-item4" tabindex="47" data-toggle="tab"  aria-labelledby="f4"><i class="fa fa-circle-o fa-3x" style="vertical-align:middle;"aria-hidden="true"></i> Stage 4</a></li>
                                    <li><a data-toggle="tab" tabindex="49"  aria-labelledby="f5"><i class="fa fa-bullseye fa-3x" style="vertical-align:middle;"aria-hidden="true"></i> Plenitude</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div tabindex="42" class="tab-pane fade active in" id="tab1-item1" style="text-align:justify">
                                        <h3><b>Course: Development of entrepreneurial skills</b></h3> <br>
                                            <p>The first phase of the training process aims to present some guidelines in the development of business ideas for entrepreneurs. Through the Process of Idea Generation this course seeks to discard ideas that do not engage the experience and motivations of the entrepreneur or its environment, to conclude with the selection of that idea that meets all expectations and is more likely to succeed. This step strengthens the Process of Idea Generation with the establishment of entrepreneurship through the implementation of a model of future business which will reflect in an orderly manner the reason of the company, the analysis of its market, its projections to the future, its strengths and weaknesses.</p>
                                       
                                    </div>
                                    <div tabindex="44" class="tab-pane fade" id="tab1-item2" style="text-align:justify">
                                        <h3><b>Course: Business Models and Defferential Values</b></h3> <br>
                                        <p>Stage 2 consists of the development of certain tools which allow us to clearly understand which the more appropriate business model is for the selected idea. We will learn how to connect our idea with the client, the suppliers, the market and  the resources we have in order to generate added value that differentiate us from competitors so as to obtain the expected profits. What makes the company be successful is innovation and differential value providing considerable advantages to our project. </p>
                                    </div>
                                    <div tabindex="46" class="tab-pane fade" id="tab1-item3" style="text-align:justify">
                                        <h3><b>Course: Validation Process</b></h3> <br>
                                        <p>Stage 3 allows entrepreneurs to validate their business idea in the market through the implementation of processes with the Trial and Error methodology which allow <u>the landing of the model</u>, product and the added value. The main tools will be analyzed in order to test the product in the market and obtain feedback from potential clients generating an innovative and <u>high-impact product</u>. This stage is strengthened by the analysis and systematization of the business process management where the tutors will present to the potential businessmen the main alternatives to <u>the correct management</u> for the future company.</p>
                                    </div>
                                    <div  tabindex="48" class="tab-pane fade" id="tab1-item4" style="text-align:justify">
                                        <h3><b>Course: Economic and Financial Feasibility</b></h3> <br>
                                        <p>The aim of Stage 4 of the training process is to bring the future businessmen the best practices to define the economic and financial feasibility of the Project though cash flow development, the analysis of financial profit indicators and the definition of points of balance. These concepts are essential in the entrepreneurship development and they are useful for having a clear picture of the expected incomes to define the investment profitability. Moreover, the tutors will present processes for a successful realization of the future company.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- Fin tabuladores -->
                            <!-- aria descripciones -->
                            <div style="display:none" id="f1" role="tooltip"><p>Stage 1.</p> </div>  
                            <div style="display:none" id="f2" role="tooltip"><p>Stage 2. </p></div>
                            <div style="display:none" id="f3" role="tooltip"><p>Stage 3.</p> </div>
                            <div style="display:none" id="f4" role="tooltip"><p>Stage 4.</p> </div>
                            <div style="display:none" id="f4" role="tooltip"><p>Plenitude.</p> </div>
                            


                            <br>
                        </div>
                    </div>
                    <center>
                        <div class="live-preview data-wow-duration="500ms" data-wow-delay="300ms"">


                         <br>
                         <br>
                         <a tabindex="50" href="#contacto" class="btn btn-lg btn-info">Contact Us for personalized answers</a>

                     </div>
                 </center>
             </div>
         </section>



         <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center bottom-separator">

                    </div>

           <!--  <div class="col-md-5 col-sm-6">
                <div class="contact-info bottom">
                  <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

              </div>
          </div> -->
          <div class="col-md-12 col-sm-12">
            <div id="contacto" class="contact-form bottom">
               <h2>Send Us a Message</h2>
               <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php"
               <div class="form-group">
                <input type="text" name="name" class="form-control" required="required" placeholder="Name">
            </div>
            <div class="form-group">
                <input type="email" name="email" class="form-control" required="required" placeholder="E-mail">
            </div>
            <div class="form-group">
                <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Your message"></textarea>
            </div>
            <div style="display:none"> 
                <input id="cc" value="emprendimiento@comparlante.com" placeholder="E-mail"> 
            </div>                         
            <div class="form-group">
                <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Send</button>
            </div>
        </form>
    </div>
</div>
<div class="col-sm-12">
    <div class="copyright-text text-center">
        <p>&copy; Fundación Comparlante 2016.</p>
        <p>Developed by <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
    </div>
</div>
</div>
</div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/lightbox.min.js"></script>
<script type="text/javascript" src="../js/wow.min.js"></script>
<script type="text/javascript" src="../js/main_en.js"></script>   
</body>


</html>