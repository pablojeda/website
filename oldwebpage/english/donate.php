<!DOCTYPE html>
<html lang="es">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Comparlante is a collective project that is nourished and sustained thanks to persons willing to help living in a more accessible world, persons committed to innovate and be change agents.">
    <meta name="author" content="Prime Developers Chile">
    
    <!-- Facebook Metadatos | Diseño web accesible -->
    <meta property="og:title" content="Fundación Comparlante | Support our Nonprofit"/>
    <meta property="og:site_name" content="Diseño web accesible"/>
    <meta property="og:description" content="With your donation you help us to build a more accessible world." />
    <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/services/2/diseno-web.jpg"/>
    <!-- <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/diseno-web.php"/> -->

    
    
    <title>Fundación Comparlante</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet"> 
    <link href="../css/lightbox.css" rel="stylesheet"> 
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" href="../images/ico/logo-icon.png">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head><!--/head-->

<body>
  <?php include("header.php"); ?>

  <section id="page-breadcrumb">
    <div class="vertical-center sun">
       <div class="container">
        <div class="row">
            <div class="action">
                <div tabindex="10" class="col-sm-12">
                    <h1 class="title">Support our Nonprofit</h1>
                    <p>With your donation you help us to build a more accessible world.</p>
                </div>
            </div>
            
        </div>
    </div>
</div>
</section>
<!--/#page-breadcrumb-->

<section id="portfolio-information" >
    <div class="container"> <br><br>
<!--         <div class="project-info overflow">
            <center>
                <img src="images/services/1/diseno-web.png" class="img-responsive " alt="Diseño web accesible">
            </center>
        </div> -->
        <br>
        <div class="row">

            <div class="col-sm-12">

                <div tabindex="11" class="project-info overflow "style="text-align:justify">

                    <h2>Comparlante is a collective project that is nourished and sustained thanks to persons willing to help living in a more accessible world, persons committed to innovate and be change agents.</h2>

                </div>
                <center>
                    <div taindex="13" class="live-preview data-wow-duration="500ms" data-wow-delay="300ms"">

                        <h2>Support us!</h2>

                        

                    </div> 
                </center>

                <div tabindex="15" class="project-info overflow padding-top" style="text-align:justify">
                    <div  class="col-sm-3">
                        <img src="../images/services/wells.jpg" class="img-responsive" alt="Wells Fargo Bank’s logo, Bank in which Fundación Comparlante can receive donations in the United States"> 
                    </div> 
                    <div class="col-sm-8">
                        <h3>
                            For donations at our US account (USD): <br>
                            Bank: Wells Fargo Bank. <br>
                            Account Number: 5553840710 <br>
                            Routing Number: 054001220 <br>
                            Customer Name: Fundación Comparlante. <br>
                        </h3>
                    </div>
                </div> 

                <div tabindex="16" class="project-info overflow padding-top" style="text-align:justify">
                    <div  class="col-sm-3">
                        <img src="../images/services/banco-nacion.jpg" class="img-responsive" alt=" Banco Nación de la Argentina’s logo, Bank in which Fundación Comparlante can receive donations in Argentina."> 
                    </div> 
                    <div class="col-sm-8">
                        <h3>
                            For donations at our account in Argentina (ARS): <br>
                            Número cuenta corriente especial en pesos: 3943428591 <br>
                            Banco de la Nación Argentina <br>
                            Sucursal 2680 - Pedro Luro <br>
                            CBU 0110394440039434285912 <br>
                            Titular: Fundación Comparlante <br>
                        </h3> 

                    </div>


                </div> 

                <br>

            </div>
        </div>
    </div>
</section>


<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">

            </div>

                   <!--  <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
                  <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                       <h2>Send Us a Message</h2>
                       <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php"
                       <div class="form-group">
                        <input type="text" name="name" class="form-control" required="required" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" required="required" placeholder="E-mail">
                    </div>
                    <div class="form-group">
                        <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Your message"></textarea>
                    </div>
                    <div style="display:none"> 
                        <input id="cc" value="pablo@primedevelopers.cl" placeholder="E-mail"> 
                    </div>                         
                    <div class="form-group">
                        <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Send</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="copyright-text text-center">
                <p>&copy; Fundación Comparlante 2016.</p>
                <p>Developed by <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
            </div>
        </div>
    </div>
</div>
</footer>
<!--/#footer-->
<!--/#footer-->

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/lightbox.min.js"></script>
<script type="text/javascript" src="../js/wow.min.js"></script>
<script type="text/javascript" src="../js/main_es.js"></script>   
</body>


</html>
