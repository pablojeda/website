<?php

var_dump($_POST);

$nombre = $_POST['nombre'];
$email = $_POST['email'];
$web = $_POST['web'];

// echo $nombre."-".$email."-".$web;
$errores = checkear($web,$email);




function enviarEmail($nombre,$email,$web,$errores)
{

	$cantidad_errores = $errores[0];
	$posibles_errores = $errores[1];
	$potenciales_errores = $errores[2];
	$para = $email;
	$titulo = 'Resultado Accesibilidad sitio web';
	$header =  'MIME-Version: 1.0' . "\r\n";
	$header .= 'Content-type: text/html; charset=utf-8' . "\r\n";
	$header .= 'From: Fundación Comparlante  <info@comparlante.com> ';

	//Mejorar mensaje según plantilla html
	$msjCorreo =  '
<html>

<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">	
		<tr>
			<td style="padding: 40px 30px 40px 30px;">
				<!-- Cabezera -->
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="800" style="border: 1px solid #cccccc; border-collapse: collapse;     background-color: rgb(252, 202, 68);">
					<tr>
						<td style="padding: 40px 30px 40px 30px;">
							<center>
								<h2 style="color: #000; font-family: Arial, sans-serif; font-size: 24px;"> WELCOME TO COMPARLANTE’S ACCESSIBILITY CHECKER </h2>
							</center>
						</td>
					</tr>
				</table>
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="800" style="border: 1px solid #cccccc; border-collapse: collapse;">
					<tr>
						<td bgcolor="#ffffff" style="padding: 0px 30px 40px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								
								<!-- <tr>
									<td style="color: #000; font-family: Arial, sans-serif; font-size: 14px;">
										<center>
											<b>
												¡Hi  '. $nombre .'! <br>
												Te damos la bienvenida al Evaluador de Accesibilidad Comparlante <br>
												¡Gracias por dar el primer paso hacia la accesibilidad universal de Internet!
											</b>

										</center>
									</td>
									
								</tr> -->

								<tr>
									<td style="color: #000; font-family: Arial, sans-serif; font-size: 14px; padding-top:10px; padding-bottom: 20px;">
										<center>
											
											'.$nombre.', THE ANALYSIS OF <b>'.$web.' </b> HAS BROUGHT THE FOLLOWING RESULTS:
											
										</center>
									</td>
								</tr>

								<tr>
									<td style="color: #000; font-family: Arial, sans-serif; font-size: 14px; padding-top:10px; background-color: #f3e9e9; border-radius: 10px; padding-bottom: 20px;">
										<center>
											<img src="http://www.comparlante.com/images/achecker/error.png" alt="Verificador de accesibilidad web" width="8%" style="display: block;" />
											

											<b>ERRORS: '. $cantidad_errores .' </b> <br>
											(CORRECTIONS NEEDED)
											
										</center>
									</td>
								</tr>

								<tr>
									<td style="color: #000; font-family: Arial, sans-serif; font-size: 14px; padding-top:10px; background-color: #fff; border-radius: 10px; padding-bottom: 20px;">
										<center>
											
										</center>
									</td>
								</tr>

								<tr>
									<td style="color: #000; font-family: Arial, sans-serif; font-size: 14px; padding-top:10px; background-color: #f3e9e9; border-radius: 10px; padding-bottom: 20px;">
										<center>
											<img src="http://www.comparlante.com/images/achecker/warning.png" alt="Verificador de accesibilidad web" width="8%" style="display: block;" />
											

											<b>POSSIBLE ERRORS: '. $posibles_errores .' </b> <br>
											(REQUIRES USER EXPERIENCE CHECK)
											
										</center>
									</td>
								</tr>

								<tr>
									<td style="color: #000; font-family: Arial, sans-serif; font-size: 14px; padding-top:10px; background-color: #fff; border-radius: 10px; padding-bottom: 20px;">
										<center>
											
										</center>
									</td>
								</tr>

								<tr>
									<td style="color: #000; font-family: Arial, sans-serif; font-size: 14px; padding-top:10px; background-color: #f3e9e9; border-radius: 10px; padding-bottom: 20px;">
										<center>
											<img src="http://www.comparlante.com/images/achecker/question.png" alt="Verificador de accesibilidad web" width="8%" style="display: block;" />
											

											<b>POTENTIAL PROBLEMS: '. $potenciales_errores .' </b> <br>
											(MANUALLY CHECK IS REQUIRED)
										</center>
									</td>
								</tr>

								<tr>
									<td style="color: #000; font-family: Arial, sans-serif; font-size: 14px; padding-top:10px; background-color: #fff; border-radius: 10px; padding-bottom: 20px;">
										<center>
											
										</center>
									</td>
								</tr>

								<tr>
									<td style="color: #000; font-family: Arial, sans-serif; font-size: 14px; padding-top:10px; background-color: #fff; border-radius: 10px; padding-bottom: 20px;">
										<center>
											
											TO IMPROVE THE ACCESSIBILITY OF YOUR WEBSITE, WE OFFER:
											
										</center>
									</td>
								</tr>

								<tr>
									<td style=" color: #000; font-family: Arial, sans-serif; font-size: 14px; padding-top:10px; background-color: rgb(252, 202, 68);  padding-bottom: 20px;">
										<center>
											
											<b>
												COMPLETE DIAGNOSIS <br>
												DESIGN AND IMPLEMENTATION OF OPTIMIZATION STRATEGIES OF ACCESSIBILITY<br>
												PROFESSIONAL MENTORING AND FOLLOW UP
											</b>
											
										</center>
									</td>
								</tr>

								

								<tr>
									<td style="color: #000; font-family: Arial, sans-serif; font-size: 14px; padding-top:10px; background-color: #fff; border-radius: 10px; padding-bottom: 20px;">
										
									</td>
								</tr>



								<tr>
									<td style="color: #000; font-family: Arial, sans-serif; font-size: 14px; padding-top:10px; background-color: #fff; border-radius: 10px; padding-bottom: 20px;">
										<center>
											
											<b> DON’T LOSE THE CHANCE! 
												<br>CONTACT US AND RECEIVE A PROPOSAL ACCORDING TO YOUR NEEDS: info@comparlante.com</b>
											 <br>
											<b> Fundación Comparlante</b> <br>
											<b> <a href="http:www.comparlante.com"> www.comparlante.com | @comparlante</a></b>
											
										</center>
									</td>
								</tr>

								<br>

							</table>
						</td>
					</tr>
					
				</table>
			</td>
		</tr>
	</table>
</body>


</html>

';
	
	mail($para, $titulo, $msjCorreo, $header);
	
}


function checkear($web,$email){


	// $sitio_web="http://google.cl";
	$key="8f77076c7d0152516cc6c2d15a4e84d6385b85d5";
	$sitio = "http://".$web;
	$url = "https://achecker.ca/checkacc.php?uri=".$sitio."&id=".$key."&output=rest&guide=STANCA,WCAG2-AA&offset=10";

	echo $url. "<br>";

	$file=file_get_contents($url);
	$name = $email;

	set_time_limit(1200);
	$archivo = fopen("check-sitios/".$name, 'w+');
	fputs($archivo,$file);
	fclose($archivo);
	
	//cortar la cantidad de caracteres del resumen
	$inicio = 0;
	$fin = 0;

	//encontrar <summary> y </summary>.
	$archivo =fopen("check-sitios/".$name,'r');
	$numlinea = 1;
	$newXml = "";
	while (!feof($archivo)) {
		$linea =  (fgets($archivo));

		if (strstr($linea , "<summary>")){
			$inicio = $numlinea;

		}
		if (strstr($linea , "</summary>")){
			$fin = $numlinea;
			$newXml .= $linea;
			break;

		}
		if($inicio > 1){
			$newXml .= $linea;
		} 
		$numlinea++;
	}

	fclose($archivo);

	$xml = simplexml_load_string($newXml);
	$cantidad_errores= $xml->NumOfErrors;
	$posibles_errores =  $xml->NumOfLikelyProblems;
	$potenciales_errores = $xml->NumOfPotentialProblems;

	$errores = array();
	$errores[0] = $cantidad_errores;
	$errores[1] = $posibles_errores;
	$errores[2] = $potenciales_errores;

	print_r($errores);
	if(!empty($errores)){
		// $bd = 'prueba';
		// $tabla	 = 'achecker';
		// $usuario = 'root';
		// $clave 	 = '';

		$bd = 'comparlante';
		$tabla	 = 'achecker';
		$usuario = 'comparlante';
		$clave 	 = 'comparlante2906';
		$todo = $errores[0] . " -posE: ". $errores[1]. "-potE: ".$errores[2];

		/* Conexión a Base de Datos */
		$link = new mysqli('127.0.0.1', $usuario, $clave, $bd,3306);
		$nombre = $_POST['nombre'];
		$email = $_POST['email'];
		/* Se agrega a la bd */
		$query = 'INSERT INTO '.$esquema.'.'.$tabla.' (`nombre`, `email`, `web`, `errores`) VALUES ("'.$nombre.'", "'.$email.'", "'.$web.'", "'.$todo.'");';


		if($link->query($query)){
			echo "200";

			//
			enviarEmail($nombre,$email,$web,$errores);
		} else {
			echo "400";
		}


		$link->close();

	} else {
		echo "400";
	}

	return $errores;
}




?>