<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities.">
    <meta name="author" content="Prime Developers Chile">
    
    <!-- Facebook Metadatos | Diseño web accesible -->
    <meta property="og:title" content="I Literary contest for girls and boys in elementary school 'My world, My way'"/>
    <meta property="og:site_name" content="I Literary contest for girls and boys in elementary school 'My world, My way'"/>
    <meta property="og:description" content="From Comparlante Foundation we want all the little dreamers to have the opportunity to develop a story that involves at least one of the four characters of our Foundation. That's why we are pleased to invite children and writers to give life to one of our four characters in a story." />
    <meta property="og:image" content="http://comparlante.com/images/concurso/personajes.png"/>
    <!-- <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/diseno-web.php"/> -->

    
    
    <title>Comparlante Foundation</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet"> 
    <link href="../css/lightbox.css" rel="stylesheet"> 
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="../images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" href="../images/ico/logo-icon.png">
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WRVC32B');</script>
    <!-- End Google Tag Manager -->
</head><!--/head-->

<body>
  <?php include("header.php"); ?>

  <section id="page-breadcrumb">
    <div class="vertical-center sun">
     <div class="container">
        <div class="row">
            <div class="action">
                <div tabindex="10" class="col-sm-12">
                    <h2 style="font-size: 20pt; font-weight: 400" class="title text-center">I Literary contest for girls and boys in elementary school "My world, My way"</h2>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!--/#page-breadcrumb-->
<div id="zooming">

    <section id="concurso" >
        <div class="container"> <br><br>
            <div class="project-info overflow">
                <center>
                    <img width="50%" src="../images/concurso/personajes.png" class="img-responsive " alt="Characters of contest, a boy with visual impairment appears, to his right a girl with motor disability, a boy with hearing impairment and a girl with down syndrome. Enter for contest information"></center>
                </div><br>
                <div class="row">

                    <div class="col-sm-12">

                        <div tabindex="11" class="project-info overflow "style="text-align:justify">

                            <h2>From Comparlante Foundation we want all the little dreamers to have the opportunity to develop a story that involves at least one of the four characters of our Foundation. That's why we are pleased to invite children y little writers to give life to one of our four characters in a story</h2>

                        </div>
                        <center>
                            <div taindex="13" class="live-preview data-wow-duration="500ms" data-wow-delay="300ms"">

                                <h2>Join in!</h2>
                                <br>


                            </div> 
                        </center>
                        <br>
                        <!-- primer personaje -->
                        <div tabindex="15" class="project-info overflow" style="text-align:justify">
                            <h2>Our 1st character is a child with visual impairment:</h2>
                            <h3>
                                <div  class="col-sm-3">
                                    <img src="../images/concurso/discapacidad-visual.png" width="75% class="img-responsive" alt="boy with visual impairment"> 
                                </div> 
                                <br> <br>
                                Visual impairment refers to the deficiency of the vision system which affects acuity and visual field, ocular motility and perception of colors and depth, resulting in diagnoses such as low vision or blindness.
                            </h3>
                            <h3>He is accompanied by his guide dog: an animal specially trained to provide assistance for the mobility and independence of the visually impaired.  </h3>
                            </div> 
                            <!-- segundo personaje -->
                            <div tabindex="16" class="project-info overflow" style="text-align:justify">
                                <h2>Our 2nd character is a girl with motor disability:</h2>
                                <h3>
                                    <div  class="col-sm-3">
                                        <img src="../images/concurso/discapacidad-motriz.png" width="60% class="img-responsive" alt="girl with motor disability"> 
                                    </div> 
                                    <br> <br>
                                    Motor disability refers to a physical condition which influences the body’s ability to control and move, characterized by disturbances in the person’s movement, balance, speech, and breathing.
                                </h3>
                                <h3>To facilitate her movement and autonomy, she has a wheelchair that adapts and responds to her needs.</h3>
                            </div> 

                            <!-- tercer personaje -->
                            <div tabindex="17" class="project-info overflow" style="text-align:justify">
                                <h2>Our 3rd character is a girl with Down Syndrome:</h2>
                                <h3>
                                    <div  class="col-sm-3">
                                        <img src="../images/concurso/sindrome-down.png" width="60% class="img-responsive" alt="girl with Down Syndrome"> 
                                    </div> 
                                    <br> <br>
                                    Down syndrome is a congenital disorder derived from total or partial triplication of the chromosome 21, a cognitive spectrum disability that results in mental retardation and growth as part of certain physical changes.  
                                </h3>
                            </div> 
                            <!-- cuarto personaje -->
                            <div tabindex="18" class="project-info overflow " style="text-align:justify">
                                <h2>Our 4th character is a child with hearing impairment:</h2>
                                <h3>
                                    <div  class="col-sm-3">
                                        <img src="../images/concurso/discapacidad-auditiva.png" width="60%" class="img-responsive" alt="child with hearing impairment"> 
                                    </div> 
                                    <br> <br>
                                    Hearing impairment or deafness refers to the impossibility or difficulty of making use of the sense of hearing due to a loss of partial
                                    (hearing loss) or total hearing loss unilaterally or bilaterally. Like other physical disabilities, deafness can originate at birth or be  acquired over the years of life.
                                </h3>
                                <h3>In order to communicate, deaf people use Sign Language: a complete system of communication which, just as the spoken language allows to transmit ideas and feelings, transforming words into gestures carried out mainly with hands.
                                </h3>
                            </div> 
                        </div>
                    </div>
                </div>
            </section>
            <hr>
            <section id="informacion-concurso">
                <div class="container">
                    <div class="row">
                        <div tabindex="18" class="project-info overflow " style="text-align:justify">
                            <h3>
                             Discover how magical and interesting the world around us is by challenging yourself to learn more about this subject by researching with your teacher, your classmates, your family and friends.
                         </h3>

                         <h3>We invite you to be a part of this literary contest that opens on March 1, 2017 by presenting your story which must comply with the
                            following guidelines: 
                        </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Genre: Story, children and youth.</li>
                            <li><i class="fa fa-angle-right"></i> Prize: Publication of the work in digital and physical version and diploma.</li>
                            <li><i class="fa fa-angle-right"></i> Open to: Girls and boys between 6 and 13 years old.</li>
                            <li><i class="fa fa-angle-right"></i> Organizer: Comparlante Foundation. </li>
                            <li><i class="fa fa-angle-right"></i> Country of the convening entity: Argentina.</li>
                            <li><i class="fa fa-angle-right"></i>  Scope of the contest: International.</li>
                            <li><i class="fa fa-angle-right"></i>  Closing date: June 15, 2017, 23.59 (Argentina time zone). </li>
                        </ul>
                        <br>
                        <br>
                    </div> 
                </div>
            </div>

        </section>
        <section id="bases">
            <div class="container">
                <div class="row">

                    <h3><b>GUIDELINES</b></h3>

                </div>
                <div class="row">

                    <div tabindex="18" class="project-info overflow " style="text-align:justify">
                        <h3>
                         The purpose of the present bases is to regulate the 1st Literary Contest for girls and boys of basic education “My World, My Way”, which is carried out through the Comparlante Foundation to encourage the literary creation of the small talents of our region, seeking to raise awareness of the capacities and strengths of children with disabilities and the advantages of living in an accessible and equitable society.
                     </h3>
                     <br>
                     <h3> <b> Participants: </b></h3>
                     <ul class="elements">
                         <li><i class="fa fa-angle-right"></i>Children between 6 and 13 years of age can participate.</li>

                     </ul>
                     <br>
                     <br>
                     <h3> <b> Category:</b></h3>
                     <ul class="elements">
                        <li><i class="fa fa-angle-right"></i> Genre of the work: Children’s story.</li>
                        <li><i class="fa fa-angle-right"></i> The submitted work must be of a single author, original and unpublished, including any Internet publication.</li>
                        <li><i class="fa fa-angle-right"></i> Authors will have full freedom of expression and focus of the subject, as long as they follow the established theme.</li>
                        <li><i class="fa fa-angle-right"></i> The story must incorporate at least 1 (one) of the 4 (four) characters proposed by the Comparlante Foundation.</li>
                        <li><i class="fa fa-angle-right"></i> The story must be written in Spanish or English, with a maximum length of 12 (twelve) pages in Word using Times New Roman
                            size 12 and 1.5 spacing format.</li>

                        </ul>
                        <br>
                        <h3> <b> Delivery format:</b></h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Participants must submit their works in digital format (Word or PDF format) via info@comparlante.com, indicating in the email
                                subject: “Title of the story, name of the author”.</li>
                                <li><i class="fa fa-angle-right"></i>  Then enter the following personal information of the author in the body of the email:</li>
                                <ul>
                                   <li><i class="fa fa-angle-double-right"></i>  Title of the work</li>
                                   <li><i class="fa fa-angle-double-right"></i> Full name of the author</li>
                                   <li><i class="fa fa-angle-double-right"></i>  ID number</li>
                                   <li><i class="fa fa-angle-double-right"></i> Date of birth</li>
                                   <li><i class="fa fa-angle-double-right"></i> City</li>
                                   <li><i class="fa fa-angle-double-right"></i>  Country</li>
                                   <li><i class="fa fa-angle-double-right"></i> Age</li>
                                   <li><i class="fa fa-angle-double-right"></i>  School</li>
                                   <li><i class="fa fa-angle-double-right"></i> Course</li>
                                   <li><i class="fa fa-angle-double-right"></i> Phone</li>
                                   <li><i class="fa fa-angle-double-right"></i> Email</li>
                               </ul>

                           </ul>
                           The organizers of the competition may ask the schools for the information to corroborate the data of the contestants.
                           <br>
                           <br>
                           <h3> <b> Awards:</b></h3>
                           <h3> The three (3) best works will be awarded:</h3>
                           <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> 1st Prize: Digital publication and paper (100 copies), online promotion for 3 months, recognition diploma.</li>
                            <li><i class="fa fa-angle-right"></i>2nd Prize: Digital publication, online promotion for 3 months, diploma of recognition.</li>
                            <li><i class="fa fa-angle-right"></i>3rd Prize: Digital publication, online promotion for 3 months, diploma of recognition.</li>
                        </ul>
                        The authors of the stories that fulfill all the requirements, even without being winners, will receive a diploma for their participation in the contest.
                        <br>
                        <br>
                        <h3> <b> Jury:</b></h3>

                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> The participating works will be evaluated and selected by a Jury composed of 3 people of trajectory in the literary field.</li>
                            <li><i class="fa fa-angle-right"></i> The jury will draw up a signed document that will designate the winners of this contest on July 30, 2017 and will announce the
                                results declaring the winners of the contest, and then be notified all participants.</li>
                                <li><i class="fa fa-angle-right"></i>  - The resolution referred to in the documents that declare the winners will be unappelable by the participants, who cannot claim
                                    compensation for any reason.</li>
                                </ul>
                                <br>

                                <br>
                                <h3> <b> Deadlines:</b></h3>
                                <ul class="elements">
                                    <li><i class="fa fa-angle-right"></i>  The deadline for submitting the participating works will be from March 1 to June 15, 2017.</li>
                                    <li><i class="fa fa-angle-right"></i>  - The works must be sent in digital format (Word or PDF format) to info@comparlante.com until June 15, 23:59 (Argentina time
zone).</li>
                                    <li><i class="fa fa-angle-right"></i> The winners will be announced on July 30 to the participants and the general public in social media of the Comparlante Foundation:
Facebook and Twitter.</li>
                                    <li><i class="fa fa-angle-right"></i>   Comparlante Foundation, after the fulfillment of the legal requirements by the winner will officially deliver the prize.</li>
                                    <li><i class="fa fa-angle-right"></i>  The participation in this competition implies the total acceptance of the present bases which govern under laws of the Argentine
Republic. </li>


                                </ul>
                                <br>
                            </div> 
                        </div>
                    </div>

                </section>
                <center>
                    <br>
                    <a tabindex="9" type="button" href="../concurso/basesen.pdf" class="btn btn-info"><h4>Download the contest rules</h4></a>
                </center>
                <footer id="footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 text-center bottom-separator">

                            </div>

                   <!--  <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
                  <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                     <h2>Send Us a Message</h2>
                     <form id="main-contact-form" name="contact-form" method="post" action="../contacto2.php">
                        <div class="form-group">
                            <input tabindex="51" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <input tabindex="52" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <textarea tabindex="53" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Your message"></textarea>
                        </div>            
                        <div style="display:none"> 
                            <input id="cc" value="" placeholder="E-mail"> 
                        </div>             
                        <div class="form-group">
                            <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Send</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Fundación Comparlante 2016.</p>
                    <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/lightbox.min.js"></script>
<script type="text/javascript" src="../js/wow.min.js"></script>
<script type="text/javascript" src="../js/main_es.js"></script>   
</body>


</html>
