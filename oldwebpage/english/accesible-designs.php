<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Comparlante Foundation provides technological and innovation services with social impact for the accessibility of people with disabilities.">
    <meta name="author" content="Prime Developers Chile">
    
   <!-- Facebook Metadatos | Diseño accesible -->
    <meta property="og:title" content="Fundación Comparlante | Accessible Designs "/>
    <meta property="og:site_name" content="Accessible Designs"/>
    <meta property="og:description" content="Comparlante takes seriously the responsibility of 'creating a more accessible world', and step by step, we are making it real!" />
    <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/services/2/diseno-accesible.jpg"/>
 <!--   <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/diseno-grafico.php"/>  -->

    
    
    <title>Fundación Comparlante</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet"> 
    <link href="../css/lightbox.css" rel="stylesheet"> 
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="../images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" href="../images/ico/logo-icon.png">

        <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
    </head><!--/head-->

    <body>

        <!--#include file="header.html"-->
        <?php include("header.php"); ?>
        
        <section id="page-breadcrumb">
            <div class="vertical-center sun">
             <div class="container">
                <div class="row">
                    <div class="action">
                        <div tabindex="10" class="col-sm-12">
                            <h1 class="title">Accessible Designs – Creative Accessibility</h1>
                            <p>Comparlante takes seriously the responsibility of “creating a more accessible world”, and step by step, we are making it real!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/#page-breadcrumb-->

    <section id="portfolio-information" >
        <div class="container">
            <br><br>
            <div class="project-info overflow">
                <center>
                    <img src="../images/services/1/diseno-accesible.png" class="img-responsive " alt="Accessible Designs – Creative Accessibility"></center>
                </div><br>
                <div class="row">

                    <div class="col-sm-12">

                        <div tabindex="11" class="project-info overflow "style="text-align:justify">

                            <h2>The purpose of the design is to create something useful. Form, color, texture and the other characteristics together with the function in itself of that creation should satisfy the context needs. That is why today, we talk about Universal Design.</h2>
                            <h2>
                                In this process, the designer should have the commitment to provide easy access to information through effective multi-sensory communication. 
                            </h2>

                        </div>


                        <div tabindex="12" class="project-info overflow " style="text-align:justify">
                            <h2> Which are those fields that can be included in Creative Accessibility? 
                            </h2>
                            <h2>
                                <ul class="elements">
                                    <li><i class="fa fa-angle-right"></i> Flexible visual identity.</li>
                                    <li><i class="fa fa-angle-right"></i> Advertising and Iconographic Design </li>
                                    <li><i class="fa fa-angle-right"></i> Ilustración en formato accesible.</li>
                                    <li><i class="fa fa-angle-right"></i> Advice for documentary, texts, and signage elaboration in accessible format.</li>
                                    <li style="text-align:left"><i class="fa fa-angle-right"></i> Development of advertising and educational campaigns adapted to different people and Social Responsibility projects.</li>

                                </ul>
                            </h2>
                        </div>

                        <br>
                        <center>
                            <div tabindex="13" class="live-preview data-wow-duration="500ms" data-wow-delay="300ms"">

                               <h2>What else? Everything, absolutely everything is possible!</h2>
                               <h2>¡Pure Life = Pure accessibility!</h2><br>
                               <a tabindex="14" href="#contacto" class="btn btn-lg btn-info" >Ask for the service!</a>

                           </div> </center>
                           <br>
                           <div tabindex="15" class="project-info overflow padding-top" style="text-align:justify">
                            <p>
                                <div class="col-sm-2">
                                    <img src="../images/team/beatriz-calvo.jpg" class="img-responsive" alt="Beatriz Calvo"> 
                                </div> 
                                <br>
                                Beatriz Calvo obtained her Degree in Fine Arts with the focus on Graphic Design in Universidad de Costa Rica. Today, she is the graphic designer in Youth Area in OAS, Young Americas Business Trust. Beatriz has taught online courses for young businessmen from Latin America about the development of graphical tools which boosted sales of products and services. She also participated in art exhibitions together with the Design Subject of Universidad de Costa Rica.
                                <p></p>She also participated in art exhibitions together with the Design Subject of Universidad de Costa Rica.</p>

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center bottom-separator">
                       
                    </div>

                   
                  <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                        <h2>Send Us a Message</h2>
                        <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php"
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" required="required" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" required="required" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Your message"></textarea>
                        </div>
                        <div style="display:none"> 
                            <input id="cc" value="" placeholder="E-mail"> 
                        </div>                         
                        <div class="form-group">
                            <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Send</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Fundación Comparlante 2016.</p>
                    <p>Developed by <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/lightbox.min.js"></script>
<script type="text/javascript" src="../js/wow.min.js"></script>
<script type="text/javascript" src="../js/main_en.js"></script>   
</body>


</html>
