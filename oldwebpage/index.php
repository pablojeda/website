<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para personas con discapacidad visual.">
	<meta name="author" content="Prime Developers Chile">

	<!-- facebook metadatos -->
	<meta property="og:title" content="Fundación Comparlante"/>
	<meta property="og:site_name" content="Fundación Comparlante"/>
	<meta property="og:description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para personas con discapacidad visual." />
	<meta property="og:image" content="http://comparlante.com/images/home/logo-fundacion-2.jpg"/>
	

	<link rel="shortcut icon" href="images/ico/logo-icon.png">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
	<link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">
	<title>Fundación Comparlante</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/animate.min.css" rel="stylesheet"> 
	<link href="css/lightbox.css" rel="stylesheet"> 
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">

	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head>
<body onload="redireccionar();">

	<section id="involucrate">
		<div class="container">
			<div class="row">

				<center>
					<img class="padding-top" src="images/home/logofundacionhorizontal.png" style="width:30%!important;" alt="Logo Comparlante">  
				</center>  
				<center>
					<div tabindex="20">
						<h1 class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">¡Bienvenidos a nuestra comunidad! <br> Welcome to our community!</h1>
						<br>
						<p class="text-center wow fadeInDown" data-wow-duration="400ms" data-wow-delay="400ms">Selecciona el idioma / Select your language</p>
					</div>
					<div class="col-sm-6 text-center  wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
						<a onclick="espanol()"><img src="images/banderas/bandera_espana.jpg" alt="Sitio en Español" style="width:20%; margin-right:20px;"> </a><a  class="btn btn-entrada" onclick="espanol()">Sitio en Español</a> 
					</div>
					<div class="col-sm-6 text-center  wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
						
						<a onclick="ingles()" class="btn btn-entrada">English Site</a> <a onclick="ingles()"><img src="images/banderas/bandera_ingles.jpg" alt="English Site" style="width:20%; margin-left:20px;"> </a>	
						
					</div>
				</center>                       
			</div>
		</div>
	</section>

	<script type="text/javascript" src="js/idiomas.js"></script>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/wow.min.js"></script>
	<script type="text/javascript" src="js/main_es.js"></script>
</body>
</html>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-75127504-2', 'auto');
ga('send', 'pageview');

</script>

