<!DOCTYPE html>
<html lang="es">


<head>
     <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="No hay camino para la accesibilidad, la accesibilidad es el camino.">
    <meta name="author" content="Prime Developers Chile">
    
    <!-- Facebook Metadatos | Consultoría en accesibilidad -->
    <meta property="og:title" content="Fundación Comparlante | Consultoría en accesibilidad"/>
    <meta property="og:site_name" content="Consultoría en accesibilidad"/>
    <meta property="og:description" content="No hay camino para la accesibilidad, la accesibilidad es el camino." />
    <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/services/2/asesoria-accesibilidad.jpg"/>
   <!-- <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/accesibilidad_es.php"/> -->
    
    
    <title>Fundación Comparlante</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">


    <link rel="shortcut icon" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">

    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->

</head><!--/head-->

<body>
  <?php include("header.php"); ?>

  <section id="page-breadcrumb">
    <div class="vertical-center sun">
     <div class="container">
        <div class="row">
            <div class="action">
                <div tabindex="10" class="col-sm-12">
                    <h1 class="title">Consultoría en accesibilidad</h1>
                    <p>No hay camino para la accesibilidad, la accesibilidad es el camino.</p>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!--/#page-breadcrumb-->

<section id="portfolio-information" >
    <div class="container"><br><br>
        <div class="project-info overflow">
            <center>
                <img src="images/services/1/asesoria-accesibilidad.png" class="img-responsive " alt="Consultoría en accesibilidad"></center>
            </div><br>
            <div class="row">

               <div class="col-sm-12">

                <div tabindex="15" class="project-info overflow" style="text-align:justify">

                    <h2>En un mundo tan globalizado como en el que vivimos, no es posible que nuestros productos, nuestros servicios, nuestras prácticas educativas, sociales, comunicacionales y culturales sigan siendo tan poco accesibles. No se trata simplemente de "incluir", la accesibilidad es equidad de oportunidades para el desarrollo de todos, más allá de nuestras características y capacidades. ¿Te atreves?
                    </h2>

                </div>
                <center>
                    <div tabindex="16" class="live-preview data-wow-duration="500ms" data-wow-delay="300ms"">

                       <h2 >¡Manos a la obra!</h2>
                       <br>
                       <a tabindex="17" href="#contacto" class="btn btn-lg btn-info" >¡Asesórate!</a>

                   </div></center>


                   <div class="project-info overflow padding-top " style="text-align:justify">
                    <p tabindex="18"> 
                        <div class="col-sm-2">
                            <img src="images/team/sebastian-flores.jpg" class="img-responsive" alt="Sebastian Flores"> 
                        </div> 
                        Sebastian Flores es nuestro Co-Fundador, un joven líder y emprendedor nacido en Quito Ecuador el 19 de octubre de 1989 quien ha desarrollado su formación profesional enfocada en asuntos internacionales, con especial énfasis en la Diplomacia, Política Internacional, Derecho, democracia y participación ciudadana, las tecnologías de la información y comunicación en el contexto del Gobierno Electrónico y la Diplomacia Digital, el papel de la juventud en el proceso global de desarrollo sostenible, asuntos humanitarios que van desde la Migración y Movilidad Humana, pasando por los Derechos Humanos, y la consultoría en materia de accesibilidad y de equidad para las personas con discapacidad, tanto a nivel local como a nivel regional e internacional.
                    </p>
                    <p tabindex="19">La gestión de Sebastian en el campo de la accesibilidad y el diseño universal comprende una diversidad de escenarios: desde el plano académico y comunitario, el acompañamiento al desarrollo de emprendimientos sociales, empresariales y humanitarios, hasta los espacios de toma de decisiones como Parlamentos en América y Europa, foros internacionales como el Foro de Jóvenes de las Américas / Cumbre de las Américas, y agencias del Sistema de las Naciones Unidas (ONU) como la UNESCO, organismos internacionales como la Organización de Estados Americanos (OEA) y redes de incidencia juvenil como lo es la plataforma del YABT, RJLA, Juventud LAC, entre otros.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>





<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">
                
            </div>
<!-- 
            <div class="col-md-5 col-sm-6">
                <div class="contact-info bottom">
                  <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

              </div>
          </div> -->
          <div class="col-md-12 col-sm-12">
            <div id="contacto" class="contact-form bottom">
                <h2>Envíanos un mensaje</h2>
                <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php">
                    <div class="form-group">
                        <input tabindex="51" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Nombre">
                    </div>
                    <div class="form-group">
                        <input tabindex="52" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                    </div>
                    <div class="form-group">
                        <textarea tabindex="53" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Tu mensaje"></textarea>
                    </div>            
                    <div style="display:none"> 
                        <input id="cc" value="sebastian@comparlante.com" placeholder="E-mail"> 
                    </div>             
                    <div class="form-group">
                        <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Enviar mensaje</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="copyright-text text-center">
                <p>&copy; Fundación Comparlante 2016.</p>
                <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
            </div>
        </div>
    </div>
</div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/main_es.js"></script>   


</body>


</html>
