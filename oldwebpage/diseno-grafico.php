<!DOCTYPE html>
<html lang="es">


<head>
      <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad.">
    <meta name="author" content="Prime Developers Chile">
    
  <!-- Facebook Metadatos | Diseño accesible -->
    <meta property="og:title" content="Fundación Comparlante | Diseño Accesible"/>
    <meta property="og:site_name" content="Diseño Accesible"/>
    <meta property="og:description" content="En Comparlante nos tomamos muy en serio la responsabilidad de 'diseñar un mundo más accesible', ¡y trazo a trazo lo hacemos posible!." />
    <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/services/2/diseno-accesible.jpg"/>
 <!--   <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/diseno-grafico.php"/>  -->


    
    
    
    <title>Fundación Comparlante</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">
        <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
    </head><!--/head-->

    <body>

        <!--#include file="header.html"-->
        <?php include("header.php"); ?>
        
        <section id="page-breadcrumb">
            <div class="vertical-center sun">
               <div class="container">
                <div class="row">
                    <div class="action">
                        <div tabindex="10" class="col-sm-12">
                            <h1 class="title">Diseño Accesible – Accesibilidad Creativa.</h1>
                            <p>En Comparlante nos tomamos muy en serio la responsabilidad de "diseñar un mundo más accesible", ¡y trazo a trazo lo hacemos posible!.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/#page-breadcrumb-->

    <section id="portfolio-information" >
        <div class="container">
            <br><br>
            <div class="project-info overflow">
                <center>
                    <img src="images/services/1/diseno-accesible.png" class="img-responsive " alt="Diseño Accesible – Accesibilidad Creativa"></center>
                </div><br>
                <div class="row">

                    <div class="col-sm-12">

                        <div tabindex="11" class="project-info overflow "style="text-align:justify">

                            <h2>El propósito del diseño es crear algo útil, y la forma, color, textura y demás características junto a la función de esa creación deben atender las diversas necesidades del entorno. Por eso hoy más que nunca, hablamos de  Diseño Universal. </h2>
                            <h2>
                                En este proceso, el diseñador tiene el compromiso gráfico de proporcionar fácil acceso a la información mediante comunicaciones multisensoriales efectivas.
                            </h2>

                        </div>


                        <div tabindex="12" class="project-info overflow " style="text-align:justify">
                            <h2> ¿Cuáles son las áreas que pueden ser abarcadas desde la Accesibilidad Creativa? 
                            </h2>
                            <h2>
                                <ul class="elements">
                                    <li><i class="fa fa-angle-right"></i> Identidad visual adaptada.</li>
                                    <li><i class="fa fa-angle-right"></i> Diseño publicitario e iconográfico.</li>
                                    <li><i class="fa fa-angle-right"></i> Ilustración en formato accesible.</li>
                                    <li><i class="fa fa-angle-right"></i> Asesoría para la elaboración de documentos, materiales de texto y señalética en formato accesible.</li>
                                    <li style="text-align:left"><i class="fa fa-angle-right"></i> Desarrollo de campañas publicitarias y educativas/de concienciación adaptadas a diversos públicos y proyectos de Responsabilidad Social.</li>

                                </ul>
                            </h2>
                        </div>

                        <br>
                        <center>
                            <div tabindex="13" class="live-preview data-wow-duration="500ms" data-wow-delay="300ms"">

                             <h2>¿Qué más? ¡Todo, absolutamente todo, es posible! </h2>
                             <h2>¡Pura vida = pura accesibilidad!</h2><br>
                             <a tabindex="14" href="#contacto" class="btn btn-lg btn-info" >¡Solicita el servicio!</a>

                         </div> </center>
                         <br>
                         <div tabindex="15" class="project-info overflow padding-top" style="text-align:justify">
                            <p>
                                <div class="col-sm-2">
                                    <img src="images/team/beatriz-calvo.jpg" class="img-responsive" alt="Beatriz Calvo"> 
                                </div> 
                                <br>
                                Beatriz Calvo, Bachiller en Bellas Artes con énfasis en Diseño Gráfico de la Universidad de Costa Rica. Actualmente es diseñadora gráfica para el Área de Juventud de la OEA, el Young Americas  Business Trust. Además del desarrollo de materiales de promoción y publicitarios para esta Organización Internacional, Beatriz ha dictado cursos on line para jóvenes empresarios de latinoamerica sobre cómo desarrollar herramientas gráficas que potencien las ventas de sus productos y servicios.
                                <p></p>También, ha sido parte de exposiciones de arte con el colectivo de Catedra de diseño de la Universidad de Costa Rica. </p>

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center bottom-separator">
                      
                    </div>

                    <!-- <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
                  <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                        <h2>Envíanos un mensaje</h2>
                        <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php">
                            <div class="form-group">
                                <input tabindex="12" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Nombre">
                            </div>
                            <div class="form-group">
                                <input tabindex="13" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                            </div>
                            <div class="form-group">
                                <textarea tabindex="13" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Tu mensaje"></textarea>
                            </div>            
                            <div style="display:none"> 
                                <input id="cc" value="" placeholder="E-mail"> 
                            </div>             
                            <div class="form-group">
                                <button tabindex="13" id="send-button" name="submit" class="btn btn-submit"> Enviar mensaje</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="copyright-text text-center">
                        <p>&copy; Fundación Comparlante 2016.</p>
                        <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/#footer-->

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/lightbox.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/main_es.js"></script>   
</body>


</html>
