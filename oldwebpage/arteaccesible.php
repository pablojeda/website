<!DOCTYPE html>
<html lang="es">


<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Arte Accesible es una campaña liderada por Fundación Comparlante que tienen como objetivo sensibilizar sobre la importancia de la accesibilidad para mejorar la calidad de vida de las personas con discapacidad y generar mayor equidad.">
  <meta name="author" content="Prime Developers Chile">

  <!-- Facebook Metadatos | Diseño accesible -->
  <meta property="og:title" content="Fundación Comparlante | Arte Accesible"/>
  <meta property="og:site_name" content="Arte Accesible"/>
  <meta property="og:description" content="En Comparlante nos tomamos muy en serio la responsabilidad de 'diseñar un mundo más accesible', ¡y trazo a trazo lo hacemos posible!." />
  <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/services/2/diseno-accesible.jpg"/>
  <!--   <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/diseno-grafico.php"/>  -->





  <title>Fundación Comparlante</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet"> 
  <link href="css/lightbox.css" rel="stylesheet"> 
  <link href="css/main.css" rel="stylesheet">
  <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head><!--/head-->

<body>

    <!--#include file="header.html"-->
    <?php include("header.php"); ?>

    <section id="page-breadcrumb">
        <div class="vertical-center sun">
           <div class="container">
            <div class="row">
                <div class="action">
                    <div tabindex="10" class="col-sm-12">
                        <h1 class="title">Programa Arte Accesible</h1>
                        <p>Arte Accesible es una campaña liderada por Fundación Comparlante que tienen como objetivo sensibilizar sobre la importancia de la accesibilidad para mejorar la calidad de vida de las personas con discapacidad y generar mayor equidad.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#page-breadcrumb-->

<section id="portfolio-information" >
    <div class="container">
        <br><br>
        <!-- <div class="project-info overflow">
            <center>
                <img src="images/services/1/diseno-accesible.png" class="img-responsive " alt="Diseño Accesible – Accesibilidad Creativa"></center>
            </div> -->
            <br>
            <div class="row">

                <div class="col-sm-12">

                    <div tabindex="11" class="project-info overflow " style="text-align:justify">

                        <h2>En la actualidad una herramienta que sirve para mejorar la calidad de vida de las personas con discapacidad es la accesibilidad web.
                            Además de permitir el uso pleno de internet y las oportunidades que esta ofrece, la accesibilidad es fundamentalmente una cuestión de
                        derechos humanos, de equidad e igualdad de oportunidades.</h2><br>
                        <h2>
                            ¿Por qué los museos están repletos de señalizaciones que dicen “no tocar”? La simpe acción de tocar vs no tocar hace que el arte pueda ser accesible para quien posee una discapacidad visual. Al permitirle explorar una escultura a través del tacto estamos generando inclusión y equidad.
                        </h2>
                        <br>
                        <h2>
                            Por ello implementaremos una campaña Latinoamericana de sensibilización sobre la importancia de contar con leyes de accesibilidad web efectivas, utilizando el arte como soporte. Trabajando conjuntamente con el artista Andrés Julio, desarrollaremos una muestra donde buscamos generar equidad en el acceso al arte interpelando los sentidos.
                        </h2>
                        <br>
                        <h2>
                         El proyecto prevé montar la muestra en los Congresos de distintos países, porque es allí donde se desarrollan las legislaciones y donde es necesarios sensibilizar a los efectores de las leyes. El piloto a implementarse en 2018 abarcará Argentina, Ecuador y Colombia que es donde se encuentra radicado el artista. El proyecto incluye la presentación de la muestra por un mes en cada país, lo que daría la oportunidad de contactar a otros artistas y generar comunidad. Junto a otras organizaciones no gubernamentales y fundaciones, desarrollar paralelamente redes que promuevan la accesibilidad web en todo el hemisferio.
                     </h2>
                     <br>
                     <h2>
                      Esta campaña de sensibilización y la muestra de arte han sido avaladas por la Organización de los Estados Americanos. Actualmente
                      nos encontramos en proceso de búsqueda de patrocinadores para poder llevar a cabo esta propuesta. Para ello, buscamos contar
                      con el apoyo del sector privado y público. Si quieres ser patrocinador contáctanos via <a href="mailto:info@comparlante.com">info@comparlante.com</a>
                  </h2>
                  <br>
                  <h2>

                      <a tabindex="" width="300px" style="background-color: #f39917!important; width: 300px!important;
                      border-color: #f39917!important;"  alt="Descarga la propuesta de arte accesible"   type="button" href="arte-accesible-propuesta.pdf" class="btn btn-info"><h4>Más información sobre el programa</h4></a>
                      <br><br>
                      <a tabindex="" style="background-color: #f39917!important; width: 300px!important;
                      border-color: #f39917!important;"  alt="Descarga la información sobre las obras de arte accesible"   type="button" href="arte-accesible.pdf" class="btn btn-info"><h4>Más información sobre las obras</h4></a>

                      <br><br>
                      #ArteAccesible Síguenos en: <br> <a href="https://www.facebook.com/pages/Comparlante/1537630623166015?ref=aymt_homepage_panel">Facebook <i class="fa fa-facebook"></i></a> <a href=" https://twitter.com/comparlante">Twitter <i class="fa fa-twitter"></i></a>
                  </h2>

              </div>


          </div>
      </div>
  </div>
</section>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">

            </div>

                    <!-- <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
  <!--                 <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                        <h2>Envíanos un mensaje</h2>
                        <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php">
                            <div class="form-group">
                                <input tabindex="12" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Nombre">
                            </div>
                            <div class="form-group">
                                <input tabindex="13" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                            </div>
                            <div class="form-group">
                                <textarea tabindex="13" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Tu mensaje"></textarea>
                            </div>            
                            <div style="display:none"> 
                                <input id="cc" value="" placeholder="E-mail"> 
                            </div>             
                            <div class="form-group">
                                <button tabindex="13" id="send-button" name="submit" class="btn btn-submit"> Enviar mensaje</button>
                            </div>
                        </form>
                    </div>
                </div> -->
                <div class="col-sm-12"><br>
                    <div class="copyright-text text-center"><br>
                        <p>&copy; Fundación Comparlante 2016.</p>
                        <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/#footer-->

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/lightbox.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/main_es.js"></script>   
</body>


</html>
