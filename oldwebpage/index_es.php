<!DOCTYPE html>
<html lang="es">


<script type="text/javascript">
    window.onload = setTimeout(function() {
        document.getElementById("instrucciones-de-uso").focus();

    },1500);

</script>

<head>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <meta name="description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad.">
 <meta name="author" content="Prime Developers Chile">
 <meta name="title" content="Fundación Comparlante"/>

 <!-- Facebook Metadatos -->
 <!--  Inicio -->
 <meta property="og:title" content="Fundación Comparlante | Inicio"/>
 <meta property="og:site_name" content="Fundación Comparlante"/>
 <meta property="og:description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad." />
 <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/home/logo-fundacion-2.jpg"/>
 <!--   <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/index_es.php"/> -->

 <title>Fundación Comparlante</title>
 <link href="css/bootstrap.min.css" rel="stylesheet">
 <link href="css/font-awesome.min.css" rel="stylesheet">
 <link href="css/animate.min.css" rel="stylesheet"> 
 <link href="css/lightbox.css" rel="stylesheet"> 
 <link href="css/main.css" rel="stylesheet">
 <link href="css/responsive.css" rel="stylesheet">


 <link rel="shortcut icon" href="images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">
 <!-- Google Tag Manager -->
 <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
 j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head>

<body>
  <?php include("header.php"); ?>
  <div style="display:none" id="mensaje-inicial"><p id="instrucciones-de-uso">La página de fundación Comparlante tiene una navegación vertical, En dispositivos móviles debes deslizarte con el dedo hacia la derecha o izquierda.</p> </div>  
  <section id="home-slider">
    <div class="container">
        <div class="row">
            <div class="main-slider">
                <div class="slide-text" tabindex="4">
                    <h1>Fundación Comparlante</h1>
                    <h2>¡Bienvenidos a nuestra comunidad!</h2>
                    <!-- <a href="#" class="btn btn-common">SIGN UP</a> -->
                </div>
                <img src="images/home/slider/1.png" class="slider-hill" alt="Al fondo una montaña con un sol y pájaros de colores entre las nubes.">
                <img src="images/home/slider/2.png" class="slider-house" alt="Un edificio rodeado de árboles, en la esquina izquierda una persona en silla de ruedas y otra utilizando bastón.">
                <img src="images/home/slider/3.png" class="slider-sun" alt="Cuatro personas felices frente al edificio, uno en silla de ruedas, otro utilizando bastón, ambos están acompañados de un hombre y una mujer.">

            </div>
        </div>
    </div>
    <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
</section>
<!--/#home-slider-->

<!-- ¿Quienes Somos? -->
<section id="quienes-somos" tabindex="5" class="padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
    <div class="container">
        <div class="row">
            <h1 class="title text-left wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">¿Quiénes somos?</h1>

            <div class="col-sm-12 padding-top-index text-justify">

                <p>Fundación Comparlante surge en el año 2015 en Argentina con el objetivo de generar herramientas tecnológicas para la accesibilidad de las personas con discapacidad visual. Hoy es un proyecto colectivo el cual opera desde Latinoamérica  y Estados Unidos (Washington DC) integrado por jóvenes emprendedores de Argentina, Chile, Costa Rica, Ecuador, El Salvador, Uruguay y Venezuela.</p>
                <p>En la actualidad, Comparlante busca establecerse como una red que ofrece productos y servicios adaptadas a las diversas necesidades de las personas con discapacidad. En una construcción colectiva, este proyecto tiene a la accesibilidad, el emprendimiento y el impacto social como bases de sus propuestas.</p>
                <p>Somos un espacio abierto a desarrollar, desde la innovación, herramientas que logren hacer de este un mundo menos discapacitado.</p>
            </div>
        </div>
    </div>
    <br>
</section>

<section id="verificador" tabindex="6" class="padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
    <div class="container">
        <div class="row">
            <h2 style="font-size: 20pt; font-weight: 400" class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Verifica si tu sitio web es accesible</h2>

            <div class="col-sm-12 padding-top-index text-justify">
                <center>
                    <a href="verifica.php">
                       <img width=" 35%" src="images/achecker/achecker.png" alt="Logo que representa el servicio de verificación de accesibilidad web">
                   </a>
                   <br> <br>
                   <a tabindex="9" style="background-color: #f39917!important;
                   border-color: #f39917!important;"  alt="ingresa al verificador de accesibilidad web"   type="button" href="verifica.php" class="btn btn-info"><h4>¡Accede al verificador!</h4></a>
               </center>
           </div>
       </div>
   </div>

</section> 


<!-- lo que hacemos -->
<section id="servicios">
    <div class="container">
        <div class="row">
            <h1 tabindex="7" class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Nuestros servicios</h1>
            <p class="text-center wow fadeInDown" data-wow-duration="400ms" data-wow-delay="400ms"></p>
            <div class="single-features">
                <div class="col-sm-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                    <img src="images/services/audiolibros.png" class="img-responsive" alt="audiolibros">
                </div>
                <div  tabindex="8" class="col-sm-6 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                    <h2>Audioteca Comparlante</h2>
                    <P>Comparlante ofrece un banco de audiolibros accesibles en 18 idiomas. De acceso libre y construcción colaborativa.</P>
                    <a tabindex="9" type="button" href="http://www.comparlante.com/biblioteca_audiolibros/" class="btn btn-info">Acceder a la audioteca</a>
                </div>
            </div>

            <div class="single-features">

                <div tabindex="10" class="col-sm-6 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                    <h2>Consultoría en accesibilidad</h2>
                    <P>Destinado a Empresas, Gobiernos, ONGs y privados que desean desarrollar y adecuar sus prácticas y servicios bajos estándares internacionales de accesibilidad.</P>
                    <a tabindex="11" type="button" href="accesibilidad_es.php" class="btn btn-info">Acceder a Consultoría en accesibilidad</a>
                </div>
                <div class="col-sm-5 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                    <img src="images/services/asesoria-accesibilidad.png" class="img-responsive" alt="Asesorías en accesibilidad">
                </div>
            </div>

            <div class="single-features">
               <div class="col-sm-5 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                <img src="images/services/diseno-web.png" class="img-responsive" alt="Diseño de páginas web accesibles">
            </div>
            <div tabindex="12" class="col-sm-6  wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                <h2>Diseño de páginas web accesibles</h2>
                <P>Desarrollamos sitios web  bajo parámetros de accesibilidad, usabilidad y navegabilidad. </P>
                <a  tabindex="13" type="button" href="diseno-web.php" class="btn btn-info">Acceder al servicio</a>
            </div>

        </div>
        <div class="single-features">

            <div tabindex="14" class="col-sm-6 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                <h2>Lecturas a medida </h2>
                <P>Para ese libro que quieres o necesitas Comparlante te ofrece horas de lectura por pedido.</P>
                <a tabindex="15" type="button" href="horas-de-lectura.php" class="btn btn-info">Acceder al servicio</a>
            </div>
            <div class="col-sm-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                <img src="images/services/lectura-a-medida.png" class="img-responsive" alt="Lectura a la medida">
            </div>
        </div>
        <div class="single-features">
            <div class="col-sm-5 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                <img src="images/services/asesoramiento-productivo.png" class="img-responsive" alt="Asesoramiento productivo para emprendedores">
            </div>
            <div tabindex="16" class="col-sm-6  wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                <h2>Asesoramiento productivo para emprendedores</h2>
                <P>Brindamos tutoría y asesoramiento uno a uno orientado al desarrollo de emprendimientos para personas con discapacidad. </P>
                <a tabindex="17" type="button" href="asesorias-emprendimiento.php" class="btn btn-info">Acceder al servicio</a>
            </div>

        </div>
        <div class="single-features">

            <div tabindex="18" class="col-sm-6 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
                <h2>Diseños accesibles</h2>
                <P>Desarrollamos diseños e ilustraciones de materiales, productos, campañas, y mucho más, para tu institución o proyecto en formato accesible.</P>
                <a tabindex="19" type="button" href="diseno-grafico.php" class="btn btn-info">Acceder al servicio</a>
            </div>
            <div class="col-sm-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
                <img src="images/services/diseno-accesible.png" class="img-responsive" alt="Diseños accesibles">
            </div>
        </div>



    </div>
</div>
</section>
<!--/#features-->

<!-- Concurso Mi mundo a mi manera -->

<section id="concurso" tabindex="6" class="padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
    <div class="container">
        <div class="row">
            <h2 style="font-size: 20pt; font-weight: 400" class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">I Concurso literario para niñas y niños de educación básica "Mi mundo a mi manera"</h2>

            <div class="col-sm-12 padding-top-index text-justify">
                <center>
                    <a href="concurso.php">
                       <img width=" 35%" src="images/concurso/personajes.png" alt="Personajes que aparecen en el concurso de comparlante, aparece un niño con discapacidad visual, discapacidad motriz, discapacidad auditiva y una niña con sindrome de down. ingresa para información del concurso">
                   </a>
                   <br> <br>
                   <a tabindex="9" style="background-color: #f39917!important;
                   border-color: #f39917!important;"  alt="ingresa al concurso de comparlante"   type="button" href="concurso.php" class="btn btn-info"><h4>¡Participa!</h4></a>
               </center>
           </div>
       </div>
   </div>

</section>

<!-- servicios -->
<section id="involucrate">
    <div class="container">
        <div class="row">
            <div tabindex="20">
                <h1 class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Involúcrate</h1>
                <p class="text-center wow fadeInDown" data-wow-duration="400ms" data-wow-delay="400ms">Comparlante es un proyecto colectivo que se nutre y sostiene gracias a personas que quieren ayudar a vivir en un mundo más accesible, personas que quieren innovar y ser agentes de cambio.</p>
            </div>
            <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
                <div tabindex="21" class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
                        <img src="images/services/unete.png" alt="Únete a nosotros">
                    </div>
                    <h2>Únete a nosotros</h2>
                    <p> ¿Te interesa ser parte del equipo? </p>
                    <p>¿Crees que tienes habilidades y te gustaría brindar algunos de nuestros servicios?</p>
                    <p> <a tabindex="22" type="button" href="contacto_es.php" class="btn btn-info">Escríbenos</a> </p>
                </div>
            </div>
            <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
                <div  tabindex="24"class="single-service">
                    <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
                        <img src="images/services/apoyanos.png" alt="">
                    </div>
                    <h2>Apoya nuestro proyecto</h2>
                    <p>Con tu donación nos ayudas a construir un mundo cada vez más accesible.</p>

                    <a href="donar.php" target="_blank">
                       <img alt="" border="0" src="images/services/donar.jpg"  >

                   </a> 

               </div>
           </div>
           <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms">
            <div tabindex="27" class="single-service">
                <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
                    <img src="images/services/idea.png" alt="">
                </div>
                <h2>¿Tienes una idea?</h2>
                <p>Tienes una propuesta que puede contribuir a generar accesibilidad para personas con discapacidad y  quisieras contar con un equipo que apoye una iniciativa.</p> <p><a tabindex="28" type="button" href="contacto_es.php" class="btn btn-info">Escríbenos </a> </p>
            </div>
        </div>
    </div>
</div>
</section>
<!--/#services--> 

<!-- TEAM -->
<section id="equipo">
    <div class="container">
        <div class="row">
            <div tabindex="30">
                <h1 class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Conoce al equipo</h1>
                <p class="text-center wow fadeInDown" data-wow-duration="400ms" data-wow-delay="400ms">Nuestro equipo se compone de profesionales de diferentes áreas que comparten un objetivo común: Trabajar día a día para lograr un mundo más accesible.</p> 
            </div>
            <div id="team-carousel" class="carousel slide wow fadeIn" data-ride="carousel" data-wow-duration="400ms" data-wow-delay="400ms">
                <!-- Indicators -->
                <ol class="carousel-indicators visible-xs">
                    <li data-target="#team-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#team-carousel" data-slide-to="1"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div tabindex="31" class="col-sm-3 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="images/team/lorena-julio.jpg" class="img-responsive" alt="Lorena Julio, Co-Fundadora">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Lorena Julio</h2>
                                    <p>Co-Fundadora</p>
                                    <p>Argentina</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="32" class="col-sm-3 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="images/team/sebastian-flores.jpg" class="img-responsive" alt="Sebastian Flores, Co-Fundador y Consultor en Accesibilidad">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Sebastian Flores</h2>
                                    <p>Co-Fundador y Consultor en Accesibilidad</p>
                                    <p>Ecuador</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="33" class="col-sm-3 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="images/team/ana-maidana.jpg" class="img-responsive" alt="Ana Maidana, Área Administrativa">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Ana Maidana</h2>
                                    <p>Área Administrativa</p>
                                    <p>Argentina</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="34" class="col-sm-3 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="images/team/guido-munoz.jpg" class="img-responsive" alt="Guido Muñoz, Departamento de Finanzas">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Guido Muñoz</h2>
                                    <p>Departamento de Finanzas</p>
                                    <p>Argentina</p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- segundo item -->
                    <div class="item">
                        <div tabindex="35" class="col-sm-3 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="images/team/jose-clautier.jpg" class="img-responsive" alt="Jose Clautier, Relaciones Institucionales">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Jose Clautier</h2>
                                    <p>Relaciones Institucionales</p>
                                    <p>El Salvador</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="36" class="col-sm-3 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="images/team/beatriz-calvo.jpg" class="img-responsive" alt="Beatriz Calvo, Diseño e ilustraciones">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Beatriz Calvo</h2>
                                    <p>Diseño e ilustraciones</p>
                                    <p>Costa Rica</p>
                                </div>
                            </div>
                        </div>


                        <div tabindex="37" class="col-sm-3 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="images/team/roberto-jaramillo.jpg" class="img-responsive" alt="Roberto Jaramillo, Asesoría en Emprendimiento">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Roberto Jaramillo</h2>
                                    <p>Asesoría en Emprendimiento</p>
                                    <p>Ecuador</p>
                                </div>
                            </div>
                        </div>
                        <div tabindex="38" class="col-sm-3 col-xs-6">
                            <div class="team-single-wrapper">
                                <div class="team-single">
                                    <div class="person-thumb">
                                        <img src="images/team/pablo-ojeda.jpg" class="img-responsive" alt="Pablo Ojeda, Desarrollador Web">
                                    </div>

                                </div>
                                <div class="person-info">
                                    <h2>Pablo Ojeda</h2>
                                    <p>Desarrollador Web</p>
                                    <p>Chile</p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <!-- Controls -->
                <a aria-hidden="true" class="left team-carousel-control hidden-xs" href="#team-carousel" data-slide="prev">left</a>
                <a aria-hidden="true" class="right team-carousel-control hidden-xs" href="#team-carousel" data-slide="next">right</a>
            </div>
        </div>
    </div>
</section>
<!--/#team-->

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center ">
                <img src="images/home/under.png" class="img-responsive inline" alt="">
            </div>

            <div class="col-sm-2" style="text-align: center;"> 
                <a href="index_es.php" tabindex="85">INICIO</a>
            </div>
            <div class="col-sm-2" style="text-align: center;"> 
                <a href="index_es.php#quienes-somos" tabindex="86">¿QUIÉNES SOMOS?</a>
            </div>
            <div class="col-sm-2" style="text-align: center;"> 
                <a href="index_es.php#servicios" tabindex="87">SERVICIOS</a>
            </div>
            <div class="col-sm-2" style="text-align: center;"> 
               <a href="index_es.php#involucrate" tabindex="88">INVOLÚCRATE</a>
           </div>
           <div class="col-sm-2" style="text-align: center;"> 
            <a href="index_es.php#equipo" tabindex="89">EQUIPO</a>
        </div>
        <div class="col-sm-2" style="text-align: center;"> 
            <a href="contacto_es.php" tabindex="90">CONTACTO</a>
        </div>


        <div class="col-sm-12">
            <div class="copyright-text text-center">
                <p>&copy; Fundación Comparlante 2016.</p>
                <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
            </div>
        </div>
    </div>
</div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/main_es.js"></script>   
</body>


</html>
