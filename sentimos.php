<!DOCTYPE html>
<html lang="es">


<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Sentimos retrata a personas con discapacidad desarrollando diferentes actividades en plenitud con el objetivo de derribar mitos, estereotipos negativos y prejuicios que generan discriminación.">
  <meta name="author" content="Prime Developers Chile">

  <!-- Facebook Metadatos | Diseño accesible -->
  <meta property="og:title" content="Fundación Comparlante | Arte Accesible"/>
  <meta property="og:site_name" content="Arte Accesible"/>
  <meta property="og:description" content="Sentimos retrata a personas con discapacidad desarrollando diferentes actividades en plenitud con el objetivo de derribar mitos, estereotipos negativos y prejuicios que generan discriminación." />
  <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/programas/sentimos-logo.png"/>
  <!--   <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/diseno-grafico.php"/>  -->





  <title>Fundación Comparlante</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet"> 
  <link href="css/lightbox.css" rel="stylesheet"> 
  <link href="css/main.css" rel="stylesheet">
  <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
      <![endif]-->       
      <link rel="shortcut icon" href="images/ico/logo-icon.png">
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
      <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">
      <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WRVC32B');</script>
    <!-- End Google Tag Manager -->
  </head><!--/head-->

  <body>

    <!--#include file="header.html"-->
    <?php include("header.php"); ?>

    <section id="page-breadcrumb">
      <div class="vertical-center sun">
       <div class="container">
        <div class="row">
          <div class="project-info overflow">
            <center>
              <img src="images/programas/sentimos-logo.png" class="img-responsive" width="40%" alt="Logo Sentimos.">
            </center>
          </div>
        </div>
      </div>
    </div>
  </section>
  

  <section id="portfolio-information" >
    <div class="container">
      <br><br>

      <br>
      <div class="row">

        <div class="col-sm-12">

          <div tabindex="11" class="project-info overflow " style="text-align:justify">
            <h2>Sentimos retrata a personas con discapacidad desarrollando diferentes actividades en plenitud con el objetivo de derribar mitos, estereotipos negativos y prejuicios que generan discriminación.</h2> 


          </div>


        </div>
      </div>
      <div class="row">

        <div class="col-sm-12">
          <br>
          <div tabindex="14" class="project-info overflow " style="text-align:justify">
            <h2>Introducción.</h2> 
            <audio tabindex="15" src="images/programas/sentimos/audios/PRESENTACION.mp3" preload="metadata" controls></audio>

          </div>


        </div>
      </div>


      <div class="row" style="padding-top: 5%;">

        <div class="col-sm-12">

          <div  class="project-info overflow " style="text-align:justify">
            <h2 style="margin-bottom: 30px;" tabindex="75">Sentimos cuenta con el apoyo de:</h2> 

            <div class="row">
             <div class="col-sm-3" data-wow-duration="500ms" data-wow-delay="900ms">
               <center>
                <img style="max-width: 250px" tabindex="77" alt="Logotipo de pollination project." src="images/programas/sentimos/pollinationproject.jpg">
              </center>
            </div>

            <div class="col-sm-3" data-wow-duration="500ms" data-wow-delay="900ms">
             <center>
              <img style="max-width: 250px" tabindex="78" alt="Logotipo de la honorable cámara de diputados de la provincia de Buenos Aires." src="images/programas/sentimos/camaradiputados.png" >
            </center>
          </div>


          <div class="col-sm-3" data-wow-duration="500ms" data-wow-delay="900ms">
           <center>
            <img style="max-width: 250px" tabindex="79" alt="Logotipo del honorable senado de la provincia de Buenos Aires." src="images/programas/sentimos/senado_buenos_aires.jpg" >
          </center>
        </div>


        <div class="col-sm-3" data-wow-duration="500ms" data-wow-delay="900ms">
         <center>
          <img style="max-width: 250px" tabindex="82" alt="Logotipo de la Caja de Abogados de la provincia de Buenos Aires." src="images/programas/sentimos/caja_abogado_buenos_aires.jpg" >
        </center>
      </div>


      



    </div>

    <br>

    <div class="row">

      <div class="col-sm-3" data-wow-duration="500ms" data-wow-delay="900ms">
       <center>
        <img style="max-width: 250px" tabindex="82" alt="Logotipo de la Municipalidad de Rauch." src="images/programas/sentimos/rauch.jpg" >
      </center>
    </div>


      <div class="col-sm-3" data-wow-duration="500ms" data-wow-delay="900ms">
       <center>
        <img style="max-width: 250px" tabindex="85" alt="Logotipo de la Facultad de periodismo y comunicación social de la universidad nacional de la plata." src="images/programas/sentimos/logo-periodismo-la-plata.jpg" >
      </center>
    </div>

    <div class="col-sm-3" data-wow-duration="500ms" data-wow-delay="900ms">
     <center>
      <img style="max-width: 250px" tabindex="87" alt="Logotipo de Carla Coria fotografías." src="images/programas/sentimos/carlacoria.png" >
    </center>
  </div>


  <div class="col-sm-3" data-wow-duration="500ms" data-wow-delay="900ms">
   <center>
    <img style="max-width: 250px" tabindex="89" alt="Logotipo de Medi Home." src="images/programas/sentimos/medihome.png" >
  </center>
</div>

</div>

<div class="row">
<div class="col-sm-3" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <img style="max-width: 250px" tabindex="85" alt="Logotipo de biblioteca de braile y parlante de Buenos Aires, Argentina." src="images/programas/sentimos/bbraile.jpg" >
</center>
</div>

<div class="col-sm-3" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <img style="max-width: 100px" tabindex="92" alt="Logotipo de Taxi FM radio." src="images/programas/sentimos/taxifm.jpg" >
</center>
</div>

</div>





</div>
</div>


</div>
<br>
<!-- imagenes galeria -->
<div class=" row wow scaleIn">

  <h2 style="margin-bottom: 30px;" tabindex="75">Fotografías de la muestra:</h2> 


  <div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
   <center>
    <h2>Fotografía 1 - Lelio 1.</h2>

    <a alt="click para ver imagen en tamaño completo" href="images/programas/sentimos/1- Lelio.jpg" data-lightbox="image-1" data-title="Fotografía 1 - Lelio 1" tabindex="17">
     <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" src="images/programas/sentimos/1- Lelio.jpg" >
   </a>
   <audio class="audio-sentimos" tabindex="18" src="images/programas/sentimos/audios/1.mp3" preload="metadata" controls></audio>
 </center>
</div>

<div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <h2>Fotografía 2 - Lelio 2.</h2> 
  <a alt="click para ver imagen en tamaño completo" tabindex="20" href="images/programas/sentimos/2- Lelio.jpg" data-lightbox="image-2" data-title="Fotografía 2 - Lelio 2">
   <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" src="images/programas/sentimos/2- Lelio.jpg" >
 </a>
 <audio class="audio-sentimos" tabindex="21" src="images/programas/sentimos/audios/2.mp3" preload="metadata" controls></audio>
</center>
</div>


<div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <h2>Fotografía 3 - Lelio 3.</h2>

  <a alt="click para ver imagen en tamaño completo" tabindex="23" href="images/programas/sentimos/3- Lelio.jpg" data-lightbox="image-3" data-title="Fotografía 3 - Lelio 3">
    <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" src="images/programas/sentimos/3- Lelio.jpg" >
  </a>
  <audio class="audio-sentimos" tabindex="24" src="images/programas/sentimos/audios/3.mp3" preload="metadata" controls></audio>
</center>
</div>

<div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <h2>Fotografía 4 - Lelio 4.</h2>

  <a alt="click para ver imagen en tamaño completo" tabindex="26" href="images/programas/sentimos/4- Lelio.jpg" data-lightbox="image-4" data-title="Fotografía 4 - Lelio 4">
    <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" src="images/programas/sentimos/4- Lelio.jpg" >
  </a>
  <audio class="audio-sentimos" tabindex="27" src="images/programas/sentimos/audios/4.mp3" preload="metadata" controls></audio>
</center>
</div>

<div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <h2>Fotografía 5 - Guadalupe 1.</h2>

  <a alt="click para ver imagen en tamaño completo" tabindex="29" href="images/programas/sentimos/5- Guadalupe.jpg" data-lightbox="image-5" data-title="Fotografía 5 - Guadalupe 1">
    <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" src="images/programas/sentimos/5- Guadalupe.jpg" >
  </a>
  <audio class="audio-sentimos" tabindex="30" src="images/programas/sentimos/audios/5.mp3" preload="metadata" controls></audio>
</center>
</div>

<div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <h2>Fotografía 6 - Guadalupe 2.</h2>

  <a alt="click para ver imagen en tamaño completo" tabindex="32" href="images/programas/sentimos/6- Guadalupe.jpg" data-lightbox="image-6" data-title="Fotografía 6 - Guadalupe 2">
    <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" src="images/programas/sentimos/6- Guadalupe.jpg" >
  </a>
  <audio class="audio-sentimos" tabindex="33" src="images/programas/sentimos/audios/6.mp3" preload="metadata" controls></audio>
</center>
</div>

<div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <h2>Fotografía 7 - Guadalupe 3.</h2>

  <a alt="click para ver imagen en tamaño completo" tabindex="35" href="images/programas/sentimos/7- Guadalupe.jpg" data-lightbox="image-7" data-title="Fotografía 7 - Guadalupe 3">
    <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" src="images/programas/sentimos/7- Guadalupe.jpg" >
  </a>
  <audio class="audio-sentimos" tabindex="36" src="images/programas/sentimos/audios/7.mp3" preload="metadata" controls></audio>
</center>
</div>

<div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <h2>Fotografía  8 - Sandra 1.</h2>

  <a alt="click para ver imagen en tamaño completo" tabindex="37" href="images/programas/sentimos/8- Sandra.jpg" data-lightbox="image-8" data-title="Fotografía  8 - Sandra 1 
  ">
  <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" src="images/programas/sentimos/8- Sandra-2.jpg" >
</a>
<audio class="audio-sentimos" tabindex="38" src="images/programas/sentimos/audios/8.mp3" preload="metadata" controls></audio>
</center>
</div>

<div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <h2>Fotografía 9 - Sandra  2.</h2>

  <a alt="click para ver imagen en tamaño completo"  tabindex="40" href="images/programas/sentimos/9- Sandra.jpg" data-lightbox="image-9" data-title="Fotografía 9 - Sandra  2">
    <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" src="images/programas/sentimos/9- Sandra.jpg" >
  </a>
  <audio class="audio-sentimos" tabindex="41" src="images/programas/sentimos/audios/9.mp3" preload="metadata" controls></audio>
</center>
</div>

<div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <h2>Fotografía 10 - Manuela 1.</h2>

  <a alt="click para ver imagen en tamaño completo" tabindex="43" href="images/programas/sentimos/10- Manuela.jpg" data-lightbox="image-10" data-title="Fotografía 10 - Manuela 1">
    <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" src="images/programas/sentimos/10- Manuela-2.jpg" >
  </a>
  <audio class="audio-sentimos" tabindex="44" src="images/programas/sentimos/audios/10.mp3" preload="metadata" controls></audio>
</center>
</div>

<div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <h2>Fotografía 11 - Manuela 2.</h2>

  <a alt="click para ver imagen en tamaño completo" tabindex="46" href="images/programas/sentimos/11- Manuela.jpg" data-lightbox="image-11" data-title="Fotografía 11 - Manuela 2">
    <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" src="images/programas/sentimos/11- Manuela.jpg" >
  </a>
  <audio class="audio-sentimos" tabindex="47" src="images/programas/sentimos/audios/11.mp3" preload="metadata" controls></audio>
</center>
</div>

<div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <h2>Fotografía 12 - Manuela 3.</h2>

  <a alt="click para ver imagen en tamaño completo" tabindex="49" href="images/programas/sentimos/12- Manuela.jpg" data-lightbox="image-12" data-title="Fotografía 12 - Manuela 3" >
    <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" src="images/programas/sentimos/12- Manuela.jpg" >
  </a>
  <audio class="audio-sentimos" tabindex="50" src="images/programas/sentimos/audios/12.mp3" preload="metadata" controls></audio>
</center>
</div>

<div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <h2>Fotografía 13 - Nicolás 1.</h2>

  <a alt="click para ver imagen en tamaño completo" tabindex="52" href="images/programas/sentimos/13- Nico.jpg" data-lightbox="image-13" data-title="Fotografía 13 - Nicolás 1">
    <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" src="images/programas/sentimos/13- Nico.jpg" >
  </a>
  <audio class="audio-sentimos" tabindex="53" src="images/programas/sentimos/audios/13.mp3" preload="metadata" controls></audio>
</center>
</div>

<div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <h2>Fotografía 14 - Nicolás 2.</h2>

  <a alt="click para ver imagen en tamaño completo" tabindex="55" href="images/programas/sentimos/13- Nico.jpg" data-lightbox="image-14" data-title="Fotografía 14 - Nicolás 2">
    <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" src="images/programas/sentimos/14- Nico.jpg" >
  </a>
  <audio class="audio-sentimos" tabindex="56" src="images/programas/sentimos/audios/14.mp3" preload="metadata" controls></audio>
</center>
</div>

<div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <h2>Fotografía 15 - Nicolás 3.</h2>

  <a alt="click para ver imagen en tamaño completo" tabindex="58" href="images/programas/sentimos/15- Nico.jpg" data-lightbox="image-15" data-title="Fotografía 15 - Nicolás 3">
    <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" src="images/programas/sentimos/15- Nico.jpg" >
  </a>
  <audio class="audio-sentimos" tabindex="59" src="images/programas/sentimos/audios/15.mp3" preload="metadata" controls></audio>
</center>
</div>

<div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <h2>Fotografía 16 - Nahuel 1.</h2>

  <a alt="click para ver imagen en tamaño completo" tabindex="61" href="images/programas/sentimos/16- Nahuel.jpg" data-lightbox="image-16" data-title="Fotografía 16 - Nahuel 1">
    <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" src="images/programas/sentimos/16- Nahuel.jpg" >
  </a>
  <audio class="audio-sentimos" tabindex="62" src="images/programas/sentimos/audios/16.mp3" preload="metadata" controls></audio>
</center>
</div>

<div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <h2>Fotografía 17 - Nahuel 2.</h2>

  <a alt="click para ver imagen en tamaño completo" tabindex="64" href="images/programas/sentimos/17- Nahuel.jpg" data-lightbox="image-17" data-title="Fotografía 17 - Nahuel 2">
    <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" src="images/programas/sentimos/17- Nahuel.jpg" >
  </a>
  <audio class="audio-sentimos" tabindex="65" src="images/programas/sentimos/audios/17.mp3" preload="metadata" controls></audio>
</center>
</div>

<div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <h2>Fotografía 18 - Nahuel 3.</h2>

  <a alt="click para ver imagen en tamaño completo" tabindex="68" href="images/programas/sentimos/18- Nahuel.jpg" data-lightbox="image-18" data-title="Fotografía 18 - Nahuel 3">
    <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" src="images/programas/sentimos/18- Nahuel.jpg" >
  </a>
  <audio class="audio-sentimos" tabindex="69" src="images/programas/sentimos/audios/18.mp3" preload="metadata" controls></audio>
</center>
</div>

<div class="col-sm-6" data-wow-duration="500ms" data-wow-delay="900ms">
 <center>
  <h2>Fotografía 19 - Nahuel 4.</h2>

  <a alt="click para ver imagen en tamaño completo" tabindex="71" href="images/programas/sentimos/19- Nahuel.jpg" data-lightbox="image-19" data-title="Fotografía 19 - Nahuel 4">
    <img style="max-width: 450px" alt="click para ver imagen en tamaño completo" a src="images/programas/sentimos/19- Nahuel.jpg" >
  </a>
  <audio class="audio-sentimos" tabindex="72" src="images/programas/sentimos/audios/19.mp3" preload="metadata" controls></audio>
</center>
</div>


</div> 


</section>

<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center bottom-separator">

      </div>

                    <!-- <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                    </div> -->
  <!--                 <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                        <h2>Envíanos un mensaje</h2>
                        <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php">
                            <div class="form-group">
                                <input tabindex="12" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Nombre">
                            </div>
                            <div class="form-group">
                                <input tabindex="13" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                            </div>
                            <div class="form-group">
                                <textarea tabindex="13" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Tu mensaje"></textarea>
                            </div>            
                            <div style="display:none"> 
                                <input id="cc" value="" placeholder="E-mail"> 
                            </div>             
                            <div class="form-group">
                                <button tabindex="13" id="send-button" name="submit" class="btn btn-submit"> Enviar mensaje</button>
                            </div>
                        </form>
                    </div>
                  </div> -->
                  <div class="col-sm-12"><br>
                    <div class="copyright-text text-center"><br>
                      <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                      <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                    </div>
                  </div>
                </div>
              </div>
            </footer>
            <!--/#footer-->

            <script type="text/javascript" src="js/jquery.js"></script>
            <script type="text/javascript" src="js/bootstrap.min.js"></script>
            <script type="text/javascript" src="js/lightbox.min.js"></script>
            <script type="text/javascript" src="js/wow.min.js"></script>
            <script type="text/javascript" src="js/main_es.js"></script>   
          </body>


          </html>
