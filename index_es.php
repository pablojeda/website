<!DOCTYPE html>
<html lang="es">


<script type="text/javascript">
  window.onload = setTimeout(function() {
    document.getElementById("instrucciones-de-uso").focus();

  },1500);

</script>

<head>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <meta name="description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad.">
 <meta name="author" content="Prime Developers Chile">
 <meta name="title" content="Fundación Comparlante"/>

 <!-- Facebook Metadatos -->
 <!--  Inicio -->
 <meta property="og:title" content="Fundación Comparlante | Inicio"/>
 <meta property="og:site_name" content="Fundación Comparlante"/>
 <meta property="og:description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad." />
 <meta property="og:image" content="http://comparlante.com/images/home/logo-fundacion-2.jpg"/>
  <meta prperty="og:image:alt" content= "Logo de Fundación Comparlante, auriculares/audífonos unidos formando una flor, en naranja, rojo, celeste, azúl y verde"/>
 <meta property="og:url" content="http://www.comparlante.com/index_es.php"/>

 <title>Fundación Comparlante</title>
 <link href="css/bootstrap.min.css" rel="stylesheet">
 <link href="css/font-awesome.min.css" rel="stylesheet">
 <link href="css/animate.min.css" rel="stylesheet"> 
 <link href="css/lightbox.css" rel="stylesheet"> 
 <link href="css/main.css" rel="stylesheet">
 <link href="css/responsive.css" rel="stylesheet">


 <link rel="shortcut icon" href="images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
 <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">
 <!-- Google Tag Manager -->
 <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
 j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head>

<body>
  <?php include("header.php"); ?>
  <div style="display:none" id="mensaje-inicial"><p id="instrucciones-de-uso">La página de fundación Comparlante tiene una navegación vertical, En dispositivos móviles debes deslizarte con el dedo hacia la derecha o izquierda.</p> </div>  
  <section id="home-slider">
    <div class="container">
      <div class="row">
        <div class="main-slider">
          <div class="slide-text" tabindex="4">
            <h1>Fundación Comparlante</h1>
            <h2>¡Bienvenidos a nuestra comunidad!</h2>
            <!-- <a href="#" class="btn btn-common">SIGN UP</a> -->
          </div>
          <img src="images/home/slider/1.png" class="slider-hill" alt="Al fondo una montaña con un sol y pájaros de colores entre las nubes.">
          <img src="images/home/slider/2.png" class="slider-house" alt="Un edificio rodeado de árboles, en la esquina izquierda una persona en silla de ruedas y otra utilizando bastón.">
          <img src="images/home/slider/3.png" class="slider-sun" alt="Cuatro personas felices frente al edificio, uno en silla de ruedas, otro utilizando bastón, ambos están acompañados de un hombre y una mujer.">

        </div>
      </div>
    </div>
    <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
  </section>
  <!--/#home-slider-->

  <!-- ¿Quienes Somos? -->
  <section id="quienes-somos"  class="padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
    <div class="container">
      <div class="row">
        <h1 class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms" tabindex="5">¿Quiénes somos?</h1>

        <div class="col-sm-12 padding-top-index text-justify" tabindex="6">

         Fundación Comparlante es una organización sin fines de lucro que promueve el desarrollo y la
         inclusión de las personas con discapacidad en América Latina. A partir de herramientas tecnológicas,
         productos y servicios promovemos la accesibilidad, el emprendimiento y la equidad. Nuestro
         objetivo es lograr que este sector vulnerable de la sociedad tenga pleno goce de sus derechos.
       </div>
     </div>
   </div>
   <br>
 </section>

 <!-- banner mi mundo a mi manera 2 -->
 <section id="quienes-somos"  class="padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
  <div class="container">
    <div class="row">

      <div  tabindex="8" class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
        <center>
          <a href="mi-mundo-a-mi-manera-2020.php">
            <img width="90%" src="images/concurso/banner mi mundo a mi manera-01.jpg" alt="Los 3 personajes del concurso al rededor del texto que dice: Gracias a todos los pequeños escritores que participaron en MI MUNO A MI MANERA. Argentina, Bolivia, Chile, Colombia, Cuba, Ecuador, El Salvador, España, Guatemala, Honduras, México, PAraguay, Perú, Uruguay, Venezuela">
          </a>
        </center>
      </div>
      
    </div>
  </div>
  <br>
</section>

<!-- sección de las 3 lineas de comparlante -->
<section id="programas" class=" wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
  <div class="container">
    <div class="row">
     <!-- accesibilidad que transforma -->
     <div tabindex="10" class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms">
      <a href="accesibilidad-que-transforma.php" style="color:#000;">
        <div   style="background-color: #f6f6f6; height: 500px;">
          <br>
          <div  class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
            <img src="images/programas/accesibilidadquetransforma.png" alt="Cuatro manos sobrepuestas que representan el trabajo en equipo">
          </div>
          <div class="row" style="margin-left: 8%; margin-right: 8%;">
            <div tabindex="12" class="row" style="height: 65px;">
              <h2><b>Accesibilidad que transforma</b></h2>
            </div>
            <br>
            <div class="row" tabindex="14">
              <p>Lograr un mundo con menos barreras requiere del compromiso de todos. Ayudamos a empresas, Gobiernos, ONGs y privados a desarrollar y adecuar sus prácticas y servicios bajo estándares internacionales de accesibilidad.</p> 
            </div>

          </div>

        </div>
      </a>

    </div>
    <!-- arte para promover la empatía -->
    <div tabindex="16" class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms" style="padding-left: 15px;">
      <a href="arte-empatia.php" style="color:#000;">

        <div   style="background-color: #f6f6f6; height: 500px;">
          <br>
          <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
            <img src="images/programas/arte.png" alt="Un plano, un lápiz y una escuadra, herramientas utilizadas en arte.">
          </div>
          <div  class="row" style="margin-left: 8%; margin-right: 8%;">
            <div class="row" style="height: 65px;">

              <h2 tabindex="18"><b>Arte para promover la empatía</b></h2>
            </div>
            <br>
            <div tabindex="20" class="row">
              <p>Una pieza artística puede movilizar nuestros sentidos y hacernos entender cómo viven el mundo las personas con discapacidad, sus derechos, necesidades y las barreras a las que se enfrentan a diario.</p> 
            </div>



          </div>

        </div>
      </a>
    </div>
    <!-- emprendimiento, independencia y dignidad -->
    <div tabindex="22" class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms" style="padding-left: 15px;">
      <a  href="emprendimiento.php" style="color:#000;">

        <div tabindex="24"  style="background-color: #f6f6f6; height: 500px;">
          <br>
          <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
            <img src="images/programas/emprendimiento.png" alt="Una ampolleta que representa las ideas">
          </div>
          <div class="row" style="margin-left: 8%; margin-right: 8%;">
            <div class="row" style="height: 65px;">

              <h2 tabindex="26"><b>Emprendimiento, independencia y dignidad </b></h2>
            </div>
            <br>
            <div tabindex="28" class="row">
              <p>El emprendimiento es una herramienta que promueve el bienestar social, el desarrollo económico y empoderamiento de las personas con discapacidad. Además de reforzar el sentimiento de dignidad y promover su independencia. </p> 
            </div>

          </div>

        </div>
      </a>
    </div>
    <!-- fin emprendimiento -->
  </div>  
</div>
</section>

<section>

  <div class="container">
    <div class="col-sm-12">
      <div class="col-sm-6">
       <b>Globalization and local Action - Globalization y Acción Local | Sebastian Flores | TEDxQuito</b>
       <iframe width="100%" height="315" src="https://www.youtube.com/embed/myeooeE6ack" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
     </div>

     <div class="col-sm-6">
       <b>“Accessibility is in our hands” | kanthari TALKS 2018 | Lorena Julio | Argentina</b>
       <iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/euC31Kw-92Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
     </div>
   </div>
 </div>

</section>

<!-- Noticias -->
<section id="noticias">
  <div class="container">
    <div class="row">
      <div tabindex="30">
        <h1 class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Noticias y anuncios</h1>
        
      </div>
      <?php
                // include database connection
                include 'config_news/database.php';
                
                
                // select all data
                $query = "SELECT id,idioma, imagen, titulo,altText, publica, fechaorigen, contenido FROM noticias WHERE publica = 1 ORDER BY fechaorigen DESC LIMIT 3";
                $stmt = $con->prepare($query);
                $stmt->execute();
                
                // this is how to get number of rows returned
                $num = $stmt->rowCount();
               
                
                //check if more than 0 record found
                if($num>0){
                     
                        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                            
                            extract($row);
                            setlocale(LC_TIME,"es_ES.UTF-8");
                            echo "
                            <div class='col-sm-4 padding wow fadeIn' data-wow-duration='1000ms' data-wow-delay='300ms'>
                            <div tabindex='32' class=''>
                              
                    
                              <div tabindex='33' class='wow ' data-wow-duration='500ms' data-wow-delay='300ms' style='height:50px'>
                              <a tabindex='34' href='https://{$contenido}' target='_blank'>
                                <img class='noticias' src='./news_img/${imagen}' alt='{$titulo}'>
                              </a>
                              </div>
                              
                    
                              <p style='text-align:left; padding-right:30px'>{$titulo} </p>
                              <p> <a tabindex='34' type='button' href='https://{$contenido}' class='btn btn-info' target='_blank'>Ver noticia</a> </p>
                            </div>
                          </div>"            ;
                            
                             
                            
                        }
                    
                    // end table
                    echo "</table>";
                    
                }

                // if no records found
                else{
                    echo "<div class='alert alert-danger'>No encontramos noticias.</div>";
                }
                ?>

      </div>
      <h3 style="text-align: center;"><a href="http://www.comparlante.com/noticias.php" class="btn btn-warning btn-lg">Más noticias</a></h3>
      <br>
      <br>

    </div>
  </section>

  <!-- newsletter -->
  <section>
    <div class="container">

      <p> 
        <b>Accede a nuestros newsletters  :</b>   
        <a tabindex="42" type="button" href="https://mailchi.mp/53f22650d11f/nuestro-increble-2018-our-amazing-2018
        " class="btn btn-info" style="background-color: #252983;" target="_blank">DICIEMBRE 2018</a>  <a tabindex="42" type="button" href="https://mailchi.mp/d0150d241b4e/comenzamos-el-ao-con-buenas-noticias
        " class="btn btn-info" style="background-color: #252983;" target="_blank">ABRIL 2019</a> 
        <a tabindex="42" type="button" href="https://mailchi.mp/0eace2fd861d/comparlante-newsletter-2019
        " class="btn btn-info" style="background-color: #252983;" target="_blank">DICIEMBRE 2019</a> 
        <a tabindex="42" type="button" href="https://mailchi.mp/7da75590ed4d/comparlante-newsletter-2734852" class="btn btn-info" style="background-color: #252983;" target="_blank">JUNIO 2020</a> 
        <a tabindex="42" type="button" href="https://mailchi.mp/c6879f34853d/2020-un-ao-de-retos-y-grandes-oportunidadesa-year-of-challenges-and-big-opportunities" class="btn btn-info" style="background-color: #252983;" target="_blank">DICIEMBRE 2020</a>
      </p>

    </div>

  </section>
  <!--  -->
  <!-- <section id="incluyes"  class="padding">
    <div class="row  padding" style="background-color: #f6f6f6;">
      <div class="container" tabindex="46" >
        <div class="row">
          <div class="wow scaleIn " data-wow-duration="500ms" data-wow-delay="900ms">
            <center>
              <a href="https://www.incluyes.com" tabindex="48" aria-label="Logo de plataforma IncluYes, la palabra Inclu de color morado y Yes de color amarillo. si presionas serás redireccionado al sitio web de incluYes.com">

                <img src="images/programas/incluyes-morado.png" width="40%" alt="Logo de plataforma IncluYes, la palabra Inclu de color morado y Yes de color amarillo.">
              </a>
            </center>
          </div>

          <div class="col-sm-12 padding-top-index text-justify" tabindex="50">

           Las nuevas tecnologías deben estar al servicio de todos, por eso creamos esta plataforma accesible de oferta y demanda de productos y servicios para las personas con discapacidad. Promovemos alternativas en servicios profesionales cuyas metodologías, formatos y procesos de implementación se dan bajo estándares internacionales de accesibilidad generando equidad en el acceso a oportunidades. 
         </div>
       </div>
     </div>
   </div>

 </section>
 -->
  




 <!-- involucrate -->
 <section id="involucrate">
  <div class="container">
    <div class="row">
      <div tabindex="52">
        <h1 class="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Involúcrate</h1>
        <p class="text-center wow fadeInDown" data-wow-duration="400ms" data-wow-delay="400ms">Comparlante es un proyecto colectivo que se nutre y sostiene gracias a personas que quieren ayudar a vivir en un mundo más accesible, personas que quieren innovar y ser agentes de cambio.</p>
      </div>
      <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
        <div tabindex="54" class="single-service">
          <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
            <img tabindex="55" src="images/services/unete.png" alt="Iconos de personas de colores que representan un equipo.">
          </div>
          <h2>Únete a nosotros</h2>
          <p> ¿Te interesa ser parte del equipo? </p>
          <p>¿Crees que tienes habilidades y te gustaría brindar algunos de nuestros servicios?</p>
          <p> <a tabindex="56" type="button" href="contacto_es.php" class="btn btn-info">Escríbenos</a> </p>
        </div>
      </div>
      <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
        <div  tabindex="58"class="single-service">
          <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
            <img  tabindex="59" src="images/services/apoyanos.png" alt="Icono de alcancía de cerdito azul.">
          </div>
          <h2>Apoya nuestro proyecto</h2>
          <p>Con tu donación nos ayudas a construir un mundo cada vez más accesible.</p>

          <a href="donar.php" target="_blank">
           <img alt="icono de comparlante que en su centro dice Donar" border="0" tabindex="60" src="images/services/donar.jpg"  >

         </a> 

       </div>
     </div>
     <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms">
      <div tabindex="62" class="single-service">
        <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
          <img tabindex="63" src="images/services/idea.png" alt="Icono de ampolleta que representa una idea">
        </div>
        <h2>¿Tienes una idea?</h2>
        <p>Tienes una propuesta que puede contribuir a generar accesibilidad para personas con discapacidad y  quisieras contar con un equipo que apoye una iniciativa.</p> <p><a tabindex="64" type="button" href="contacto_es.php" class="btn btn-info">Escríbenos </a> </p>
      </div>
    </div>
  </div>
</div>
</section>
<!--/#services--> 



<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center bottom-separator">

      </div>
<!-- 
            <div class="col-md-5 col-sm-6">
                <div class="contact-info bottom">
                  <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

              </div>
            </div> -->
            <div class="col-md-12 col-sm-12">
              <div id="contacto" class="contact-form bottom">
                <h2>Envíanos un mensaje</h2>
                <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php">
                  <div class="form-group">
                    <input tabindex="70" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Nombre">
                  </div>
                  <div class="form-group">
                    <input tabindex="71" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                  </div>
                  <div class="form-group">
                    <textarea tabindex="72" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Tu mensaje"></textarea>
                  </div>            
                  <div style="display:none"> 
                    <input id="cc" value="sebastian@comparlante.com" placeholder="E-mail"> 
                  </div>             
                  <div class="form-group">
                    <button tabindex="73" id="send-button" name="submit" class="btn btn-submit"> Enviar mensaje</button>
                  </div>
                </form>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="copyright-text text-center">
                <p>&copy; Fundación Comparlante <?php echo date('Y') ?>.</p>
                <p>Desarrollado por <a tabindex="110" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <!--/#footer-->

      <script type="text/javascript" src="js/jquery.js"></script>
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <script type="text/javascript" src="js/lightbox.min.js"></script>
      <script type="text/javascript" src="js/wow.min.js"></script>
      <script type="text/javascript" src="js/main_es.js"></script>   
    </body>


    </html>
