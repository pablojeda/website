<!DOCTYPE html>
<html lang="es">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad.">
    <meta name="author" content="Prime Developers Chile">
    
    <!-- Facebook Metadatos | Diseño web accesible -->
    <meta property="og:title" content="II Concurso literario para niñas y niños de educación básica Mi mundo a mi manera"/>
    <meta property="og:site_name" content="II Concurso literario para niñas y niños de educación básica Mi mundo a mi manera"/>
    <meta property="og:description" content="Mi Mundo a Mi Manera es un Concurso Literario Internacional que promueve la concientización sobre las personas con discapacidad en el ámbito escolar. Propone, a través de la literatura, ofrecer un espacio de reflexión en la sociedad. 
    " />
    <meta property="og:image" content="http://comparlante.com/concurso/banner-mi-mundo-a-mi-manera-2020-jpeg"/>


    
    
    <title>Fundación Comparlante</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head><!--/head-->

<body>
  <?php include("header.php"); ?>

  <section id="page-breadcrumb">
    <div class="vertical-center sun">
       <div class="container">
        <div class="row">
          <center>
            <img width="50%" src="images/concurso/mi-mundo-a-mi-manera2.jpeg" class="img-responsive " alt="Ilustración de los 3 personajes animados de Mi Mundo A Mi Manera 2: niña con parálisis cerebral, niña con amputación de miembro superior/muñón y niño con Trastornos del Espectro Autista (TEA) respectivamente."></center>
        </div>
        <div class="row">
            <div class="action">
                <div tabindex="10" class="col-sm-12">
                    <h2 style="font-size: 20pt; font-weight: 400" class="title text-center">“Mi Mundo A Mi Manera II” CONCURSO LITERARIO PARA NIÑAS Y NIÑOS DE EDUCACIÓN BÁSICA
                    </h2>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!--/#page-breadcrumb-->
<div id="zooming">

    <section id="concurso" >
        <div class="container"> <br><br>
            <div class="project-info overflow">

            </div><br>
            <div class="row">

                <div class="col-sm-12">

                    <div tabindex="11" class="project-info overflow "style="text-align:justify">

                        <h2>Desde Fundación Comparlante entendemos que concientizar a la sociedad sobre las distintas discapacidades 
                            promueve la equidad y la literatura es una herramienta muy poderosa que nos permite trabajar con los niños. <br>
                         <br>
                         En esta segunda edición del concurso "Mi mundo a mi manera", los pequeños de entre 6 y 13 años de edad
                         tuvieron la oportunidad de desarrollar un relato que involucre a, como mínimo, 
                         uno de los tres personajes propuestos por Fundación Comparlante. </h2>

                 </div>

                <br>
                <!-- primer personaje -->
                <div tabindex="15" class="project-info overflow" style="text-align:justify">
                    <h2>Nuestro 1er personaje es un niño con autismo:</h2>
                    <h3>
                        <div  class="col-sm-3">
                            <img src="images/concurso/1p.png"  class="img-responsive" alt="Personaje con trastorno del Espectro Autista (TEA)"> 
                        </div> 
                        <br> <br><h3>
                            El Trastorno del Espectro Autista (TEA) es una condición del neurodesarrollo que se caracteriza por la presencia de alteraciones en 3 áreas: comunicación, interacción social y percepción sensorial.<br><br>
                            Las personas con TEA pueden presentar patrones de comportamiento como intereses restringidos y actividades repetitivas, con dificultades para hacer frente a cambios inesperados.
                        </h3>
                    </div> 
                    <!-- segundo personaje -->
                    <div tabindex="16" class="project-info overflow" style="text-align:justify">
                        <h2>Nuestro 2do personaje es una niña con parálisis cerebral:</h2>
                        <h3>
                            <div  class="col-sm-3">
                                <img src="images/concurso/2p.png"  class="img-responsive" alt="Personaje con parálisis cerebral infantil"> 
                            </div> 
                            <br> <br>
                            La parálisis cerebral infantil (así definida por la etapa del desarrollo humano donde se origina) es un grupo de trastornos que afectan la capacidad de una persona para moverse voluntariamente de forma coordinada, manteniendo el equilibrio y la postura. Es la discapacidad motriz más frecuente en la niñez.

                        </h3>
                        <h3>Dependiendo del grado de parálisis cerebral algunas personas podrían hacer uso de implementos o dispositivos de apoyo para su asistencia en la movilidad u otras funciones motrices, e incluso en algunos casos, estas podrían verse reducidas en su totalidad.
                        </h3>
                    </div> 

                    <!-- tercer personaje -->
                    <div tabindex="17" class="project-info overflow" style="text-align:justify">
                        <h2>Nuestro 3er personaje es una niña con miembro superior amputado/muñón:</h2>
                        <h3>
                            <div  class="col-sm-3">
                                <img src="images/concurso/3p.png" class="img-responsive" alt="Personaje con miembro superior amputado/muñón"> 
                            </div> 
                            <br> <br>
                            La amputación es el procedimiento por medio del cual se extrae una parte o miembros del cuerpo (ya sean superiores: como brazos o manos, o inferiores: como piernas o pies) a través de uno o más huesos. 
                            <br><br>
                            Son varias las causas que pueden derivar en una amputación, como accidentes de tránsito, domésticos o laborales, heridas graves de bala o por explosión, y aquellas relacionadas a las condiciones de salud o a afectaciones congénitas, entre las cuales se destaca la diabetes.
                            <br><br>
                            Se denomina muñón a la terminación del miembro amputado. Generalmente, las personas con pérdida de miembros utilizan una prótesis: extensión artificial que reemplaza a la parte del cuerpo faltante, apoyando a la persona en la realización de las actividades diarias y con fines estéticos.

                        </h3>
                    </div> 

                </div>
            </div>
        </div>
    </section>
    <hr>
    <div class="project-info overflow">
                <center>
                    <br> 
                    <br> 
                    <img width="60%" src="images/concurso/ganadores2020.png" class="img-responsive " alt='Los tres personajes del concurso acompañando el siguiente texto. Ganadores: primer lugar, Diago Rotman, “Abrazándonos con el corazón” PERU. 
            Segundo lugar, Violeta Parra, "El susurrador mágico", ARGENTINA. Tercer lugar, Alejandro Valenzuela, "Unos lentes para Sara", ECUADOR'></center>
                </div>
                <br>
    <section id="ganadores">
            <div class="container">
                <div class="row">
                    <div tabindex="18" class="project-info overflow " style="text-align:justify">
                        <center><h2 class="center">¡Conoce a los ganadores! </h2></center>
                        <h2>
                            Accede a las historias ganadoras de la Segunda Edición y sorpréndete con la creación de los pequeños escritores quienes con su imaginación ya están dando forma a un mundo más accesible y en equidad para todos.
                        </h2>
                        <br>
                        <h3> <b>Primer lugar: Diago Rotman</b></h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Cuento: Abrazándonos con el corazón.</li>
                            <li><i class="fa fa-angle-right"></i> País: Perú.</li>
                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/ABRAZANDONOS-CON-EL-CORAZON.pdf">Ver cuento en versión digital.</a></li>

                        </ul>

                    </div> 
                    <div tabindex="22" class="project-info overflow " style="text-align:justify">

                        <h3><b>Segundo lugar: Violeta Parra</b> </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Cuento: El susurrador mágico.</li>
                            <li><i class="fa fa-angle-right"></i> País: Argentina.</li>
                            
                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/EL-SUSURRADOR-MAGICO.pdf">Ver cuento en versión digital.</a></li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->

                        </ul>
                    </div> 
                    <div tabindex="25" class="project-info overflow " style="text-align:justify">

                        <h3><b>Tercer  lugar: Alejandro Valenzuela</b> </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Cuento: Unos lentes para Sara.</li>
                            <li><i class="fa fa-angle-right"></i> País: Ecuador.</li>

                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/UNOS-LENTES-PARA-SARA.pdf">Ver cuento en versión digital.</a></li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->

                        </ul>
                    </div> 
                    
                    <div tabindex="29" class="project-info overflow " style="text-align:justify">

                        <h3><b>Mencion especial - Rotary: </b> </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Cuento: Una oportunidad para Rocío.</li>
                            <li><i class="fa fa-angle-right"></i> Autor: Valentín Humberto Chavarría Márquez.</li>
                            <li><i class="fa fa-angle-right"></i> País: El Salvador.</li>
                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/UNA-OPORTUNIDAD-PARA-ROCIO.pdf">Ver cuento en versión digital.</a></li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->
                        </ul>
                        
                        <h3><b>Mencion especial - JCI: </b> </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Cuento: Aviones de papel.</li>
                            <li><i class="fa fa-angle-right"></i> Autor: Ambar Virginia Carolina Palacios Gómez.</li>
                            <li><i class="fa fa-angle-right"></i> País: Argentina</li>
                            
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->
                        </ul>
                        <br>
                        <ul class="elements">

                    </div> 
                </div>
            </div>

        </section>

 </section>

</section>


<section id="informacion-concurso">
    <div class="container">
        <div class="row">
            <div tabindex="30" class="project-info overflow " style="text-align:justify">
            <center>
                <br>
                <a tabindex="29" type="button" href="concurso/mimundoamimanera2.pdf" class="btn btn-info"><h4>Bases del concurso</h4></a>
                <br>
                <br>
                <br>
                <br>
            </center>
                <br>
                <h3>
                  En 2017 desarrollamos la 1ª Edición de este concurso literario infantil. Trabajamos de la mano de las escuelas y las familias para derribar estereotipos en la sociedad y promover la equidad en 5 países de América Latina. ¡CONOCE LOS CUENTOS GANADORES!

              </h3>
              <br>
              <center>
                <a tabindex="31" type="button" href="mi-mundo-a-mi-manera.php" class="btn btn-info"><h4>Ganadores primera edición</h4></a>
            </center>
        </div> 


    </div>
</div>
<br>
<!-- seccion de auspiciantes del concurso-->
<section id="auspiciantes">
  <div class="container">
      <div class="row">
        <h3>
            Con el apoyo de:      
        </h3>
        <br>
        <center>
          <div  class="col-xs-12 col-sm-3">
            <a title="riadis" href="https://www.riadis.org/"><img src="images/auspiciantes/Logo+RIADIS+transparente.png" class="img-responsive" alt="Logotipo de Riadis, Red latinoamericana de Organizaciones no Gubernamentales de personas con discapacidad y sus familias. Lo dirigirá al sitio web de Riadis" /></a>
          </div> 
            
          <div class="col-xs-12 col-sm-3"> 
            <a title="rotary" href="https://www.rotary.org/es"><img src="images/auspiciantes/rotary-transparente.png" class="img-responsive" alt="Logotipo de Rotary, Bicentenario Segundo de Pedro Luro. Lo dirigirá al sitio web de Rotary"/></a>
          </div>
          
          <div class="col-xs-12 col-sm-3">
            <a title="jci" href="https://jci.cc/"><img src="images/auspiciantes/logo+jci.png" class="img-responsive" alt="Logo de JCI Salta, Argentina. Lo dirigirá al sitio web oficial de JCI" /></a>
          </div> 

          <div class="col-xs-12 col-sm-3">
            <a title="Municipio de Villarino" href="http://www.villarino.gob.ar/"><img src="images\auspiciantes\Logo+MunicipioVillarino.png" class="img-responsive" alt="Logo del Municipio de Villarino, Provincia de Buenos Aires, Argentina. Lo dirigirá al sitio oficial del Municipio de Villarino, Buenos Aires, Argentina" /></a>
          </div> 
        </center>
      </div>
  </div>

</section>

</section>
</body>
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">

            </div>

                   <!--  <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
                  <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                       <h2>¿Tienes alguna duda? Envíanos un mensaje</h2>
                       <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php">
                        <div class="form-group">
                            <input tabindex="51" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Nombre">
                        </div>
                        <div class="form-group">
                            <input tabindex="52" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <textarea tabindex="53" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Tu mensaje"></textarea>
                        </div>            
                        <div style="display:none"> 
                            <input id="cc" value="" placeholder="E-mail"> 
                        </div>             
                        <div class="form-group">
                            <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Enviar mensaje</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                    <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/main_es.js"></script>   
</body>


</html>
