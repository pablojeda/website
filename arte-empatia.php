<!DOCTYPE html>
<html lang="es">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad.">
    <meta name="author" content="Prime Developers Chile">
    
    <!-- Facebook Metadatos | Diseño web accesible -->
    <meta property="og:title" content="Fundación Comparlante | Arte para promover la empatía"/>
    <meta property="og:site_name" content="Arte para promover la empatía"/>
    <meta property="og:description" content="Fundación Comparlante brinda servicios de impacto social para la accesibilidad de las personas con discapacidad"/>
    <meta property="og:image" content="http://www.comparlante.com/images/programas/sentimos-logo.png"/>
    <meta property="og:image:alt" content="Logo de Sentimos: Con sus letras en varios colores, S: amarillo claro, E: amarillo fuerte, N: naranja, T: fucsia, I: morado y MOS: negro. La letra O tiene en el centro una mano blanca"/>
    <meta property="og:url" content="http://www.comparlante.com/arte-empatia.php"/>

    
    
    <title>Fundación Comparlante</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head><!--/head-->

<body>
  <?php include("header.php"); ?>

  <section id="titulo" tabindex="10">
    <div class="row  padding" style="background-color:  #f6f6f6;">
        <div class="container" >
            <div class="row">
                <div class="wow scaleIn " >
                    <center>
                        <img src="images/programas/arte.png" alt="Un plano, un lápiz y una escuadra, herramientas utilizadas en arte ">
                    </center>
                </div>

                <div class="col-sm-12 padding-top-index text-justify">

                   <center>
                       <h2><b>Arte para promover la empatía </b></h2>
                   </center>
               </div>
           </div>
       </div>
   </div>

</section>

<section id="portfolio-information" >
    <div class="container"> 
        <br><br>
        <!-- asesoria en accesibilidad -->
        <div class="row" tabindex="15">
            <div class="project-info overflow">
                <center>
                    <a href="arteaccesible.php">
                        <img src="images/programas/arte-accesible.png" class="img-responsive" width="40%" alt="Logo de arte accesible.">
                    </a>
                </center>
            </div>
            <br>
            <div class="row">
                <center>
                    <h2><b>Arte accesible</b></h2>
                </center>
            </div>
            <div class="row">

                <div class="col-sm-12">

                    <div  class="project-info overflow " style="text-align:justify">

                        <h2>Es una muestra de arte que tiene como objetivo promover la empatía y el pensamiento crítico en los tomadores de decisiones. Escultura, arte audiovisual, fotografía e intervenciones invitan a los visitantes a sentir, a ponerse en los zapatos de las personas con discapacidad para cambiar la actitud de la sociedad y en lugar de solo mirar la discapacidad focalizarse en la capacidad y las posibilidades.</h2>
                        <p><center><a tabindex="16" type="button" href="arteaccesible.php" class="btn btn-info">Más información</a></center>  </p>

                    </div>
                    <br>
                </div>
            </div>
        </div>

        <!-- fin asesoria accesibilidad -->
        

        <!-- desarrollo web -->
        <div class="row" tabindex="16">
            <div class="project-info overflow">
                <center>
                    <img src="images/programas/sentimos-logo.png" class="img-responsive" width="40%" alt="Logo Sentimos.">
                </center>
            </div>
            <br>
            <div class="row">
                <center>
                    <h2><b>Sentimos</b></h2>
                </center>
            </div>
            <div class="row">

                <div class="col-sm-12">

                    <div  class="project-info overflow " style="text-align:justify">

                        <h2>Una muestra fotográfica en donde se retrata a personas con discapacidad desarrollando distintas actividades en plenitud. Esta campaña de sensibilización ofrece un acercamiento real a la cotidianidad de las personas con discapacidad cuando aman, gozan, crean, danzan, pintan, enseñan,... viven.
                        </h2>
                        <p><center><a tabindex="16" type="button" href="sentimos.php" class="btn btn-info">Más información</a></center>  </p>

                    </div>
                    <br>
                </div>
            </div>
        </div>

        <!-- diseño universal -->

        <div class="row" tabindex="17">
            <div class="project-info overflow">
                <center>
                    <a href="mi-mundo-a-mi-manera-2020.php">
                        <img src="images/concurso/personajes2020.png" class="img-responsive" width="40%" alt="Todos los personajes animados del Concurso.">
                    </a>
                </center>
            </div>
            <br>
            <div class="row">
                <center>
                    <h2><b>"Mi Mundo a Mi Manera"</b></h2>
                </center>
            </div>
            <div class="row">

                <div class="col-sm-12">

                    <div  class="project-info overflow " style="text-align:justify">

                        <h2>“Mi Mundo a Mi Manera” es un Concurso Literario Internacional que promueve la concientización sobre las personas con discapacidad en el ámbito escolar. Propone, a través de la literatura, ofrecer un espacio de reflexión en la sociedad. <br><br>

                        </h2>
                        <p><center><a tabindex="16" type="button" href="mi-mundo-a-mi-manera-2020.php" class="btn btn-info">Más información</a></center>  </p>

                    </div>
                    <br>
                </div>
            </div>
        </div>


    </div>
</section>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">

            </div>

                   <!--  <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
                  <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                     <h2>Envíanos un mensaje</h2>
                     <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php">
                        <div class="form-group">
                            <input tabindex="51" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Nombre">
                        </div>
                        <div class="form-group">
                            <input tabindex="52" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <textarea tabindex="53" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Tu mensaje"></textarea>
                        </div>            
                        <div style="display:none"> 
                            <input id="cc" value="sebastian@comparlante.com" placeholder="E-mail"> 
                        </div>             
                        <div class="form-group">
                            <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Enviar mensaje</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                    <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/main_es.js"></script>   
</body>


</html>
