<!DOCTYPE html>
<html lang="es">


<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Arte Accesible es una campaña liderada por Fundación Comparlante que tienen como objetivo sensibilizar sobre la importancia de la accesibilidad para mejorar la calidad de vida de las personas con discapacidad y generar mayor equidad.">
  <meta name="author" content="Prime Developers Chile">

  <!-- Facebook Metadatos | Diseño accesible -->
  <meta property="og:title" content="Fundación Comparlante | Arte Accesible"/>
  <meta property="og:site_name" content="Arte Accesible"/>
  <meta property="og:description" content="En Comparlante nos tomamos muy en serio la responsabilidad de 'diseñar un mundo más accesible', ¡y trazo a trazo lo hacemos posible!." />
  <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/services/2/diseno-accesible.jpg"/>
  <!--   <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/diseno-grafico.php"/>  -->





  <title>Fundación Comparlante</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet"> 
  <link href="css/lightbox.css" rel="stylesheet"> 
  <link href="css/main.css" rel="stylesheet">
  <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
      <![endif]-->       
      <link rel="shortcut icon" href="images/ico/logo-icon.png">
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
      <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">
      <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WRVC32B');</script>
    <!-- End Google Tag Manager -->
  </head><!--/head-->

  <body>

    <!--#include file="header.html"-->
    <?php include("header.php"); ?>

    <section id="page-breadcrumb">
      <div class="vertical-center sun">
       <div class="container">
        <div class="row">
          <div class="action">
            <div tabindex="10" class="col-sm-12">
              <h1 class="title">Programa Arte Accesible</h1>
              <p></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--/#page-breadcrumb-->

  <section id="portfolio-information" >
    <div class="container">
      <br><br>
        <!-- <div class="project-info overflow">
            <center>
                <img src="images/services/1/diseno-accesible.png" class="img-responsive " alt="Diseño Accesible – Accesibilidad Creativa"></center>
              </div> -->
              <br>
              <div class="row">

                <div class="col-sm-12">

                  <div tabindex="11" class="project-info overflow " style="text-align:justify">
                    <h2>Arte Accesible es una campaña de concientización que usa el arte como herramienta para sensibilizar sobre la importancia de generar entornos accesibles.</h2> <br>
                    <h2>Objetos escultóricos, fotografía, instalaciones y material audiovisual son los soportes con los que queremos generar un cambio de pensamiento en las personas sin discapacidad. Utilizando psicología inversa pondremos a tomadores de decisión en los zapatos de aquellos que sufren exclusión. 
                    </h2><br>
                    <h2>
                      Arte Accesible será exhibida en los Congresos de distintos países, porque es allí donde se desarrollan las legislaciones y donde es necesarios sensibilizar a los efectores de las leyes. El piloto a implementarse en 2019 abarcará Argentina, Ecuador y la sede de la Organización de los Estados Americanos en Washington DC.

                    </h2>
                    <br>
                    <h2>
                      El proyecto incluye la presentación de la muestra por un mes en cada locación, lo que daría la oportunidad de contactar a otros artistas y generar comunidad. Junto a otras organizaciones no gubernamentales y fundaciones, desarrollar paralelamente redes que promuevan la accesibilidad en todo el hemisferio.

                    </h2>
                    <br>
                  
                   <br>
                   <h2>
                    Actualmente nos encontramos en proceso de búsqueda de patrocinadores para poder llevar a cabo esta propuesta. Para ello, buscamos contar con el apoyo del sector privado y público. Si quieres ser patrocinador contáctanos via <a href="mailto:info@comparlante.com">info@comparlante.com</a>
                  </h2>
                  <br>
                  <h2>


                    <br><br>
                    <a tabindex="" style="background-color: #f39917!important; width: 300px!important;
                    border-color: #f39917!important;"  alt="Descarga la información sobre las obras de arte accesible"   type="button" href="arte-accesible.pdf" class="btn btn-info"><h4>Ver catálogo de obras aquí</h4></a>

                    <br><br>
                  </h2>

                </div>


              </div>
            </div>
          </div>
        </section>

        <footer id="footer">
          <div class="container">
            <div class="row">
              <div class="col-sm-12 text-center bottom-separator">

              </div>

                    <!-- <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                    </div> -->
  <!--                 <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                        <h2>Envíanos un mensaje</h2>
                        <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php">
                            <div class="form-group">
                                <input tabindex="12" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Nombre">
                            </div>
                            <div class="form-group">
                                <input tabindex="13" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                            </div>
                            <div class="form-group">
                                <textarea tabindex="13" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Tu mensaje"></textarea>
                            </div>            
                            <div style="display:none"> 
                                <input id="cc" value="" placeholder="E-mail"> 
                            </div>             
                            <div class="form-group">
                                <button tabindex="13" id="send-button" name="submit" class="btn btn-submit"> Enviar mensaje</button>
                            </div>
                        </form>
                    </div>
                  </div> -->
                  <div class="col-sm-12"><br>
                    <div class="copyright-text text-center"><br>
                      <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                      <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                    </div>
                  </div>
                </div>
              </div>
            </footer>
            <!--/#footer-->

            <script type="text/javascript" src="js/jquery.js"></script>
            <script type="text/javascript" src="js/bootstrap.min.js"></script>
            <script type="text/javascript" src="js/lightbox.min.js"></script>
            <script type="text/javascript" src="js/wow.min.js"></script>
            <script type="text/javascript" src="js/main_es.js"></script>   
          </body>


          </html>
