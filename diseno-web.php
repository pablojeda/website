<!DOCTYPE html>
<html lang="es">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad.">
    <meta name="author" content="Prime Developers Chile">
    
   <!-- Facebook Metadatos | Diseño web accesible -->
    <meta property="og:title" content="Fundación Comparlante | Diseño web accesible"/>
    <meta property="og:site_name" content="Diseño web accesible"/>
    <meta property="og:description" content="Transforma o diseña tu sitio web en accesible.." />
    <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/services/2/diseno-web.jpg"/>
   <!-- <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/diseno-web.php"/> -->

    
    
    <title>Fundación Comparlante</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">
        <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
    </head><!--/head-->

    <body>
      <?php include("header.php"); ?>

      <section id="page-breadcrumb">
        <div class="vertical-center sun">
           <div class="container">
            <div class="row">
                <div class="action">
                    <div tabindex="10" class="col-sm-12">
                        <h1 class="title">Diseño web accesible</h1>
                        <p>Transforma o diseña tu sitio web en accesible.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#page-breadcrumb-->

<section id="portfolio-information" >
    <div class="container"> <br><br>
        <div class="project-info overflow">
            <center>
                <img src="images/services/1/diseno-web.png" class="img-responsive " alt="Diseño web accesible"></center>
            </div><br>
            <div class="row">

                <div class="col-sm-12">

                    <div tabindex="11" class="project-info overflow "style="text-align:justify">

                        <h2>Actualmente el 90% de los contenidos disponibles en Internet no son accesibles para personas con discapacidad. ¿Imaginas a una persona con dificultades visuales navegando por Internet? Aunque no lo creas, sí es posible, y nosotros podemos ayudarte a convertir tu sitio web en accesible.</h2>

                    </div>
                    <center>
                        <div taindex="13" class="live-preview data-wow-duration="500ms" data-wow-delay="300ms"">

                            <h2>¡Transforma tu sitio!</h2>
                            <br>
                            <a tabindex="14" href="#contacto" class="btn btn-lg btn-info" >Accede al servicio</a>

                        </div> </center>
                        <br>
                        <div tabindex="15" class="project-info overflow padding-top" style="text-align:justify">
                        
                            <h3>
                                <div  class="col-sm-3">
                                    <img src="images/team/perfil-prime.png" class="img-responsive" alt="Logo Prime Developers, empresa desarrolladora web y de software."> 
                                </div> <br> <br>
                                Prime Developers Chile es una empresa de desarrollo de software, que se dedica al diseño de páginas web accesibles, entre otras áreas relacionadas a la tecnología. </h3>
                                <h3>Junto a Prime Developers puedes potenciar, diseñar y desarrollar ideas tecnológicas innovadoras para diferentes rubros. Cuentan con programas de apoyo y desarrollo de ideas de negocio para iniciar con tu propia startup.  </h3>
                              
                            
                                <h3> Algunas de las áreas en las que se desempeñan son:
                                <ul class="elements">
                                    <li><i class="fa fa-angle-right"></i> Sistemas Web.</li>
                                    <li><i class="fa fa-angle-right"></i> Sistemas de escritorio.</li>
                                    <li><i class="fa fa-angle-right"></i> Automatización de procesos.</li>
                                    <li><i class="fa fa-angle-right"></i> Desarrollo de hardware.</li>
                                    <li><i class="fa fa-angle-right"></i> Diseño web accesible.</li>
                                    <li style="text-align:left"><i class="fa fa-angle-right"></i> Diseño de aplicaciones para dispositivos móviles</li>

                                </ul>
                            </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <footer id="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center bottom-separator">

                        </div>

                   <!--  <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
                  <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                       <h2>Envíanos un mensaje</h2>
                       <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php">
                        <div class="form-group">
                            <input tabindex="51" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Nombre">
                        </div>
                        <div class="form-group">
                            <input tabindex="52" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <textarea tabindex="53" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Tu mensaje"></textarea>
                        </div>            
                        <div style="display:none"> 
                            <input id="cc" value="pojeda@primedevelopers.cl" placeholder="E-mail"> 
                        </div>             
                        <div class="form-group">
                            <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Enviar mensaje</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                    <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/main_es.js"></script>   
</body>


</html>
