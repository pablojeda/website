<!DOCTYPE html>
<html lang="es">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad.">
    <meta name="author" content="Prime Developers Chile">
    
    <!-- Facebook Metadatos | Diseño web accesible -->
    <meta property="og:title" content="I Concurso literario para niñas y niños de educación básica Mi mundo a mi manera"/>
    <meta property="og:site_name" content="I Concurso literario para niñas y niños de educación básica Mi mundo a mi manera"/>
    <meta property="og:description" content="Desde Fundación Comparlante queremos que todos los pequeños soñadores tengan la oportunidad de desarrollar un relato que involucre a, al menos, uno de los cuatro personajes de nuestra Fundación. Por ello tenemos el agrado de invitar a niños y niñas escritores a darle vida en un cuento a uno de nuestros cuatro personajes." />
    <meta property="og:image" content="http://comparlante.com/images/concurso/personajes.png"/>


    
    
    <title>Fundación Comparlante</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRVC32B');</script>
<!-- End Google Tag Manager -->
</head><!--/head-->

<body>
  <?php include("header.php"); ?>

  <section id="page-breadcrumb">
    <div class="vertical-center sun">
       <div class="container">
        <div class="row">
            <div class="action">
                <div tabindex="10" class="col-sm-12">
                    <h2 style="font-size: 20pt; font-weight: 400" class="title text-center">I Concurso literario para niñas y niños de educación básica "Mi mundo a mi manera"</h2>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!--/#page-breadcrumb-->
<div id="zooming">

    <section id="concurso" >
        <div class="container"> <br><br>
            <div class="project-info overflow">
                <center>
                    <img width="50%" src="images/concurso/personajes.png" class="img-responsive " alt="Los 4 personajes animados del Concurso: niño con discapacidad visual, niña con discapacidad motriz, niña con Síndrome de Down y niño con discapacidad auditiva, jugando juntos con el globo terráqueo como un balón."></center>
                </div>
                <br>
                <div class="row">

                    <div class="col-sm-12">

                        <div tabindex="11" class="project-info overflow "style="text-align:justify">

                            <h2>Los niños son el presente y el futuro. Desde Fundación Comparlante entendemos que concientizar a la sociedad sobre las distintas dis-capacidades promueve la equidad. En 2017 desarrollamos este concurso literario infantil para trabajar desde las escuelas y con la familia para derribar estereotipos en la sociedad. 
                                <br>
                                Este concurso internacional para niños de 6 a 13 años tuvo en su primera edición la participación de niños de Argentina, Ecuador, Costa Rica, México, Perú y Colombia.  En sus relatos debieron involucrar a los personajes propuestos por Fundación Comparlante. 
                            </h2>

                        </div>

                        <br>
                        <!-- primer personaje -->
                        <div tabindex="15" class="project-info overflow" style="text-align:justify">
                            <h2>Nuestro 1er personaje es un niño con discapacidad visual:</h2>
                            <h3>
                                <div  class="col-sm-3">
                                    <img src="images/concurso/discapacidad-visual.png" width="75% class="img-responsive" alt="Personaje con discapacidad visual"> 
                                </div> 
                                <br> <br>
                                La discapacidad visual se refiere a la deficiencia del sistema de la visión la cual afecta la agudeza y campo visual, la motilidad ocular y la percepción de los colores y profundidad derivando en diagnósticos como la baja visión o la ceguera. 
                            </h3>
                            <h3>A su lado, lo acompaña su perro guía o lazarillo: animal especialmente adiestrado para brindar asistencia a la movilidad e independencia de las personas con deficiencia visual.  </h3>
                        </div> 
                        <!-- segundo personaje -->
                        <div tabindex="16" class="project-info overflow" style="text-align:justify">
                            <h2>Nuestro 2do personaje es una niña con discapacidad motriz:</h2>
                            <h3>
                                <div  class="col-sm-3">
                                    <img src="images/concurso/discapacidad-motriz.png" width="60% class="img-responsive" alt="Personaje con discapacidad motriz"> 
                                </div> 
                                <br> <br>
                                La discapacidad motriz se refiere a una condición física la cual influye en la capacidad de control y movilidad del cuerpo caracterizada por alteraciones en el desplazamiento, equilibrio, habla y respiración de la persona.
                            </h3>
                            <h3>Para facilitar su desplazamiento y autonomía ella cuenta con una silla de ruedas la cual se adapta y responde a sus necesidades.  </h3>
                        </div> 

                        <!-- tercer personaje -->
                        <div tabindex="17" class="project-info overflow" style="text-align:justify">
                            <h2>Nuestro 3ro personaje es una niña con Síndrome de Down:</h2>
                            <h3>
                                <div  class="col-sm-3">
                                    <img src="images/concurso/sindrome-down.png" width="60% class="img-responsive" alt="Personaje con sindrome de down"> 
                                </div> 
                                <br> <br>
                                El Síndrome de Down es una alteración congénita derivada de la triplicación total o parcial del cromosoma 21, discapacidad del espectro cognitivo que deriva en retraso mental y del crecimiento como parte de determinadas alteraciones físicas.  
                            </h3>
                        </div> 
                        <!-- cuarto personaje -->
                        <div tabindex="18" class="project-info overflow " style="text-align:justify">
                            <h2>Nuestro 4to personaje es un niño con discapacidad auditiva:</h2>
                            <h3>
                                <div  class="col-sm-3">
                                    <img src="images/concurso/discapacidad-auditiva.png" width="60%" class="img-responsive" alt="Personaje con discapacidad auditiva"> 
                                </div> 
                                <br> <br>
                                La discapacidad auditiva o sordera se refiere a la imposibilidad o dificultad de hacer uso del sentido del oído debido a una pérdida de la capacidad auditiva parcial (hipoacusia) o total (cofosis) de forma unilateral o bilateral. Al igual que otras discapacidades físicas la sordera puede originarse en el nacimiento o ser adquirida a lo largo de los años de vida.  
                            </h3>
                            <h3>Para comunicarse, las personas sordas cuentan con la Lengua de Señas: un completo sistema de comunicación el cual del mismo modo que la lengua fonética o hablada permite transmitir ideas y sentimientos transformando las palabras en gestos efectuados principalmente con las manos. 
                            </h3>
                        </div> 
                    </div>
                </div>

                 <div class="project-info overflow">
                <center>
                    <br> 
                    <br> 
                    <img width="60%" src="images/concurso/infografico.PNG" class="img-responsive " alt="Infografía con los resultados del concurso literario 'Mi mundo a mi manera'. 5 países participando, 3 premios y 3 menciones especiales."></center>
                </div>

            </div>
        </section>
        <br>

        <section id="ganadores">
            <div class="container">
                <div class="row">
                    <div tabindex="18" class="project-info overflow " style="text-align:justify">
                        <center><h2 class="center">¡Conoce a los ganadores! </h2></center>
                        <h2>
                            Accede a las historias ganadoras de la Primera Edición y sorpréndete con la creación de los pequeños escritores quienes con su imaginación ya están dando forma a un mundo más accesible y en equidad para todos.
                        </h2>

                        <h3> <b>Primer lugar: Agustina Irene Abdo Valdiviezo </b></h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Cuento: Los colores de Tom.</li>
                            <li><i class="fa fa-angle-right"></i> Colegio: Liceo Internacional, Quito.</li>
                            <li><i class="fa fa-angle-right"></i> Edad: 11 Años.</li>
                            <li><i class="fa fa-angle-right"></i> País: Ecuador.</li>
                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/los-colores-de-tom.pdf">Ver cuento en versión digital.</a></li>
                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/biblioteca_audiolibros/comparlante/index.php?r=libros/view&id=215">Audiolibro.</a></li>

                        </ul>

                    </div> 
                    <div tabindex="22" class="project-info overflow " style="text-align:justify">

                        <h3><b>Segundo lugar: Ariana Valeria Valenzuela Muñoz</b> </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Cuento: Mi mundo se llamaba silencio.</li>
                            <li><i class="fa fa-angle-right"></i> Edad: 12 Años.</li>
                            <li><i class="fa fa-angle-right"></i> País: Ecuador.</li>
                            <li><i class="fa fa-angle-right"></i> Colegio: Colegio Ecuatoriano-Español América Latina,Quito.</li>
                            
                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/mi-mundo-se-llama-silencio.pdf">Ver cuento en versión digital.</a></li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->

                        </ul>
                    </div> 
                    <div tabindex="25" class="project-info overflow " style="text-align:justify">

                        <h3><b>Tercer  lugar: David Rodolfo Jiménez Caamaño</b> </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Cuento: Felipe.</li>
                            <li><i class="fa fa-angle-right"></i> Edad: 11 Años.</li>
                            <li><i class="fa fa-angle-right"></i> País: Costa Rica.</li>
                            <li><i class="fa fa-angle-right"></i> Colegio: Saint Gregory School, San José.</li>

                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/felipe-spreads.pdf">Ver cuento en versión digital.</a></li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->

                        </ul>
                    </div> 
                    <div tabindex="29" class="project-info overflow " style="text-align:justify">

                        <h3><b>Menciones especiales: </b> </h3>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Cuento: Un breve relato sobre mis super-poderes.</li>
                            <li><i class="fa fa-angle-right"></i> Autor: Galo Dana.</li>
                            <li><i class="fa fa-angle-right"></i> Edad: 13 años.</li>
                            <li><i class="fa fa-angle-right"></i> País: Argentina</li>
                            <li><i class="fa fa-angle-right"></i> Colegio: Santo Tomás Moro, La Plata.</li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->
                        </ul>
                        <br>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Cuento: Una gran amistad.</li>
                            <li><i class="fa fa-angle-right"></i> Autor: Melina Bogarín Monge</li>
                            <li><i class="fa fa-angle-right"></i> Edad: 11 años.</li>
                            <li><i class="fa fa-angle-right"></i> País: Costa Rica.</li>
                            <li><i class="fa fa-angle-right"></i> Colegio: Saint Gregory School, San José.</li>
                            <li><i class="fa fa-angle-right"></i> <a href="http://www.comparlante.com/cuentos/una-gran-amistad.pdf">Ver cuento en versión digital.</a></li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->
                        </ul>
                        <br>
                        <ul class="elements">
                            <li><i class="fa fa-angle-right"></i> Cuento: This is me.</li>
                            <li><i class="fa fa-angle-right"></i> Autor: Estefania Alexandra Moreno Marin</li>
                            <li><i class="fa fa-angle-right"></i> Edad: 10 años.</li>
                            <li><i class="fa fa-angle-right"></i> País: Ecuador.</li>
                            <li><i class="fa fa-angle-right"></i> Colegio: Liceo Internacional, Quito.</li>
                            <!-- <li><i class="fa fa-angle-right"></i> <a href="">Audiolibro.</a></li> -->
                        </ul>
                    </div> 
                </div>
            </div>

        </section>



        <center>
            <br>
            <a tabindex="9" type="button" href="concurso/bases.pdf" class="btn btn-info"><h4>Descarga las bases del concurso</h4></a>
        </center>
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center bottom-separator">

                    </div>

                   <!--  <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
                  <div class="col-md-12 col-sm-12">
                    <div id="contacto" class="contact-form bottom">
                       <h2>¿Tienes alguna duda? Envíanos un mensaje</h2>
                       <form id="main-contact-form" name="contact-form" method="post" action="contacto2.php">
                        <div class="form-group">
                            <input tabindex="51" id="nombre-formulario" type="text" name="name"  class="form-control" required="required" placeholder="Nombre">
                        </div>
                        <div class="form-group">
                            <input tabindex="52" id="email-formulario" type="email" name="email"  class="form-control" required="required" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <textarea tabindex="53" id="mensaje-formulario"  name="message" required="required" class="form-control" rows="8" placeholder="Tu mensaje"></textarea>
                        </div>            
                        <div style="display:none"> 
                            <input id="cc" value="" placeholder="E-mail"> 
                        </div>             
                        <div class="form-group">
                            <button tabindex="54" id="send-button" name="submit" class="btn btn-submit"> Enviar mensaje</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                    <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/main_es.js"></script>   
</body>


</html>
