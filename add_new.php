<!DOCTYPE html>
<html lang="es">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Fundación Comparlante brinda servicios de innovación tecnológica y de impacto social para la accesibilidad de las personas con discapacidad.">
    <meta name="author" content="Prime Developers Chile">
    
    <!-- Facebook Metadatos | Diseño web accesible -->
    <meta property="og:title" content="Fundación Comparlante | Arte para promover la empatía"/>
    <meta property="og:site_name" content="Arte para promover la empatía"/>
    <meta property="og:description" content="Fundación Comparlante brinda servicios de impacto social para la accesibilidad de las personas con discapacidad" />
    <!-- <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/services/2/diseno-web.jpg"/> -->
    <meta property="og:image" content="http://fundacioncomparlante.primedevelopers.cl/images/home/logo-fundacion-2.jpg"/>

    <!-- <meta property="og:url" content="http://fundacioncomparlante.primedevelopers.cl/diseno-web.php"/> -->

    
    
    <title>Fundación Comparlante</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
    <link href="css/main.css" rel="stylesheet">
    <link href="css/noticias.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-icon.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/logo-icon.png">
    
</head><!--/head-->

<body>
  <?php include("header.php"); ?>

  <?php
    if($_POST){
 
    // include database connection
    include 'config_news/database.php';
 
    try{
     
        // insert query
        $query = "INSERT INTO noticias SET idioma=:idioma, publica=:publica, titulo=:titulo, imagen=:imagen, altText=:altText, contenido=:contenido, fechaorigen=:fechaorigen";
 
        // prepare query for execution
        $stmt = $con->prepare($query);
 
        // posted values
        $idioma=$_POST['idioma'];
        $publica=$_POST['publica'];
        $titulo=$_POST['titulo'];
        $imagen=$_POST['imagen'];
        $altText=$_POST['altText'];
        $contenido=$_POST['contenido'];
        $fechaorigen=$_POST['fechaorigen'];
 
        // bind the parameters
        $stmt->bindParam(':idioma', $idioma);
        $stmt->bindParam(':publica', $publica);
        $stmt->bindParam(':titulo', $titulo);
        $stmt->bindParam(':imagen', $imagen);
        $stmt->bindParam(':altText', $altText);
        $stmt->bindParam(':contenido', $contenido);
        $stmt->bindParam(':fechaorigen', $fechaorigen);

         
        // specify when this record was inserted to the database
        //$created=date('Y-m-d H:i:s');
        //$stmt->bindParam(':created', $created);
         
        // Execute the query
        if($stmt->execute()){
            echo "<div class='alert alert-success'>Record was saved.</div>";
        }else{
            echo "<div class='alert alert-danger'>Unable to save record.</div>";
        }
         
    }
     
    // show error
    catch(PDOException $exception){
        die('ERROR: ' . $exception->getMessage());
    }
}
?>

<section id="noticias">
    <div class="container">
        <div class="row">
            <div tabindex="30">

               <div class="col-sm-12  text-justify">
               <article>
               <h3 class="title"><strong>Agregar noticia:</strong></h3>
               <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <table class='table table-hover table-responsive table-bordered'>
                        
                        <tr>
                            <td>Publico?</td>
                            <td>
                            <select name="publica" id="publica">
                                <option value="1">Si</option>
                                <option value="0">No</option>
                            </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Fecha: (YYYY/MM/DD)</td>
                            <td><input type='text' name='fechaorigen' class='form-control' placeholder="2020/01/01" /></td>
                        </tr>
                        <tr>
                            <td>Imagen</td>
                            <td><input type='text' name='imagen' class='form-control' /></td>
                        </tr>
                        <tr>
                            <td>Texto Alterno de la imagen</td>
                            <td><input type='text' name='altText' class='form-control' /></td>
                        </tr>
                        <tr>
                            <td>Título:</td>
                            <td>
                                <input type='text' name='titulo' class='form-control' />
                            </td>
                        </tr>
                        <tr>
                            <td>Título en Inglés:</td>
                            <td>
                                <input type='text' name='idioma' class='form-control' />
                            </td>
                        </tr>
                        
                        <tr>
                            <td>Link:</td>
                            <td> <input type='text' name='contenido' class='form-control' /></td>
                        </tr>
                        
                        <tr>
                            <td></td>
                            <td>
                                <input type='submit' value='Publicar' class='btn btn-primary' />
                                <a type="button" href='lista_noticias.php' class='btn btn-secondary'>Ver todas las noticias</a>
                            </td>
                        </tr>
                    </table>
                </form>
                
               </article>
                
            </div>






        </div>
    </div>

</div>
</section>


<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center bottom-separator">

            </div>

                   <!--  <div class="col-md-5 col-sm-6">
                        <div class="contact-info bottom">
                          <a class="twitter-timeline" data-width="400" data-height="400" href="https://twitter.com/Comparlante">Tweets by Comparlante</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                      </div>
                  </div> -->
                  
            <div class="col-sm-12">
                <div class="copyright-text text-center">
                    <p>&copy; Fundación Comparlante <?php echo date("Y") ?>.</p>
                    <p>Desarrollado por <a tabindex="94" target="_blank" href="http://www.primedevelopers.cl/">Prime Developers.</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/lightbox.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/main_es.js"></script>
<script src="./tinymce/tinymce.min.js" referrerpolicy="origin"></script>
<script>tinymce.init({selector:'textarea',plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table paste code help wordcount'
  ],
  toolbar: 'undo redo | formatselect | ' +
  'bold italic backcolor | alignleft aligncenter ' +
  'alignright alignjustify | bullist numlist outdent indent | ' +
  'removeformat |code | help', });</script>   
</body>


</html>



